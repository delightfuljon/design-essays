# A Better Design Critique 

I believe tech and design culture are in an intensely cynical and self-defeating place. It seems as if we’re celebrating the most mean spirited and shallow critiques and glossing over analysis that encourages us to think deeper. It’s easier to retweet an angry headline to your followers than to spent time really considering the nuances of a topic. And even if you do put in the time to do some research, how are you going to get Twitter famous with slow, considered, nuanced material? Information is speeding up every day, and we’ve got quotas to hit, people!

As I’ve gotten older, I’ve gotten to meet people who are more interested in the nuance. They’re less likely to be on Twitter, and they’re less likely to think things have quick and targeted solutions. They use phrases like “wicked problems,” a term used to define complex issues like climate change or poverty. But that same advanced level of thinking can be used anywhere, from software to governmental policy. And when you do, you realise a few things. Like the fact that your first opinion about a topic is incomplete, and usually wrong.

Think about your field, or your job, or your hometown, or your country. Now think about an article you've seen written about it. If you're like most people, the article probably disappointed you. Sure, it had some commonly understood facts you can find on Wikipedia. And the article probably wasn't lying outright. But it's likely it was written without the depth of understanding that you personally have about it. Chances are, it missed the real story in favour of a shallow one.

Years ago, I started writing a series called Design Explosions. It was a new kind of software design critique based on these principles:

1. *The team that built this product is full of smart people.*
2. *There are many things we can't know without joining the team.*
3. *You can't judge and learn at the same time.*
	 
Every time I revisit these principles, I believe in them a little more. Especially when I’m upset about something. The product or policy that's annoying me? It's easy to assume the people who came up with it are all awful, greedy, and stupid. But what if they're smart? What if they care as much as I do? What if they're working as hard as I am? What if there are details I’m not aware of? That requires a new mode  of thinking. A better one. And it leads to richer, more full answers. With the right frame, the world becomes slightly more clear.

Instead of assuming I know every tech company’s motivation, I like reminding myself that I'm just guessing from the outside. There are always more details than appeasing Wall Street. Most people don’t know that, meaning they’re stick with a freshman’s understanding of the world. Take it from someone who’s been on the inside and fighting like hell, there's more to it than "it's all doomed" or "they must be corrupt."

And that brings us to judging versus learning. I think the design critique world is under a collective delusion that what the world needs is less common ground and more demonisation. We need to call bullshit more! We need to listen less! That's what we seem to believe right now, but we have it fully backwards.

A closed question, such as "How could you think these colours are acceptable" will only make people defensive. You won't learn a thing. But an open question, like "Can you tell me more about your process for picking these colours?” sets judgement aside, which allows you to learn something. We should always be trying to learn things and make things better for people. If you aren’t, I’m not sure design is the right career for you.

# Ugly Babies
There we were, a squad of fancy designers dispatched from our proud design firm. We were about to have an introductory dinner with our new clients. Before we ordered drinks, our most important stakeholder told us "Our baby is ugly." 

What a great thing to lead with!

Notice how succinctly he was able to state a universal client truth:
- We are very passionate about this (it's our baby)
- … but we know it needs work. (it's ugly)

All product work starts with an ugly baby. Without the belief that changes need to be made and the passion to get it done, there's no product brief or budget. Ugly Babies are why product designers have jobs! But…

**Don't Insult the Baby
This client did us a big favour. He signalled that he was open to change, but also reminded us to keep it civil. And that's easy to overlook. Sometimes we're so focused on explaining how to improve things that we forget that every decision was made by someone who was doing their best. Maybe they're the client, or your co-worker. They're often sitting in the room while you call their baby ugly. 

I've worked with designers who swoop into a new job and lecture everyone about how poorly run the design studio is and how they can do it better. I've worked with product managers who introduce themselves by saying all their co-workers are bad at their jobs. I've worked with engineers who seem to have two settings: "I agree with this idea“ or “this idea is stupid."

None of these approaches work. What does work is moving past the ugly baby and into the next stage, where you explain what you're going to do.

**Explain What's Next
You don't need to say "This flow is a catastrophe," you can say "I wonder if we can make successful task completion 50% faster." You don't need to say "I can't believe how dated this looks," you can say "We were thinking of aligning with Material design and explorations kicked off yesterday." Don't bother saying "this is so bad," just jump straight to the standard and much more engaging "what are the problems we're trying to solve?"

It makes all the difference.

**Study How You Make People Feel

Sometimes hiring managers reach out to me to ask about my experience working with a particular designer. When they do, I don't remember how quickly they could churn out art boards. Sometimes I don't have any idea if they use Photoshop or Sketch. Even their talent level is sort of a wash . I know a bunch of "good designers," so that's not a very significant metric.

The only thing I consider when I'm asked to give feedback on a designer is "how did they make me feel?" Were they able to disagree without being disagreeable? Were they able to listen? Do they take feedback? Do I like them? If I answer no to any of these questions, they don't get my reference.

There are a lot of designers in the world. There are a lot of good designers in the world. But I'm only interested in good designers who are kind. How do I find them? I pay attention to how they talk about clients and their ugly babies.

# Getting To “It Depends”
  
I love learning from people who know they don't know anything about a topic. Their opinions aren't sophisticated, but they're pure. They're not necessarily right, but they're great at listening and absorbing new information with an open mind.

I love learning from people who are masters of their field. They have a lot of hard-won experience. Their intuition is pretty solid. And most importantly, they know enough to know they still have a lot to learn. They say things like "it depends" a lot. And that’s the goal. Learning enough to see shades of grey.

It’s that middle space where many of us struggle. It’s really easy to learn a few things, lose our humility, and fall for the illusion that we know more than we do. From that place, it’s almost impossible to listen well. Things seem obvious to us, and we have a difficult time understanding how people might see things any different.

But even then, there are always things we don’t know. When I’m feeling like things are black and white, I try to dive into something I don’t know. I make all my best progress when I have an open mind. Not just as a designer, but as a person.

# Be Still

When AirPods were announced, the reaction was immediate and boilerplate: Too expensive. Short battery life. They look stupid. They're nothing new. Apple has forgotten how to innovate. But the specifics jumped out at me. A lot of people declared on Twitter that they’d fall out of your ears constantly.

So I went to an Apple Store and asked for a demo. I did everything I could to make the AirPods fall out. I jogged in place, I thrashed my head around, and I opened and closed my jaw. They didn’t fall out a single time, which felt like magic since every other pair of headphones do. I had always assumed I have defective ears.

The next time I went on a run, I paid attention to the black wire connecting my standard headphones to my phone. I was constantly looping the wire around my fingers, and being careful that my phone didn't extend too far away. I hadn't considered it before, but do you know the single greatest cause for headphones flinging themselves out of your ear?  

1. A wire.  
2. Connecting your headphones to a heavy weight.  
3. Dangling across your arms.  
4. While you're pumping your arms during a run.  

Which, of course, wireless headphones avoid altogether. It’s funny how the number one criticism levelled at the AirPods is actually its greatest strength. But you’d have no way of knowing that by looking at an image on Twitter. It’s something you have to actually experience for yourself.

There’s a lesson here. When you're presented with something new, be still. Slow down. Learn about it. Try it. Form your own opinion. Don't measure yourself or your design ability by how quickly you can shoot down an idea. Instead, try measuring yourself by how well you give the benefit of the doubt, do original research, and listen.

# Chapter One: Listen

Table of contents:

* Getting To “It Depends”
* Be Still
* A Better Design Critique
* Ugly Babies
* Some of the Many Times Steve Jobs Fell Short
* Open Letter to People Who Send Feature Requests to Product Team Members
* How to Write Design Documentation
* The New Table Stakes

# How to Write Design Documentation

What if your team had an actual history book explaining the tradeoffs that led to the feature or product you're being asked to work on? Imagine if you joined the team working on Powerpoint for Android at Microsoft, and on your first day you were handed a book titled *Powerpoint: A Design History*.

Typically a book like this is filled with glossy marketing photos that focus on what shipped, which is of limited value. Instead, what if this book described every notable change to the product's UX over its whole life, with detailed explanations for why each product decision was made? Helpful, right?
  
So I’ve been working on writing docs just like these for a few years. I call them DHRs, for “Design & History Rationale” documents. Here are some tips our teams found when we adopted them.

**Cultivate a neutral tone**

It's important to explain each decision in good faith while explaining why changes were made. Writing “We did this dumb thing because the lead was a jerk, but then he quit and that's why the product got better" doesn’t help much. On the other hand, “we believed we could get more x if we sacrificed some y. After launch, we decided we had lost too much y, so our follow-up version aimed to address that." does. The more you can define the tradeoffs, the better people can learn. 

**Speak in layers**

Decisions are multi-layered. There's no one diagram or chapter that can explain everything. You have to work in layers to isolate each topic from others.

For example, if I was writing the design history of Powerpoint, I'd spend time explaining the evolution of the navigational structure from menus to the ribbon to the simplified ribbon that came when Surface got popular. But the key would be going beyond screenshots and embracing instead the meaty discussion about the pros and the cons of everything that led to it.

With that overview established, you could then turn to more detailed topics. A section on design decisions around animation. Another for presenter mode. Another for composition and editing of content. Eventually you'd have to explain differences with the Mac and how the design direction was reconciled with the Windows, web, or Android versions. It wouldn’t be a quick read, and that’s the point. This stuff is worth documenting clearly and comprehensively.

**Explain what happened to understand what might be next**

Things that are in the middle of being designed require a format that’s more similar to a diary or a journal. Link to meeting notes, research papers, sketches, images of whiteboards, everything.

Things that recently shipped should start to add more rationale to go alongside the history you’ve already written. Ideally you can hit a point where the documentation is done and packaged up. Then the later versions can simply build on the previous ones, like episodes of a television show.

The more you document your past, the easier it is to make a roadmap for the future. If you know where you've come from, and what tradeoffs got you here, it's easier to know where you want to go next, and how to best proceed.

**Invest the time**
  
The companies that document better have better documentation.  There are no shortcuts; you simply need to invest time and resources to make it happen. If you don’t, your competition will. I remind myself of this when I’m trying to get motivation to keep my documentation clear and up-to-date!

# Open Letter to People Who Send Feature Requests to Product Team Members

To Whom It May Concern,    
Thank you so much for taking the time to write. Everyone wants to feel they're having an impact, so emails, tweets, private messages, and hallway conversations at conferences remind us that *people care about our work.* That's huge.

I'm sorry I can't say much more than that. I can't tell you if your feature is in progress, or if we've decided never to do it, or if it's brand new to us and we're all throwing a party in your honour of your stellar idea. Why? For legal, competitive, and intra-personal reasons. We haven't come up with any way to signal "thanks!" without breaking our contract or causing trouble.

For example, let's say I want to do feature X. But my product manager really does not want to do feature X. We're doing a delicate dance every day where we're working out the best overall answer as partners. But if you tweet at me and say "do feature X!" and I say "working on it!" or retweet it, or tap the heart icon, I'm doing three things wrong.

First, I may be breaking my NDA. Second, I'm publicly disagreeing with my team. Third, and worst of all, I'm making it seem like I get to make decisions unilaterally. That's not how software works unless you're a one person team.

So, again, please keep the feedback coming. Please know that we read all of it, and yes, the feedback often finds its way into our internal discussions. But it's not really a good idea for us to respond. We're sorry about the silence.

Sincerely,    
Someone on a product team who has signed an NDA


# Some of the Many Times Steve Jobs Fell Short

One of the most important lessons in design is that there is no such thing as perfect. There are only tradeoffs, and it’s your job to balance them.

So your phone has a big, beautiful screen? Takes more battery than a small one. The fastest car on the road? No one can afford it. Really tiny file sizes for movies streamed over the internet? Then it won't be as crisp as one at full resolution. A sweater that keeps you warm outside? It's probably too hot when you're riding on a bus with heating. Absolutely everything is a tradeoff. 

What sets good designers apart from bad designers isn’t how close they get to perfection. It’s how well they find balance. Early Steve Jobs designs often failed  because he picked incorrectly. The story of Steve Jobs is how he learned to hone his approach and eventually find success. But it took decades!

**Lisa**
The team wanted to have a graphical user interface, accurately believing it to be the future of computing. It cost about $25,000 in 2016 dollars. It failed.

**Macintosh**

The team wanted a cheaper (and more beautiful) version of the Lisa. They got the price down to around $5700 in 2016 dollars. It did better, and inspired the industry, but it was still too expensive and had reliability issues. It failed. 

**NeXT**
The team wanted to make a high end workstation with cutting edge technology and features. They spent tons of money on the hardware design, the branding, and some truly amazing software. The world wide web was invented on a NeXT machine! Still too niche, still too expensive. It failed.

**Bondi Blue iMac**
The team wanted to remind the world that Apple still existed and still had the ability to inspire. The result was an inspiring product that was inferior in almost every way (limited software selection, sluggish specs, a mouse people didn't like, no restart button, and no disk drive, more expensive). This was one of the first times Jobs actually used design successfully. That iconic blue case saved Apple.

**G4 Cube**
Riding Apple's resurgence, the team decided to take a standard tower configuration, make it much smaller, prettier, harder to upgrade, and more prone to overheating, and sell it for almost the same price as a real tower. Inspiring to look at, but it failed.

**iPod**
The team wanted to make the best music player in the world. And they eventually got there. But history has forgotten those first three years. The device was far too expensive, the screens scratched too easily, they only worked on Macs, and the addition of capacitive buttons was usability nightmare. It was a great product in some ways, but it took a while to find mainstream acceptance. 

**iPod Shuffle**
The team wanted the shuffle to be cheap. So they were ok with any tradeoff they needed to get there. So they removed the screen. Later, they even tried making a version that required you to control it with your voice. 

**Apple TV**
The team wanted a drop-dead simple interface, so that's what they did. The tradeoff was that it couldn't do much while still being much more expensive than its competitors.

**iPhone**
After ten years, we understand iPhone was a massive success. But it was rough for those first few years. There were no apps for it at first. It was far more expensive than anything else. It was on a slow wireless standard and had slow software. It could barely make successful phone calls, and it was only on one carrier. It wasn’t even close to perfect, but it did change the world.

**Removable Media, Ports, Standards**
When the Macbook Air launched, Apple removed the DVD player to save some space and weight. People hated it at first, but over the years no one missed it. The same happened with the headphone jack, USB, MagSafe, Firewire, serial, SCSI, and plenty of other standards. Not to mention the move from OS 9 to OS X, which required a whole new operating system, with completely new apps and drivers. Or the much from PowerPC to Intel chips, another enormous tech migration.

**What’s Going On Here?**

The designs made by Steve Jobs and Apple are famous for their imperfections. And yet they make some of the best rated, best loved, highest valued designs in the world. How can this be? How can a company seemingly uninterested in perfect continually make things that people love so much? I have a theory.

When you try to make something perfect, you become timid because you can’t bear to fail. But when you understand your job is to try again and again, you do. The more you try, the more you improve. The more you fall short, the further along you get.



**What Jobs Thought of The Past**

I love this story. The original Macintosh team was in crunch mode back in 1984, trying to ship the first Macintosh, and someone asked Jobs about typewriter-specific documentation. Had he considered making a migration guide for people used to typewriters who wanted to buy a Macintosh? He thought about it and reportedly said something like this:

> “People will figure it out. And the others will die off."

That's a big part of his design philosophy, and by extension, Apple's. They always try to make the best thing they can, based on the things they deem most important. But to celebrate the things they want to go do, they’re going to have to be sub-standard at other things. The product may be the most beautiful, but it’ll be the most expensive. The technology may be years ahead, but it’ll only work on Macs, or only on AT&T. The phone may be slimmer than anything else, but you may drop a call if you grip it wrong.  Think that’s crazy? Steve has an answer for you.

> “You know what? Some mistakes will be made along the way. That's good. Because at least some decisions are being made. We'll find the mistakes … and we'll fix them!"

That’s why he eventually succeeded. He didn't believe in perfect. He believed in working hard, doing your best, seeing what people thought of the result, and trying again. And again. And again. It’s not easy, but it is simple. Anyone can do it once focus more on iteration than perfection.

# The New Table Stakes

Software companies often use the phrase "table stakes" to describe something that users expect. For example, you might have heard Microsoft executives say in 2010 "Windows needs an App Store, that's just table stakes now."

But if you dial the clock back a year or two, you'd see the same Microsoft leadership explaining why Apple's App Store was too strictly controlled. This isn't unique to Microsoft, of course. Apple wrote off the two-button mouse for years, or the importance of gaming, or VR, or a million other things. User expectations have a funny way of inflating over time, and the key is evolving your product offerings to address user needs.

Here are some features I consider to be table stakes as of September 2017. These are not all things that are being actively requested by users. But they are things that make your product, platform, or service compare unfavourably if you don't include them. I suspect over time these items will become more and more obvious, to the point where it was odd I ever had to write these down.

**End-to-end encryption
Some apps have encryption, others don't. If you're making a messaging app in 2017 that hackers and governments can spy on, why would someone choose you over your competition?

**Identity theft safeguards
We keep seeing data breaches where personal information like social security numbers, credit cards, addresses, phone numbers, and even passwords are all stored together for easy theft. This is bonkers.

There are better, more secure ways to handle data. Your customers won't ask about this, but they will eagerly join a class action lawsuit against you if their information ends up being stolen.

**Polite email notifications
Customers will put up with some emails from your company. And if you work hard, they might even look forward to the emails the way some people look forward to Medium's Digests.

But most people's email boxes are completely jammed full and they don't want more junk. Some companies understand and respect this by using email sparingly and featuring a one-click way to unsubscribe. If a company can't run their business in 2017 without spamming customers, they won't be around long.

**Respect of platform conventions
Material Design, designed by Google, should be followed on Android. Apple's design language, designed by Apple, should be followed on iOS. There's always some wiggle room, but copy/pasting iOS patterns to Android or vice-versa causes all kinds of issues down the line.

**A trustworthy app store
There used to be a time where you could install software that could ruin your computer. With the arrival of the App Store, we were able to trust that the software you installed would usually run correctly without malicious side effects. Sometimes bad software gets through, of course. But it's not nearly as bad as it used to be. This is no longer an optional feature. Good.

**No invasive tracking
People understand that apps are going to track user data to help improve their product. And they’ve resigned themselves to the idea that the Googles and Facebooks of the world track them around the web.

But a product that can explain exactly what data it wants access to, and why, and give them easy and transparent ways to disable it, will be in a better position than ones who don’t. There was a time when seatbelts were seen as too expensive. Then companies like Volvo made it a big part of their brand. We’re see the same with companies that don’t track you, and I expect we’ll see even more of it.

**Graceful offline mode handling
Your SMS, email, calendar, and phone app don't fall apart with a series of confused error messages when the internet isn't available. They just do the best job they can without an internet connection, then they reconnect when they can. Every app should handle the internet this gracefully.

After all, most people don't experience a fast, cheap, or reliable internet. They experience spotty connections that work for a bit, then lose connection, then come back again, then get slow, then drop out, then back again. "Offline Mode" isn't binary. It's an approach to design that means you understand the internet isn’t reliable for anyone, no matter how much money you have.

**Low impact data
If your app uses fifty meg of data per day, and your competitor finds a way to provide a similar experience for only one meg, they'll have the advantage. People expect apps to be careful with their data.

**Low impact storage
The same applies to how much space your app uses. If you're using a gig of space on the phone and your competitor can provide a similar experience at ten meg, you're the first to get deleted when a user needs more space.
And to be clear: almost everyone needs more space on their phone. It's more intense in poorer countries, but it's a nearly universal issue across the world.

**Graceful "out of space" handling
People run out of space on their phone all the time. If your app can't help people in those situations, it will lose to the ones who can. Google brilliantly highlighted this difference between Google Photos and Photos.app in their famous ad campaign. Expect to see more of this sort of messaging.

**Actual customer support
Customer support is better and worse than its ever been. There are some pioneers who have built great reputations around their superior customer support, and they've been rewarded for it. Good!

On the other hand, most tech companies try to dodge customer support costs entirely. A relationship with a user is a privilege, not a right. If you wish you didn't have to deal with customer's requests quickly and fairly, you might soon get your wish. They'll move to your a competitor who treats them better.

**Access to files from anywhere
People expect to be able to have files where they need them, when they want them. This is easy to say, but extremely difficult to actually pull off. Apps need to be able to communicate across different platforms and screen sizes, then store data in the cloud, then synchronise everything correctly without dreaded merge conflicts. It's devilishly complex to get right, and the people who make it look easy are going to run away with the market.

**Simple backup/restore
People don't like to back up their data, even if they know they should. But when disaster strikes, they want to be able to get the files they lost as simply as possible.

**Years of security upgrades
Hackers are constantly finding ways to attack devices. Software updates (For example on iOS and Android) as well as carrier updates are the only way for devices to stay ahead of the curve and keep customers as safe as possible. No one actively asks about this feature in the store, but the lack of it will provide a demonstrably worse experience in the long run.

**Leadership people can respect
There are still plenty of people with questionable morals running companies. But the PR damage from being sexist, racist, homophobic, or just plain mean has never been greater. In the modern era, an increasing number of people expect to give their money to people and businesses they can respect. Or at least not actively hate.

**Accessibility
Some products and features work well if you have a disability or just prefer a more usable interface. Most don't. This is no longer a low priority, constantly-delayed feature, because some companies have made it a priority and shown the world how it can be done. 

**A clear and consistent abuse policy
For many years, there were two beliefs that made abuse on the internet worse. One was the idea that simple is always better, meaning an interface shouldn't be cluttered with user controls like block, mute, and report.

The other mistaken belief was that you could just throw everyone's words into a giant pile on the internet and the best, most true, most trustworthy, and most noble content would magically win and rise to the top. Free Speech! This led to a disastrous hands-off policy of hoping everything would get better even as it was proved it there was no limit to how awful people could be to each other on the internet.

People expect companies to notice that abuse and harassment is a real problem. They expect terms of service that are clear and followed consistently. People expect that they have a right to say what they want, but also that they have a right to not be attacked just for existing.

---- 

People talk a lot about the future. Are we going to be commuting via self-driving tube shuttles? Are we going to use VR and AR to experience a new layer of reality on top of the one we already know? Are we going to use nanotechnology to wage war and cure diseases? Will we move to a system of wearables and leave our traditional phones at home? I'm not sure.

All that is really fuzzy to me. I don't know, and I can't get too excited about trying to predict the specifics of what's going to happen. But I do know that trust, privacy, and respect are evergreen concepts that mattered one hundred years ago and will still matter in the next one hundred. People aren't thinking about a specific technology, they're thinking of completing tasks that matter to them. If they can trust that a tool will complete their task in the right way, respecting them and their privacy, and work in any context, that tool will win.

Conversely, if the tool can't gain trust then it'll have a tough time. You might not trust that the app will work right because it's poorly designed. You might think the views of the CEO are abhorrent, and as a result not trust the company's whole mission. You might not trust that the company isn't going to invade (or leak) your personal data. None of these are easy things to paper over or fix.

The future, like the past, will come down to trust. To me, that's the central goal every tech company needs to work towards. In a world with a lot of options and not a lot of trust, how can you act in a way that makes people want to support you? It's not just up to engineering, or design, or marketing, or PR. It's a question of morality and ethics. And that's not something tech companies have been great at.

Which means there's an opening. And that's exciting.



# Reasonable Defaults
### A book of design essays  

# Preface

Five years ago, I published a book of design essays. When I was done, I thought I had nothing else to say. It turns out I was wrong. I kept writing and eventually I had a stack of new things to publish.

While editing, I realised that everything could fit into three topics: Listen, Learn, and Lead. But that was a lousy name for the book, so I went with Reasonable Defaults instead. Naming things is hard!

Now I’m done and I have nothing else to say. Again. For now.

# Some Stories About Designing for Voice

In 2007, I worked at a voice recognition startup. You'd call a phone number, speak to the service, and we'd send you a transcript via email. Because it was done with human assistance, you didn't need to be as careful with your speech, or get frustrated nearly as often. 

A year later, Apple announced an official SDK for writing iPhone apps. I lobbied my CEO and CTO to give me budget for hiring someone who could make an iPhone app. I got the budget, then designed and PM'd the release. It went live on day one of the App Store, hit #1 in Productivity, got featured by Apple in print media, and eventually the company was bought out by Nuance. But before all that was the meeting with Steve Job's friend, a venture capitalist in Seattle.

We arrived at his office and showed him a beta of our app. He gave us some good feedback, then said “Steve is obsessed with audio input. And he always has been. Remember the 1984 Mac?" Over several minutes, this man impressed on us very clearly that Steve believed the future of computing, or at least a big part of it, would be voice controlled. 

“We've got this iPhone, a big sheet of glass, and you can fit, what, five navigational items across the bottom? And a few actions on the top?" This guy knew, back in 2008, that we'd soon ask our phones to do nearly everything we ask of desktop operating systems, with far fewer pixels to work with. 

Then he asked about the elephant in the room. To really do voice right, it would need to be done at a system level. You wouldn’t want to launch an app before asking it for a weather forecast, or telling it to call Betsy. You’d want to tell the phone, meaning it would be controlled by Apple, not our tiny start-up.

Back in the office, the CEO and CTO quizzed me about what the VC had said. I agreed wholeheartedly that Steve Jobs has historically been obsessed with audio input and output. I agreed wholeheartedly that Apple would add something like this at the OS level. And I agreed wholeheartedly that a dictation scenario was a sliver of the overall audio strategy for Apple. This was bad news.

"There's got to be some hook into the OS we can get via the SDK," they said. "There's got to be some workaround for being the app that Steve is looking for across the entire phone."

"Maybe," I said. I hated breaking the bad news to them. "But Apple believes in owning the whole widget. If they think it's a big deal, they're going to build it themselves. At the OS level."

Within a year, the startup had been bought by a leading voice recognition company called Nuance. Two years after that, Siri launched as an OS-level voice assistant powered by Nuance. It was exactly what his VC friend had told us, and exactly what Steve had been aiming Apple towards since 1984. His company had finally kicked off his long-dreamed about voice assistant.

Steve Jobs died the day after Siri was announced to the world.

---- 

Six months prior to Siri's beta release, I was a lead designer on Windows Phone's Apps team. We built first party apps for the device such as Mail, Calendar, SMS, and Internet Explorer.

We had been working on an idea called "Capture" which sprung from the insight that jotting down quick notes via voice, scribble, or text is really important. I was fond of saying that our main competition for this scenario was the simple sticky note. Software is slow, sticky notes are fast. We racked our brains trying to figure out a way to address this scenario.

Capture got killed off multiple times to make room for other features. We kept bringing it back by looking for new ways to justify the investment in the feature.  At one point, I remembered what Steve's VC friend had told us years earlier about voice input. And I remembered about a vague rumour floating around amongst the Apple faithful, that Apple was working on an "assistant" for iOS.

iPhone and Windows Phone already had a rudimentary voice-to-text feature, but "assistant" sounded like a bigger bet. It sounded like it might be Steve's voice dream coming true. So I put together a PowerPoint presentation explaining that I thought the future of "Capture" was not just a single feature idea. I thought the concept of easily capturing any audio, text, or doodle and performing actions with it had big implications. I predicted it’d soon become a “table stakes” feature.

Which isn’t to say Cortana was kicked off because of my PowerPoint, because it wasn't. There were a million moving parts and I was just some designer trying to argue for a feature he believed in.

But however it happened, Siri launched and then the Cortana team was created soon afterwards. Many of the designers were my friends, and they sat right next to me, so I got to listen in and share thinking through the early stages.

I remember one of the key things I was concerned about was the idea of Cortana being too much of a "black box." What if you don't care for Taylor Swift but somehow the computer has decided you love her? What if your entire search and entertainment experience gets corrupted by this bad assumption? And worse, what if there's no way to see the bad assumption? And worse still, what if there’s no way to correct it?

These discussion led to Cortana Notebook, a place to see the assumptions Cortana is making about you and train her when she gets something wrong. When things are stored in black and white, you can correct mistakes and even input important insights to allow Cortana to help in far more intelligent ways than a black box assistant might.

---- 

We’re at an interesting point in software design. When product designers were asked to transition our software from big monitors to small mobile devices, we found ourselves cutting back all but a few features to fit on the screen. That was a tough transition. But moving from mobile app designs to voice will be even harder. When designers can’t rely on any visual design to communicate a product’s feature-set, status, or error states, much of what we’ve learned will go out the window. I’m looking forward to it.


# Chapter Two: Learn

Table of contents:

* “I Thought You Were Born With It”
* The Triangle Epiphany
* Jim Morrison vs No Man’s Sky
* The Graph That Changed Me
* First, Best, Free
* “Shipping the Org Chart” and Omelettes
* Deiter Rams Could Not Have Foreseen Dick Pics
* What the Orange Badges Taught Me
* Leaning Out
* Four Tricks
* Seeking vs Flow
* I No Longer Trust Notification Badges
* Some Stories About Designing for Voice
* Robots Are Replacing Designers

# Seeking vs Flow

Flow is that fascinating, productive, magical state where you’re focusing so intently that time can melt away. You might discover it playing music, writing comics late at night, or knitting.

Then there’s another state called seeking. It's how you feel when you're foraging for new content, new information, or new ideas. You're seeking right now. And you're in the same mode when you scroll through a timeline, or put together a mood board, or when people-watch at the park.

I've been thinking about these modes a lot. I'm always either seeking or I'm flowing. Seeking is metaphorically sitting on the couch, watching TV, and daydreaming about your next move. It's your default state, it's easy, and it’s perfectly healthy in moderate amounts. On the other hand, Flow is going to the gym. It's hard, but it's good for you. It makes you feel better. And even an hour here and there is worth a lot, if only you can get the motivation.

But the key  — and this is really important  — is you can only do one at a time. You're reading this article right now, meaning you're not making anything. And while you're making things, you don't have time to absorb other people's thoughts. Seeking and flow are mutually exclusive. So go forth and seek, but don't forget to flow. That's what works for me.

# Jim Morrison vs No Man's Sky

Jim Morrison wrote a poem that asks "Did you have a good world when you died? Enough to base a movie on?" It's an exciting premise. Travel to interesting places, meet and fall in love with amazing people, do great things, rack up the life experiences, leave behind a lot of great memories. Enough to base a movie on.

I briefly had a manager that had a similar worldview. He sketched it all out on a whiteboard while I watched silently. "I want to retire in ten years. It takes two years to make anything great. I have five shots left to be great. That's it."

I thought about his working style. He was confident and brash, and people found it really hard to work with him. Many people quit because of him, including me. I remember thinking "This is a deeply unhappy man," and it felt like the reason why might have been scrawled on that whiteboard, a dead-end screenplay masquerading as something more noble.

---- 

No Man's Sky is easy to describe. It's a video game where you can fly through the galaxy and discover new planets and the lifeforms on them. There's practically no plot, character development, or conflict. First you're on a planet. You see things. Then you fly to another planet and see different things. You just keep doing that. It’s more meditation than task completion. It’s more Thoreau than Tolkien.  Thoreau wasn’t interested in goals. So you do well in school to get a good job to afford a house to … what? What comes next?

Games like No Man's Sky put that question front and centre. There's no princess to save, no dragon to slay, no mid-career slump. You're on a planet. You're alone. There's a strange creature over there. Now what? What can you come up with all by yourself? I’ve learned, slowly and haltingly, and over my entire life, to love that feeling.

---- 

One of my favourite essays is called Solitude and Leadership. His points about hoop jumping really resonated with me.

> I know what it's like for you guys now. It's an endless series of hoops that you have to jump through, starting from way back, maybe as early as junior high school. Classes, standardised tests, extracurriculars in school, extracurriculars outside of school. […] what I saw around me were great kids who had been trained to be world-class hoop jumpers. Any goal you set them, they could achieve. Any test you gave them, they could pass with flying colours. They were, as one of them put it herself, "excellent sheep." […] What gets you [up the career ladder] is a talent for manoeuvring. Kissing up to the people above you, kicking down to the people below you. […] Being whatever other people want you to be, so that it finally comes to seem that, like the manager of the Central Station, you have nothing inside you at all. Not taking stupid risks like trying to change how things are done or question why they're done. Just keeping the routine going.

I meet a lot of Jim Morrisons in tech, each trying to make their life a movie. They want to be on the team developing the coolest features, with the smartest people, eating only the best food, drinking the best drinks, and getting the most credit from the widest range of people. Some succeed, but they tend to have a reputation for being hard to work with. I think a lot of it stems from learning how to be a hoop jumper, then running out of hoops.

My career increasingly reminds me of No Man's Sky. If I look at it one way, it's all the same thing with slightly different textures or colours. There might be a trillion design projects, but if you squint, they all pretty much work the same. And that can feel stale and stifling if you're trying to make a highlight reel. But if you're just trying to learn and stay curious as long as possible, wow, the possibilities can feel vast, confusing, and exciting.  Let's go exploring!

# The Triangle Epiphany
  
In the thirty years since we’d spoken, he had been to war several times. I squinted and tried to reconcile the enormous man before me  — fingers so large he couldn't use a smartphone!  — with the four year old I remembered.

We talked about a lot of things. At one point he said "You're thinking in a spectrum from left to right. But that's one-dimensional." He held his two fingers together in a plane. "But, see, there's a whole other world over here." And he rotated his fingers to form a triangle with his thumbs.

That was the triangle epiphany. It's stuck with me ever since.

I lean on this framing technique all the time when I’m designing. When I'm helping a team to the best of my ability, it's because I'm facilitating and coordinating and helping reconcile a million different incentives and goals across lots of different people.

My best work, the stuff for the trophy case,  never comes down to the pixels. It's when I've been able to see the spectrum of ideas from A to Z, but I've somehow been able to turn the entire spectrum of thinking on its side in order to add a third dimension to it.

# "I Thought You Were Born With It"

Imagine a man in the corner of a large design studio, pacing in tight circles, pulling at his sleeves, muttering to himself. "Hi everyone, today we're going to- no. Damn it. Product design is about tradeoffs, so- fuck. Hi, today I want to talk about design tradeoffs, because without them… gaaaaah. No."

Another designer approaches and says, "This is good to see, Jon."

"What, your design lead pacing around and talking like a crazy person?"

"It's good to see you practice. I thought you were born with it."

Wow. No. I was absolutely not born with it. And that’s when I realised I need to make this much more clear. If you think some people are naturally good at certain skills, it follows that most people aren’t. And if that’s the case, why bother trying at something? Why not just leave it up to gifted people?

---- 

Years ago, I worked at frog design. I tried to keep my head down and do good work. I was intimidated by the other designers, and I was not known for my speaking abilities.

So you can imagine my surprise when I was asked to speak to the entire studio as part of our "Third Thursday" speaker series. Someone else had dropped out and a fellow designer decided I'd be a good replacement. I was hard at work when she wheeled her chair over to mine. I scooted away. She scooted again, and I scooted away again.

"You're doing Third Thursday," she informed me.  
"What? No thank you," I deflected.  
"You're doing it," she said, and wheeled away before I could stop her.  

I had a talk lying around. It was a presentation I had used once on university students. I figured I could re-work it for my designer co-workers. Maybe. They were used to seeing really talented designers, but instead they were going to get me. So I set out to make my presentation as good as possible. 

For the next 20 days or so I practiced my talk 2 or 3 times a night. I recorded myself on camera and studied the tape to see what parts worked and what parts didn't. I timed myself again and again to make sure nothing lagged. I poured extra attention on the intro and the conclusion.

Later, Dawn's husband explained his theory about how much I practice. "You're terrified it won't go well," he said, and he's right. It's not because I'm a naturally gifted speaker, or I read an inspiring book about public speaking, or I'm trying to hone my craft. Nope. I'm terrified, so I practice. That pretty much sums it up.

---- 

The deadline arrived, as it always does. I was terrified, as I always am. So I did my best. When I was done, the head of the studio barely waited for the crowd to stop clapping before saying, "That was good. That was really good. You should … do that always. I'm serious. Like, you need to submit that to SXSW or something."

I started working on other talks and I got to experience life of a speaker. Green rooms. A/V checks. Q&A. Hotel and airfare paid for. The speaker's dinner. People recognising you at the breakfast bar from the day before. And in time, I did end up speaking at SXSW. But it was a slow road. Paved with lots of practice.

And that was right around the time my direct report said he thought I was born with it. I wish I was! When I do well, it's not because of raw talent, it's because I've practiced more than anyone says you should. Anyone can do that. Some people do, and those are the ones that are mistaken for having natural ability. But anyone can do it.

# Four Tricks

1. Scientists learned that talking about doing something actually releases happy chemicals in your head. And that makes you less likely to actually do it. So if I say "I'm going to write a novel!" I'm actually robbing myself of some of the chemical motivation. So, step one: stop talking about it.

2. The human mind likes to grow and succeed. (The three key words are mastery, autonomy, and purpose - you want to get better, control your destiny, and feel like it matters.) So you can set up little games for yourself. My favourite is making "chains" and then trying not to break them. Each day I write. Then I keep a log of my hot streak. As of today, I'm up to day 38, a record! So, step two: make things into a game.

3. Inspiration is misunderstood and overrated. Imagine 100 people are reading this essay, and 90 are inspired to go create something. How many do you think actually turn that into a tangible result? Probably five or less. And that adds up. Those 5% are the ones getting the good jobs, breaking through writer's block, and finding meaning. So, step three: get to work. There's no trick, there's no secret. It's just work. Be one of the 5% or someone else will. Go!

4. Now, you've probably read this kind of self-help essay a million times, right? What can you learn from this? Maybe reading isn’t helping. Maybe you don't need any more lofty talk. Maybe you don't need to passively look on the internet or in books for inspiration. That brings us to step four: don’t read so much. You want time to seek, but also time to flow.


# Leaning Out

Sometimes I can't stop talking. I tell very long stories and I tend to turn two-sided conversations into one-sided lectures. The irony is I would much prefer to stay quiet and listen. But sometimes it's a compulsion. I can't stop talking.

I've had many female friends and co-workers describe the other side of this. Not just working with me, but most men. When a person won't stop talking, it's hard to wrest control of the conversation. Most women I knew end up thinking "Ok, I guess I'll just be taking notes in this meeting." Sometimes that's easier than finding a way to get a word in edgewise.

Women are often told to "lean in" when faced with these situations. But to date, I've never had a conversation with a man about "leaning out." Going out of your way in a meeting to say less than last time. Looking for opportunities to hear a statement you agree with to then amplify it. Trusting that not every topic needs you to weigh in, and knowing that the more you lean out, the more space you're leaving for others to fill.

I still talk too much. I haven't suddenly gotten great at saying less. But it's something I'm working on, slowly. And the results have been fascinating. It's not as if I've disappeared from the radar. I'm still working just as hard. I'm still contributing just as much. But our discussions have gotten richer as more people have gotten comfortable talking. And that's always a good thing.

# "Shipping the Org Chart" and Omelettes

From Walt Mosberg's [review of the Samsung Galaxy S7](http://www.theverge.com/2016/3/8/11176606/walt-mossberg-samsung-galaxy-s7-smartphone-review):

> Out of the box, there are two email apps, two music services, two photo-viewing apps, two messaging apps, and, except on Verizon, two browsers and dueling wireless payment services. (Samsung says Verizon barred including Samsung's browser and Samsung Pay out of the box.) And Verizon builds in a third messaging app.
> The setup process also guided me to using Verizon's messaging app rather than Samsung's and a Verizon backup service. It even warned me I might lose important stuff if I didn't sign up for the Verizon service.

That's called shipping the org chart. Or in this case, the org chart of three separate companies. It also happens within single companies. Each division pushes for their own goals, meaning the user experience is left to suffer. 

Imagine making an omelette where you had to deal with the egg team, the mushroom team, the butter team, and the tomato team, where each had made a team goal of increasing their usage by 20% quarter over quarter.
You'd make a bad omelette, and that's called shipping the org chart.

What you really need to do is use the correct amount of each team's output to make the best overall experience, even if it means some teams don't get to shine quite as bright. That's good management and should result in good product design. I think all of us in tech can nod our head at this assessment. Yes. Design is good. Experience is important. Companies should get their act together and prove they care about people using their products! Huzzah!

Not so fast. It turns out breaking things into little pieces and then hoping it’ll all come together can’t be blamed just on giant multi-national corporations because designers do it to themselves. Don’t believe me? Find any designer’s website and if it could talk, it’d say

"Hi. My name is John Doe, and I like perfect pixels and strong coffee. Here are six different links to learn more: LinkedIn, Behance, Dribbble, Twitter, Instagram, PDF of my resume, and my email. Good luck."

So then you click through each of the links to learn more. LinkedIn gives you a sense of what they've done in their career. Behance shows their creative work in a safe, gridded, vanilla portfolio format. Dribbble shows their creative work in a series of thumbnails. Twitter shows the last time they were annoyed at an airline delay. Instagram shows the sunsets and brunches they enjoyed last weekend. And that's all ok. But it's not great product design.

To use our omelette metaphor, designers are basically putting a bunch of ingredients on the counter and walking away. It's not a good omelette or even a bad omelette. It's just an indication that you didn't feel much like cooking. And if you're trying to get a job as a chef, that's probably not the message you're looking to send.

# I No Longer Trust Notification Badges

Many social apps started out with a simple pattern: one tab for global stuff, one tab for stuff that has to do with you. It was usually called Notifications, and people learned to place higher importance on it. It’s similar to how you care more about a personal letter than a generic advertisement sent to everyone in your neighbourhood. For a while, the “unread notification” badge was special.

Then things changed. Companies realised they could put all sorts of stuff in there, even general advertising, and people would look. Or "engage," as we say in the biz. Each time people “engage” with a feature, someone in the company gets closer to reaching their metrics. Which makes money. Which means promotions. Hurray for capitalism!

But I don't trust notification badges anymore, and I bet I'm not alone. We’ve been trained over time by companies like Facebook that the red badge can’t be trusted anymore. Which makes us engage less. Meaning someone is reading a spreadsheet somewhere inside Facebook and freaking out. There's an important lesson here.

It's easy to boost numbers in the short term with design decisions like these. But the gains are often temporary, and it's incredibly hard to win that trust back. But “trust” is such a squishy word. Let’s use real dollars.

Let's say your company is making $10 per user, then you mortgage long term trust for short term gain by putting spam in notifications. That boosts the number to $11 per user. That's a 10% increase! Wall Street rejoices!

But as time goes on, people like me learn to ignore the badge. And whoops, now your per user value goes back to $10, then to $9. What does the company do? How can it reverse this slide? How can it please Wall Street? 

Most designers would like to believe we could revert to the old non-spam pattern. But nope, that would mean the numbers could drop another 10%, which will make the money problem worse. They're stuck. 

And what about accountability? The person or team that originally made the change are probably on another team or even another company by then.
So the spam sticks around with no easy fix, and with no one taking responsibility for addressing it or causing the problem in the first place. The experience gets worse, meaning the product is worse, meaning the company makes less money. And the downward spiral continues.

John Gruber has a great insight on this phenomenon for everyone at a product-making company, from CEO to designer to engineer to product manager to CFO. As an industry, we should all take note:

> Once you're backed into a corner like this, where your users' happiness and satisfaction are no longer aligned with your revenue, you've already lost. It's like the dark side of the Force - you should never even start down that path, or you'll be corrupted.

You can convert trust to impact for a while, but not forever. Don't even try.

# What the Orange Badges Taught Me

One day at work they gave everyone orange badges to hang on the doorknobs of our offices that said something like "Warning: there may be content unsuitable for children being viewed behind this door. Please be advised." We had signed a deal with Playboy, and that started one of the most odd projects I’ve ever worked on.

**Insight #1: You can get used to anything

One day I needed to work on an audio/visual bug with my co-worker. I went to her cubicle and we tried to reproduce the issue. That’s how we ended up with a graphic sex scene playing, in a loop, while we worked to troubleshoot the issue. Eventually we figured it out, I wrote some stuff on a whiteboard, we agreed on next steps, we stopped the video, and I went back to my desk. No big deal.

**Insight #2: We go with our personal taste more than we think

Our VP was a woman, and most of our engineers were men. At one point we were in a meeting and the female VP asserted what porn genre was best to put at the top of the page. I disagreed, but didn't want to say anything.

If we're discussing the design of anything other than porn, it's my job to have an opinion, express it, listen to others, and then work together to move forward. But when the topic is porn, everything changes. Suddenly I have to decide if I want to tell a co-worker that I disagree with their taste in porn. No thanks.

Situations like this happened a lot, and they acted as a great lesson. The things we feel strongest about are often wielded as fact, when in reality they're opinion. Even if you’re a VP.

**Insight #3: Power dynamics are everywhere

I'm typically confident in meetings. I have opinions. I never have to wonder if someone disagreed with me because of bias against my gender, race, or nationality. But once I had to frame my design decisions through the lens of sex, I immediately got more shy. Which meant I wasn't designing to the best of my ability. There was an invisible power dynamic at play, and I immediately saw the corrosive results in my work. I lost half my impact.

---- 

So that’s what I learned from the orange badges. That project gave me a deeper understanding of how the status quo is designed to benefit people like me the most. Whenever I see an under-represented person holding back in a meeting, I don't blame them. I relate a little more than I might have ten years ago. Sometimes the culture isn't designed for everyone to speak up the same way and the product suffers as a result.

# Deiter Rams Could Not Have Foreseen Dick Pics

Dieter Rams famously wrote Ten Principles for Good Design that states that good design:

- is innovative
- makes a product useful
- is aesthetic
- makes a product understandable
- is unobtrusive
- is honest
- is long-lasting
- thorough down to the last detail
- is environmentally friendly
- is as little design as possible

These points all sound fine because they don’t have to grapple with any tradeoffs or real world scenarios. They’re as uncontroversial as saying that puppies are cute, or that cake tastes good. And I’ve learned over time that this simplistic, platitude-driven approach to evaluating design actually results in worse, more harmful, less human designs. Let’s use dick pics as a case study.

In 2016, Ash Huang went to her Instagram messages and saw an inappropriate photo waiting for her. She long-pressed on it, swiped at it, tapped around for some sort of edit button, or mute, or block. Something. Nothing was there to help her with this scenario. The feature was designed to communicate, with no answer for when people communicated in an offensive way.

She eventually found a way to report the guy. But she had some insights about the experience that I think are important for every product designer to think about and be aware of.

> It feels like everyone got busy on something else and then forgot to finish this feature. They implemented the simplest solution and then never went back to it. Sorry, Dieter. Simple is *not* always better. Sometimes simple is way, way worse.

The more "simple" a social product is, the more trouble it can cause. No one taught me that in art school. Maybe we should start.

> I'm sick of living in a graveyard of V1's that never get fixed and ultimately become confusing places to get lost in, or worse, backdoors for abuse from the badly behaved.

When Rams wrote his guidelines, “simple” meant clean, minimalistic, and pure. But things have changed. These days, the word simple just means you didn’t think through the edge-cases. It strongly implies that it only works in English,  chews through people’s data plans, only works on modern phones, is not accessible, and has no answer for handling online abuse.

Anyone can make something simple. But the people practicing great design are on a whole other wavelength. They’re building things that are thoughtful instead. And they’ll be richly rewarded for it.

# First, Best, Free

I read an article a while back that talked about three ways for a product to win in a category, especially software: be first, be best, or be free.

For example, there have been smartphones for a long time. You could argue that Blackberry was first successful one, and iOS did it better, and Android did it free. We can debate if iOS is still best, or if Android is really free, but the point still stands: there are multiple ways to set yourself apart.

Most items in the The Internet of Things are currently the "first" category. A lot of early adopters are having trouble getting their smart lights, smart speakers, and smart thermostats to talk nicely to each other, or even work well by themselves. These products are relying on a single marketing point: "now you can do this!" And that's fine for people who want to help beta test new computing interaction paradigms. They make a reasonable size of the market.

But the fun part for me is when someone makes something that's actually good, not just novel. The marketing shifts from "hey nerds, are you ready to do something complicated and new?” to “this is useful and you don’t need to be good with technology to make it work.” That's when I like to get involved. Not only as a designer, but also a consumer. Although sometimes I forget and get involved too early.

Case in point: I recently bought some remote control cars for my son that are powered by your smartphone or tablet. You can even race against a computer opponent that controls the second car via Bluetooth. Sounds fun! Sounds novel! So I bought them.

And they're ok. But the handling is really bad, and they go really fast. Meaning you spend most of the time unable to control your car and hitting walls at high speed. After the geeky novelty of "cars that can talk to each other in real time" wore off, we asked ourselves if we were having fun. We weren't. Into a drawer they went, like most firsts.

It's a good reminder for your own product. History tells us that "first" success is fleeting, but the product perceived to be "best" gets to live on. And there's hardly ever any overlap between the two. Plan accordingly.

# The Graph That Changed Me

I worked at RealNetworks in 2000. At that time, it was a proud internet pioneer. But then the dot-com bust hit, and the company made a lot of user-hostile decisions. And over time people learned not to trust the company anymore.

As employees, we weren't proud of our business tactics, and we griped about them frequently. The topic came up at company meetings, round table conversations with executives, and through a lot of water cooler conversation over email, during lunch, and across the foosball table.

One day my manager showed me a graph. It was pretty simple: the graph was steady, then it dropped straight down, then after a short period, the line shot straight back up and stayed level again. It looked like a cup, or the Big Dipper.

"That's what happens when we do the right thing", he said while pointing at the drop, "and that's how much money we lose. We tried it just to see how bad it was for our bottom line. And this is what the data tells us."

"Wow," I said. My employer clearly had two options: "do the right thing" or "be profitable". That was the position they had manoeuvred themselves into through a series of bad management decisions.

My manager then said, "More than half the company would have to lose their job in order for us to stop these tactics … so are you volunteering to be one of them?" I learned a lot that day.

Once people become wary of your products or your business ethics, it's game over. You can't sustain for long, because you won't keep your customers much longer. Not to mention your employees.

# Robots Are Replacing Designers

Art is subjective, product design is not. You can prefer a certain typeface, colour, layout, design pattern, composition, set of icons, or visual trend. But if it makes the company less money, and results in less engagement, and causes confusion amongst more people, it's an objectively worse design.

I learned this the hard way in 2005. I was asked to rank 5 banner ads based on how I thought they’d perform. We ran the objective results against my subjective art school training, and I was 100% wrong. The ads I liked the most did the worst. The ads I thought were the worst, the grossest, the ugliest, the ones lacking class performed the best. By far.

I was looking at the ads through the lens of art school, a common mistake for young designers. I was trained to look for beauty, tight Swiss grids, and pleasing colour choices. But that's not how people choose to click on ads. Sometimes they click on the garish, blinking, awful ads the most. That's why they're used. They work.

The biggest myth in art school is that fine taste somehow overrides the objective facts of what non-artists want. They don't. Increasingly, the data is proving that art school sensibility is out of touch and quaint when compared to the rigour and objectivity of machine learning.

---- 

This isn’t an issue for the future, either. Already there are huge bot ad networks that collage together thousands of variations of advertising in real time. They can see which ads are getting the most attention, and algorithmically make new versions on the fly. It’s fast, it’s effective, and it doesn’t care how intelligent you think your design is: these Darwin-born ads will always win. They're millions of times faster, get provably better results, and work for free. The robots are already putting designers out of work, and fancy art school degrees won't stop it. If anything, they might accelerate the trend.

As designers, we can sit on the sidelines and grumble about it. Or we can figure out how to harness the power of these robots and put them in our creative toolkit and processes. And the first step to doing that is embracing the reality of these tools. They work, they matter, and designers that resist them will be replaced by designers who learn to leverage them well.

Personally, I’m excited by the challenge. Designers have always been hired problem solvers whose results are judged against clear metrics. It always been our job to make the numbers go up. The higher they go, the better the design and the better the designer. Objectively speaking.


# Getting Stuck in the Gobi Desert

My elementary school did "writer's workshop," time set aside for kids to learn creative writing. Somewhere along the way, I began writing an epic story. I invented a new society and a new race with their own customs and rituals. I even went into detail about the special kinds of caves they built, and set the story in a real place, the Gobi Desert.

My teachers were charmed and thrilled. I was only in second grade, but I had charted a bold trajectory across an entire trilogy of epic-length books. My enormous project was a novelty around school. I was sent to other classrooms to show it off, like a traveling roadshow.

Here’s the problem. The story was all talk. I only ever wrote a single chapter. There were lots of discussions, some sketches, and my roadshow. But when it came to the actual work, it was one lonely little chapter. It turned into a thing to talk about, not a thing to actually do. It took me days just to settle on a location. That's nuts, but it looked on the outside like hard work. It was actually fear.

---- 

At the XOXO conference a few years ago, I spotted one of my early web heroes. I love his writing style, so I struck up a conversation with him. I asked how his novel was coming along. Soon it was clear he was stuck in the Gobi Desert.

"Well the thing about writing an epic novel is first you have to define the history, which takes some time. Then you need to layer in the culture, and then-" and I tuned him out like the Peanuts listening to the droning teacher. "Oh my," I thought. He is never going to write this book. Unfortunately, I was right. It’s such a common story. How can we prevent it from happening to us?

---- 

Here’s what I do. Imagine every single piece of writing or design in the entire world, all lined up like they're in a horse race. Imagine you can see how far along they are, from conception to being released into the world. From 0% to 100%. How would the dots animate over a year? How would the dots move towards their respective finish lines?

You'd see one in a billion that just seems to fly along the track, from start to finish, again and again over the course of months. That's Stephen King. Hi Stephen King! That guy is in his own league. Ignore him.

But the other thing you'd notice is how few most of the dots ever move. They flicker into life at the starting line, and maybe lurch forward 1% or 5%. And then they stop. And fade away. That's their entire lifespan.

That's how my Gobi Desert epic died. That's how my web hero’s story died. That's happening, again and again, across the entire creative universe, every day. That's how creative works are born and die too young, over and over, making it by far the most common ending. It happens to me, it happens to you, it happens to everyone. But it doesn't have to.

Aim for the space between Stephen King and letting your projects die out. Do it by doing a little bit, over and over, and one day you'll be done. And then you’re in good shape, because done is the engine of more.

# Chapter Three: Lead

Table of contents:

* The Ideal Speech for a New Lead
* Pick a Lane
* Bury, Bloat, or Kill
* The $1000 Birthday Bonus
* Slow or Choppy? A Litmus Test
* Speed
* Getting Stuck in the Gobi Desert
* Solo, Paired, Grouped
* Puppy Features
* Hire the Settings Designer
* All the Things That Don’t Matter
* My Big Pink Watch and Dorky Headphones
* Machine Learning Is Indistinguishable from Magic
* Surviving Information Warfare

# Puppy Features

People often fall in love with a silly, adorable, floppy-eared, brand new puppy. Then time passes, the puppy becomes a dog, and the owner realises dogs are a lot of work. As a result, city shelters are full of animals that weren't properly cared for after the novelty wore off.

It's sad, but it’s human nature. New things are fun, sustaining things over time is hard. It applies to everything. Pets, relationships, jobs, and especially software features.

I've worked at a lot of companies. I've been on a lot of teams. I've worked on lots and lots of features. But I can categorise all the work I've ever witnessed into to clear buckets: Puppy Features and Other.

Everyone wants the Puppy Features. A bold new bet! "Blue sky" thinking! Discovery! A way to not mess up like all the other dumb features that came before it. Sometimes they even make a whole new team for it. And it's hard not to fall in love . Everything is exciting and new, and the amount of maintenance and long-term responsibility required is close to zero.

That's not to say shipping a feature like this is easy, but there's no question it's easier than version five. Just like your first move in chess is a big deal, but it's nothing compared to the complexity of move five. Or move twenty-five.

When version one ships and grows up, you then have to take care of it. But that's not as fun. It's harder to find people who want to work on it. It's harder to prioritise it. You have to support users, which costs more time and money than you planned for. And as technology marches forward, aspects of the code or design patterns become outdated. Taking the time to get it feeling bright and fresh feels like a waste of time. After all, there are new puppies coming in every day. Shouldn't we focus on them instead?

It’s hard to fight human nature and not fall in love with the puppy features. But it's the old things that need the craftsmanship. It's the boring foundational stuff that needs your best designers and engineers. And if your "best" are bored by that work, I question your definition of best. They might just love puppies, and that’s not a particularly hard trait to replace.

Puppy Features don't make for great design. They’re novel, original, and fun. But great design that solves real-world problems effectively doesn't happen until after version one, if ever. Invest your design resources accordingly.


# Surviving Information Warfare

Zen, meditation, living in the moment, being present, learning to do nothing, solitude retreats, deep breaths, finding your centre, listening to your inner child, following your bliss, you've probably read those articles before. I'm here to tell you that things have gotten a lot more real over the last few years. And I have some media training suggestions I hope you take to heart.  

**1. Understand you're being manipulated

Nearly everything you see, read, or hear is trying to get you to buy into something. And there's nothing inherently wrong with that. If a company makes a product, and you want to buy that product, there's nothing sinister going on.
But you need to understand that you're outgunned. They have a lot of money to spend on making you feel like you need to buy more things. Your gut feeling about a product is extremely easy to manipulate.

The biggest mistake you can make is believing your instincts are operating cleanly, with no outside interference. You're being manipulated every day. Just being aware of that can help a lot.

**2. Reconsider how much news you really need

Older generations taught us that good citizens keep up with the news. And I think that used to be true. But the volume and velocity of news has increased to a point where there are some serious downsides to trying to keep up.

And if it were a bunch of respectable news organisations duking it out, it wouldn't be so bad. Because facts are stubborn things, and you'd find yourself reading pretty much the same story everywhere. So that's not so bad.

But information has been weaponised. And I don't use that word lightly. I mean, literally, people are attacking you with disinformation. It's part of a broader plan, and its much more effective and widespread than people know.

You don't actually need to read the news every day. You don't need to follow news sources on Twitter or Facebook. You think you do, to be an informed citizen. But it's backfiring. You're getting more overwhelmed, your moral compass is getting randomised, and you feel like you're drowning in news. That's by design. Reconsider how much news you really need.  

**3. Leave Twitter
The stated design goal and business plan of Twitter is to tell you what’s happening in the world, but there are no safeguards against disinformation. Twitter is how you mainline disinformation, fear, anger, and division straight into your mental process. Twitter’s drawbacks outweigh its benefits.

**4. Demote Facebook

Keeping up with friends and family is important, and that’s where Facebook is strong. On the other hand, those tight bonds are even more damaging when they’re used for disinformation. Hang tight to your friends, but understand the longer you spend on your timeline the worse off you’ll be.

This goes for other social networks as well. Do a search for a video called This Video Will Make You Angry and it will tell you everything you need to know.

**5. Audit your time

I'm going to say something controversial. You’ll probably disagree with me at first. Ready?

*You might have too much free time.*

I know, I know. Everyone's busy. Everyone's balancing a million things. We're stressed and harried and there's never enough time and and and-

But hear me out. If you couldn't check any social media at all throughout the day, meaning no Twitter, Facebook, Instagram, or Snapchat, how would your life change? And what if you went a step further and decided you weren't allowed to look at any news at all? If you're like me or most people I know, an interesting thing would happen.

You'd have more time, and your schedule would reshape itself in odd ways. It’s a bit like putting coins in a jar for a year and then discovering you have $100. When I cut out news, I regained 2 minutes here, or 5 minutes there. I wasn’t getting hours back, but my day started to have more daylight. That made me want to fill it up with better things.

When I didn’t have the built-in distractions, I found myself get bored more often. When faced with a choice to be bored or get my work done faster, I found myself preferring productivity. Which did leave me with more time. I filled it with books and writing.

This took  time and effort. I didn’t figure this all out in a day. It was a multi-year process, and I'm still working at it. But it all started by auditing my time.  When it occurred to me that reading news and opinion online was the single biggest obstacle to a healthier mind, the choice was clear.


# Solo, Paired, Grouped

I learned this trick from Quiet, a book about introverts. Instead of running a brainstorm the standard way, where everyone groups themselves together, try going solo, then paired, then grouped.

Imagine you're asking the room to collect insights about your team's current product. First get everyone to write ideas on sticky notes. As many as possible. Then, instead of going straight to the big discussion where you group sticky notes and rank them, like most sessions do, you pair up.

In the pair, nothing gets lost. It's two people talking, regardless of circumstance. Even if they're introverted. Even if they're in the throes of impostor syndrome. Even if they think their ideas aren't as important, or won't be popular, or a million other little doubts that get in the way. It's just two people sharing their thoughts as efficiently as possible.

A Venn Diagram appears. There are ideas that person A has, but person B does not. Then the reverse. Then ideas they both have. All three circles have valuable information. Document them. Then it's time to jump into the broader discussion, the standard brainstorm. You’ll find they run much better when you ease into them. Which means better teams, designs, and products. 

# The Ideal Speech For a New Lead

Let's say you got hired to spearhead a cool new product at Google. Imagine that the team had been operating for about a year before you showed up, but the previous head stepped down for some reason, and now it's your first day. What would you say? What’s the ideal speech for a new design lead?

**1. Make it about them, not you

Maybe I'm alone here, but I don't really care much where the person came from and what they did. Either they're so impressive that I know them already, or we're going to waste a few minutes letting the person brag about themselves while we escape into our phone.

When I'm the audience, I want to know how things will change. Is a re-org coming? Is my project going to be canceled? Am I going to be able to work with you? Trust you? Do you listen well?

**2. Skip the boilerplate high level stuff

It’s not your job to say we’re going to shoot for excellence, it’s your job to explain how we’re going to get there. “Let’s go hit a home run!” is not a particularly inspiring message in sports or product design.

**3. Don't imply that you're saving them

I see this mistake all the time. People start a pitch by saying "We all know that feature X really sucks, so my proposal is going to finally make it great." Or "It's been a tough road until now, but if we work together we can really do something great." I don't care how true you think it is, it's rude.

People are ready to accept new ideas, but not if you frame everything in a "it's a good thing I'm here" kind of way. So maybe don't do that.

**Here’s a talk I’d lead with:

Hi everyone! Thanks for that nice introduction. I'm really looking forward to jumping in and learning more, and that's exactly what I want to talk about today. I'm going to talk for about two quick minutes, and then we're going to open up for discussion. Sound good? Here goes.

Any designer knows that you need to understand the problem you're trying to solve. Which means humility and listening. So, to that end, it's my job in these first few weeks to listen. The better I listen, the better I'll be able to lend a hand. So that's what I'll be doing, before anything else.

I've sat through this kind of meeting many times. Some new person shows up, brags about how they’re going to do such a great job, probably does a re-org, things don’t get much better, then after not too long, they leave. Right? Too harsh? Maybe. But I see it a lot. I think we all have.

Here's the thing. I have ideas, obviously. Otherwise I had no right to take a job as important as this one. But my ideas mean nothing if I don’t listen well right from day one. I don't want to shoot from the hip or drastically change direction, I want to help accelerate whatever's already working. Which means less talking from me and more feedback from you.

So! That's my piece. Now I'd like to open it up for Q&A, or even better, just straight feedback. If you want to tell me about a thing you think is going well, or not going well, or anything else, please do! Because the more honest we can be right now, while I'm still learning, the better it is for all of us. So, does anyone want to be the brave person to go first? Gold star to the first one who says something. Which is pretty hypocritical of me to say because I'd rather die than ask the first question.

Ah ha! First question! Very brave. Please take my mic.

# Pick a Lane

When people tell me they love taking the time to get it right, I mentally calculate in my head "high quality work, but has trouble hitting deadlines." If they say the reverse, something about how "done is the engine of more," I think "fond of MVP releases, might not be as good at polish, user delight, and long term brand strategy." Both are perfectly valid approaches. The problem is when you refuse to believe there's a tradeoff to make at all.

Here's an example I see often: when design teams say that reusing components in a design language is important for UX consistency, I agree. Strongly. That's my default design philosophy and it'd take a unique client, product, or scenario for me to not default to it.

But componentised work, for all the benefit, has downsides as well. Some feature ideas just aren't possible with standard components. Meaning new work often takes much longer, with lower morale, because teams have to wait for the design system team to bless a new interaction model, or for the platform to provide it. There’s no magic bullet, only tradeoffs.

This is why it’s helpful if teams don't just use the happy words (consistent, user-focused, delightful, simple, clean, fast, awesome) but factored in the scary words too. For example, instead of "Quality first," it should be "We're comfortable shipping late in order to make sure the feature is as good as possible." Don't say "MVP" if you're not willing to say "We're ok with a product that could negatively affect our brand because we think the lessons we learn will be worth it."

Pick a lane. Don't tell me how great it's going to be, tell me what you're willing to make less great in order to get there. That's where the real conversations happen. That’s where the real work is done. The rest is just talk.

# Hire the Settings Designer

I've seen this play out several times on multiple teams. Everyone wants to captain the fancy new features, the sorts of things that might appear on billboards and commercials. But no one asks to work below deck, in the app's version of the boiler room: the settings screens.

Not only are settings extremely boring, they're also much harder than almost every other screen in the app. There's little room for animations, or delight, or even visual design. But it's where legal requirements collide with business objectives. It's where what you want the app to do runs headlong into how people are actually using it. It's where you want to be concise and clear but end up having to fit in a paragraph of explanation text because the feature is just too complicated to explain otherwise.

You spend a lot of time pondering the merits of clumsy, inelegant design patterns such as “don't show me this again" checkboxes, and after exhausting every other option, you sigh and go with them because there's just no other way. The life of a designer deep in the bowels of settings is disappointing and ugly. And the older an app, the more settings there are, because toggles and switches never die. They're the living embodiment of the design debt built up from hundreds of tough design calls deferred.

But it's great training. There's no better antidote to Diva Sickness. There's no quicker way to understand that compromise is not a dirty word. It's a swift kick in the gut for young designers who have been misled about what software design really entails. If I could choose between a person who only worked above deck on user-facing features or below deck on settings, I'd go with the settings designer every single time.
 

# Slow or Choppy? A Litmus Test

On Twitter, Dustin Senos asked the following question:  

> Which do you ship? A screen that: 
> A:  takes 10–15 seconds to load, scrolls smoothly 
> B:  loads instantly, incredibly choppy scrolling

51% picked A and 49% picked B, which is great. But I was more interested in the ways people tried to get additional context on the question. These were the most frequent ones.

**"It Depends"

26% of his responses asked for more information. This is how designers are trained to approach problems, so this is a good start. But it quickly leads into something designers struggle with, which is …

****"Neither"** 
… refusing the premise outright. Sure, there's nothing in the real world that would force cause you to make this exact decision. There's always going to be additional context.

But refusing to go with the premise, in a dumb little Twitter poll, is a red flag for me. It’s not a sign of mental strength to be unable to decide. It’s a sign you have a fixed mindset and trouble making decisions.  This isn’t a good answer.

**"Both, then test it"

I'm torn on this one. It's refusing to answer the question, which isn't great. But it's also pointing to an emerging reality in software design: computers know more. Ad companies literally generate a few thousand different visual layouts via code, then find the one that tests best. It always tests better than the one the designer made by hand, which is a sobering fact that designers should understand and accept.  I have to give some credit to that thought process.

**"Both, then let the server determine the right path"

One person said this, and it's an evolved version of the previous answer. All software design requires context. Change the brightness when the room is dark. Change the layout when the language reads right to left. Play different sounds based on whether or not the phone has been set to vibrate. Show different content behind China's firewall. Show the full version on desktop but load smaller assets on mobile. And so forth.

So this answer isn't just saying "ship it and track the results," it's implying that a lot of the design happens via smart algorithms in the background. This is a Real World way of thinking about the problem, so whatever points are docked for not following directions are added back on for seeing where a lot of the design work of the future will live: in the algorithms powering the rendering of the eventual experience. Good design is contextual.

**Slow load

Those who chose option A are dooming their users to a 15 second wait, which is an eternity. Some people said they'd get around it with informative or cute loading screens, and only if it didn't happen frequently. With that context, I'm ok with it. But otherwise, no. A 15 second delay without an extraordinary amount of user messaging is not an acceptable design, no matter how buttery smooth the app is once it's loaded.

I sometimes call these "load-bearing animations," meaning the whole thing comes crashing down if you don't get the animation and messaging just right. It’s easier said than done.

**Choppy

The remaining folks chose choppy and used the same rationale I would: sure, a choppy UI is unacceptable, but loading immediately is more important that smoothness, if you have to pick one. And without additional context to understand what kind of experience we're designing, we should default to fast loading speed.

**What these answers say about you

Yes, always gather data. But you're going to hit a point, even after prototyping and researching, where you have to take a leap of faith. And people on your team may be divided. Embrace those situations. Make your best guess, learn from the data quickly, and wherever possible, let the computer help frame the context and gather result data for you. There's always a way forward, and it's your job to find it. Not reject the premise on its face. Where’s the fun in that?

# Bury, Bloat, or Kill

Apps are always adding new features. Features demand screen space. Mobile devices have a limited amount of space available. Something has to budge.
When faced with this tradeoff, you have three options: bury, bloat, or kill.

Bury is the dismissive term for putting a button a few taps away, behind a "long press," or in a hamburger menu. The argument against burying is that engagement goes down if it's not on the front screen. This is a very good point. But it's not a veto card. Not everything gets to be one tap away.

Bloat is the dismissive term for putting the button on the main screen. Maybe your app starts with five buttons at first. Then you roll out a new feature and need a new button. Then your sales team signs a deal and it requires a new button. Over time, people say your app is bloated and they're right.

How do you get around the choice between bury and bloat? You kill. But good luck with that. Removing features once people rely on them in software is extraordinarily dangerous to the health of the business. It's easy for a designer to mock up at a whiteboard, but it's very hard to explain to paying customers or customers why they lost their favourite feature. Not to mention why they should trust you anymore.

So what's the answer? As with anything else in software design, the answer is "it depends." But don't go into these debating thinking it's easy. Don't think if someone buried they're a bad designer anymore than someone who bloated is a bad designer. And don't assume just because someone managed a kill that makes them a good designer. Product design requires a lot more from its practitioners than one-size-fits-all answers. If it didn’t, it wouldn’t be so fun!

# All The Things That Don't Matter

In 2011 or 2012, I pulled aside two friends at work and asked if they wanted to work on a new side project. Over the next few weeks we followed a standard design process to discuss insights, experiment with some directions, and put together a little movie and a PDF to help explain the idea to others.

We called it Project Fuzzy. The big insight was something we hadn’t seen anywhere else: messaging bots that help users get things done, notably around group calendaring. Now it’s been the better part of a decade and this is a direction that’s seen lots and lots of investment. 

This is where a lot of designers go wrong. We get sad and think "That could have been me!" or if our product shipped we say “Yeah, but we shipped it first." But that attitude isn't just defeatist, it misses the point.

The fact is, if you give 100 design teams the same set of insights and the same problem to solve, they'll come up with ~90% the same solutions. Not because they're lazy, but because there are finite ways to solve problems well. This is  tremendously exciting.

A design process is more scientific, structured, and reproducible than most people realise. Great design doesn't run on magic, muse, and talent. It's just a process. One that can be taught, learned, and improved on. It's not any different from learning any other skill, like woodworking or accounting.

But you wouldn’t know this by how the tech press talks about design. When a new product is announced, everyone rushes to write stories about how it's imitating another product. Or how someone else had that idea first. Or how the team's talent pre-destined them to find that design solution.

These are all the things that don't matter. What matters is taking some insights, running them through a design process, and then asking yourself if you solved the problem you set out to solve. Then you do it again. That's what matters in software. Anyone can do it, and they do.

# Speed

My friend recently posted this quote on Twitter.
> "Indifference towards people and the reality in which they live is actually the one and only cardinal sin in design" – Dieter Rams

Which immediately reminded me of this quote.
> 'It is difficult to get a man to understand something, when his salary depends on his not understanding it.' - Upton Sinclair

The first quote, while perhaps true, is a feel-good motivational speech that leaves everyone feeling like winner. Designers skim it, say "Yup!" and feel like great designers since they agree with the mightily respected Dieter Rams. There are no designers who read that quote and think "Oh. I wonder if I do that." Because of course not. That would mean they're not awesome.

The second quote explains why most apps and websites are too slow. The very things we know to recognise in a "good design" (massive hero images, 2x retina graphics, custom fonts, customised and specialised controls, Disneyified motion design, and three megs of Javascript tracking scripts that the business requires) are the things that are slowing software down more year over year.

Now, no one's arguing that we should change the web to a purely text medium, but the ballooning size of apps and websites has gone from notable to incredible to a mockery of basic design tenets. And it's getting worse.
"#notalldesigners," you might be thinking. "#yesallconsumers,” I say.

---- 

I was once in a design review. There were two teams present, one that was trying to make a splashy new feature, and my team, who was trying to help the feature work in bandwidth constrained/expensive networks like India and Brazil. 

This conversation happened:

> Me: "This looks great. Have you considered using some of our tools that help your feature's images degrade gracefully in places like India?"  
> 
> Team: "We don't have time to redesign this for every country."  
> 
> Me: "Understood. But if you just use our framework, you won't have to redesign anything. It just means we'll automatically compress your images when we detect a slow connection."  
> 
> Team: "The design is solid. Besides, India will eventually get broadband."  

This is a team incentivised to ship something “great.” But thinking about slow connection speeds, scenarios where internet is expensive, or the fact that almost no one in the world has a reliably fast connection contradicted those incentives. It would mean their graphics couldn't be as slick. It would mean they couldn't ship as quickly as possible. It would make their design less great, according to their narrow, high speed, skyscraper, San Francisco, designer-oriented definition of great.

Let's read those quotes again.

> Indifference towards people and the reality in which they live is actually the one and only cardinal sin in design.

and

> It is difficult to get a man to understand something when his salary depends on his not understanding it.

And there’s your problem. Designers won’t fix this problem because they’re not incentivised to think about it. They think they’re empathetic, and they are. As long as they don’t have to change anything they’re used to. That’s not empathy. And that’s not good design.

Postscript: it's worth pointing out that even on wifi, even on LTE in San Francisco, even with a high end phone with no data cap or memory problems, bloated design is still a big problem. These issues are not limited to someone on a feature phone in rural Africa, as is often implied. Speeding an experience by a single second, on any network, leads to direct financial and user experience gains. Speed matters, every time, everywhere, always.

# My Big Pink Watch and Dorky Headphones


A few days ago I decided to put my phone in my bag and see if my cellular-enabled Apple Watch Series 3 could get the job done by itself.

I popped in some AirPods and they chimed in anticipation. I held my Watch up to my face. It was 7:10am, I had a meeting at 8:00am, and I had 100% battery life. I tapped the Music complication I had placed for quick access and saw some albums that had automatically synced overnight. The second option was Kendrick Lamar. Sure, let's do it, K-Dot.

I usually skip the first song, so I brought the Watch up. The software was still on the Music app. This is notable because the first edition always went back to the standard watch face. It was nice not having to hunt and peck for the app.

I know where my bus stop is, but I was curious how directions work. I tapped my AirPods twice and said "Hey Siri, directions to work." My Watch buzzed with a response. I had expected the directions to play over the headphones, but I realised it was confirming the results on the watch face. It took me some hunting to find the way to change directions from car to transit. Turns out it was behind Force Touch.

On the bus, I realised the first big loss of my phoneless existence. Books. I can delay most of my phone tasks while I’m on the bus and I feel like I’m living a more virtuous life. But going without books seems like a step backwards.

At work, I worked. That means I had access to a full keyboard, the Mac version of iMessage, and a full email client with lots of bells and whistles. Then work was over and I got ready to head home. I didn’t miss my phone all day.

---- 

The evening commute was the same as the morning commute. I read a book while listening to music. Sometimes I had a text to respond to, and I did. A few times I needed to do other things like look up a friend on Find My Friends or look up an old photo, and I could. There were two exceptions. The sun was reflecting off a building in a beautiful way, but I couldn't take a picture. Later I wanted to listen to a podcast, but Apple Watch doesn't support them yet. So I settled for Music.

Later that night, I had to move some boxes around. I never realised what a pain wired headphones are, but now that they're gone, I feel it. I spent hours exerting myself and the headphones popped out zero times. With traditional headphones, I'd give up on listening to music after accidentally hooking them on furniture for the second or third time. It makes a huge difference for me, something I confirmed when I went on a run.

---- 

So that's how it's been so far. It reminds me of the iPod review I wrote after buying one the first week it was on sale:

> The most noticeable thing about owning such a gadget is how boring it is. I opened the case, peeled off the sticker on the faceplate that said "don't steal music" in 5 languages, and plugged it into my computer.

> With no fanfare, it started to swallow my entire MP3 collection, and about 8 minutes later, it was done downloading.

[…]
> After the 30 second learning curve, all that's left to do now is buy some new music. Anyone have music recommendations?

That's how the Apple Watch Series 3 is working for me. It's just doing what I want it to do, and there’s not a lot more to say. Leaving the iPhone at home no longer feels like a radical five year vision. If you have access to a computer during the day, and perhaps a paperback book or two, and can go without podcasts, going phoneless is starting to become feasible.

Maybe it's not for you, and maybe it never will be. But for those looking for a little less tech in our life, that day may be getting closer. It took three years for the iPod to start to find mainstream success. I suspect this shift will go even faster.

# Machine Learning Is Indistinguishable from Magic

Machine Learning and Artificial Intelligence have gotten a lot of press over the last few decades, and for good reason. In 1997, we witnessed a robot beating the best chess master in the world. It was a landmark moment, because it foreshadowed a day where machines could perhaps outthink humans.

(Historical note: some disagreed with this conclusion, because chess isn't as nuanced a game as the ancient Chinese game of Go. But twenty years later a robot beat a human Go master. And so progress marches on.)

I've seen a worrying trend in tech circles over the last few years. Machine Learning, or "ML," has turned into a shorthand for "magic." For example, you might say "We could use ML to make sure your phone automatically gets set to silent when you walk into a movie theatre." Or "We could use ML to make sure you never see abusive comments on the internet." Sounds great, like everything that comes from a magic wand.

But it doesn’t happen as easily as magic. All of this delight needs to be designed for, which means we need some guidelines. Here’s what I’ve learned designing for ML-powered experiences over the years:

1. Don't draw attention if you don't need to
2. Obvious always wins
3. Explain the implications
4. Provide opportunities to teach the system

**1. Don't Draw Attention

When Google auto-fills search results, they don't need to say "brought to you by Machine Learning." They just auto-fill search results.

When your car notices that you're skidding and goes into a specially designed skid algorithm, it lets you believe you are still braking while it silently gets the job done.

When your phone knows you always check your email at 7:02 in the morning, then tap Twitter, then tap Facebook, it can learn to pre-load all those requests one minute earlier. It doesn't need to provide a setting for it, and it doesn't need to explain it's doing it.

**Obvious Always Wins
Sometimes people miss important things on Facebook because its algorithms buried something they cared about. This leads to a loss of trust. Even if the algorithm is right 99% of the time, our brains are designed to feel anxiety about that 1% that the robot is getting wrong, and to overstate how often it happens.

The answer is not to forego all ranking and sorting, just to spot and neutralise situations that could lead to a drop in trust. For example, you can provide messaging that explains how things are sorted, and provide ways to see how the timeline would look without any ranking at all.

**Explain the Implications
Gmail has an option that says “never show me spam like this again." And then it will show you all the similar messages that would be affected by your new filter. This way you can see exactly what assumptions the product will be trying to make on your behalf, which increases the quality and the trust level.

If you've decided you need to tell the user about some gee-whiz ML feature, then you've signed up for clearly explaining the implications of their actions. And you also need to…

**Provide Opportunities to Teach the System

Spam filtering can guide us here as well. Sometimes the robot gets a bit over-zealous with its filter and suddenly your birthday greeting from Aunt Gretta is banished to the spam folder. But when you find it, no problem, you can tell the system "never mark this sender as spam."

This is a great pattern for a few reasons. 

1. You are able to see the implications of the incorrect decision
2. Failures don’t lose data, they move into a special folder
3. You’re able to train the system to do better next time


These steps make the system smarter while increasing trust. Win-win.

---- 
If you're working on products as part of a team, and someone refers to Machine Learning, here's what you do. First, you should appreciate that they're thinking boldly. That's a good start. But then you should get to a whiteboard with this person as soon as possible to hash out the specifics. The seeds of success or failure are sown in those first discussions. What is the problem we're trying to solve? Where do we want to take the user?

Do you ever wonder why Steve Jobs was so unsuccessful early in his career and so successful later on? This lesson:

> *"One of the things I've always found is that - you've got to start with the customer experience and work backwards to the technology. You can't start with the technology and try to figure out where you're going to try to sell it. And I've made this mistake probably more than anybody else in this room. And I've got the scar tissue to prove it. And I know that it's the case.*
> *And as we have tried to come up with a strategy, and a vision for Apple, um, it started with … what incredible benefits can we give to the customer? Where can we take the customer? Not starting with - let's sit down with the engineers and figure out what awesome technology we have, and then how are we going to market that. And I think that's the right path to take."*

And that's where we are with ML. As powerful as it is, it can't change the reality that the human mind hates mysterious and inconsistent black box design. Humans hate it when the robot makes the wrong decision, hates it when the machine won't allow the user to undo incorrect assumptions, and hates over-hyped and underwhelming products.

These concerns collide against Machine Learning’s default state, which is to powerfully guess, with no human input, and no clear rationale or documentation that explains how or why it performed an action. In other words, magic. When it works, it’s amazing. When it doesn’t, it’s far worse than any software we’ve seen until now. So it’s up to us to be careful, listen well, and work hard.

The best design of the Machine Learning era won’t take place in pixels or algorithms. It’ll all be in the messaging. That’s where the differentiation will be.

# The $1000 Birthday Bonus

Everything seems obvious in hindsight. Of course Steve Jobs’ product genius would save Apple. Of course the electric car was destined to happen. Of course Google’s search engine would make the company a lot of money as we moved from boxed software to internet delivery. But none of those were a given, and few of us would have made those bets back when they were new ideas.

At first, new ideas look unprepared, disheveled, and dangerous. It’s easy to see their flaws, hard to see their potential, and most of the time we’re right to be sceptical. Most ideas are not good. But as they prove themselves, and time passes, we tell ourselves the ending was always clear. But it wasn’t. Spotting potential amongst the silly ideas is the fun part.

Imagine the two of us start a company together. We have a product, we have a name, we and now we’re trying to figure out how to hire people and grow the business from scratch. What if I said “Hey, I think at our company we should give everyone $1000 on their birthday?” If you’re like most people, it’s easier to see the downsides than the advantages. Imagine how you’d respond. 

You might think the idea is interesting, but wonder if that’s really a high priority task. After all, we need to get office space, and buy equipment, and find clients, and a million other things. Do we really have the luxury of thinking about interesting perks when we don’t even have a staff? If you’re like most people, you’d move on to the next item on the list.

Imagine it’s a year later. The company has grown and you’re hiring as fast as you can. One promising hire turns down an offer to work at your company to work at a competitor across town. You hear that they give everyone $1000 on their birthday. Wait, we had that idea! Maybe it’s time to revisit it. Maybe there’s more merit to it than you first thought?

Can the company afford it? Well, sure. It’s just $1000, and you have 20 employees. That’s $20,000 versus payroll which is over two million dollars. If you’re really struggling, you can always pay everyone $1000 less of a salary to offset the bonus. In fact, you can even make it a marketing or recruiting expense. You can make it work.

But it sure is a lot easier when someone else proves the idea first, and it starts affecting your competitive standing. Going from zero to one takes guts. But being a “fast follower” is easy. It’s just good business. Imagine a year goes by, and suddenly everyone in town is offering $1000 birthday bonuses. It’s gone from a novel idea, one that brings prestige to the first ones to adopt it, to part of the expectations landscape. Now all you can do is provide larger birthday bonuses, but that won’t have the same impact as when the idea was new.

This happens again and again. A lot of new ideas are crazy until they’re expected. I saw this happen with Silicon Valley benefits step by step. It used to be crazy for work to pay for your lunch, or provide free yoga classes. Now if you can’t keep up, you’re not part of the conversation.

We’re seeing this in all sorts of fields and trends, and we’ll continue to. The question is not whether or not new ideas will seem crazy at first, because they always will. The question is which ones are actually good, and how long will it take for other people to notice?

# Notes

Published in July 2018  
ISBN: 978-0-692-15847-0

Licensed under the Creative Commons Attribution-Noncommercial-No Derivative Works 3.0 United States License. You are allowed to quote, send, translate, and reuse as much of this as you’d like as long as you kindly respect these two wishes:

First, you must give me credit. Second, you cannot make money off this work. Email me at reasonabledefaults@lot23.com if you have any questions or comments. Thanks for reading!


Radical Transparency, Go!

On writing the kind of “behind the scenes” essays I want to read more of.

I’m often disappointed by company dispatches, whether on Twitter, Facebook, or on their official blogs. They do a great job announcing new features, new products, and good publicity, but they come off like that friend who only tells you good news but doesn’t think to ask how you’re doing.

I want to read a “radically transparent” company blog. I want to know every interesting detail, and not just the good news. I doubt I’m alone in this; real, hard-earned, human insights can be invaluable. But too often companies hide all the good stuff behind a PR curtain, so all you hear is the launch, the new features, and the closing. That’s a shame. We could all learn from those wide gaps in between the official PR.

My company, UX Launchpad, is all about teaching design. So it seems fitting that I’d share every lesson learned in designing and launching a business. We’ve already seen some embrace this “radical transparency” concept, of course. Everpix and Nick Disabato are two good examples.

That’s how I want to run my company from the start. Apart from obvious exceptions (breaking someone’s confidence, jeopoardizing a deal, etc), I’m going to write to write here as if you, dear reader, are on the (fictional) board. Or working with me as a co-founder. Or just brainstorming at a whiteboard.

Those are the kind of company communications I’d personally want to read, so that’s the kind of way I’m going to write. Please enjoy, and if you have any questions at all, please let me know at @uxlaunchpad.

Thanks!
Jon

P.S. I re-wrote this essay quite a bit, and in the interest of transparency, you can view every edit as a movie if you’re curious ☺

By UX Launchpad on January 25, 2014.

Exported from Medium on May 19, 2017.

When I Realized I Had a Shot

I’ve always been terrified of starting a company.

I don’t know how entrepreneurs do it. I’m too risk averse to understand how someone can support a family, including insurance, without working for a larger company. Sure, you may make it eventually, but what about the early years getting the thing off the ground? How do you pay the bills?

About ten years ago, I saw Todd Dominey give a talk about not quitting your day job. His proposal was to work nights and weekends on your side project while still working at your primary job. If the company makes it big, quit your job! If it doesn’t work out, at least you’ve got some stability backing you up.

Last summer, I ran through some numbers in my head. If you multiply …

$300 per student times
20 students per class times
4 classes per year

… that works out to $24,000. Even if you take away 50% for taxes and overhead, that’s still $12,000 for only four sessions. Interesting!

So then I ran a different calculation. What if I did a class every two weeks? I couldn’t believe the numbers: $156,000 gross, then after calculating taxes and overhead, I was coming in around $100,000.

And that’s when I realized UX Launchpad may actually have a shot.

By UX Launchpad on January 26, 2014.

Exported from Medium on May 19, 2017.

No Exit

I don’t know why you say goodbye, I say hello

I don’t have it in me to “flip” a company. When I see giant buyouts, I don’t think “wow, I wish I had a jillion dollars in my pocket”, I wonder if the founders will get bored. I know I would.

When I saw Nest get bought out, and I was a little surprised by my reaction:


My immediate, visceral reaction to the Nest news
There’s nothing wrong with making a lot of money. I wish those founders and VCs the best. I just know that’s not where my head is. I want to love my work, and that’s worth a lot to me. No amount of money makes me enjoy my life if I’m not enjoying the work. Period.

I just read a great post by Adrian Holovaty called Why Chicago needs to stop playing by Silicon Valley’s rules. In it, he argues that Chicago is great at bootstrapping. Going it alone. D.I.Y. Not needing to take funding. Starting small and working really hard on the craftsmanship.

That’s something I can relate to. I think that’s my business philosophy too: find something you love, do it as well as you can, then keep it up. No Exit.

By UX Launchpad on January 29, 2014.

Exported from Medium on May 19, 2017.


Overhead

Spending (real) money to make (possible) money

My very official spreadsheet
As of today, UX Launchpad is $1300 in the red. Here’s how the expenses look when you write them all out.

Renting a day of space from Sole Repair costs $500, then we end up spending another $500 in coffee, lunch, snacks, and an open bar at the end of the day. So that’s about $1000 for the class, depending on how thirsty everyone is.

There are other expenses, of course. I set up the LLC, bought a bunch of sketching supplies, and did some advertising on KEXP and Twitter. But when it comes down to the core income/expense numbers, the math is pretty simple:

A class has ten people in it and costs $300 per person, so that’s $3000 gross. $3000 minus $1000 for the day means 33% overhead, meaning $2000 in profit. Not too bad.

So if I spend $500 on advertising, then sell out the next class, I’ll net $1500, at which point the entire business will be $200 in the black. Seems like a reasonable goal. I’m a fan of profitibility. Wish me luck! ☺

The next classes are on March 31st and May 1th.

By UX Launchpad on January 29, 2014.

Exported from Medium on May 19, 2017.


Perfect Ten

All my original calculations were based on having twenty people per class. Multiply 20 people times $300 times 26 classes, and that’s a business!

But I capped tickets at ten people last time, and it was wonderful. Large enough to feel busy and bustling at the appropriate times, but still intimate enough to bring a certain sense of comraderie to the day. It was really nice.

It brings up some interesting tradeoffs, though. It’ll take twice as many classes to make the same income targets I had predicted. But it’s a more enjoyable class size. What to do?

Well at this point, the answer is simple: focus on making the best possible product, then trust that the rest will fall into place. Maybe later we’ll find a way to accomodate twenty people as well as ten. Maybe we’ll use multiple teaching assistants to get the same effect. Who knows?

But for now, it’s moot. I’ve learned that ten is a great number of participants, so that’s what the upcoming classes will be limited to. Simple ☺

By UX Launchpad on January 31, 2014.

Exported from Medium on May 19, 2017.

A New Website

It’s time for a change

I want to update UX Launchpad’s website.

Initially the site was a simple landing page. Here’s what UX Launchpad is, here’s who I am, here are some testimonials about my teaching style, now please go to the Eventbrite page to sign up for the next class. Easy.

But now I have some photos and video from the day, plus new testimonials about UX Launchpad itself, and I may be getting a partner to run the business with me, which changes the whole voice of the site.

I’m hoping it’ll be more like “Hi, we’re two experienced designers that run this awesome one day design course. Here’s what to expect. Here’s what people have said about previous events. Now go to the Eventbrite page to sign up for the next class.”

The tone of the site would be changed only slightly, but I think it’ll come more in line with why UX Launchpad is worth your hard-earned money.

Oh, and the bacon! Everyone seems to love the bacon that the catering serves at lunch. We should find a place to mention that.

By UX Launchpad on February 8, 2014.

Exported from Medium on May 19, 2017.

Overhauls and Tweaks

The first UX Launchpad event was fun and I learned a lot. But afterwards I knew I wanted to overhaul the whole day. Some key things stayed in place, but everything else was significantly reworked.

Thanks to the lessons of the first event, the second one was a lot better. And afterwards I realized that the third wouldn’t need an overhaul to succeed. It would benefit from some targeted tweaks, like anything, but I no longer needed a hachet. A scapel would do.

Which is an exciting place for any design to be, because you can spend more time getting better at the small details rather than having to re-imagine the whole thing from scratch.

This goes double for any kind of lecture content, because delivery makes a huge difference. I think the schedule for the day is locked, so now I can look forward to getting more experience, and the day getting better and better.

By UX Launchpad on February 8, 2014.

Exported from Medium on May 19, 2017.

Focusing Your Efforts

If you’re fortunate enough to get to a place where you’re pretty good at something, you’ll have people asking you to join them at their company, their team, their book club, or whatever. It’s flattering, especially when you can recall a time when no one invited you to anything.

But just because you can do something doesn’t necessarily mean that you should. Pick your battles, save your gunpowder, live to fight another day, marshall your forces, more wood behind fewer arrows – you can take your pick of militarily-inspired phrases — but it all comes down to one thing: focus.

I just saw a post on Medium titled Be a Manager. His thinking was that “people like you” that refuse to be managers are why there aren’t enough great managers in the world. It’s a clever way to phrase it. He’s basically double-dog daring you to join him in the managerial ranks by appealing to your ego. “Have you no sense of decency, sir?”

I’m told I’m not bad at management. According to that essay, I should keep it up for the good of mankind. But here’s the rub: the more things you do, the less time you have to get really good at each one. Sure, you should expand your horizons. And yes, having a blend of skills is important to be a well-rounded individual. Most people would agree.

But I don’t think the average person needs help aimlessly scattering their ever-expanding interests into the wind. I think the average person needs to get better at focusing, really drilling in on a skill, and mastering it.

When I was interviewing for a design job a few years back, I was asked where I want to be in five years. Without thinking I said “teaching”. I think I surprised him, and I definitely suprised myself. But my answer was exactly right, I just hadn’t noticed it until then.

I can design, I can draw, I can write code, I can manage, I can make movies, I can write, and I can teach. I could do all of them at the same time if I wanted to. But I’m making a conscious effort to do less of the other stuff so I can get better at this. I want to teach for a living, so that’s what I’m focusing on.

By UX Launchpad on February 9, 2014.

Exported from Medium on May 19, 2017.

The Graph That Changed Me

Why UX Launchpad Won’t Be Like RealNetworks

RealNetworks popularized “streaming media” in the 90's, but stumbled after the dot-com crash in 2000, and never fully recovered.

I worked at Real from 2000 (before the crash) to 2005. And I noticed an interesting thing right away: RealPlayer was seen as annoying. Really annoying. Legendarily annoying. Pop-ups, forced upgrades, spyware, deals with shady partners, the works.

Which was a shame, because there was a time when RealPlayer was a pioneering technology, and one that people actually liked to use. But through a lot of user-hostile decisions by management, people learned not to trust RealPlayer or RealNetworks as a whole.

As employees, we weren’t proud of our business tactics, and we griped about them frequently. The topic came up at company meetings, round table conversations with executives, and through a lot of water cooler conversation over email, during lunch, and across the foosball table.

One day my manager showed me a horrible graph. It was pretty simple: the graph was steady, then it dropped straight down, then after a short period, the line shot straight back up and stayed level again:


Artist’s rendering of why you probably don’t like RealPlayer much
“That’s what happens when we do the right thing”, he said while pointing at the drop, “and that’s how much money we lose. We tried it just to see how bad it was for our bottom line. And this is what the data tells us.”

“Wow,” I said, taken aback. My employer clearly had two options: “do the right thing” or “be profitable”. That was the position they had maneuvered themselves into through a series of bad management decisions.

My manager then said, “More than half the company would have to lose their job in order for us to stop these tactics … so are you volunteering to be one of them?”

That was the day I learned I’d rather lose respectfully than win without honor. Once people become wary of your products or your business ethics, it’s game over. You can’t sustain for long, because you won’t keep your customers much longer.

Not to mention your employees.

By UX Launchpad on February 9, 2014.

Exported from Medium on May 19, 2017.


My Friend Bill

UX Launchpad Just Got Twice As Great

Bill (l) and Jon (r) playing video games
One of my dearest friends is named William Van Hecke, and I call him Bill.

He’s the User Experience Lead at the multiple-award-winning OmniGroup, he wrote my favorite book about software design, and he’s the kind of respectful and understated designer I wish we had more of in the industry.

(Look at the calm look on his face in that picture. Now compare it to mine. That pretty much sums it up.)

When Bill was writing his book, I helped brainstorm its contents and I was one of his reviewers. When I started UX Launchpad, I found that my syllabus was pretty directly influenced by his book.

So we’re going to make it more official. Bill’s going to help teach UX Launchpad courses, and I’m going to help with his next book. This will mark a small change with big implications; rather than his writing and philosophy being “the unofficial inspiration for UX Launchpad” it’ll instead be “the official course material for UX Launchpad”.

I love Bill’s writing, teaching, and design style and together we make a great team. UX Launchpad just got a whole lot better.


Bill communing with nature. Photo by Joel Reuter
By UX Launchpad on February 22, 2014.

Exported from Medium on May 19, 2017.

Fireballed!

It’s really nice being linked to by a big site. It’s happened to me a few times over the years, and it’s always fun. So much traffic! Some emails! New connections and opportunities! People talking about your work! Huzzah!

But it’s happened enough times that I’ve learned to look past it, to five days later. That’s when your baseline traffic often goes right back to where it was before the spike. The internet shines a huge spotlight on you, which can feel wonderfully warm and affirming, but then it moves on. Because the internet.

So I try really hard not to get distracted. The question can’t be “how can I get linked to again?” The question can only be “am I making my product better every single day?” A lot of companies get stuck with the former attitude, but I’d love it if more companies had the latter.

I can’t control inbound links, only the quality of what I’m asking people to spend their money on. So I focus there and do my best.

By UX Launchpad on February 27, 2014.

Exported from Medium on May 19, 2017.

The Wait

My biggest struggle with UX Launchpad

Here’s how it goes each time. I announce the upcoming classes on the website, and then I wait.

I hate the wait. It’s the hardest part for me.

The day I set up the first class, zero people have signed up. By the time I’m greeting people as they walk in the door for a full day’s class, I’m sold out. I should trust that I’ll always get enough people. So far it’s worked out.

But. None of that matters when I’m impatiently waiting for sign-ups.

I do subtle, careful advertising with the right advertisers. I remind attendees how much I appreciate their attending a class, their feedback, and their word-of-mouth. I study and practice and find ways to make the product better. I keep myself busy trying not to overdo it. I really dislike over-marketing, so I try not to do it myself.

But until I have a full class, it feels like I’m failing in slow motion. I wonder what will happen if this class only attracts a single person. I fret that maybe this isn’t a product people want. It feels like maybe it’ll never take off. It’s agonizing.

If I could fast-forward to where I have an extensive waiting list, that’d be great. But until then, I wait. And not particularly gracefully ☺

By UX Launchpad on February 27, 2014.

Exported from Medium on May 19, 2017.

The Maximum Viable Event

The “minimum viable product” philosophy has limits

“Minimum viable product” is a common term in the software community. Instead of spending a lot of time building a set of features no one cares about, conventional wisdom says should move fast and break things. Fail fast. Iterate. Pivot. Put the bare minumum product out there and then see what happens. Don’t overthink it.

But this advice doesn’t always translate. It certainly doesn’t work for what UX Launchpad is trying to do. We’re trying to sell a day-long design course for $300. It may be reasonable (or a drop in the bucket) compared to other training, but it’s still a lot of money! We try to treat the event that way.

Here are the ways we’d be able to turn UX Launchpad into a “minimum viable product”:

Don’t rent a great space in a great location; let’s just meet at the library
Don’t spring for A/V equipment, we can use cheap handouts
Don’t pay for an amazing lunch with high quality ingredients, brownbag it
Don’t pay for the snacks or coffee, point people to local business
Don’t pay for sketchbooks, pens, or paper; ask everyone to bring their own
Increase the number of people per class so we profit more
No open bar at the end of the day (this would save us a ton)
But then all you’re left with as an attendee is the literal content. Which is technically “viable”. You’ll learn, I guess. But it doesn’t sound like a lot of fun.

UX Launchpad isn’t perfect. We put a lot of time into hearing feedback and making it as good as we can. But that’s the key: we’re actually trying to make it the best we can while still staying in business, not just trying to make a quick buck.

We’re with Cabel on this one: we think the the world needs more “maximum viable products”. For us, it means making something that helps people learn design principles while having a lot of fun. We’re trying to sell a product, but more than that, we’re trying to make something we can be proud of.

By UX Launchpad on February 28, 2014.

Exported from Medium on May 19, 2017.


XOXO, KEXP

Seattle’s best radio station is great to work with too

KEXP mural
I spent some time figuring out how to advertise for UX Launchpad. I considered a week-long sponsorship on Daring Fireball but $9500 is too steep. I spent $200 on Twitter ads and came away impressed with the potential but wasn’t able to convert any leads. So I turned my attention towards radio, specifically KEXP.

Most people in Seattle know about KEXP. It’s an independent radio station with the slogan “where the music matters”. They’re well loved enough that they’re able to do NPR-style fund-raising rather than using traditional radio station ads. So I wrote them a short email to kick things off:

Hello! I run a design course for non-designers on Capitol Hill.
Was curious what your rates are.
Thanks,
Jon
A few weeks later, I had a deal to spend $980 on a week of sponsorship, which worked out to a handful of mentions on different spots each day. The process was pretty great. I told them how much I had to spend, they sent me documentation on KEXP listener demographics, we went through a few rounds of editing the script, then I got an MP3 of how it would sound on air. Pretty neat.

(Side note: turns out they can’t legally have the word “fun” in the copy because that’s an opinion. So we had to adjust to use words that are all provable, which was an interesting exercise. “One-day, hands-on, fun” is a phrase I try to use everywhere, but laws are laws.)

The ads ran in January and were received well. I had some friends write me to say they heard my ad, and the class ended up selling out. So this week I signed up for a new round of ads for the March 31st class, and the process was just as easy and friendly as the first time around. I may end up doing a studio tour or seeing an in-studio performance. Bonus!

I already liked KEXP as a listener, now I like them as a business partner too. Thanks, KEXP!

By UX Launchpad on March 2, 2014.

Exported from Medium on May 19, 2017.

Wow, Now I Like Spreadsheets

I’ve tinkered with spreadsheets before, but now I’m fascinated by them. This weekend I was doing the books for UX Launchpad and I noticed some good news on the horizon.

Here are all my expenses over time:


At least it’s consistent!
And here they are when I overlay income:


Green. Green is very important. More green please.
Here’s what happens when the numbers are combined:


Huzzah! In the black!
That final graph feels like seeing my baby’s smile for the first time. See, that’s my company becoming profitable. So you’ll excuse me if I dance a little jig.

Two notes you may be wondering about. First, why the big dip on the second-t0-last item? That’s the upcoming class, so it’s not sold out yet. If it does sell out, the number would actually be at -$450, at the very top of the red.

And second, what’s going on with that massive spike at the end? Well, that’s due to some April news. I’m still working out a few details before I can share, but as soon as I can I’ll write about it.

Spreadsheets are awesome. As long as they’re telling you good news. And these are definitely worth a jig. Maybe two.

By UX Launchpad on March 3, 2014.

Exported from Medium on May 19, 2017.

Barnacles on the Boat

Your product will never be cleaner than it was in version one. Somewhere between the first release and release four, a million little things attach themselves to the product. I call them barnacles.

UX Launchpad has only been a company for a few months, but I’m already noticing a similar effect taking hold. Two small decisions have had consequences that I want to fix.

First up, Slack. I love Slack, and it’s how Bill and I communicate all company business. But I signed up with a joke company name of “The” because I liked owning http://the.slack.com. I don’t know why.

Well, I went to register http://uxlaunchpad.slack.com, but there was a waiting period. And in that time, we talked a lot. So now I have to decide if I want to start all over on the right address or just stick with calling ourselves “The”.

The second decision was choosing to go with a product called Koken for writing some blog posts. The idea was that I’d write 19 essays about design, post them publicly, and that would free up the in-person class to focus more on hands-on techniques rather than me writing 19 essays worth of blog posts out loud. It was a good idea that a lot of people enjoyed.

But.

Now I want to move the content of those posts somewhere else. Or learn how to make Koken do a few things it wasn’t designed for. But it’ll take time either way. And in the meantime I’ve started writing posts (such as this one) on Medium. So I have some work to do.

Barnacles! They sneak up on you, no matter how well you try to plan. The key is accepting that messes will emerge and planning clean-up time in advance.

By UX Launchpad on March 6, 2014.

Exported from Medium on May 19, 2017.


Principles & Intuition

We’ll be visiting New York City soon

“You negotiate like a terrorist,” he said through his grin.

In 2003, my manager asked me to his office to offer me a promotion. More money, more responsibility, a new team. But I turned it down several times, which led to his bemused reaction.

But he understood why I couldn’t take the position. The new team had a co-worker who was infamous for being difficult to work with. I can’t put a dollar amount on team harmony, so turning down the promotion was easy.

Ten years later, when setting up UX Launchpad with my wife, I told her “I want to be like Big Nerd Ranch. I don’t want to travel all over the country, I want people to come to me.”

The classes in Seattle have been great, but I keep getting asked if I’ll take UX Launchpad on the road. And in the beginning, I kept saying no. I kept sticking to my early principle that I didn’t want to travel. I want people to come to Seattle. It’s a lovely city and I don’t like to be away from home.

But my intuition is telling a different story now. My kids are getting older, so travel isn’t as hard. My wife is supportive. It is kind of fun to travel, and corporate training is great because I can tune the syllabus to the needs of each business.

Principles only get you so far. In 2004, I didn’t need a principle that said “I need X number of dollars to work with someone Y difficult”, I only needed my intuition. And in 2014, I don’t need a principle that says “I will not travel”, I only need to trust my insticts.

And so, UX Launchpad will be traveling to New York City on the first week of April to teach a class. It’ll be for a company whose product we’ve respected for a long time. It should be fun, even though it breaks with an early principle. But I don’t mind. It just feels right.

By UX Launchpad on March 8, 2014.

Exported from Medium on May 19, 2017.

Designing the 201 Level Class

We could call it “Design While Shipping”

UX Launchpad was started to help people learn more about interaction design. And it’s getting pretty good at that. Developers, PMs, and visual designers have all come to classes and walked away knowing a lot more about the field. Hurray!

But it’s becoming clear that we need another class, one designed for working interaction designers. Why? Because learning interaction design requires a totally different set of skills than learning how to ship. Art school encourages a solitary and high-minded approach to design which simply falls flat when you’re on a real team, in a real corporation, shipping real software.

A friend and designer I respect recommended naming the class “Design While Shipping”, because that’s where the magic is. That’s where the best designers of our generation are making their mark, and it’s something more designers need to learn about.

So we’ve been drawing up plans for offering a second class. Please drop me a line if you’re interested or would like to share what you’d hope to learn from a class like this.

By UX Launchpad on March 11, 2014.

Exported from Medium on May 19, 2017.


Investing Behind the Scenes

I’m surprised how often people tell me they don’t like to practice their presentations, lectures, lessons, and pitches. It’s not that they don’t have time, but they think it’ll make them sound wooden. Stilted. Forced.

I know from my own experience as a teacher and student, a presenter and conference attendee, a team leader and a team member, that the most stilted deliveries come from not practicing. You don’t sound energetic, loose, improvised, and exciting, you typically just sound un-prepared.

Great stand-up comedians, actors, musicians, and visionaries don’t come off as wooden to me. On the contrary, their message sounds polished and compelling. Sometimes they can even reach a level where they are so poised and in command that it comes off as natural as breathing. It’s anything but, yet the masters can pull off the illusion. As long as they put in the time to practice.


Behind-the-scenes notes I’m drawing up for UX Launchpad
On Monday, March 31st, Bill and I will be teaching our next UX Launchpad class in Seattle. Then we teach it again on Thursday in New York City. It’s the same class we’ve presented before, so we don’t need to write anything new.

But.

In a full day course, timing is everything. So I’ve been putting together some behind-the-scenes notes. While one of us is presenting, it’s up to the other to analyze the time, our trajectory, and call audibles if necessary. The day has built-in “crumple zones” in the schedule so we can easily cut content to get get back on schedule.

Teaching is just one more design challenge. After doing this for many years (and sitting through a ton of wooden deliveries) we’ve learned to invest behind the scenes. Making things look effortless can be devilishly difficult, but it’s time well spent.

By UX Launchpad on March 23, 2014.

Exported from Medium on May 19, 2017.

All My Domains Were Deleted

Last week, the company that hosts my 15 domains went out of business and decided to delete everything. Which meant my company’s website got completely erased.

Poof.

The timing wasn’t great. I recently spent about $1000 to advertise for our upcoming class on KEXP, a local radio station. (I wrote about the experience here.) Then a few days later, Bill and I were going to head out to New York City to teach another class to the great team at Harvest. Both these classes and our plans relied on the domain being up and running.

Fortunately, everything worked out in the end. Between backups, Google caching, and some quick thinking, we survived the week. The classes went really well so now we have some time to catch our breath and reflect.

As a designer, I spend a lot of time talking about “delight” and “cool features” and “hero moments”. But what a lot of people want is less sexy, really hard to pull off, but a lot more profound: people simply want to trust your product.

I never want to disappoint a customer as badly as my hosting company disappointed me last week. Delight is good. But I want UX Launchpad to feel solid. Reliable. Worthy not just of payment, but of trust.

By UX Launchpad on April 6, 2014.

Exported from Medium on May 19, 2017.


That Magic Word

What Harvest Taught Us

“We’re bootstrapped.”

I couldn’t help but break into a huge grin. See, “bootstrapped” isn’t another silly buzzword, it has real meaning. What they told us in that one magical little word was “we don’t owe anyone money”, which sounds a lot to me like “we can afford to treat our customers well.”

It’s not polite to badmouth venture capitalists. VCs are people too, some business ideas really do require a lot of outside funding to get started, “this is just how it works in tech”, blah blah blah.

But let’s call a spade a spade. Nine times out of ten, a company that accepts venture capital finds itself needing to “return value” to their early investors, which translates to juicing their numbers, which almost always means decisions that actively make the design of the product worse. It’s just that simple, and it happens over and over and over again. It’s a shame.

So after a lovely day with the team from Harvest, we learned that they grew organically rather than taking a big VC check. And in that moment, I realized why their product is so strong. And why their team seems to genuinely enjoy working there. And why the founders are so focused on great design. Because they can afford it. They don’t have to buy the VC monkey off their back.

That’s the kind of company Bill and I want to build too. It reminded us a lot of The Omni Group, where he’s their design lead. Here’s a fitting line from their website:

The Omni Group, like our software, is designed for the long haul. A quick growth, buy-out culture works for some, but doesn’t fit our philosophy of long-term support for our customers.
Bingo. If you really care about your users, you have to really care about your product. You have to nurture it, grow it, and protect it from those who would mortgage its future for short-term profits. If you’re really serious, you’ll find a way to grow without taking a giant loan from someone who just sees your product, your users, and your company as a mere investment.

Coincidentally, I ran the numbers for UX Launchpad today and it turns out our young company is now profitable. Thanks for the inspiration, Harvest. You reminded us that new companies can make it on their own, and we hope to follow your example as we grow.

By UX Launchpad on April 7, 2014.

Exported from Medium on May 19, 2017.

Profits, Pressures, & Portland

It’s been almost two months since William and I flew to New York to have a great day with the folks from Harvest. (If you haven’t seen the post about our trip, read it here. It got us thinking a lot about growing your business the right way, even if that means it’ll go a lot slower.) Here are some things William and I have been discussing since.

Profits
I was nervous about running the numbers from the NYC trip, since I was putting a lot more emphasis on “have fun” than “pinch pennies”. Today I sat down, loaded Simple.com, typed “NYC April 1-5" in the search box, and got this graph. This is another reason why I love Simple so much.


Here are all our expenses in New York, minus $200 of Uber that ended up on another credit card. And in this graph I’m drilling in on “Food & Drink” … the longer we spent in the city, the more lavish our food bills became. Gulp.

But it turns out we planned well, because here’s the new income/expense graph over the life of the company. The red line represents $0. We were deficit spending for a while to get everything off the ground, but now things are a finely oiled machine. I am thrilled that this graph is looking so healthy, even after all our Manhattan dining.


Pressures
William’s writing a new design book, I just started a new job, and we’d like to work on tweaking the existing class and adding a new one called Design While Shipping. So we decided not to do any classes this summer other than one day-trip to San Mateo coming up in a few weeks. No reason to stress ourselves out. It’s important to enjoy the summer. Done and done.

Side note: Design While Shipping is also a talk I’ll be giving in June at the HIVE conference in Seattle with some incredible speakers. You should go!

Portland
I love Portland. William loves Portland. It’s not too far away and it’s nice to change the scenery sometimes, so we’re considering doing some classes down there. Sound like something you’d come to? Let us know!

By UX Launchpad on May 18, 2014.

Exported from Medium on May 19, 2017.

And Now For Level Two

Our company is profitable — now what?

If you’re like most people, you celebrate when you get unexpected money. Even if it’s just $20 more than you budgeted for, you’ll find ways to spend it. Buy something you’ve had your eye on, go for drinks, whatever.

But I’ve learned that making money as a small business owner feels very different. On one hand, UX Launchpad is profitable. High five! On the other hand, it’s not super profitable. It’s just this thing William and I do on the side that has recently been upgraded from “money-losing labor of love” to “break even” to “profitable”.

Which is huge, emotionally speaking. Running a business is hard, and profitability is difficult to pull off (and rare!). But you know that phrase that a hungry person only has one problem, but a person with a full stomach has many? That’s where we are now. Our biggest goal is getting more customers, so we’ve made a few moves.

Increased Advertising
KEXP and UX Launchpad have a great relationship. Recently they told us they were having a big discount on advertising spots and asked if we were interested. My gut said yes, there was enough money in the bank to cover it, so we bought a ton. Way more than we ever had before, so many that friends keep telling me “I heard your spot on the radio!” Excellent.

Upcoming.org Sponsorship
Did you know Upcoming.org is coming back? Back in the day it was the only events calendar that was any good. It’s been many years since Yahoo bought it, let it grow stale, and eventually killed it, but no other events calendar emerged to sufficiently scratch the itch that Upcoming did.

Until now! It’s coming back thanks to a bunch of sponsors including UX Launchpad. We sponsored the Seattle pages for the site for six months.

New website
Our current website is ok. But we could do much better on all fronts. Plus we’re going to add a second class based on a recent talk I gave called Design While Shipping.

Plus we’re starting to travel more, William’s working on his second book that’s designed to be the textbook for the class, we have new testimonials to post, and so on. So a new website is coming.

The obvious metaphor is that we’ve taken the training wheels off our bike. But to me it feels a lot more like a role-playing video game. At the very beginning, the game trains you up slowly. Talk to this person. Get a sword. Ok, now defeat this enemy. Ok, now talk to this person.

But that’s all level one. You eventually get to the point where you get to really play and take control. You can go in any direction, you can buy whatever equipment you think will help the most, you can talk to anyone you want. You have a goal, but the way you achieve that goal is up to you. It’s scary but fun.

I’m pleased to report that UX Launchpad is now at level two.

By UX Launchpad on July 20, 2014.

Exported from Medium on May 19, 2017.

Dear William

Thoughts from XOXO 2014 about our little company

Alright, I have some new ideas about UX Launchpad. Some ideas have grown organically over the years, others have popped into my head due to XOXO conversations this weekend. And there’s a lot of overlap between the two. For example, I’ve had some sneaking suspicions that have turned into no-brainer insights after seeing someone talk about them on stage.

A side note before I get started. Yesterday someone asked if we really need so many breaks between talks. Yes, yes, a thousand times yes. This is something UX Launchpad is good at, but most people aren’t. Most events don’t leave enough buffer at the beginning or the end, and don’t provide enough breaks. I consider UXL and XOXO a few of the pioneers of “don’t try to cram too much down people’s throat”. Good for us!

Ok, so I like the heart of the class. Designing a product from scratch throughout the day works. It works when I do it internally at Twitter, it works when we do our stuff in Seattle, it worked when we took it on the road. It works!

But. I think we need to add more.

Do we add more days? No way. It changes the whole thing. Maybe some day we’ll find a way to do that, or determine it’s useful. But I don’t think it solves the problem I want to address.

Metaphor Time

You know how when you pour water into a plant, you can pour faster than the soil can absorb? That’s no good. What you want is to pour enough water without pouring too much water. And that takes patience. You can’t just set down a two gallon jug, say “here you go”, and walk away.

Another metaphor is feeding your goldfish while you’re on vacation. You can’t just say “here’s two weeks of food” or “you won’t get fed for two weeks”. You need something in between. Again, it takes patience. A metering out of food at the right time.

What We Need To Do

We need to get better at take-away content. Today’s laser-focused goal is to inspire people to feel like they can do it (because they can), and then give them good takeaway content. Today I think we do a great job at the former. We demystify design. Good for us. But I want a better take away. I want UXL to be a gift that keeps giving.

When people pay us $300 I want to change their life. I want them to all achieve every dream they have. I don’t just want to inspire them, I want to mentor them all the way forward as far as I can take them. It’s up to them, but we’re in a position to help them a lot, and I want to.

Not as a “value add” but because I want to make the best possible thing I can. This ain’t just pixels, this is something that can change someone’s whole life. They’re betting on us. They’re investing in their own growth. I want it to be a slam dunk for everyone. I want it to feel like a bargain at 10 times the price.

If I had infinite time and resources, I’d become everyone’s best friend. I’d help them build a portfolio, I’d help them get a job, we’d go for coffee. That’s what my heart wants to do. But, uh, perhaps I don’t have enough time to do that. So what can we do? The day is a “launchpad”, but what does this person’s flight path look like? Again with the metaphors!

Some Ideas

I do a lot of silly little videos. I call them “two minute design videos” and they’re super effective for teaching design concepts. At work, a co-worker I respect a lot said:

PS Perhaps you’ve got a bigger gift than just working design at a big corp. These vids make me think that they’re a business in and of themselves. #justsayin
So. What if I did a bunch of instructional design videos? What if I wrote a bunch of nuts & bolts design essays? What if I did Fuck Jetpacks meets This Old House? So many designers talk about end product against a backdrop of “we iterated a lot”. What if we talked more about the iterations? The tradeoffs?

What if by the time someone came to UX Launchpad, they already had watched a bunch of Fuck Jetpacks meets This Old House videos and essays? Well heck, that’d be like watching Julia Child for years then getting to make a meatloaf with her. Right? Wouldn’t that be great?

And what if there was a little mailing list for alumni? Or hell, what if it was public? What if there was a forum for asking design questions and getting feedback? This places exist, yes. But in the way phones existed before iPhone.

Summary

Some changes are coming. Let’s talk more soon.

Jon

By UX Launchpad on September 14, 2014.

Exported from Medium on May 19, 2017.

It Turns Out We Love Running a Company

Last spring, we wrote about turning a profit, deciding to slow down over the summer, and maybe expanding to Portland. In July we discussed adding a second class, improving the website, and making bolder strides. This September, we wrote about expanding the courses to have a better focus on continued education. We have some updates to share on all fronts.

Not In Debt, Jiggity Jig

Here’s a graph of our finances. In debt, then profitable, then a tapering off. Why the tapering off? And what are those black dots on the right side? I’m glad you asked.

We’ve put a lot of money into marketing. William and I have always said we just need to a) be awesome b) tell people we’re awesome. We can control our own awesomeness by practicing and improving. But getting the word out is harder. So the lower black dot represents our cash flow if we spent a ton of money on advertising and no one showed up.

Fortunately, people showed up! So we made back all our money plus a little bit. Then the grey dot at the top represents a long term advertising investment we made in Upcoming.org, a site that won’t launch for a while.

Absent the Upcoming.org investment, we’d be slightly up. But with it we’re slightly down. I’ll take it!


How We Addressed Our Biggest Concern

It’s painful to spend a bunch of money on advertising and then hope that people fill seats. Especially when most people don’t sign up until the very last minute.


So we’ve re-designed the website and our entire flow. Instead of us saying “We’ll be doing a class on July 1st” and hoping for the best, we now ask people what day is best for them. When enough people have expressed interest in a particular day, then we set up the class. It’s been a huge help, and it was all due to a simple form change.

We Took Some Pictures

Wesley Verhoeve is doing this great project called One Of Many. He’ll be visiting 12 cities around the country in order to write essays and take pictures of creative people in their communities. He took this picture of us:


William on the left, Jon on the right
We Launched New Courses

Our original course and reason for starting UX Launchpad is to teach the basics of design to anyone who’s interested. But we’ve always wanted to add more advanced classes for working designers too. So we added intermediate and advanced classes as well. Check out the descriptions on the site!

We’ve Expanded to Portland

Portland is amazing. The people of Portland are amazing. Specifically, Tilde.io is amazing. They’re a small team of developers do consulting, run training, and have an event space! So UX Launchpad is now available in Portland thanks to the awesome people at Tilde.io.

We’re Going On the Road

We’ve been doing more “on the road” courses for companies, and we’ve decided we like it. So we’re going to start adding more traveling courses to our mix. If that sounds interesting to you and your team, please get in touch.

We’ve Started an Alumni Collection

We give the same advice every course: write out your design thinking in essay form. Then again and again. Then show it to other people. It’s a great way to get your brain in a design mindset even if you’re not already employed as a full time designer.

To this end, we’ve put togther an Alumni Collection for anyone who’s writing about design after attending our courses.

More and More Fun

I’m still not sure how we’ll go from “self-sustaining profitable” to “support our families profitable”, but that’s ok. We’re having fun, we’re helping people, and that’s been really rewarding.

It turns out we love running a company! Who knew? ☺

By UX Launchpad on November 4, 2014.

Exported from Medium on May 19, 2017.

Design Explosions

A New Kind of Design Critique

William Van Hecke and I are co-founders of UX Launchpad, a Seattle-based company that teaches design. We spend a lot of time thinking about how to teach better, and recently we noticed these bad habits in design critique:

Brand cheerleading
“I love them” or “Well, at least they’re not company x, I hate them”
Vague opinions stated as truth
“They nailed it” or “FAIL”
A disregard for complexity or tradeoffs
“I could do better in two days” or “It’s easy, they should just-”
These phrases aren’t helping anyone. We want to approach written design critiques differently. This is the result.

Deep like Siracusa, helpful like Mr. Rogers

Wouldn’t it be great if someone would combine the deeply informative and insightful style of John Siracusa’s technical reviews with the warm and kind teaching approach of Mr. Rogers? The essays would treat you like a smart and curious person who’s interested in learning more about great design, but without all the posturing and snark that passes for design critique today.

On Naming

Naming is hard! What we’re doing isn’t quite critique, it’s not just analysis. It’s sort of like teaching, but in a more conversational style. The word “explosion” spoke to us because it implies a glorious mess. Writing about something nuanced and messy is hard, and it takes a lot of humility.

After all, if you’re not on the team that built something, there’s a lot you don’t know. There’s no way to say “oh they just should have done x” because no feature or tradeoff exists in isolation, and you’re only seeing the results, not the discussions that went into them.

Which led to this discussion:


Our Three Rules


Your humble authors. Photo by @wesleyverhoeve as part of his http://oneofmany.co project.
Judging work without offering the benefit of the doubt is a common mistake in design critique, and something we’re hoping to avoid. So to that end, we’re following some internal rules while writing about products:

The team that built this product is smart and talented.
There are many things we can’t know without joining the team.
Design Explosions is here to teach, not to judge.
The first issue is a doozy, and it’s all about Google Maps and Apple Maps. Here is your complimentary poncho ☺

By UX Launchpad on November 16, 2014.

Exported from Medium on May 19, 2017.

Design Explosions #1

Mapping on iOS

Hullo! Design Explosions is a new series by UX Launchpad, a one-day, hands-on, fun design class based in sunny Seattle. Today we’re going to use Google Maps and Apple Maps as a design lesson.

If you haven’t already, please check out our brief introductory post before you read on. It’ll help you understand what we’re doing, and more importantly, what we’re not doing.


Google Maps on the left, Apple Maps on the right
Guidelines

You probably didn’t read the introductory post. That’s ok, we probably wouldn’t have either. The summary is that Design Explosions are written with three things in mind while we analyze the work of other teams:

The team that built these products are smart and talented.
There are many things we can’t know without joining the team.
Design Explosions is here to teach, not to judge.
So you’re not going to see us rolling our eyes or shouting fail. We’re going to assume that every flow and feature came into the world after a lot of debate and careful tradeoffs. In short, we’re going to give the designers the benefit of the doubt and focus on pulling lessons from the resulting design.

Hold Up, Hold Up—

Before We Get Started

If you’re reading this to see a head-to-head comparison where we crown a winner, you’re going to be disappointed. That’s not what this is about. Let’s use Medium’s handy pull-quote feature to make sure no one misses this:

This is not a contest, it’s a detailed design lesson.
Also, we’re not going to be spending any time on the accuracy of Apple’s map data, which is considered less accurate than Google’s. We’ll be focusing on the flows in the apps themselves, because those are details the designers can actually control.

Onward!




Canvas Layout

To kick things off, let’s take a look at the main screen for Google Maps in both high fidelity and as a low fidelity silhouette.


And here’s Apple Maps with the same treatment:


Right off the bat we have an interesting difference to explore: how the canvas is being used. Let’s compare the low fidelity versions side-by side:


Google Maps (l) and Apple Maps (r) as silhouettes
The screen size of an iPhone 5 is 640 pixels wide and 1136 pixels tall. In that space, both Apple and Google have a map with several buttons and options. But it’s interesting to look at how differently they approach the canvas.

Apple has a very conservative style (four right angles, no appreciable transparency) whereas Google is cutting everything back as much as possible (floating buttons and transparency). Let’s dive into the math for a moment to see exactly how many pixels they’re using.

The math for the Apple design is easy — 640 horizontal pixels times 920 vertical pixels is 588,800. Google’s is a bit more tricky because they’re using circles and some transparency. But our calculations come to 620,814 pixels. So the Google design has approximately 7% more real estate for the map.


Case closed, right? Google has more space for the map, the map is the primary element, so Google is better. Right? Well, it’s not that simple.

Kudos to Google for opening up more pixels for the maps canvas. But there’s also a visual simplicity to the Apple model. It’s not that one or the other is better, it’s that each team is optimizing in different directions. Which leads us to one of the biggest lessons of platform level UX design. Much of what we’ll be talking about today is something that many people don’t fully understand: 1st party app design.

1st Party App Design, Y’all

Millions of people have done 3rd party application design. If you expand the pool to people who have sketched out app ideas, the number is probably closer to tens of millions. But how many people have designed 1st party apps? Ones that come on the device rather than being a download? I’d wager the number is less than a thousand.

To count this number, you count Google, Apple, Microsoft, plus companies like Blackberry and Samsung, each with their own design teams. And even at large companies, the design teams aren’t very large. No where near as large as the engineer corps. There’s just not that many of us. And as someone who’s been in this tiny club, let me tell you, it’s absolutely nuts.

See, as a first party designer, you don’t get to do whatever you want. Because everything you do is imitated (whether explicitly or implicitly) by potentially millions of other designers and developers. And you need apps on your platform to be coherent and follow certain rules of the road.

I love using metaphors, but it’s hard finding one for how it feels to be a first party designer. One is the concept of democracy. Democracy may be a fine system, but when compared to communism or a monarchy it’s sometimes referred to as “fighting with one hand behind your back”. That’s like being a 1st party app designer. You actually have less control than you might think.

Another metaphor is becoming a parent. When you have children, it’s humbling to see them imitate everything about you. Because they’re not choosy. They don’t zero in on the good stuff, at least not exclusively. Your kids see and imitate all of you. Once you realize this, it’s a good motivator to become a better person. Even if that new you is a lot more boring.

When you’re a first party app developer, there’s no saying “do as I say, not as I do”. If you create some crazy interaction for an app, it will be seen as Officially Sanctioned By Your Company Forever And Ever Amen. Other people will imitate anything you do. So you have to be extremely careful about the example you’re setting. And trust me, this makes your designs a lot more boring than when you’re in a small 3rd party startup.

Until you’ve been in the room with a team agonizing over issues like this, it’s hard to fully understand how hard it can be. Imagine coming up with a great design. Something everyone on the team is proud of. But you soon realize it’s using an interaction that’s alien to the system your company has worked to standardize on. So you consider another design, one that’s more standard. Everyone agrees it’s watered down, suboptimal, and nothing like what a third party could get away with. No one loves it. So what do you do?

The safe, standard, example-setting, long term, big picture one. Every time.

And it’s painful, because you want to yell “but if we broke with the conventions, just this once, we could make things so much better for our app!” But it doesn’t matter. You’re a first party designer. You have an example to set. So the question is not “how much can you optimize for your single app,” but rather “how well can you champion the principles of an entire OS?” Which is why it’s fun to be a 1st party app designer, but it’s also tough. And it goes a long way towards explaining why all the sex appeal for apps tends to live in 3rd party software design, not 1st party apps.

Let’s look back to what Google and Apple have done with the canvas. On the Google side, you see a perfect alignment with their new Material design language. On the iOS side, you see perfect alignment with iOS 8's design principles. This is not a coincidence.


Simply put, both sides are “right” because both app teams need to uphold the principles set forth by their platform teams. Apple Maps would look wrong if it embraced Material, despite the 7% gain in real estate, and Google Maps wouldn’t feel “Google-like” if it embraced a pure iOS design. They’re both doing exactly the right thing here.

So that’s the canvas. Let’s move on to the buttons and the features.

Affordances

I think of an affordance as “a thing you can take action on,” but that’s not quite accurate. It’s more about the signal given to a user that something can happen. There’s a ton of science and art behind figuring out what to place where, and in what way. It reminds me a lot of ikebana, the Japanese art of flower arrangement. Wikipedia can tell you more, but for this article I’ll be saying “affordance” when I mean “a thing you can take action on”.

Let’s take close look at the arrangements Google and Apple have presented us with. There’s a lot to learn from here.


Before we get into visuals or placement, let’s just list what each design is letting you do in this screen.

Similarities

A search field for typing an address or location
A way to center the map on a user’s current location
A “directions” button
Google Only

The “hamburger” (the icon with three horizontal lines)
The microphone icon for voice input
Apple Only

The standard iOS share button (the box with an arrow pointing up)
An options menu (the lowercase “i” in a circle)
There are great reasons for every one of these differences. And even the items in the similarities bucket have different executions. Let’s go through them one by one, shall we?

Google’s Microphone

If something is related to something else, they should be near each other. This sounds obvious at first, but lack of grouping is a surprisingly common mistake. But not for Google! Here, and in many other apps, they put the microphone directly to the right of the text field. This clearly communicates that a user can tap the text field or the microphone, and either approach will affect the single text field.


Why have a microphone at all? Is it really used 80% of the time? Or 50%? Maybe it’s not even used 20% of the time. Does it really need to be front and center? Yes, but not because of frequency of use; that’s only one metric amongst many to consider. In this case we need to consider an important environmental context: driving a car.

There are a ton of scenarios where talking into your phone is awkward. Transcribing a long email. Having a private conversation on a bus. Tweeting from the bathroom. But while driving, voice input is huge. Huge! Which (partially) explains why Google makes it so prominent. It may be a minority use case, but when you need it, you need it to be great.

But wait — Apple doesn’t put it anywhere on the front screen. They tuck it a screen away as part of the keyboard. Why? Platform conventions. Remember our discussion before about 1st party apps? Google has decided, in many places in Android and their iOS apps, to feature search prominently. And that prominent placement puts the microphone to the right of the text field. Apple, on the other hand, puts their microphone in the keyboard itself.

We could go deeper into the analysis of these choices and the resulting tradeoffs, but for now let’s just say both Google and Apple are staying true to the precedent that their design teams have established. Next let’s talk about color.

In the Google image above, the microphone and hamburger are dark but the Google text is light grey. Why? Because if the word Google was dark, it would look like a logo, so people wouldn’t think to tap it. Let’s compare it to what Apple does:


This design is far more obvious, pixel-wise. There’s a surrounding grey box that looks like a button, and they’re using the same light grey text that’s standard for text fields. And even better, the text literally says “Search or enter an address,” which doesn’t leave a lot to the imagination.

But while Apple and Google agree on a few things like making the side buttons vivid while dimming the text field, they’re doing something pretty different in the default state of the text field. Why?

The Search Field

Google is search. It’s how the company began, and it’s the first thing people associate with the brand. It makes sense that Google doesn’t need to write “Tap here to search for things” since that’s pretty much what the logo means to people. So as a Google designer, you can consider putting a Google logo there and call it a day. (I’d still test it, of course, but it’s a reasonable thing to attempt even though it’s gutsy for most companies)

But Apple is not in the same privileged situation. Their maps aren’t Google-powered, and the Apple services that power them don’t have a brand such as “Apple Search”. But even if the product did have a name, it’d be risky to put the logo up there in place of descriptive text. A company known for search can consider it, but I doubt it’d work for anyone else.

So Apple is left treating the search field like any other search field in the OS. You see it in Maps, in the App Store, in Mail, in SMS, and it’s always the same: grey text clearly explaining how to use the field. This is not an accident, because the more places an iOS user sees search used the same way, the more the user cognitively gets “for free”. They learn to assume that all search on iOS will work the same way, and that reduces friction.

But Google’s not in the same situation. They don’t want to use Apple’s standard search control. They’re rightfully proud of their search technology, and by extension their brand, so they put their logo front and center. It’s a reasonable tradeoff, though it doesn’t look as predictable or actionable:


Now compare it to the Apple version:


In Apple’s version they use that rounded grey box, a standard control on iOS. It’s very purposely designed to look like a (flat) button. Whereas the Google logo is floating there. Why? Because they can … as long as the tap targets are generous enough. Let’s double-check just to make sure they did their tap target homework.

On the original iPhone, Apple recommended that tappable areas be no smaller than 44 pixels by 44 pixels. But remember, that’s the smallest they recommend — bigger is better.

[Note: Apple recommended pixels in 2007 but now that we have lots of different screen sizes and resolutions, points is the more appropriate way to consider tap targets. But this document assumes an pre-Retina display](#)

Let’s overlay 44x44 measurements on Google’s design to see what’s going on here:


Interesting! The microphone and hamburger fit snugly within 44x44. Not that I’m surprised — Google has great designers and this is a common consideration when designing for touch screens. But the news is even better when you consider that the tappable area is probably far wider on both icons, stretching to the edges of the canvas. Good for them! Hurray for generous tap targets!

But. Wait. This is interesting. The second “g” in the Google logo is hanging below the baseline thanks to its descender. Take a look above. See it? That’s proof that the text area is taller than 44 pixels. (or that the whole area is positioned about 9 pixels lower than the other two items)

What happens when a user taps the text field? Here’s the next screen:


Now let’s draw some guides to see how things line up when I overlay both screens on each other.


That looks pretty messy because I’m putting a line underneath everything. Let’s just concentrate on the Google logo that converts to the “Search” helper text:


Ok, let’s unpack this. The blue bar on the left is the standard iOS cursor, so we can anchor our size analysis there. The word “search” is vertically aligned to the middle of the cursor. Makes sense. But then the word Google is larger, rendered in a different typeface, and has that pesky descender.

Should they have abandoned the logo to make things line up more precisely? Should they have made the word “search” larger? Made the logo smaller? Repositioned the search text to hit the exact middle of the logo? Nope. I think the way they landed makes sense. How it looks mathematically doesn’t matter as much as how it feels, and it feels right.

(But even if it didn’t feel right to me, remember rules #1 and #2 of Design Explosions: assume the design team is smart, and that we can’t know all the tradeoffs that went into any one decision.)

So that’s the search box. Shall we move on?

The Hamburger

You’ve probably seen the hamburger everywhere. Here it is again, the far left icon on Google’s bar but nowhere to be found on Apple Maps.


There’s a lot of discussion about the hamburger, but I’ll just say Apple recommends against it (as does Microsoft, last I checked) and Google uses it a lot. What does it do? Here’s what happens when you tap it in Google Maps, which is similar to how it’s used in most Google apps:


This is the kind of interaction that inspires lots of passion. Some people think it’s brilliant, others think of it as a junk drawer and A Very Bad Idea That’s Currently Trendy Despite Being A Very Bad Idea PS It’s Bad. Let’s imagine we’re a Google designer and the team is split 50/50 on whether or not to use the hamburger. Let’s reverse engineer how they got here. We’ll start by recording everything it’s doing:

Switch Google profile
See “Your Places”
Explore nearby
Toggle traffic on/off
Switch to the transit map
Switch to the biking map
Switch to satellite view
Switch to terrain view
Launch Google Earth (or go to the App Store if not installed)
Settings (for the app, not the current trip you’re trying to take)
Help & Feedback
Tips and Tricks
Now let’s try bucketing the features into related options:

Personalization stuff
A bunch of presentation toggles
The ability to explore around you
A link to the Google Earth app
Miscellaneous
Speaking of toggles, several of them can be enabled at the same time. For example, here I’ve turned on traffic and the satellite map simultaneously:


So, back to our role playing. We’re pretending we’re a Google Designer assigned to think about this. To hamburger or not to hamburger?

First off, let’s assume that Google isn’t interested in losing core functionality. For example, Google Maps without transit or biking directions is a non-starter. It’s somewhere they’re differentiated and strong, so there’s no reason to remove it from the app.

We also need to keep all the toggles like terrain and traffic because that’s expected in any mapping app. In fact, let’s peek over at Apple for a second to compare. When you tap the information icon, you see this:


Ah ha! There’s the traffic toggle, there’s the terrain/satellite toggle, plus some other stuff that Google doesn’t have. (Drop a pin, a 3D map toggle, and a way to report a problem) So both mapping apps handled the important stuff with a drawer one tap away. Apple deploys theirs from the bottom, Google deploys theirs from the side (which means it can also be triggered with a left-to-right swipe gesture) So these are similar needs, addressed by similar features, displayed in similar ways. None of these features should be dropped, obviously.

So what’s left on the Google side? What does it have that Apple doesn’t? Some pretty powerful stuff, actually. Personalization features like profiles and saved places, plus the “Explore Near You,” which competes directly with folks like Yelp and Foursquare. And don’t forget the Google Earth link.

Could a designer waltz onto the Google design team and say “Hey, I think we should remove personalization from our maps?” They could try, but on what grounds? The features may not be used by 80% of people, but the people that use those features really love them.

What about “Explore Near You”? Could that be removed? Sure, I guess. But again, why? I’m as suspicious of feature bloat as anyone, but I don’t think it’s a stretch to imagine that when someone is in an map they may be looking to explore the area around them to find a good place for coffee. It makes total sense that it’d live in the app.

(Interesting aside: the Android version of the app puts “Explore Near You” right on the mapping canvas rather than tucking it away like on iOS.)

Which brings us to our last item in the list, the link to Google Earth. If this were an upsell to something unrelated (for example the Google+ app), it’d clearly be out of place. But going from Google Maps to Google Earth seems obvious. It doesn’t need to be there but it makes sense that it would be. It’d be hard to argue it away.

So Google is trying to do more, and Apple is trying to do less. I wouldn’t argue that Apple should try to pack in more features, and I wouldn’t argue that Google Maps should try to drop functionality we’ve come to expect. I think they’re playing each of their hands exactly right.

Onward!

By now we’ve thoroughly critiqued Google’s top bar. How many pixels it takes, the colors it uses, where it places things, all of it. Are we done? Did we miss anything in Apple’s version of the top bar? Indeed we did. Apple has two buttons we haven’t talked about yet, one for directions and one for share. Here they are:


Directions

We’re going to talk more about directions in a minute. But one detail I’d like to discuss is the “tapability” of it. A lot of people don’t even notice the icon, or think to tap it. Let’s think back to guideline #1, and assume the designers are smart, they’re aware that buttons should look like buttons, and they’re aware that people often miss this functionality as it’s currently designed. Where does that leave us?

Platform conventions, as we discussed earlier. Apple Maps could have made that icon into a big round circle, but it would clash with the rest of iOS 8. And clashing with the rest of the platform is unacceptable when you’re a built-in app that other software designers will look to for inspiration.

The share icon can get away with being more subtle than the directions icon, because it’s used all throughout the system. Over time people start to understand what it means, they understand that it’s tappable, and they know to look for it and what to expect from it.

But the directions button, despite being the same color, used in the same way, and aligned with the rest of the system, is only used in one place. Right there in Apple Maps. Which means people don’t acclimate to it as quickly.

I think they made the right call in aligning with the rest of the system, though they do end up with a less obvious directions button than they’d probably like. A natural tradeoff. Perfectly understandable, and better than if they had broken the system just to amp up a single icon.

(On product teams, PMs will often talk about the need to “celebrate” a feature to make it more obvious. It’s understandable, they’re judged on their work’s impact to the overall product. It’s hard to have restraint when you’re assigned vertically, which is why designers play a vital horizontal role. I often say “if we celebrate everything, nothing will be”.)

Share

Sharing holds a special place on mobile. Because copy and paste is so much harder than on a PC, mobile apps have had to build in simple ways to share images, links, and in this case, directions. Sure, you could make everyone drop a cursor in a text field, select all, tap copy, open a new app, find a new text field, drop the cursor, and tap paste, but using a standard share icon is more pleasant, consistent, and predictable.

So even if sharing a location is only used 5% of the time, or even less, there’s a strong rationale for putting it next to the text field. Doubly so since 1st party app designers need to set a good example. I could imagine the Apple Maps designers arguing for putting share in a sub-menu (or a hamburger!), but then realizing that they’re a first party app, so they should align with other 1st party apps like Safari and Photos.

So there it is on Apple Maps. But where did it go on Google Maps? I went looking and I found it several screens away. First you load a location, then you drag from the bottom of the screen to see the location page, then you tap the the vertical dots overflow menu to load a half-sheet that has “Share” on it, then the half-sheet gives you additional options like “Message” and “Mail”. Personally, I struggle to find it each time.

Once again, this is easy to respond to unfairly. A lot of people count the number of taps in a flow, and if there are too many, they think it’s bad design. The reality is often more nuanced. (in fact, there are a ton of designs that are needlessly complex because the designer was so committed to reducing taps — perhaps the most frequent example of cargo cult science I’ve seen in design)

Of course it shouldn’t take 100 taps to get something done. But it’s just as bad to have 100 items on screen at all times. Again, if everything is celebrated, nothing is. So the key is finding the right balance. Personally, I like to say “Two easy taps are better than one hard one”. It’s ok to hide lesser-used things one screen away. So where does that leave share?

To answer that question, we need to consider how important share is. It’s important, sure, but I wouldn’t call it vital. Then why does Apple put it right at the top, front and center? And Google doesn’t just hide it behind one screen, they offer less functionality in a much less discoverable location. Why?

I’d start by considering all the Apple hardware they’re trying to design for, from Macs to iPhones to iPads to Apple Watches. On most of them, “Share” is the way to do things like printing or interacting with third party apps. It’s almost like a right-click on Windows in terms of its ubiquity and utility.

On the other hand, Google can think about sharing in a fairly scoped way:
“Hm, someone might want to share a destination. Maybe we should put a share option in the location screen”. Whereas Apple may well be thinking in a far-reaching way: “We need to come up with a single affordance, one that can be understood across all screen sizes, multiple form factors, and scenarios. And it has to work across two major platforms. And it has to do a lot of heavy lifting.” Whew!

So not only does Apple want to set a good example by making share highly visible and highly predictable across an array of situations, I’d go further. I’d argue they can’t hide it. Because again, sharing (and printing, and working with other apps) might not be the most common thing you need to do with Maps, but it’s still really important when you consider it at a multi-device, multi-platform level. Important enough to take space on the main screen.

So back to Google, then. Let’s say Google’s design research team discovered that their implementation of “Share” was too hard for people to find. If you were the designer assigned to make it more prominent, where would you put it? Here’s the UI:


The top of the UI already has three tappable targets. The hamburger, the Google search field, and the microphone. It’s hard to see the benefit of cramming it in there.

After all, hamburgers are usually flush with the left side of the screen and microphones are flush with the right. Could we put it between the text field and the microphone? It’s best not to — as we discussed earlier, the text field and the microphone are paired. They belong together visually.

Then we have our two buttons on the bottom. One for centering the map, the other for directions. Could we put a third item there for share? Well, we could… but I wouldn’t recommend it.

More options means people slow down, especially when the buttons command this much attention. And is a share button really that important? For Apple, yes. For Google’s third party app on iOS, less so.

What about putting a share icon in the hamburger somewhere? It could work. After all, people often refer to it as a “junk drawer” so why not put it there? Let’s take another look at the deployed panel to take a look.


This is a great example of where hamburgers do well versus where they do poorly. They’re pretty great as a launching-off point for other places. After all, if you’re about to load Google Earth, it doesn’t matter if most of the screen is covered up.

But sharing location, by definition, is a contextual action. You’re picking a noun (Shake Shack in Manhattan) and trying to perform a verb on it (Share via SMS). So if context matters, it’s not great that the hamburger menu covers the whole screen.

Sure, you know you just typed “Shake Shack” but still, it’d be a bit clumsy to just say “Share” here on a mostly covered screen. Better would be to put share one screen away, on the actual location page itself.

And wouldn’t you know it … that’s exactly what Google did. Maybe they could pull it forward a screen or two, but it just doesn’t make sense for it to be on the first screen or a more prominant action than things like the phone number, the website, or a way to kick off directions.

Affordance Placement

Ok! We discussed all the various features that any mapping application needs to account for, plus called out places where Apple and Google designed differently based on their goals and direction.

But not every affordance is equally easy to reach or to use. Let’s analyze where they put everything. And for some context, let’s talk about what areas of the screen are easiest to reach. For a long time, people thought of “reach” on mobile devices very simply, like a game of Hungry Hungry Hippos. Things on the bottom are easy to reach. Things on the the top are hard, as shown on the left diagram:


The visual on the right approximates the “thumb zone” for a right-handed person
But the reality is more tricky. We don’t grasp our phones uniformly across the bottom of the unit. We grasp them, usually with our dominant hand, across the middle. And our thumb works in a bit of a windshield wiper motion, which creates a pattern more like what’s shown on the right.

At first glance, it looks like the diagram is for a left-handed person, since there’s more dark blue on the left. But your right thumb has a harder time hitting the bottom right of your phone than the bottom left. The right forces more of a scrunching motion because it’s more under your palm.

So how did Apple and Google do? What did they place where?


It goes without saying that this “thumb zone” is different for everyone. If you have a larger phone, or smaller hands, or grip your phone differently, or are left handed, your reach will be affected. But these graphics represent a reasonable rule of thumb. (Har har.)

Notice the bottom right corner. I consider this a magical spot on the phone because users won’t accidentally tap it, but it’s highly discoverable. And just because it’s harder to tap relative to the center of the phone doesn’t mean it’s actually hard in absolute terms. This means this location is easy to tap, harder to tap by accident, and hard to miss. So what do Google and Apple place there?

Google uses this spot for their “Load directions” icon and Apple puts their Information/Settings icon there. Pretty different approaches! But let’s look closer. If we look to the top left (another magic place that’s a reach but highly visible) we see Google’s hamburger and Apple’s “Load directions” icon. They’ve mirrored each other almost exactly. Let’s take a look:

Information/Settings



As we discussed earlier, this is where Apple lets you turn traffic on and off, change to a satellite view, and so on. It’s accessible by tapping the little i icon and it loads a menu from the bottom of the screen like this.

And of course here’s Google’s hamburger. It does a lot more by design, so it benefits from taking up almost the entire page.

But the key takeaway here is that these are screens refer back to what I call “vital edgecases” — you probably won’t use them very often, but when you do want them, you want to find them with as little friction as possible.

Though I wonder about that “Explore Nearby” option. It’s practically an app within an app, I could imagine some people getting a lot of value from it. But buried in the hamburger like this probably harms its discoverability. Before we mentioned how Android puts it right on the map canvas, which is harder to miss. Maybe there’s a third place for it?

Oh, here we go. When you type directions, it shows things near you. That way they can feature helpful stuff without taking up chrome on the main mapping canvas. This is clever and obvious in hindsight:


Plus, the keyboard doesn’t appear right away, it animates in from the bottom. This means they’re a split second where you can see more of the list before it gets covered by the keyboard. A great example of how a screenshot only tells one glimpse of the story, whereas smart use of animation can help make a design more clear.

Ok, what next? Let’s talk about another very important feature: centering the map on your current location.

Centering the map on your current location

Mapping is a pretty complex experience, and it’s easy for the map to be focused on the wrong place. So having the ability to center the map in one tap is vitally important. Let’s see what Google and Apple do here:


Google has placed it as its second “FAB” (floating action button”) at the bottom right. Hard to miss, easy to tap.

Apple has placed it at the bottom left of their screen. Icons at the bottom left are often seen first (in languages that read from left to right) and Apple has used their standard location icon to make it as clear as possible. You see this visual everywhere — in settings, in apps, and if you look at the top right of this screenshot, you see it next to the Bluetooth and rotation lock icons. By using it this way, users are more easily able to understand what it does and what to expect from it.

But wait, what about Google’s design? They’re not using the standard location icon. Why not?

Again, let’s do some role playing. Let’s say you’re new to the Google design team. And to make things even more interesting, let’s say you were hired away from the design team at Apple that worked on iOS Maps. You might say “Hey team! I was wondering, why not use the Apple-recommended location icon?” It’s a good question — but it’s worth remembering that there’s no single “right answer” here, only tradeoffs.

Apple has standardized horizontally across all their products, from iPods to iPhones to iPads to Macs. But Google has a much more complicated alignment story, because their products don’t just span hardware, they span different operating systems. Windows, iOS, Android, Chrome, Web, Blackberry, and Windows Phone to name a few. So that means they’re dealt very different hands, ones that play out very differently.

If Google aligns with Apple’s location icon on iOS, that’s one exception to their visual language. If they align with Windows Phone’s, that’s a second exception. But also remember you can access the web on iOS and Windows Phone. So should they change their web products to sniff out the OS they’re on and change the assets on the fly? They could, but it sounds like a lot of work.

Another issue is that Google’s “center the map on my location” graphic, the crosshairs icon, is pretty good. People know it well. I’m not sure when it first appeared, but I’m pretty sure it’s been around much longer than Apple’s location icon.

So there are a lot of reasons not to align with Apple’s location icon, and only a few reasons to align. I’m not surprised they landed where they did.

Entering Directions

Entering directions! This is the core functionality for the app, the 80% use case. An app that doesn’t get this part right might as well not exist, and everything we’ve talked about before (all six thousand words or so) is just setting the stage for this flow. Let’s dive in.

(Again, to reiterate, I’m not going to get into the quality of the mapping data stored in the cloud. By all accounts, Google Maps has the edge here, but we’re going to focus on the things that a design team can control themselves.)

It turns out this section is surprisingly complicated to analyze. I can’t just say “screen X goes to screen Y,” because both Google and Apple have chosen two different entry points for entering maps! Here I’ve called out the text field option with a (1) and the button with a (2):


We’ve already explained the differences in visuals and placement. But actually, when you tap though both entry points, both Google and Apple are doing pretty much the same flow for both (1) and (2). But, as always, the slight differences contain a bunch of interesting things to learn.

Golden Path #1 (the text field)

Let’s draw some boxes and arrows to understand what happens when you tap on the text field. First Google, then Apple.


Google Maps flow: searching for a location to the en route screen (low fidelity)

Apple Maps flow: searching for a location to the en route screen (low fidelity)
Ok, it looks pretty abstract like that. But the big takeaway is that they’re doing the same thing, in the same order, but Apple uses five screens and Google uses six. But, again, fewer steps doesn’t necessarily mean better! We’ll analyze those screens in a moment. For now, let’s show everything at full fidelity before we dive in.


Google Maps flow: searching for a location to the en route screen

Apple Maps flow: searching for a location to the en route screen
Ok, let’s take it screen by screen and look for big differences:


Google left, Apple right
Screen 1: They start pretty similar. Google has a much greater emphasis on your favorite places and Yelp-like suggestions, but otherwise it’s a text field that’s showing suggestions based on your search history. What you’d expect.


Google left, Apple right
Screen 2: Here, the auto-suggest region no longer has to guess, now it can just do a search based on what you typed so far. A standard typeahead pattern, found in lots of apps and on the web.

[Note: I accidentally dismissed the keyboard on the Google example but not the Apple example.](#)


Google left, Apple right
Screen 3: Functionally, these screens are very similar. They’re showing your chosen destination and waiting for you to signal that you’re ready to route this trip.


Google left, Apple right
Screen 4: The plot thickens. This screen is unique to Google. Apple skips right to the next screen, whereas Google lets you choose from a range of options.

[Note: sometimes Google lets you kick off the directions process from here, sometimes it doesn’t. I haven’t been able to figure out the difference. But when that happens, the user jumps to screen six rather than having to tap through screen five.](#)


Google left, Apple right
Screen 5: Once again, these screens are functionally similar. The path has been routed and now all that’s left is committing to the route. But notice that Apple’s screen is showing multiple variations. It turns out Google Maps does the same thing with those grey lines, they just don’t call them out with labels.


Google left, Apple right
And then the en route screen, which is its own ball of wax. That’ll get its own section later in the issue.

What have we learned? Well, both applications have the same general flow. But what about those screens in the middle? Let’s analyze them in isolation. Here’s Google Maps, followed by Apple Maps:


Google is very consistent with the Material guidelines here. On screens 1 and 3, there’s a giant blue button leading you forward in the flow.

Of course screen 2 is a list of options, so a single call to action wouldn’t make sense.


Now look at Apple’s screen 3. Their call to action is the word “Start” at the bottom of the screen. While a lot more subtle, this also is a faithful execution of platform guidelines.

But what about that big blank step? What’s that about?

Explaining Apple’s Missing Step

In Google Maps, that second screen does a lot for the user. The focal point of the screen is on the list, particularly on what Google is predicting as the best option. But there’s also a mode switcher across the top so you can choose between car, public transit, walking, and biking. And a way to enter your starting and stopping point. And a way to reverse the order. Plus an “Options” button, and if you’re taking public transit there are yet more options for departure and arrival time. Whew! That’s a lot of options.

Apple offers none of these things.

But like the other screens we’ve looked at, I wouldn’t be so quick to say Apple is too featured limited or Google is too feature rich. I’d argue that both companies are playing to their strengths and these screens reflect that.

Apple doesn’t have public transit or biking information. Maybe they’ll add it one day. But for today, that means their flow can be a lot simpler. The single feature they can match with Google is “Choose alternate paths, including walking”. So Apple featured it on their third screen with a label rather than using more complex and heavyweight controls.

The “Dropped Pin” Screen

With screens 2 and 3 addressed, I’d like to turn your attention to the first screen, especially to focus on the call to action.


On the Google side, we have their FAB, or “Floating Action Button”. Easy to know what to press. On the Apple side, we have a more vague call to action. I’ve spoken to people who get confused here. Some don’t know to tap on the little car icon. Others don’t know you can tap the “directions” icon on the top left. I think a fair number of people tap into the place page itself, then tap “Directions to Here”.

Rule #1 reminds us that these designers are smart. They’ve probably seen people struggle with this screen. So what’s going on here? I think it’s an issue of enforced consistency with the iOS guidelines, and how they probably didn’t want to make a custom control for this screen.

Look on the bottom and top chrome elements. Totally standard. Now look at what Google did — they placed the name of the place on the bottom of the screen, underneath their FAB. It’s a look that may match Google’s other products, but on iOS it’s out of place. And that’s perfectly fine for Google, but it’s not fine for 1st party Apple designers.

So Apple went with a contextual menu with two buttons. Tap the car to get directions, or tap the name of the location to read reviews, see photos, and a bunch of other options. I’m not sure if it’s a truly common control, but it is very reminiscent of the standard “long press” present in iOS since 2007. This is not an accident.

Regarding Common Controls


Designers refer to “common controls” as the standard OS provided options. Think dropdowns, the contextual menu shown here, buttons, grids of images, and so on.

Whereas “custom controls” are so much more fun to use. You get to design something! From scratch! And it can do exactly what you want, with no compromises! But it’s often a mistake.

You can compare it to code, actually. It’s a rookie mistake for an engineer to write a bunch of code by themselves, whereas a veteran developer has learned how to be lazy and get code “for free”.

This is done by using other people’s code and knowing how to tie it together effectively. It turns out design is pretty similar. New designers often want to reinvent the wheel because it’s fun to try designing new things. But veteran designers know how to use the common controls in clever ways to achieve their goals without taking on too much custom work.

Take the color wheel. On the Mac, there’s a standard color wheel that most apps use because it has a lot of powerful features. Windows also has a standard color wheel, but it’s far less powerful. Which means developers keep designing new color wheels, especially Microsoft Office. Which means customers have to learn how to use lots of different color wheels, which slows down learning across the system.

The same thing happened with printing. Most people want to hit a standard keyboard shortcut and have everything work. It’s pretty standardized on Mac OS X, but also includes the ability for developers to add functionality. Whereas on the Windows side it works in some places, not in others, Office does its own thing entirely, and so printing on Windows requires more effort. It’s not uncommon to re-learn behaviors as you move from app to app, and that’s not a great place to be.

As a UX designer, you will often write custom controls. But it should never be your first inclination. Your design and your product needs to truly earn the right to be different. If a custom control will make your app 15% better, that’s probably not good enough. You should be holding out for 50%.

So that’s my best guess about what happened with Apple Maps. Their call to action isn’t as clear, but I wouldn’t say it’s 50% worse. I wouldn’t say it’s worth writing a whole new custom control to maintain and for users to have to learn. I often like to say “it’s not very discoverable, but it is learnable”. I think Apple Maps is in a similar boat here — once you realize you can tap the car, things make a lot more sense.

But this is a scenario where Material’s super-obvious call to actions (FABs) really shine. All without being custom to a single app. That’s no small feat, and kudos to their design team for working towards something more standardized and predictable.

Golden Path #2 (the button)

It turns out that there’s another way to trigger directions in both apps. Wow. We just analyzed what happens when you tap the text field. But what happens when you tap the big FAB on Google Maps or the subtle directions button on the top left corner of Apple Maps?

Let’s compare. Here’s Google Maps, followed by Apple Maps:


Google Maps

Apple Maps
The last two screens are the same, and we already talked about why Apple is missing the third one. So we’ll be focusing on the first two screens.

But there’s another thing to discuss first. Why did Apple and Google both make two ways to do the same thing? Typically it’s a bad idea to let users do the same action is two different ways. Users don’t logically memorize how your app works. If something is complicated, they just keep trying until something works. And if you provide two paths to the same thing, you’ve thrown a big roadblock in the way of the user remembering how to use your app. So what are Google and Apple doing?

Let’s take a look by comparing what happens when you tap the “directions” button (shown in the first screen) versus tapping on the text field (shown in the second screen) in Google Maps:



You can spot the difference even while squinting. See that Google blue? It happens right away on the first example but at the end of the flow on the second example. And what does that screen let you do? Everything. You can change the starting location and the destination, reverse their order, and chose different forms of transit. Google passes you through this screen in both flows, but in different orders. Huh! Puzzling.

What did Apple do? The same thing, just different.



Once again, tapping the “directions” button changes the flow from three screens to two. Once again, they drop the user directly into a screen that assumes your starting location is where you’re standing. Once again the suggestions switch from general guesses to a search-while-you-type pattern.

Whereas tapping the text field doesn’t bother letting you monkey with the starting location right away. Just like Google’s example. Though in the Apple example you have to dig further to complete that task, whereas Google uses that screen with the blue header every time.

So why the differences? What’d we learn?

All of my design training has taught me to remove things like this. When you’re doing essentially the same thing from two entry points, you should do everything in your power to combine them. And I’m sure they tried and found it just wasn’t possible.

Recall, yet again, rule #1 of Design Explosions: assume the design team is smart. So let’s reverse engineer how they got here with that as our starting assumption.

First, if they combined the text field and the button, which affordance should they drop? You could get rid of the text field and rely on the button. But in Apple Maps it’d be far too subtle. Maybe Google Maps could get away with it? Ok, what about the other way around — maybe you get rid of the button but leave the text field? Apple could, probably, but Google’s got their FAB pattern to worry about. Even if the designers on Google Maps wanted to go in that direction, it wouldn’t align well enough with Material design.

So it’d probably be possible, in theory. But let’s go a bit further. Golden Path #1, the one that goes through the text field, is best at looking for stuff without turning everything into directions. Golden Path #2, the one that goes through the button, is better at getting you directions as soon as possible. To use a Google metaphor, the text field is like using Google’s search results pages, whereas the button is a bit more like the “I’m Feeling Lucky” button.

If we were to combine the two, we’d need to somehow serve both the searching and the directions scenarios equally well. But as a screen tries to do more, it gets harder to use and less focused. I’m sure it’d be possible, and it could probably even be done well. But when two strong design teams come to the exact same conclusion, take notice.

En Route

Here’s where you spend most of your time. And it’s not just about the total time spent here, it’s also the fact that you’re using this part of the app in a distracted way. Most of us just want to get to our destination with the absolute minimum amount of crashing. So it’s important to design the screen in a “slippy” way, as opposed to sticky, as my friend Jake Zukowski says. The visual design of this screen matters a whole lot, so let’s see them both at high resolution:


Let’s strip them down to silhouettes to see how each of the screens look at low resolution. At first we should focus on how much canvas the screens are taking up. First Google, then Apple:


Google Maps

Pretty different silhouettes this time! Let’s look at them side-by-side:


Clearly Apple is optimizing its real estate here. All right angles, no OS bar across the top for cell reception, wifi, time, battery, etc. But how much can Apple really fit into such a small amount of chrome? Let’s count the number of affordances in each. Google on left, Apple on right:


Whoa. What?

In Apple Maps, no matter where you tap on the screen, the same thing happens. It’s one giant tappable target. Whereas Google Maps has a more traditional design that features seven different touch targets. And that’s not including the mapping canvas itself — on both apps, dragging the map lets you “peek”. (In Apple Maps, it’s temporary and the map resets when you complete the drag. In Google Maps, you can drag as far as you want and you’re brought back with a “RESUME” button.)

That’s a pretty big difference. What’s going on here? Either Google has been overrun with feature creep or Apple dropped core functionality. What other option could there be when you have such a huge differential? Well, Apple’s doing something a bit tricky. Tapping anywhere goes to this secondary screen. That’s where they’ve put all their functionality.


See, now it’s a lot more like Google. The top bar is there so you can check battery life and time, plus a button to cancel the trip, overview and text directions, as well as a toggle for 3D mapping and volume controls. Let’s compare Google’s single screen with Apple’s two so we can see what we’re dealing with:


Left: Google Maps. Center: Apple Maps default en route screen. Right: Apple Maps after a tap.
There’s a ton to learn here, but first I’d like to tell a story about how I used to play football video games. I’d blitz. Every time. In every situation. 1st down. 3rd down. Long distance, short distance, blitz blitz blitz!

No one ever explained the tradeoffs to me. See, blitzing optimizes for one thing (hurrying the quarterback) by de-emphasizing another (having enough people on the rest of the field to handle a pass effectively).

You don’t need to understand sports to grasp the concept of a tradeoff. Put three men here and that means you can’t put them in this other place. Buy a cheap item and you’ll replace it more frequently. Buy an expensive item and you’ll run out of money faster. Everything is a tradeoff. If you ever only do one thing, you’re missing all the nuance, like 10 year old me blitzing on every play.

So let’s look back to the en route screen with tradeoffs fresh in our mind.


I love this comparison. Google is optimizing for driving because everything is one tap away. Want to cancel the trip because you’re looking for parking? One tap. Want to figure out how to turn on the traffic map? One tap. Want to re-orient the map to the direction you’re facing? One tap. It’s a very flat system where everything is right there, even things like seeing what time it is or checking to make sure your battery is ok.

Apple is optimizing for driving because it’s tucking everything away. There’s far more canvas available to show the map. When you drag the screen with your finger, it snaps back into place rather than putting you in another mode. It doesn’t show the current time, but it does tell you how many more minutes you’ll be driving, and your estimated time of arrival. Apple is doing what Apple does, for better or worse. They’re cutting as close to the bone as they possibly can. Nothing is assumed to be necessary on this screen.

In this case, I’d argue Apple is blitzing. It’s not “right” or “wrong” but I would call it high risk with high reward. We’d need more data to know how successful it is in the real world. Maybe it’s worth it! Maybe not!

But even without testing, we can think through the tradeoffs. In that split second while driving when you need to get to the cancel button, or see the actual time, or change the volume, Apple’s design takes one more tap. And sometimes that can be frustrating.

On the other hand, Google Maps is running up against Hick’s Law which can be summed up as “options slow people down”. Sure, the available options are all a single tap away. But there are a lot of them.

It’s a wonderful example of tradeoffs. They’re both right. You can prefer one, you can argue why one is worse, but I’d prefer that you simply admire two solid answers to a very complex design problem.

(Personally, I like to say “two easy taps is better than one hard one”. So I’d probably be inclined to come up with something closer to what Apple has done. But again, that doesn’t make it “right”. I’ve had these debates at my job dozens of times. People I work with sometimes don’t understand why I like to tuck things away into a second screen. And it’s a great discussion to have! But I could just as easily argue the other side. It just depends on the goals for your app.)

The En Route Lock Screen

We could get into a bunch of little side discussions about the En Route screen, but I’d like to focus on a big one, Apple’s customized lock screen. Here it is on the left, with the standard en route screen on the right for comparison:


There are just a few differences here. The lock screen shows the full OS status bar across the top, a simplified map canvas with no labels, and the standard bottom area with “slide to unlock,” access to the control center, and a camera entry point.

This lock screen is a bit risky. While I’m driving, I’m not thinking clearly, I’m not tracking what screen I’m in or the status of the lock screen timeout. But the lock screen puts a password between me and what I’m trying to do. (My iPhone doesn’t have Touch ID) So that’s a bummer.

Let’s put ourselves in the designers’ shoes and review their options.

Don’t use the lock screen at all. This means you see your default lock screen wallpaper (a polar bear, let’s say), then after a swipe you type your password, then once you get through all that, you finally see where you are on the map. That’s not a ton better than putting on the lock screen itself. It’s more clear that you need to swipe to unlock, but it leaves you in the dark while you’re driving and you just want to see the next turn.
Ignore the lock screen timeout. I’ve been on a design team working on lock screen/CTO corporate policy politics, and it’s intense. Enterprise customers want to know that their phones will auto-lock if they’re company property. Meaning they’re not interested in exceptions to their strict lock screen policy. You can’t just ignore CTOs when you’re courting their business and trying to prove that your product will work with their proprietary data. Maybe somone in the company can lobby them, but that’s beyond the scope of a designer.* So this option isn’t really on the table.
Use the lockscreen as it exists today. Maybe tweak some of the visuals, but essentially keep the design flow the same.
The way I read it, #1 makes the product harder to use while driving and #2 breaks contractual obligations. So I’m not surprised they landed on #3, even if it’s imperfect.

*This was the beauty and power of someone like Steve Jobs — I get the feeling he’d call up some key CTOs personally to explain this somewhat esoteric but tangible user pain, and say “we’d like to make an exception for when the user is en route. If the car is moving, and a destination has been entered, and we know it’s 20 minutes away, fuck the lock screen for 20 minutes. What do you say?”

The en route lock screen, while I do struggle with it sometimes, is a great example of the limitations of design. #2 might be the best overall option, but shipping it has nothing to do with pixels. It’s all about relationships, and politics, and legal teams, and so forth. This is why it’s important to have design courage at an organizational level. Otherwise you can only get so far with your designs before the best ideas are overruled by legal, executives, and your partners. Effective designers have effective relationships.

Summary

If you’re like me, you look at at massively long post like this, especially one comparing two products, and you scroll to the end. You want the summary. If I came across this post, I’d say “Dude, I have 907 tabs open and no time to spare. Can’t you just give me the answer? Which product is better? It’s Google Maps, right?”

But this is a different kind of post. There’s no winner this time, just a bunch of design lessons inspired from studying two similar products. It’s fun to compare and contrast the approaches of two highly skilled teams working on a pretty unique design challenge. That’s huge, if you’re looking to learn.

Design is simply “deciding how a thing should be,” as William says in his book. But every decision has a tradeoff. Every thing you make easier can make another thing harder. There is no such thing as “the right design”. But we can learn a lot from seeing how experts in their field weighed the pros and cons of different approaches.

And with that, we’re done! William and I hope you enjoyed the first issue of Design Explosions. Also, we hope you have a very nice day.

The Commerce Section

We teach a one day, hands-on, fun design course in Seattle called UX Launchpad. If you want to learn more about design, we’d love to help you out. We have three classes:

Introduction to UX Design, for everyone
Design While Shipping, which helps working designers be more effective
The Future of Design, a crash course in new tools like Sketch and Pixate
We hope to see you in Seattle soon!

By UX Launchpad on December 1, 2014.

Exported from Medium on May 19, 2017.

Slow Epiphanies

UX Launchpad was dreamed up about 18–24 months ago. As part of that early swirl of excitement, I remember pitching the business plan to my wife many times. A lot of details shifted, but I had the same three bullet points from day one. They are:

Be awesome
Tell people we’re awesome
Pay overhead costs
You could also describe them this way:

Product
Marketing
Ops/management
It seems so obvious written out that way, right? But the big innovation in my mind was that I could actually imagine making money at this! I have no idea how anyone starts a drug store, opens an Arby’s franchise, or turns a profit with apps ($1) while still paying a reasonable dev salary ($7 jillion megabux). But asking people to pay me to teach them? I could do that! And so I did. I kept costs low, taught some classes, and UX Launchpad turned a profit pretty quickly. We were off!

Last week I had another obvious insight. I wish it had occurred to me earlier, but I guess it’s better late than never. Here it is:

When William and I decide to host a class, I send $500 to Sole Repair, the event space we love to use. At that point we’re $500 in the red. Then we spend money on advertising, which puts us deeper in the red. This is stressful when we haven’t yet sold any tickets. Wouldn’t it be cool if we didn’t have to put down the $500 deposit?

Eureka!

If we can host a class without a deposit, we can plan out the whole year with no risk and no overhead. So I asked Sole Repair if we can work out a no deposit situation. They said yes. I danced a silly jig in my basement and booked six rentals from now to the summer.

It’s such a simple idea. And it’s going to make a huge difference. Hurray for epiphanies even if they take a while to figure out.

We now have three different classes on offer, and more to come. Check out the details on UXLaunchpad.com if you’d like to learn more.

By UX Launchpad on December 31, 2014.

Exported from Medium on May 19, 2017.

So Much To Say; Enjoy This Placeholder

Hello! It’s been a while.

Over the years I’ve heard a lot of insights about running a company. They take lots of time. You need to marshal your energies. Learn to delegate. Prioritize effectively. There’s just too much to do, and you can’t do it all.

And it’s true — this year is shaping up to be a big deal for the company, and the bigger it gets the harder it is to find time to write. But writing isn’t a luxury any more than changing the oil in your car. When I write, I think better. When I think better, I run my company better. So.

I have too much new information to share at once, so I’ll throw out some tidbits that I’ll explain more about at a later date.

I’ll be teaching the classes by myself. William is focusing on the book and we decided I’ll focus on the teaching.
We’re adding another class soon, bringing the total to four. And it won’t be taught by me! We have a new instructor joining UX Launchpad. She has some great ideas and experience. It’s gonna be amazing.
Our “Design While Shipping” class is going to include a design research segment to see how well it fits. It’ll be run by another new face at UX Launchpad.
Money, cash flow, and a noticing general sense of velocity has been on my mind recently. I’ve got a lot to say on this topic.
Issue #2 of Design Explosions is coming soon. It’s been pretty fun to write so far.
I like the term “a good problem to have.” A lot is going on! I don’t have enough time to write! I now have 1000 people following the Design Explosions collection here on Medium! My upcoming classes are sold out and I’m having to book further and further in advance! These are all problems, sure, but they’re such great problems to have.

By UX Launchpad on February 22, 2015.

Exported from Medium on May 19, 2017.

Design Explosions #2

PBS Kids

Hullo! Design Explosions is a series by UX Launchpad, a company that teaches hands-on design classes in Seattle. Issue #1 compared Apple Maps and Google Maps, and today we’ll be learning from PBS Kids, an iOS app.

If you’re new to Design Explosions, you can learn more in this introductory post. Here’s the gist: Design Explosions tries to do design critique in a polite and gracious way. Here are our guidelines while analyzing designs:

The team that built this product is full of smart people.
There are many things we can’t know without joining the team.
We’re here to teach and learn, not judge.
Ready, set, YELLOW PBS KIDS SPLASH SCREEN


Scenarios & Mantras

Product folks talk a lot about scenarios, but there’s some confusion about what the word really means. Personally, I use the word to describe a common flow while using the product. So Amazon’s core scenarios are searching for a product, reading about it, and completing your order.

But sometimes people get a bit too fancy with scenarios. It’s not uncommon to spot scenario descriptions like “Anna wakes up in the morning! What a lovely day! She decides she wants to sync her Exchange work calendar with her family’s team calendar so she taps the calendar app, taps the overflow menu, then goes into settings. Whee! While drinking coffee and smiling, she simply taps these four steps and then she’s done. Great! Now she has plenty of time to take Barky to the dog park. Thanks, Windows and Azure! The PM who wrote this really deserves a promotion, don’t you think? I do!”

That’s not a scenario. That’s a pitch.

Guy Kawasaki uses mantras when trying to figure out what a company stands for. The trick is to remove the features and fluff to see what’s left. He proposes that Wendy’s could be “Healthy Fast Food.” March of Dimes could be “Save Babies.” And PBS Kids? It’s practically all in the title. “PBS for Kids.” But blink and you’ll miss it. Those three words can provide a ton of directional guidance while designing the app and making tradeoffs.

Clear visions like this are extremely useful when you’re in the middle of a long and complex project. Great design is about saying no, and principles help you understand what your team stands for when tough issues come up.

It sounds obvious, but in practice it’s incredibly difficult to keep an app this focused. Especially as years go by and features are added, something PBS Kids so far has avoided. It reminds me of a sticky note I post often:

Q: How does an app become bloated?
A: One reasonable feature request at a time.

If you’re a professional product designer, you can probably relate. Perhaps bitterly. It can happen to every product, on every team, at every company. So how did the PBS Kids team do it? How did they structure this thing? Explaining why their solutions are so clever requires some background knowledge. Travel with me to the turn of the century.

Search Versus Browse

About 15 years ago, my team at RealNetworks was tasked with designing an online video store. This was before iTunes, Hulu, YouTube, and Netflix so we didn’t have a lot of examples to learn from.

I held an early opinion that we should design the entire store around a search box. No marketplace, no genres to browse, just a big Google-like search. It was an awful idea because that’s not actually how most people consume media. We quickly learned that they don’t search, they browse. But it turns out that assertion is only half right.

They expect search too, something we learned the hard way when we removed ours entirely. Let me repeat for emphasis: we launched a video store with no search. By 2015 standards that sounds crazy, but we only had the rights to 100 movies. Which meant almost any search resulted in an embarrassing “zero results found” page. It seemed like removing the search box was the best answer. (It was a simpler but far more frustrating time for media on the web, as a designer and a consumer.)

I’ve since learned a lot about search versus browse, and so has the software industry at large. We’ve learned that you need both. If you remove the search box, it’s too limiting. You only see what’s in front of you (typically in the form of ads), so much of the content is hidden. But a spartan search field, with no browse experience, feels too broad. Many people get writer’s block when they come across a text field asks them to type in a tv show, album, or movie name.

This is why you see the same pattern most places you consume media — a big grid of suggestions accompanied by an obvious search box in the corner. But what happens when you click or tap on some media? You’re introduced to one one of the most common design patterns: hub and spoke. Like a bike.

Hub & Spoke


Credit: http://www.biascagne-cicli.it/bici/bici-scatto-fisso-in-vendita/
Imagine a bike wheel. Wait, don’t imagine it, I’ll just throw a picture up instead with this handy embed feature. (I knew my Multimedia degree would come in handy!) Notice how there’s There’s one “hub” and a bunch of “spokes” coming off it. Many apps use a similar pattern.

The “hub” is a menu of options, and each “spoke” shows each one in greater detail. For example, a list of emails, SMSes, or restaurants on Yelp.

Here’s a small sample. Note the list (hub) on the left, then the details (spoke) on the right. It makes a lot of sense for a lot of apps.


Example of the hub & spoke pattern on Yelp, iMessage, and iPhone’s Mail. List on the left, object on the right
You can see how simple the approach is by looking at a visual representation of it. For now we’ll refer to it as “unmodified hub & spoke.”


As common as this approach is, it doesn’t really work for complex media apps like Hulu and Netflix. Why not? It’s pretty easy to spot why if you look at a basic wireframe of how a media app might work. Look at this:


We’ve got a list on the left, and the video player on the right. But this means every episode would load automatically on tap, with no way to read reviews or read a description. That’s not a good experience.

And auto-loading isn’t even the biggest problem. This model forces every episode into one giant list. But that’s not how people browse TV. They first find a show they like, then pick an episode. Something like this:


Ok, now we’re getting somewhere. But even this is starting to get pretty complicated for our poor PBS Kids viewer. Let’s take a look at a handy visual representation of how our slightly modified hub & spoke is looking:


Getting complex, Gus.
So now we’ve got a central hub for showing all TV shows. Then you tap into a TV show and you’re shown every episode available. But this doesn’t tell the whole story either. We also need to break things into seasons. So let’s try that. How much more complicated can it get? A lot. Behold:


I don’t even.
Hm, that’s getting crazy complicated. (But kind of pretty too. I want to make a t-shirt out of this.) So now imagine you’re a toddler who has navigated to your favorite episode of Sid the Science Kid and now you want to go watch Clifford the Big Red Dog. If the designers don’t modify the experience from a standard multi-level hub & spoke pattern, here’s the path our poor four-year-old kid is going to have to take:


So if you’ve ever been frustrated with software in your car, your stereo, your television, or anywhere that feels a bit deep hierarchically, you’re probably traversing an experience like this. But is there anything we can do? Isn’t this just something humans have to deal with? Kids are going to have to learn this eventually, right? Well, about that…

Kids: Illiterate, Impatient

Kids can’t always read. Plus their fine motor skills aren’t that great, so tiny tap targets aren’t particularly useful either. So where does that leave us? It means that a seemingly simple interaction like this:


… isn’t as easy for them to figure out. This design isn’t bad per se, and I understand why everything is here. But there is some complexity. And this is not unique to kids! No one reads. Everyone just wants to complete their task as quickly as possible. These are universal truths of software design.

Sure, kids might throw more tantrums than adults, but everyone can benefit from great design. And kids are more likely to struggle with something too complex (without blaming themselves!), which makes them fascinating and incredibly enlightening to design for.

So how did Netflix do? What problems are they trying to solve with this design? Let’s break it down to see how the page is structured.


Ah, so Netflix is combining items that go down one level (the arrows) with things that jump down two or more levels to auto-play media (the play buttons). It’s a clever way to have your cake and eat it too, and they’re doing it carefully and conservatively to make sure it works.

The play buttons on the first page are limited to things you were halfway done watching, whereas everything else goes to a details page. This is smart, because they’re differentiated clearly both in visual details and in terms of common sense expectations. You’re not surprised when a halfway watched episode of Cheers starts playing when you tap it, whereas tapping a giant House of Cards banner feels like it should go to a details page.

And then there’s the main video player page, which looks like this:


Again, this is not bad work. But imagine if you don’t know what an X does, or what an arrow does, and you can’t read. Before a person gets acclimated to what everything does in an app, the screen might as well look like this:


There are some exceptions that everyone understands, notably back arrows and “X” close buttons. But other than a small handful of very common icons, most are hard to understand and remember if they’re not accompanied by clear text. So that’s important to keep in mind while designing for everyone, but doubly so for kids.

Guess what. We’re done with background, and I’m pleased to report:

Now It’s Finally Time To Explode Some (PBS) Kids!

There are plenty of other issues to design for when designing video apps for kids, but we’ll stop there. Just being aware of the hub and spoke model gets us pretty far. Let’s take a look at the finished product so we know what we’re going to be exploding:


Now let’s break things out into regions. There are only four this time:

Video playback
Video controls
Parent Settings
Browse

1: Video playback 2: Video controls 3: Parent Settings 4: Browse
Like most things, it seems obvious when it’s written out in a plain list. But in fact, simple is extremely hard. It takes a lot of work to tuck everything away into these four easy-to-understand sections. Let’s go section by section to see what we can learn.

Video Playback


The first thing they’ve done is break free from the inherent drawbacks of the standard hub & spoke model. How? By putting the video and the browse section on the screen at the same time, every time you load the app. And every time you come out of full screen mode. The implications of this are huge. Which is why we spent so much time analyzing the model first, so you can have a full appreciation of the complexity they dodged with this move.

Let’s compare two visualizations of the models. On the left I’ve put the hierarchical hub & spoke model. Notice that there’s a single place to start, x number of shows, y number of seasons per show, and n numbers of episodes per season. On the right you can see what PBS kids did. Whaaaaaaaaaa—


There’s a lot of goodness here. First, PBS Kids immediately starts playing video. And when the first clip is over, another loads. And another. You can watch for hours with zero taps, which is pretty clever. Ima let you finish, but this might be the simplest media app of all time. OF ALL TIME.

Second, tapping any of the options on the side takes you straight to an episode. They don’t bother with seasons and episode selection at all, which means getting to your favorite show takes one tap. And once the first episode is over, the next one loads up right away.

The third great thing is letting you browse for your next thing while your current thing is playing. And this isn’t just a big deal for kids, this is a core scenario for anyone consuming content. More and more examples are springing up, from Rdio to iTunes to Netflix to Hulu to YouTube and (hot off the press) YouTube Kids.

(Shout-out to the YouTube Kids team — great app, my kids love it, and you made me scramble to fit you into this issue of Design Explosions. Kudos!)

Here’s YouTube Kids with their implementation:


And here’s what YouTube does. Notice here I’m watching a video, but I can tuck it away while I look for other stuff, and then bring it back up when I’m ready.


Watching video while looking for your next video is a pretty big deal for apps like this. Children of the 80's and 90's might remember “picture in picture” functionality in some fancy TVs. But these days it’s just a requirement for media playback apps. You have to be able to search and browse while watching, or it feels needlessly clunky to use.

A Note on Assumptions and Demographics

The model I’m celebrating is making an important tradeoff. They think the simplified design is worth it even if it means videos auto-play and you can’t pick a precise episode.

But I’d wager that the majority of adults would be annoyed if a video started playing without their permission. And they definitely don’t want to see something random, or even something popular. Say they’ve been binge-watching How To Get Away With Murder and they’re looking for season 1, episode 7. If you automatically play a Bob’s Burgers episode, you’re just getting in their way and frustrating them.

But kids are different. In my experience, they have just as much of an opinion about content as anyone … except they’re not as married to watching things linearly. They’ll watch an episode over again. They’ll watch episode 38 before episode 1. Whatever. In most cases landing on their favorite show is good enough. The exact episode is a secondary concern.

And this is why it’s important to keep an eye on your assumptions and the audience you’re designing for. Designers often fall into the trap of designing for themselves, then wondering why their app wasn’t a fit, and cursing at customers for having no taste. Nope. You just didn’t do design research. No one does it enough, so this is your friendly nudge. Do more. It’s important, and it’s the easiest way to become a better designer who can deliver relevant experiences that people love.

And Here Comes Fullscreen

The app makes a reasonable assumption that if you don’t tap anything for about 7 seconds that you’re probably satisfied and it can probably go straight to full screen mode. Meaning the experience has no friction. You show up, video plays, and goes full screen.


Think about the alternative! A lot of video playback software over the last few decades made you find the “full screen” button. Which means you need to find the “get out of full screen” button (or gesture). PBS kids has abandoned that model entirely. When you’re watching content, it’s going to go full screen. Done with full screen? Touch anything in the main view to make it shrink down.


As with all great design, it doesn’t sound like much, but there’s a lot going on here, and the right way forward is never perfectly obvious. It’s time to talk again about my favorite word: tradeoffs. Mmm, tradeoffs.

Tri-States and Madness

Toggles are easy to understand cognitively. “When I tap this, the thing happens. When I tap it again, the thing un-happens.” Simple. Think of a light switch or an on/off switch on your phone.

But video players have so much more to offer! Forward/backward buttons. A media “scrubber” to manually select a part of the video. Volume. A full screen toggle. These are all believed to be vital parts of the media playback experience, but allowing access to them complicates the UI. In most cases, one tap goes to full screen, but a second tap displays a bunch of options, and one of those icons gets you out of fullscreen. That’s three states! Which isn’t necessarily bad, but it’s another set of tradeoffs to take into consideration.

And so, PBS Kids’ one-tap fullscreen toggle is not an accident. They’re optimizing for simplicity on one axis (tapping is a true two-state toggle) while increasing complexity on another (where’s volume? why can’t I scrub the playback head forward? Where’s AirPlay?).

So where did they put everything? Well, let’s take a look in—

Video Controls


Three options, are you kidding me right now
Wow, for real? Three options! So in PBS Kids you can move backward, pause/play, and move forward. Nothing else. I’d wager they debated this one a lot, but I think they chose well.

Adding a media playback scrubber would add a lot of complication, and the biggest issue is it has no way to “undo” an action. Say you drag a scrubber back 3 minutes and realize you made a mistake. How can you reverse your action? By dragging the scrubber forward 3 minutes. Kids ain’t got time for that (not to mention the fine motor skills). Most adults don’t either!

But where’s volume? Where’s AirPlay? PBS Kids has decided to leave them at the OS level. Click the volume buttons on the side of your iPhone and PBS Kids will get louder or softer. (in my household, it’s always louder) Or you can go into the Control Center by swiping up from the bottom. Oh, and hey, that’s where AirPlay is for every app too. PBS is leaning on a strong OS level design here, which lets them keep their app cleaner and feel like a good iOS citizen. Good call.

Speaking of which, they’re doing something sort of non-standard in the bottom left corner. Let’s talk about Parent Settings.

Parent Settings


Parents are the same no matter time nor place. They don’t understand that us kids are going to make some mistakes.
This area has a different color, a label, and it’s clearly something that can be tapped. Yet tapping it does nothing. Wait, what? Say hello to one of my favorite features of the whole app. Tapping does nothing, but swiping works, which is why they have that little “drag me!” visual treatment. Excellent.

Swipe gestures are harder than taps. They may not require an advanced degree in Swiponomics, but unless they’re clearly called out they can be trickier to perform and are more hidden than a tap.

Which is why there’s a little “grab me” icon there. The thinking is that parents can read, and they’ll know to drag to get to the parent features. I tried this on my two year old girls and they never figured out the swipe. They tapped a few times, nothing happened, and they went to tapping elsewhere. All by design. Pretty cool.

The actual contents aren’t particularly relevant for this issue of Design Explosions. You can see info about the app, learn when a show will be on TV next, buy an episode, turn on closed captioning. Just boring adult things.

Next we’ll look at browse. I already mentioned how smart it is to put browse and video playback next to each other, and the tradeoff that led to most of the screen being a giant toggle between full screen and browse. So there’s probably nothing left to get effusive about, right? Wrong!

Browse


There are two big things to call out in the Browse section. Thing the first: the section animates into place when the app launches and is very explicit about what it does.


I sometimes refer to this as a “load-bearing animation”. Some components of a design live or die on how well the animation explains itself, and this is a fantastic example. By auto-scrolling the area with realistic physics, it says a few things remarkably clearly:

This section moves!
There are other things to discover!
You should totally move it to discover things!
Wheeeeee!
My whole career, people have talked about “the fold.” The theory is if your content falls off the screen, sometimes people won’t know to scroll down to see it. So you try to let content “peek” into the frame to imply there’s more to be discovered. So this auto-animation they’re doing here? I’m not saying they deserve a Nobel Peace Prize or anything, but it’s a very nice touch for a standard UX problem. One made much worse when your target audience is still wearing diapers. So kudos to them.

The interactions here are pretty obvious. Tap a thing to load it. While it’s loading, it immediately shows a loading animation. The one you’ve tapped on turns yellow. If you hold your finger on them, they depress like real buttons. These are all so obvious, yet a lot of apps miss the basics.

Kobe Bryant recently said that he’s frustrated by young basketball recruits because they spend all their time doing fancy stuff but don’t even know the fundamentals of basketball. The same happens in UX design. Selected states matter. Every millisecond counts. When things take a while, explain the state and provide a way to cancel. Make buttons look actionable. Provide “down states.” These are the fundamentals. PBS Kids nailed them here.

But then! Want to get more fancy? Need to do boring adult things? Then you can drag the section out to get to more options, just like the parent section.


This reminds me of Shakespeare and Bill Watterson. Both artists (who are equally talented) made a point to appeal to two audiences simultaneously, the sophisticated and the unsophisticated. Great apps can achieve a similar result by this sort of technique. Most young kids will never find this side drawer, and most adults will be able to find it without much effort. Win-win.

Catering to one audience is hard enough, but true artists can find a way to cater to two. It’s extremely hard to pull off.

Summary, Of Course

The app doesn’t look like much. You load it, you see video and some other options, and then you pick something. Easy, right?

But next time you’re at a whiteboard and you assume it should be simple, or think “we’ll basically make it like Hulu”, make sure you think a bit deeper. Make sure to do some real design research. Learn to distrust your gut when it comes to representing audiences different than you. That’s what PBS Kids did, and the results are fantastic. But not in an obvious way. PBS Kids isn’t shooting for “wow,” they went instead for “of course.” They pulled it off and made something we can all learn from. It’s a delight to use.

Or in the stereophonic words of my twin girls, “Yayyyyyyyy Peebeeyessssssss Kids yay yay yay!” Yay indeed.

The Commerce Section

This was written by the fine folks at UX Launchpad. We teach one day, hands-on, fun design courses in Seattle (and on-site at your company). If you want to learn more about design, we’d love to help you out. We have three classes (and soon a fourth):

Introduction to UX Design, for everyone
Design While Shipping, for working designers
Sketch and Pixate crash course
We hope to see you in Seattle soon! Thanks for reading ☺

By UX Launchpad on March 1, 2015.

Exported from Medium on May 19, 2017.

Our New “No Advertising” Strategy

Last year UX Launchpad spent a fair amount of money on advertising. We’re pretty young and it’s important to get the word out, so every time a dollar arrived I lovingly wrapped it in another dollar and sent it out into the world.

2014 was a great year. It was fun, we got to help a bunch of people, we were profitable, and we got the word out. This year we’re rolling out two brand new classes and in January we scheduled ourselves all the way into summer. (See the classes here)

So for 2015 we’re going to try to have no paid advertising. There will still be new Design Explosions issues and behind-the-scenes posts like these. And while they do count as advertising, they’re free. And that change makes a big difference for the company in the long run.

In fact, it’s already made a meaningful difference. We used to hand out Field Notes sketchbooks for our Intro to UX class. They’re small and come in packs of three, so they cost about $3 each. But now we’re springing for larger and more beautiful sketchbooks at about $10 each.

It’s a small thing, but it’s one of my favorite things about running a company: as the product gets better, more money comes in, which I can use to make the experience better still.

So far it seems to be working, fingers crossed!

By UX Launchpad on March 12, 2015.

Exported from Medium on May 19, 2017.

Sim City vs The Lottery

Danny Sullivan wrote a great post recently:

After GigaOm, The Non-VC “SimCity” Approach To Growing A Media Business
I can’t speak with any authority about why GigaOm suddenly ran out of money. But I can already seem the rumblings about…medium.com
Sim City (all versions since the 80's, and I particularly like the newest one) is my favorite game, so I appreciate the metaphor. But he’s also touching on a greater truth: the lottery gets all the press but the grind doesn’t. Getting a lot of VC money, making a lot of money, and getting mega rich? It’s possible, but it’s not the only approach. The other approach is building carefully, in a conservative and sustainable way. And as great as it is, there’s no headline. “Company hires 50 people and everyone gets paid” isn’t sexy. But it’s real.

Last year we traveled to New York City to run a UX Launchpad class with the great folks from Harvest. Everyone in the class was awesome, the day was fun, the founders were really impressive, but my ears really perked up when they said the magic word: bootstrapped. They didn’t take VC funding.

Meaning they don’t owe anyone anything. They are profitable. They’re just trying to make the best thing they can and getting paid for it. That’s approximately one jillion times more inspiring to me than anything I’ve heard come out of the go-go until you crash-crash VC world.

So I wrote this last year, which overlaps a bit with the Sim City article:

That Magic Word
What Harvest Taught Usmedium.com
There’s nothing wrong with trying to win the lottery. There’s nothing wrong with needing funding. Or having a dream way too big to get off the ground by yourself. The problem is when you’re not even aware there’s another way. But there is. And in my experience, it’s pretty great.

By UX Launchpad on March 12, 2015.

Exported from Medium on May 19, 2017.

The First UX Launchpad Class Sucked

Everything exists relative to something else. “Sucked” can mean anything depending on where you’re standing, what you’re expecting, and what actually happened. I was standing at the front of the classroom, expecting to do a good job, and here’s what happened.

First: I had a miscommunication with the person renting the space, meaning no A/V equipment. Meaning I was literally reading from my laptop and periodically holding it up like a kindergarten teacher during story time.

Second: I didn’t notice at first, but the room was freezing all day. The class was during the fall or winter in stormy Seattle, and my attendees shivered through most of the day.

Third: I gave everyone Field Notes sketchbooks and pens. Except I chose pens with runny ink and the Field Notes were the special outdoors edition, designed for camping. Meaning slippery pages. Put those pens and those sketchbooks together and you get a giant smeared mess.

Later someone told me they thought it was part of the event. A trick. A design challenge, like “What would you do if all your notes smeared?” Which was very kind of him to say. Nope, the first day just sucked.

Fortunately, I made the class free so I didn’t have to issue refunds. And I learned a lot of lessons about things I’d need to do better the next time. And there were a lot of kind-hearted people in attendance so no one actually told me they thought it sucked. People got something out of the class, after all. It wasn’t a total waste.

But it did suck. I spent months dealing with the sting of it. Friends told me it probably wasn’t that bad. I was probably being too hard on myself. But remember, both things can be true. I did a bad job, but eventually I got over it. I made adjustments and I got better, and that’s all anyone can hope for.

UX Launchpad is kicking off another new class on Friday, one that teaches Sketch and Pixate. I’m not concerned about the A/V equipment, temperature, or poor pen choices. But something else is going to suck, because that’s just how it goes with new things. And it’ll be my job to see what sucked and make the next one better.

But I won’t have much time. I purposely made the follow-up class three weeks later this time. Because biggest thing that sucked about the experience of that first class was that I spent too long sulking about it.

By UX Launchpad on March 18, 2015.

Exported from Medium on May 19, 2017.

Twitter Isn’t Optional

[Disclosure: I work at Twitter as a designer](#)

I run a business called UX Launchpad. We teach hands-on design courses. And as a result, a lot of attendees ask course instructors about how we got our design jobs, or how they can get a better one, or how to break into the tech industry in the first place.

I’ve been thinking about this topic for years, and I’ll be writing more about it soon, but I think it’s important to say something right up front: there are many careers (design being a big one) where you need to be on Twitter. And ideally you’re not just there, you’re actively tweeting.

Why? Because Facebook, Instagram, and LinkedIn are great at a lot of things, but they are not designed for staying up-to-date as part of a global conversation. When you’re trying to get a sense of what’s going on, who’s hiring, where the best work is coming from, what techniques are getting popular, and so forth, there’s nothing as good as Twitter.

Last night my friend pointed out that popular designers have never been easier to contact. You can just favorite their tweet, or @reply to something they say, and you’re on their radar. I know several people who scored their initial job interview because a hiring manager noticed them on Twitter. That’s huge! But a lot of people never reach out, and that’s a shame.

Of course being active on Twitter is just the first step, not the last. You still need to work on your portfolio, sharpen your skills, and be someone people want to work with. It’s like buying a good pair of running shoes. That single purchase isn’t going to win a marathon, but I guarantee most marathon runners are wearing shoes.

Twitter is like running shoes for your career: no longer optional.

By UX Launchpad on March 29, 2015.

Exported from Medium on May 19, 2017.

An Apple Watch Heuristic Evaluation

At UX Launchpad, we teach design courses. So I went to my local Apple Store with a notebook and a camera to analyze the design of Apple Watch as a way to learn some things they did well and point out some things they can improve. The design issues I found fell into a few distinct categories:

1. Straight Up Buggin, Y’all
2. Troubling But Easily Fixed Issues
3. Fundamental Concerns

And then of course there’s a grab bag of other insights and closing thoughts. Note that this isn’t an issue of our popular Design Explosions series, because those are all about staying positive. Whereas the point of a heuristic evaluation is to tease out and discuss problems with a design, so that’s where I’ll be focusing.

First up, bugs!

1. Straight Up Buggin, Y’all

My first experience with Apple Watch was pretty bad. I was attempting to broadcast on Periscope but I kept tapping things on Apple Watch that didn’t work. Afterwards, I talked to an Apple Store employee who said he found the demo units to be buggy. So I tried again, this time without trying to use the device through a viewfinder, and everything worked as expected.

Since then, I’ve tried the device a few more times, and I never struggled as much as I did with that first demo unit. But I think my wife has the best explanation why: I was trying to tap a tiny screen by aiming my finger at what I saw through a phone screen, while a hundred people chatted with me (including some trolls), while trying to narrate. I was probably distracted.

But there’s a third factor that explains my original impression, and it’s a big one. Certain things in the demo unit just don’t work, but they fail silently. For example, you can try to turn on Airplane Mode or compose a new message, but you’ll tap several times before you realize it’s not a mis-tap, the demo won’t allow you to perform those actions.

(This video is ugly when I embed it so please enjoy a link instead)

This is a software design no-no. An interface most always displays a response, instantly, even when it can’t succeed. Other places on Apple Watch’s demo get this right. For example, if you try to use Siri to set up a calendar appointment, it apologizes and says the software can’t yet do that.

Of course the final units will be able to perform all advertised features. So those issues, while extremely concerning to me at first (and I assume many other early adoptors who hit similar problems), don’t actually point to a bad interface design. They point to some early bugs in the demo units. Not ideal for the initial rollout, but not a huge concern in the long run.

2. Troubling But Easily Fixed Issues

My time working on the Windows Phone design team helps me know where some tricky interaction design challenges live in built-in apps. For example, when setting alarms, you can sometimes get a few levels deep in the hierarchy. (for example, when deciding when an alarm should repeat) So what do you do when a user cancels out of the app? Do you save their changes made until that point? Or do you destroy the changes they made? (There are other options, like not letting you leave an app until you select “Save” or “Discard” from a prompt, but that’s a non-starter.)

Windows Phone went with destroying changes if you don’t tap save, a choice made worse by certain flows (again, setting repeating alarms) that actually make you tap save twice. It’s a flaw in Windows Phone’s interaction design, but I’m happy to report that Apple Watch always saves your progress on the fly. I believe it’s a good choice because it’s one less thing to think about, and I doubt anyone will miss the PC-style “Save” and “Discard Changes” dialog.

(Video of making a choice, exiting out, and having the changes saved)

But that doesn’t mean all dialogs are bad. For example, destructive actions (like deleting an alarm) should always ask you to confirm, and ideally let you undo. Apple Watch does neither. If you accidentally delete an alarm, it’s gone. (Check out this video — I perform a bunch of actions, then at the end I tap delete and whoooosh a destructive action without a confirmation.) I chalk this up to a silly oversight or a bug. It’s easy to fix and there’s no reason to believe they won’t.

I found a few other minor things like this, but none really worth calling out. Just odds and ends and things left undone. Anyone who’s worked in software knows how it goes. You address priority 1/severity 1 bugs first, and a bunch of pri3/sev5 stuff goes unaddressed. I bet Apple has a bunch of bug fixes waiting and ready to go in a 1.01 update.

Like category #1, these are things that jumped out at me as problems, because I’m a designer it’s my job to know this stuff. They damped my enthusiasm a bit, but they’re not deep structural problems. They’re just bugs that they didn’t get to yet.

But that’s not all I found. I also found bigger stuff. Grab a comfy chair.

3. Fundamental Concerns

I think the Apple Watch is pretty solid overall. The word “concerned” doesn’t mean I think they’re doomed or that I’d expect to see an embarrassing number of returns. But they are definitely going to cause friction, and they’re inherent in the design of the system and the constraints Apple is working under. There’s no quick fix for these.

Tiny touch targets

In 2007, Apple was very explicit about touch target sizing. They knew the average finger size, they knew what factors make it easier or harder to tap a target on a screen, and they informed their developer community to exercise judgement when designing tap targets. But the Watch screen is crazy small compared to an iPhone, meaning some tap targets are tiny. When you go to tap a back button to move you back a screen, you’re going to miss sometimes. And whether you miss 50% of the time or 5% of the time, it just feels more fiddly than you’re used to on an iPhone.

Which is why the Digital Crown is such an experiential breakthrough. When I can use it to manipulate the UI, the phone feels smooth and beautiful. When I can’t, and I have to tap a little target to complete a task, my heart sinks a little bit. It’s not that the touch are a disaster, but they’re certainly harder to access than I’m used to. This is a video of me having to tap multiple times frequently. And I have small fingers!

Apple could have made the Watch bigger to account for this, but that only gets you so far. There’s an immovable law in play here — smaller things are harder to tap. And you don’t want a watch to be phone sized. So tapping a reasonably sized watch screen will always be harder than a phone. Thank goodness for the digital crown.

Some Overloaded Actions & Gestures

On the Maps app on iOS, you can do a few things, using a few distinct gestures. In order of most common to least common, you can:

Drag to pan the map around
Tap to toggle the chrome
Stretch/pinch to zoom in or out
Double-tap to zoom in
Long-press to drop a pin

This means even if you never discover anything other than scrolling and tapping, you’re probably ok. Tap the address bar and type an address, and you will probably complete your task successful. Power users might use the long-press, but you certainly can’t rely on that gesture, so they don’t.

Whereas Apple Watch has to rely on it if you don’t come through Siri, and that adds a learning curve. And it’s not just that you have to learn a new thing, it actually seems to work inconsistently. Here’s a video of me using Maps and getting confused.

Digital Crown zooming works. Panning works. I don’t think tap does anything. But long-press (aka Force Touch) seems to drop a purple pin. Except when it brings up Search and Contacts. I couldn’t manage to figure out how to trigger one versus the other, and that’s concerning.

Putting myself in the designer’s shoes for a moment, there a few things the design is attempting to allow, beyond zooming and panning:

Orient the map on your location
Let you drop a pin
Type an address
Go to a contact’s location

So orientation comes via a button on the bottom left of the screen. But then the other three are all behind force touch, with no clear (to me) way to distinguish between them. I suspect a lot of people are going to dead-end in the Maps app if they load it directly. And of the people to try Force Touch, some are going to get a confusing purple pin when they were expecting a search field.

Similar problems are found throughout the interface. You can’t clutter the UI with buttons (I remember when designing for iPhone felt cramped!) but as an app creator you only have touch and force touch to work with. That leads to some situations where people will struggle to complete their tasks because you have to guess at how to complete your task.

Launching An App Is Hard

I love the visual design of the app screen. But the icons feel microscopic, certainly too small to tap. I find myself panning to get the icon somewhere near the middle of the display, then zooming with the Digital Crown. When it works, whew. But it often doesn’t. If it’s that hard to launch with the larger size of the watch, I’m going to struggle with the smaller one I bought for me and my dainty Tiny Person Wrists.

But people are resilient. “Satisficing” is a real thing when people use software, and I’m sure my experience will be no different. You can launch apps via Glances, the Widget or Google Now-esque mini-apps available on Watch, and I suspect that will be my primary entry point. In a perfect world, I’d be able to launch apps reliably every time, but it’s a small screen. Tradeoffs about in software, doubly so for software this tiny, so thank goodness for Glances.

Two Buttons

I should say right up front that I think two buttons was the right call. If there was only one button, it would take waaaaaaay too long to load up a contact. Digital Crown press-\>Apps-\>Load the Contacts App (see above, this is not trivial)-\>Find the Person-\>Finally start completing your task. Whew.

You know how lots of people hate the idea of Apple Watch and can’t understand why you wouldn’t just use your phone? Well, most of the time they’re willfully ignoring the benefits of wearables to make a point. But on this, they’d be 100% right. If I have to fiddle for 15 seconds to contact my wife, yeah, I might as well just be using my phone.

Which is why having a dedicated button is the right call. But it does lead to cognitive overhead. Personally, I think people will acclimate to it just fine. One button is like the home button on an iPhone. The other loads contacts. It’s not as easy as iPhone, but it’s not as complex as Android. It’s harder, relatively speaking, but I wouldn’t call it hard in an absolute sense.

Still, it’s a little more cognitive load to consider. For example, my son never did figure out that he could press the Digital Crown. He found the Contacts button, but that was it. Whereas I have video footage of him as a baby navigating through the iPhone lock screen, to apps, and back again by tapping the Home screen, all within a few seconds. So.

(Note: I also have footage of him using Windows Phone. Notable insight — he and his friends always hold it with his palms and his fingers splayed because he kept accidentally getting whisked out of his game and taken to Bing, thanks to the overly sensitive capacitive buttons.)

So two buttons is something people will get used to, and I understand the design tradeoff, but it’s one more little bit of cognitive load. Should I click here? Or here? But the buttons are nothing compared to Apple Watch’s modes.

Modes Complicate Things

Oh, modes. Early in my design education, I was always taught that modes cause a ton of problems in a design, and that’s been borne out by all the data and research I’ve been involved in or read about in my entire career. Sometimes modes work well (like “edit” mode when you’re going to delete a bunch of messages or “wiggle mode” to rearrange app icons) but they should be used sparingly and carefully.

Y’all, Apple Watch has a lot of modes.

If you’re looking at the clock, you can swipe from the top or bottom to get notification center stuff or Glances. This is the only place you can access these locations. (after all, if every swipe down loaded notification center, you’d accidentally trigger it constantly on such a small screen)

If you’re looking at Glances you can swipe down to dismiss, or right and left to see other glances. But you can’t get to apps or the clock without exiting out with the Digital Crown.

If you’re in an app you can use it fully. But you can’t get to Glances or Notification Center. This means if you go from a Glance to the app and then decide you want to get to another Glance, you press the Digital Crown to return to home, then drag up to get back to where you were in Glances. On one hand, it’s only two steps. You’ll figure it out. On the other hand, there’s a clear cognitive burden this places on you while using the app. Here’s a video of me going from the music’s Glance to the full app, then having to stop to consider how to get back to where I was.

(One of my favorite articles about these small but important moments is Your App Makes Me Fat. If you haven’t read it, I highly recommend it.)

It’s Trying To Do A Lot

People have written about this, but they usually mean at a high level, like it’s trying to perform a lot of different tasks, or install a lot of apps. But I actually noticed it most when I was trying to personalize the watch face. Check out this demo at Apple’s website and watch the steps involved in customization.

At a high level, it’s just “Get into edit mode, then spin the dial to pick what you want, then confirm when you’re done.” Sounds reasonable. But the sheer number of personalization options means the UI has to flex and scale awkwardly to address a visual smorgasbord of options.

This explains why Apple has their reputation for products that quote-unquote just work. Historically they’ve reduced the number of options to reduce fiddling, while their competition bets on choice at the expense of simplicity. This time around the tables have turned and Apple has gone big on personalization. There’s no doubt it complicates the overall experience, and it feels un-Apple to me.

But hold on to your hats. I actually want all these choices that are bloating the experience. I did not see that coming. I’ve leaerned that I don’t want a dizzying array of options for a laptop, or a smartphone, or even the watch hardware, but software is different. If the UI needs to be more complicated in order to help me feel like a super special unique snowflake with great fashion sense, fine. That’s a price I’m willing to pay.

All is vanity.

Other Than That, How Was The Play?

If you’re looking for me to throw the Watch under the bus, sorry. Write me off as an Apple fanboy (it wouldn’t be the first or last time!) but I’m still incredibly excited about Apple Watch. Despite all these design concerns, I’m definitely going to buy one. Why?

First, the small, bug-sized stuff doesn’t bother me. And even the larger, more fundamental design concerns I have can be boiled down it “it’s hard to navigate to different areas on the Watch.” But the device isn’t meant for bouncing between apps like a laptop or smartphone. I predict I’ll use it largely as designed, as a glance-and-go experience. To use a phrase my friend Jake Zukowski coined, the software isn’t trying to be “sticky,” it’s striving to be “slippy.” And in fact, I think it’ll do better than any other smartwatch on the market (both UX and sales) for the foreseeable future.

Why? Because when I think about ways to address the concerns I have, the tradeoffs come down to things like “make the device a lot bigger,” or “don’t let me customize as many things,” or “make it so I’m constantly accident-swiping into Notification Center,” or “make the beautiful display of apps into a grid of 2x2 icons that I have to swipe through, iPod mini style.” And none of those sound good to me. So I’m ok with where they landed. These are tradeoffs to be sure, but that’s what design is. I think they chose well.

But I have a personal consideration that’s much bigger than all that, and it makes all the discussion about fashion and vertical integration and marketshare and finding new billion dollar businesses and all the rest of it pale in comparison. It’s the peace of mind I get from a tap on the wrist.

In my family, we’re living with illness, and I have three young children. I have frequently missed important texts from my wife because it’s hard to find the balance on my phone between keeping it quiet but letting important things through. Sometimes I put it in my jacket pocket, so I don’t feel the vibration, but I’m in a movie theatre so it has to be on silent. And these considerations aren’t theoretical, they’ve happened and they add stress to how my family communicates, lives, coordinates, and plans.

What I see in Apple Watch is a way for my wife to be able to contact me from her Mac, iPhone, or Apple Watch (if she gets one) with almost perfect certainty that I’ll notice it. Other than running out of battery or forgetting to wear it in the morning, it seems like a solid way to stay in touch. The most reliable ever. And to me, that’s valuable on a life-and-death level.

There’s no one thing that Apple Watch can do that the iPhone (or Android devices) can’t do. But that’s the wrong metric. From the Uber app to my family members tapping me on the wrist to monitoring my heart to helping me be more playful through little drawings on my phone, the watch is helping me do things better. In some cases, significantly so.

So it’s not perfect, because nothing is. It’s complex, but I understand the tradeoffs. They have a lot to work to do, like any company shipping the first version of something. But I have no doubt it will change my family’s life and health for the better. That’s worth more to me than any family heirloom.

By UX Launchpad on April 20, 2015.

Exported from Medium on May 19, 2017.

UX Launchpad’s Mid-Year Check-in

This year we’ve been trying a no paid advertising marketing strategy. Here’s how it’s been going.

Canceled Class #1

A few months ago my wife got sick right before a sold-out class. We canceled the class, refunding everyone’s money, and pointed people to the rescheduled event. Less than half made the switch, which meant a big hit to the profitability of the event.

Canceled Class #2, Maybe

Last Friday we kicked off a new class called Design While Shipping. It’s tailored for working designers who would like to be more effective in their jobs. The class was over capacity and it went well.

The second class is coming next week, meaning only a two week gap between each class. It’s hard to sell out one class, let alone two in a single month. If we don’t get enough sign ups, we may cancel it.

The Triumphant Return of Advertising

We’ve enjoyed our no paid advertising experiment, but I think the data is in. It turns out when you advertise, more people hear about you! And in the case of UX Launchpad, it’s still worth spending the money to get the word out. So we picked two places to advertise.

There’s a great podcast for mobile developers called More Than Just Code. One of the hosts, Jaime Lopez, came to a UX Launchpad class a few weeks ago and he’s a great guy. We sponsored their podcast this week.

We also advertised on KEXP, the great Seattle radio station, for the umpteenth time. They’re always excellent to work with and it’s always fun to hear our own ads while listening to the radio.

Status: Our 2015 Mission is Complete

We’re hoping we don’t have to cancel next week’s class, but it’s no biggie. Thanks to our Slow Epiphany last year, we don’t pay for the rental space up front, so it won’t cost anything. It’ll just be a sign we need to advertise more effectively.

But it’s worth remembering, while the sun is high and half the year has gone by, that we’ve already achieved what we wanted to this year. We announced, developed, tested, and fine-tuned two new classes for a total of three. We’ve been writing Design Explosions, which people seem to like. We get to teach throughout the year and meet cool new people.

So 2015 is right on track. We just need to advertise better.

By UX Launchpad on June 14, 2015.

Exported from Medium on May 19, 2017.

Equity

The two of us stood there in an elementary school cafeteria while a science fair swirled around us, two men with young children, talking about equity. We’re both co-founders of start-ups. In my case I’m the first employee. In his case he’s the second. Equity is a difficult topic for both of us. I think it is for everyone.

There’s an amazing moment in the highly recommend Startup podcast where the founder brings on a trusted friend and advisor to be the company’s second employee. The question of equity comes up. The founder, Alex Blumberg, offers his friend something low like 5% or 10%. His friend counters that he was expecting more like 45%.

Uh oh.

They end up patching it up, one way or another. But now the podcast is in its second season, following another start-up, and equity is back in the spotlight. The co-founders go to a kind of “couples therapy” to discuss the issue further. And it becomes clear that it’s not really about money. It’s more about control, but even that word has too much baggage.

In my view, these three stories come down to aligning self-worth to what the company sees in you. It’s about making sure you’re appreciated, supported, and frankly, loved. You’re in a relationship with your co-founders and your company, and that relationship needs to feel right.

I started UX Launchpad by myself, and I was determined to never, ever give up a majority stake. I did not want to allow myself to get voted out of my own company. No venture capital, no board, no co-founders. My goal isn’t to get rich, I just want to run a company the way I want. Sometimes founders forget those two goals can contradict each other.

Somewhere along the way, William and I put together a novel arrangement. We noted that we had each done work (he wrote an incredible design book and I put together UX Launchpad) inspired by the other. So why not be named as collaborators in the other person’s work? So he became a co-founder of UX Launchpad and started teaching classes alongside me while working on his second design book.

But when it comes to equity, in the lawyers-and-contracts sense, he owns the rights to his book 100%. And I own UX Launchpad 100%. We’ve tried to find a way to overlap and assist without it being a power struggle. It seems to have worked pretty well so far.

But that’s the equity question. What about salary? So far it’s been simple: no one at UX Launchpad draws a salary, not even me. Our accounting is simple and our overhead is low — we rent space, pay for advertising, buy materials, and so forth, and anything left over gets put in the bank. End of story.

I’ve been contacted several times by people asking to help in various capacities, and I try to be up front: UX Launchpad is profitable, but not so profitable it can kick off a formal payroll. And even if it was, I’d still prefer to grow slowly. I’d still prefer to do less, with fewer people, more deeply.

I find that a lot of things that feel most comfortable for me go directly against conventional wisdom. I don’t want to get rich. I don’t want to grow fast. I don’t want to quit my day job. I don’t want outsider opinions. I don’t want to split responsibilities amongst a group. I don’t want to come up with the magical equity percentage that makes someone feel valuable without diluting my own impact on my own company. I just want to design a company and a situation that lets me teach and sustain my business.

So far things are working out pretty well! I’m very thankful for that fact.

By UX Launchpad on July 8, 2015.

Exported from Medium on May 19, 2017.

There’s No Perfect Design, Only Tradeoffs

Tomorrow I’ll be speaking at Creative Mornings about collaboration. One thing I say in my talk is “there is no perfect design, only tradeoffs.” This is a point of view that frustrates some designers. There’s a mythology around designers that says only they fight for the user. That only they know how to make something great. That if a company is “design-led” then it can be great, but otherwise, it’s in trouble. Only designers can succeed, right?!

It’s a self-serving land grab and I call BS.

I’ve seen designers ruin a product just as surely as the most myopic bean counter or the most stubborn dev. The only difference is the tools they use and the biases they lean towards. But in all cases, the failure can be traced back to ego. The belief that they’re seeing clearly and everyone else is just in the way. That if people would just give them all the control, they’d make something amazing. It’s a romantic notion, but it’s just not true.

Here’s an example.

How to Handle Command-1 in a Browser

I prefer Safari to Chrome. There are several reasons, but a big one is in how it handles Command 1–9. If I press Command-1, it goes to my Gmail account. If I press Command-2, it goes to my calendar. Safari has decided to align the command keys to bookmarks. Until yesterday.

My version of Safari got updated to a beta and now Command-1 takes me to my first tab. Command-2 takes me to the second tab. This design baffles me. It’s mystery meat navigation — tabs always shuffle around, so Command-1 will never go to the same place. Personally, I consider this bad design.

And it would be easy to stop there. I’m right, you’re wrong, end of story, case closed because I’m an uppercase-d Designer so you should listen to me or else the world will fall apart, Mike Monteiro said so. But that’s not really how the world works. And that’s certainly not how healthy teams work.

If I was the designer on Safari, and my engineer proposed this change, the original conversation would have gone like this:

Engineer: I think we should align with Chrome.
Me: You mean making Command-1 go to tab #1?
Engineer: Exactly.
Me: But I hate that approach.
Engineer: Sure, but it’s what people expect. It’s the standard.
Me: But it’s mystery meat navigation! It’s bad design!
Engineer: Ok, but. It’s what people expect. We’re the odd one.
Me: Because we’re on the right side of history!
Engineer: Are you sure about that?

And the engineer would have a point. If the whole world aligned to a certain expectation, continuing to buck the trend brings its own burden. No matter how certain we are that we’re right. There’s a fine line between knowing what you’re talking about and being impossible to talk to.

Don’t Be So Sure

I can debate until I’m blue in the face that keyboard shortcuts shouldn’t ever move around. I can cite scientific research that describes how buggy human memory is. I can draw pictures, create motion studies, make prototypes, and parade around on stage about how right I am. But to what end?

The industry has decided that Command-1 should take to your first tab. I can disagree with it, I can think it’s wrong, but I can’t wave away people’s expectations as irrelevant. And more importantly, I can’t overplay my hand. My engineers and I need to work together on literally hundreds of issues like this. If I can’t have this conversation in a reasonable way, what hope will the team have for debating more complex or contentious issues?

It’s fine to think you’re right. But don’t overplay your hand. Sometimes it’s best to count votes, see you’ve lost, and move on to the next thing. It doesn’t mean you don’t care about good design. It means you’re mature enough to know you’re not always right, and that ego is fueled by insecurity.

By UX Launchpad on July 9, 2015.

Exported from Medium on May 19, 2017.

Steve Jobs Insult Response: An Analysis

A funny thing happened on the way to writing the next issue of Design Explosions and preparing for our next class: I couldn’t stop thinking about this five minute exchange during a Q&A session at Apple’s 1997 developer conference.


In a short five minutes and seventeen seconds, Jobs manages to cram in multiple life lessons about how to listen well, design well, manage well, respond well, and how to build things as part of a team. I discovered it a few years ago and I return to it every few months. It’s an embarassment of riches for any designer trying to get better at shipping great products.

Design, the real kind, is supposed to be about communication. Yet a lot of designers I know are strong on the pixel side but pretty under-developed on the communication side. I think we all struggle with it, so pull up a chair! I’m going to aim my Design Explosions laser on this video because it has a ton of things to teach.

The Background

This video takes place in 1997. Steve Jobs is back at Apple, but he has no real authority yet. He’s just an advisor to the CEO answering some questions with the audience at WWDC, Apple’s developer conference.

In the months before WWDC, an Apple-developed technology called OpenDoc was killed. It upset many in the Apple developer community, so earlier in the Jobs Q&A someone asks about it. Jobs apologizes for Apple having to put a “bullet in OpenDoc’s head”, but says it had to happen, and moves on. Then, 45 minutes later, someone follows up, and that’s where it gets interesting.

The Lead-Up

When the audience member takes the microphone, he leads with a seemingly complimentary line:

“Mr Jobs, you’re a bright and influential man.”
But there’s a flatness in his voice and intensity in his face that Jobs spots immediately. From the stage, he quips “Here it comes!” and holds up a chair like a lion tamer. The audience laughs as the question continues. (Side note: I really wish I could see Jobs’ face drop as he realizes how rude the question is, but the camera cuts away.)

The Question

“It’s sad and clear that on several counts you’ve discussed you don’t know what you’re talking about. I would like, for example, for you to express is clear terms how, say, Java, in any of its incarnations, addresses the ideas embodied in OpenDoc. And when you’re finished with that, perhaps you can tell us what you personally have been doing for the last 7 years.”
You hear someone say “ouch” as the crowd mumbles. Jobs takes a quick drink from his water while looking down and trails off with an “uhhhh…”

The Pause

Nearly 15 awkward seconds go by between the end of the question and when Jobs finally begins to speak. And even then, it’s just a little joke about not being able to please everyone all the time. Then he pauses for another long eight seconds before setting his water bottle down and responding fully.

That’s where we discover lesson number one. In most cultures, we’re trained to never back down, never surrender, eye for an eye, and so forth. Allowing that much dead air looks weak, some people argue. But Jobs was figuratively counting to ten and catching his breath, something we should all do more.

Nothing good comes from sending that angry email in a hurry. Responding in the heat of the moment never works. It feels good for about 5 seconds but it’s counter-productive if you want to do (and ship) great things. I’ve worked at mastering those reactionary and unhelpful impulses my entire life. It’s hard!

So don’t respond right away. Have the confidence to give yourself some space. It’s those 30 seconds or so that let Jobs see the situation clearly and respond as well as he does. There are very few emails that can’t wait a day, no matter what anyone says. Don’t get sucked in to rapid fire responses.

The Surprise Jujitsu Maneuver: Agreement

Finally Jobs speaks:

“One of the hardest things when you’re trying to affect change is that … people like this gentlemen … are right! In some areas!”
Ok, pause. Look what he did right there. He heard the man’s point. And he’s not doing it as a ploy or a tactical maneuver. He understood in that moment, as he did his whole life, that everything is a trade-off. Only a fool would believe his questioner doesn’t have a point. Jobs receives the point, gives credit to it, and in doing so, changes the tenor of the whole discussion, tilting the advantage towards him.

“I’m sure that here are some things OpenDoc does, probably even more that I’m not familiar with, that nothing else out there does. And I’m sure that you could make some demos, maybe a small commercial app, that demonstrates those things.”
Here it comes.

The Counter-Argument

The argument sufficiently absorbed and given credit, now Jobs can retort more effectively. Everything has a counter-argument and Jobs’ boils down to how OpenDoc is interesting but hard to fit into a broad overall strategy.

“The hardest thing is — how does that fit in to a cohesive larger vision, that’s going to allow you to sell — um — 8 billion dollars, 10 billion dollars of product a year.
And one of the things I’ve always found is that — you’ve got to start with the customer experience and work backwards to the technology. You can’t start with the technology and try to figure out where you’re going to try to sell it. And I’ve made this mistake probably more than anybody else in this room. And I’ve got the scar tissue to prove it. And I know that it’s the case.
And as we have tried to come up with a strategy, and a vision for Apple, um, it started with … what incredible benefits can we give to the customer? Where can we take the customer? Not starting with — let’s sit down with the engineers and figure out what awesome technology we have, and then how are we going to market that. Um … and I think that’s the right path to take.”
So there it is. You can agree or disagree with his point. But now both sides are on the table. OpenDoc is neat, both men agree. But in Jobs’ view, he can’t understand how to sell technology and doesn’t think it works. He only knows how to sell experiences.

Did you spot that little “I’ve made this mistake probably more than anybody else in this room” line? I love that. It’s a humble brag. He’s saying “I used to think like you, you know, before I ran three of the most innovative companies on the planet. So I hear you … but I probably know more.”

Then he takes the conversation back to 1984. Anecdotes can be magic for shoring up your point, and that’s what happens here.

The Anecdote

“I remember with the Laser Writer, we built the world’s first small laser printers, you know. And there was awesome technology in that box. We had the first Canon laser printing, cheap laser printing, engine in the world, in the United States here, at Apple. We had a very wonderful printer controller that we designed. We had Adobe’s PostScript software in there, we had AppleTalk in there, just awesome technology in the box.
And I remember seeing the first print out come out of it. And just picking up, and looking at it, and thinking, you know, we can sell this. Because you don’t have to know anything about what’s in that box. All we have to do it hold it up and say “do you want this?” And if you can remember back to 1984, before laser printers, it was pretty startling to see that. People went “whoa! yes!”
And that’s where Apple’s gotta get back to.”
Boom.

These points make all the sense in the world when you’re reading in a Think Piece-y Medium post, but it’s a central problem that tech companies stumble over when trying to make great products.

Customers have a pretty simple barometer: does this product meet my needs? Yet tech companies get wrapped around the axle when they make a tech discovery they deem to be cool and try to productize it without a clear sense of the problem it is trying to solve.

Every company I’ve been at frequently forgets the core scenario and starts playing with fun future tech instead. They get married to possibility, and potential, and cutting edge computer science work they respect, which makes it harder to realize when the product is just some new tech with a price tag, not a compelling overall experience.

If your value prop is more complex than holding something up and saying “do you want this,” it’s too complicated. Or if you do, and no one is excited about it, it’s not a good product — regardless of the exciting tech. Again, Jobs knew this better than any one of his contempories and was able to eloquently explain it to skeptical stakeholders over and over in his career.

This is why framing is so vital for designers. We need to be relentlessly talking about the core benefit our product will bring, and not let ourselves get bogged down in spreadsheets and backlogs too soon. Focus on the experience, then find the tech to match. If you get it backwards, you’ll fail.

By this point Jobs isn’t debating OpenDoc itself, he’s gone far beyond to what Apple needs to do at a macro level. But he does return briefly to the original question, on his way to another key lesson for designers.

And you know, I’m sorry OpenDoc is a casualty along the way. And I readily admit there are many of things in life where I don’t have the faintest idea what I’m talking about that. And I apologize for that too.
Don’t Be Afraid To Let Teams Move Forward

This is my favorite section in the whole response.

“But there’s a whole lot of people working super super hard right now at Apple, you know. Avi, Jon, Guerrino, Fred, I mean the whole team is working, burning the midnight oil — and hundreds of people below them — to execute on some of these things. And they’re doing their best.
And I think that what we need to do — and some mistakes will be made, by the way. Some mistakes will be made along the way. That’s good. Because at least some decisions are being made along the way. We’ll find the mistakes, we’ll fix them! — I think what we need to do is support that team.”
I’ve worked at a lot of big companies and some start ups too. Everyone, regardless of company size, gets paralyzed. People get protective over their turf, new people are told not to be bold, and products begin to stagnate and suffer.

Some scientists tested this phenomenon. They took a group of monkeys, put a banana at the top of a ladder, and when a monkey tried to reach it, all the monkeys got sprayed with water. Repeat. Now remove monkeys one at a time until every monkey is new … but they all beat up anyone that reaches for the banana. Even though they collectively forgot why. Welcome to organizations! [edit: this story is an urban legend that combines a few studies and exaggerates the findings](#)

Trust your teams. Give them a bit of space. Understand that mistakes will happen, but trust that the company can then course correct. At least some decisions are being made. Can I get a hell yes?

Designers should remember this every day. Be polite. Be collaborative. Don’t leave people in the dark. But be careful with people that get anxious on your behalf, that maybe someone else might possibly be upset somewhere about a change you might make somewhere. They’re a monkey stopping you from the banana and even they don’t know why. Hear them, respect their concern, go build the relationships you need to, but don’t let them scare you off. It’s better to try and fail than stay still. Ask RealNetworks. Who? Exactly.

The first design meeting I had on Windows Phone was pretty clear cut. I made a call. We moved on. Afterwards, in the hallway, a PM told me “it’s nice hearing a designer with a point of view.” What a damning thing to hear! If designers aren’t being thoughtful, collaborative, and clearly communicating their vision to others on the team, they’re not living up to their job title.

Summary, Conclusion, Good Night Irene

“Going through this very important stage as they work their butts off — they’re all getting calls, being offered three times as much money to go do this and that, the valley’s hot — none are leaving. And I think we need to support them, and see them through this, and write some damn good applications to support Apple out in the market. That’s my personal point of view.
Mistakes will be made. Some people will be pissed off. Some people will not know what they’re talking about. But I think it is so much better than where things were not very long ago. And I think we’re going to get there.”
Preach. Trust the teams. Know they’re not going to be perfect. Be happy that we’re marching in a clear direction, and yes, that means your pet project might get canceled for the greater good. But that happens. There are two options in a situation like this: you can cling to a single project/technology or you can think long term.

Companies tend to cling, designers tend to think long term. If you can figure out how to do this translation half as ably as Jobs does in this response, you’ll be very successful indeed.

By UX Launchpad on July 12, 2015.

Exported from Medium on May 19, 2017.

Changing Things Up

This year at UX Launchpad we spun up two new classes, for a total of three. We’re currently offering a 101 class as an introduction to ux, a 201 class for helping designers be more effective, and a 301 class for learning Sketch and Pixate. The classes went well. They’re fun to teach, and people enjoyed them.

We also came up with a no advertising strategy that we then revised to a not-much advertising strategy. The end result has been smaller classes that are more profitable. Which means our classes in Seattle have made more money this year than last year. Hurray!

Upgrading Our 201 and 301 Classes

We’re going stop to teaching our 201 and 301 classes in Seattle and offer them as on-site classes instead. This means we’ll be working with a group of people who all know each other, which changes the syllabus and the energy of the day. For example, our 201 class can be more about “how do healthy design teams work together,” which works best if we’re talking to folks who are already on the same design team.

About the 101 Class

We may move our Introduction to UX class to the same model. We’ve taught it many times, both in Seattle and around the country, and it works really well for any group of non-designers. But there is something special about teaching to a group of people who work together, because the content can be tailored towards their day-to-day experience.

That said, there’s still benefit to doing the Seattle-based class. So we’re still considering what to do with it. We’ll share more information as we have it.

The New Book

William has been working on a new book. Stay tuned for that.

Online Videos

We’ve always prided ourselves on doing one day, hands-on, fun design courses. We like how the experience isn’t reproducible on YouTube or from reading a book. The world is full of educational content, but we think there aren’t enough fun learning experiences.

Most people don’t live in Seattle, so we’re often asked if we’ll offer online content for people who don’t want to travel. Up until now the answer has been no, but now we’re considering it for the first time.

Design is just problem solving. No more, and no less. One of our favorite things about running our business is seeing what works, what doesn’t, and designing solutions to make the product as good as we know how.

So these are our next moves. Wish us luck!

By UX Launchpad on August 30, 2015.

Exported from Medium on May 19, 2017.

3D Touch & Federal-level Design Thinking

You know that crime movie trope where a local police department is at the scene, but then the men in dark suits show up? “Damn, it’s the Feds,” the local law enforcement grumbles. Invariably, the Feds pull rank. They don’t listen. They don’t know the small town the way the sheriff does.

It’s always the same story, and it reminds me of what happens at every tech company when designing new features across iOS and Android. There’s a natural tension between what the platform recommends (Federal-level laws), what the company’s branding objectives are (State), and what each new feature is trying to do (local).

Apple’s new 3D Touch is the latest platform-level design change that product designers will have to respond to. If past history is any guide, I think we can expect to see a few things bear out, in a predictable order.

Step one: Wait and See

Part of effective design work is being aware what Google and Apple are proposing at the “Federal” level while having a measured response. You can’t change your app every time the platform comes up with a new idea.

Imagine pitching your board by saying “Hey, so Apple’s doing this new thing where if you press hard on the screen, it can trigger different interactions. It’s sort of like a long-press, but not really, so anyway, can I have budget to redesign everything from scratch?”

A smarter approach is, of course, to wait and see. Do users want the feature? Do they expect it? Do they miss it if it’s gone? And let’s say for argument’s sake that 3D Touch is a hit, and it is expected, and App Store reviews start reflecting that. What next?

Step two: Try to Esperanto Your Way Out

Esperanto was an idealistic but ultimately flawed idea: “let’s get everyone in the world to learn a single, universal language, then we all can talk to each other!” Sounds great, but it didn’t work. And if you’ve ever struggled to use a Java applet, you’ve experienced the same wishful thinking in software product design.

I have yet to work at a company that skips this step, because the dream of design utopia is too tempting. Imagine telling the board “We’re going to come up with a strong visual identity for our products across all platforms so they all look and feel the same. It will save time, money, and make it look like we have a singular design vision! Design once, use everywhere!” Sounds amazing, right? Yeah! It’ll be just like Java applets and Esperanto!

Which is a fancy way of saying it’s doomed to fail.

Other than your logo and your color scheme, most things in UX design don’t port cleanly from system to system. For example, using a custom typeface has huge implications as you try to push it across a whole universe of different products and form factors, each with different technical capabilities. Plus some countries have slow bandwidth speeds, so bloating your app has serious consequences. It only gets worse from there. Check it:

So you want to have the same navigational structure across iOS and Android? Cool, Android encourages putting everything at the top, often with a hamburger menu. iOS is ok with things on top, but also frequently uses a “tabs on bottom” pattern. And strongly recommends against a hamburger. Oh, and Android has a back button. iOS doesn’t. (The ripple effect from that difference alone could fill a book!)

Also Material would really like you to use a Floating Action Button, but it looks pretty out of place on iOS. Oh, did you remember to think about responsive design across all screen sizes? Right to left text? Localization for languages that are much more verbose or much more succinct? Did you know swiping gesture expectations vary between Android, iOS, and Windows Phone 8? Oh, and they changed that in Windows Phone 10. FYI.

I’d say this is the tip of the iceberg, but it’s more like an onion. As you design, you’ll find layer after layer each more painful than the last, all while crying and hating your life. I may or may not be speaking from experience. Lots and lots of experience. So much experience y’all. Where by “experience” I also mean “tears.”

Step three: Hand-crafted for each platform

Eventually, after enough painful adventures into trying to make a Java applet, some companies realize maybe sticking with common components and best practices recommended by Android and iOS are worth a look. This is the equivalent of admitting that maybe the Feds in dark suits doesn’t know your town as well as you do, but maybe they have a point. It’s a hard pill to swallow.

(Here’s a test to see how ready your design team is to embrace Federal-level thinking — are they ok sticking with San Francisco on iOS and Roboto on Android, even if that means the screens look different side-by-side? It’s worth a conversation, and remember you’re designing adaptive software UI, not living in Mad Men where you get to design magazine layouts to look perfectly identical in every scenario.)

On Android, this means celebrating widgets, understanding how different the notification center is from iOS, thinking about Chromecast integration, using swipes for navigation, Google Now integration, and a bunch of other details found on their great Material design site.

On iOS, this means thinking about how your app might benefit from 3D Touch, Touch ID, Handoff, and other iOS-only functionality, all while still being backwards compatible with older devices that might not have these features.

Designing for each platform takes a lot of work and deep platform knowledge, but it results in better products. Better products drive usage. Which can lead to more money and marketshare. If you do a dumb port while your competition embraces the platform, they will be at an advantage.

Final Thought: Brand Versus Experience

Anyone that’s been in in software long enough has seen this play out a million times. Consistency sounds good at first. Brand does too. But customers just want the damn thing to work the way other apps on their phone do. They really don’t care how your app manifests itself on a platform they never use anyway. The consistency argument doesn’t hold water outside of a board meeting or amongst novice (single-OS using) designers.

So when you override an OS share dialog, or use custom control that isn’t worth the cognitive overheard, or use a custom font because your creative director is really excited about this new typeface, I know your heart is in the right place. You’re trying to make something great. You’re betting on your brand. But you should be betting on experience.

And sorry, product designers. I know it irks you, and I know it hurts, but great experience often means ceding control to the Feds sometimes. Apple and Google have some good ideas, ones that your customers expect you to follow. And whether they’re right or wrong, well designed or not, they trump your local and state laws every time.

By UX Launchpad on October 2, 2015.

Exported from Medium on May 19, 2017.

Going Flat

UX Launchpad has been around for several years with the same per-seat billing plan. Up until now, one person had cost about a tenth as much as ten people. But we’re moving to a flat model instead.

The reasoning is simple: it takes us approximately the same amount of effort to teach to a room of 40 people as it does 4. And besides that, we prefer teaching at companies because teams of co-workers tend to be more fun than a collection of strangers.

We used to charge $10,000 for gigs like this, but we’d rather teach more people more often than hold out for a big, infrequent payday. We’re thinking $2500 plus travel expenses is a reasonable place to start.

Want to learn more? Learn more about our classes or shoot us an email.

By UX Launchpad on October 15, 2015.

Exported from Medium on May 19, 2017.

Design Explosions #3: NeuBible

Hullo! Design Explosions is a series by UX Launchpad, a company that teaches one day, fun, hands-on design classes. Issue #1 compared Apple Maps and Google Maps, Issue#2 went deep on the PBS Kids design, and today we’ll be discussing NeuBible. We’re not religious, but we love to learn from well-designed apps. NeuBible definitely fits the bill.


If you’re new to Design Explosions, you can learn more in this introductory post. Here’s the gist: Design Explosions tries to do design critique in a polite and gracious way. Here are our guidelines while analyzing designs:

The team that built this product is full of smart people.
There are many things we can’t know without joining the team.
We’re here to teach and learn, not judge.
Video!

The first two issues of Design Explosions were lengthy essays, and this one was going to be the same. But the more we explored, the more we realized text wasn’t going to do the app justice. We needed a video to show how some of the interactions and animations work, so here it is!


(Note: this works best in full screen on a mobile device!)
3 Notes

NeuBible is gorgeous, but I skipped those details in this video. There’s not a lot I can add to the visual design discussion other than “whoa isn’t this great visual design?”
For every 10 bold visual design directions I see, 9 (or 10) of them have untenable downsides. They load slowly, or their animations get in the way, or one of a hundred other issues appear that make them better suited as glamour screenshots in a static web portfolio than useful living, breathing, relied-on software. Yet NeuBible manages to have great visual and interaction design, all with risky but worth-it custom design patterns. NeuBible manages to pull it off, which is, as they say, about as likely as a camel walking through the eye of a needle.
Here’s a link to NeuBible’s website. Android users, their website says “We absolutely plan to expand to Android,” so fingers crossed! You can also find them at @neubibleco.
We Now Teach On-Site Classes

UX Launchpad was founded several years ago and until now it’s always been Seattle-oriented. With a handful of exceptions, we always preferred that people come to us and our beautiful city.

But we love traveling to teach on-site classes, so now we’re going to focus on them more. Classes are $2500 per team and our classes are customized to your team. Learn more at http://uxlaunchpad.com.

Lastly…

Thanks for reading! See you next time :)

By UX Launchpad on November 1, 2015.

Exported from Medium on May 19, 2017.

Software Has Never Been Better, and It’s Still Really Bad

Over the last few years, I’ve found myself in design debates where we end up talking past each other. “Smartphones are getting too complicated!” they say. “But they’re not as hard to use as personal computers in the 80s!” I say. But these points don’t contradict each other. We’re both right.

Last night I spent hours trying to respond to a recent essay that says that Apple is destroying design by focusing so much on “pretty” they have abandoned more important principles like “useful.” I wanted to take the kernels of truth in the article but add some nuance. Because they’re right: some things are harder in iOS than on Mac OS and Windows. But as a whole, software design has never been in a better place.

Which isn’t to say there aren’t improvements we should make. Oh gosh, there are so, so many. And no one’s saying that computing is effortless. Just that it’s better. And it will get better still. But it will never be good enough. And as a designer who likes to work, I find that tremendously exciting. Things always getting better but never being good enough? That sounds like job security to me, not a reason to be sad.

In business, you know you have a good idea that can make money when you find a need that no one else is addressing. Design is the same way. Find the thing that’s broken, figure out why it’s broken, and fix it. Designers should be problem solvers, not just complainers. Get to it. Make the next big thing that’s better than anything before, but still flawed. Then do it again.

By UX Launchpad on November 18, 2015.

Exported from Medium on May 19, 2017.

Introducing Jetpack

When William and I were learning to be designers, we did all the normal stuff. Books, lectures, college courses, chatting with people online, building products. But one thing was missing: we needed someone to believe in us. I believed I was better than my portfolio, or my work experience, or my connections. And that was frustrating. For too many years.

UX Launchpad is named “launchpad” for a reason. We don’t expect to turn you into an expert designer after 8 hours. The day is meant as a boost, a big first step, a way to fill you with the inspiration to know what design is and to want to keep learning. The problem is, it’s hard to keep the momentum going by yourself. It helps to have someone helping you on the way.

To solve for this, we’re announcing our new service. It’s not a fellowship, it’s not an apprenticeship, it’s not a mentor program, it’s not an internship, it’s not homework assigned on a schedule, it’s not a source of inspiration. It’s all of the above in a new package. We’re calling it Jetpack.

Sign up if you’re interested in learning more. This application process will be open until January 2016. [update: Jetpack is off and running!](#)

By UX Launchpad on November 19, 2015.

Exported from Medium on May 19, 2017.

The Mystery of the Missing Undo

For decades, software design professionals stressed the importance of some bedrock principles like discoverability, feedback, and recoverability. In other words, software should explain what’s possible, explain what’s happening when you take an action, and let you change your mind. Solid advice.

As a young designer, I read all the old-school research and best practices, so I consider a lot of this stuff ironclad. Irrefutable. Of course you need the ability to undo. That’s just a universal principle. Right? Well…

When the modern smartphone hit the market, undo mostly disappeared. The knee-jerk reaction of old folks like me was “if there’s no undo, it’s bad design.” Because for decades and decades, that was proven true. But are smartphones poorly designed? Worse than PCs? Well… it’s complicated.

I find it useful to force myself to solve a design problem when I’m annoyed by someone else’s solution. So let’s figure out where to put the undo button. We’ll do it together. It’ll be fun.

Imagine we’re iPhone designers in 2006 and we’re trying to make a brand new operating system for Apple. Let’s say we’ve decided we’re going to use multi-touch, so we know stylus, mouse, and keyboard aren’t available. Oh, and the screen size is tiny. And it’s hard to type on.

So. Where does undo go? This is not rhetorical. It’s a real question that real designers had to really think about. Where in the world can it go? What would you do? Pinging Don Norman and Tog: it’s provocative and lucrative to say “Apple is destroying design,” using examples like undo but really, what would you have done? Any designer should be able to answer this question, or at least walk through their process towards finding a solution.

Ok, so one idea is to put in a traditional menu structure like Windows and Mac OS. But the touch targets would need to get big, large enough to tap with your finger, and presumably the control would be there at all times. Pros: you could always have undo/redo. Cons: it would take a ton of chrome to make it work. Plus those menus would get really cluttered. Hamburger times a thousand, if PCs and Microsoft’s Ribbon design are any indication.

So let’s focus on those insights a bit further. Do you need undo while you’re reading a book on Kindle? Do you need undo when browsing websites? Instagram? Twitter? Facebook? At first glance, probably not. With one exception: text input. So let’s drill down on that.

Lots of apps don’t accept a lot of text input. Kindle, for example, lets you search using a text field. Twitter lets you write 140 characters. Instagram allows more, but its focal point is photos. Facebook posts can get long, but an average user’s time on Facebook is weighted more heavily on reading than writing. So where does that leave us?

It’d be a mistake to say “everyone’s on Instagram, Snapchat, and Tindr anyway, so undo is dinosaur and no one will mind if we remove it.” No, that’s provably false. There are plenty of situations like emails, status updates, messaging, or web apps where undo/redo is a priority one feature. It causes problems that people notice. It’s clearly worse to not have it than to have it.

But it would also be a mistake to over-index on those very real, very painful, very obviously inferior mobile experiences if it means you’re bringing a menu-based paradigm into your smartphone OS. One of the best things about smartphone chrome (when done right) is how often it gets out of the damn way. It knows not to bother you in Kindle. It knows to tuck out of the way when you’re reading an article like this in a browser. It’s doing its best to be transient instead of permanent, and that’s great. (Let me stress: only when done well. It’s a delicate balance.)

Ok, so where does this leave us? It’s complicated. Great design always is!

To start, we have to understand it’s possible to simultaneously believe these two true statements:

Lack of undo is a design flaw on smartphones
All things considered, it’s the best overall approach
When we critique design, we have a bad habit of calling out problems without analyzing the pros and cons of the solutions. We put all our eggs in the basket of “I found a problem” and often ignore “I tried sketching out some other ways to do it and here’s what I found.” It takes more time to solve than to snipe.

You also look really smart calling shenanigans with razor sharp wit. Whereas proposing a solution opens yourself up to criticism, and that’s scary! The return on investment favors yelling “fail” on Twitter over working hard on an alternate solution and sharing it with the world. Snark is easier, makes you look better, and might go viral! Of course people gravitate to it.

With this particular issue, I’m speaking from direct professional experience. I grappled with The Undo Problem as a part of Windows Phone and later when I was designing Word, Excel, and Powerpoint for mobile. A lot of smart people worked this problem over many years, across many companies. I’m here to tell you that Apple’s approach (“undo isn’t usually possible but sometimes shaking your device like a crazy person works”) is the best on the market. Isn’t that nuts?

But you don’t have to take my word for it. Try it. Take out a sketchbook and try to design an undo/redo pattern for smartphones. It’s a great exercise and it’ll help you see many more shades of grey when it comes to design critique.

By UX Launchpad on November 19, 2015.

Exported from Medium on May 19, 2017.

A Proposal: In-Context Undo

(This post originally appeared in Microcosmographia, William’s email newsletter described as being “about honestly trying to understand design, technology, and humanity.” It’s a response to yesterday’s The Mystery of the Missing Undo.)

Jon, My partner in UX Launchpad, put his prodigious design insight and experience into a post about why undo has been demoted in mobile interfaces. I tend to agree that for designers, just complaining is not going far enough — we should try to understand why smart people designed the system the way it is, and whether there really is a better way. All while being realistic about all of the pressures and compromises that shipping design teams need to balance.

I hate shake to undo so much. Here’s a sidebar from my book about it:

Shake to Undo is, honestly, kind of terrible. Here’s how it’s supposed to work. In interfaces without a dedicated Undo button, you can flail the device back and forth to summon an alert with an Undo button and a Cancel button. In reality, people who know about it can never seem to reliably produce the right flailing motion to trigger the feature, so they end up repeatedly flapping their device about until the alert finally appears. People who don’t know about it have to live with their mistakes, wondering why the system doesn’t offer an undo feature — until one day when they’re absentmindedly gesticulating with their phone hand or momentarily holding their iPad at their side while they get on the bus, and then suddenly the thing is inexplicably suggesting that they undo their hard work. If you combine all three situations, you have a feature that’s unreliable, undiscoverable, and presumptuous. What would work better? If that were easy to answer, we probably would have seen it by now.
With a couple more years to think about it since having written that, if I’m so smart, what do I think would be better? I took Jon’s exhortation seriously and did some sketching. You know what? I’m still convinced. I don’t think there is a good system-wide standard way to offer undo on smartphones. But! I think there is a next best thing.

A system-wide standard interaction isn’t the only way to solve a problem on a platform. Lots of problems are instead dealt with via recommendations and precedents. For instance, Apple didn’t make a standard iOS document picker because it couldn’t work for every single app’s needs. But they did write advice about how to deal with documents into the iOS Human Interface Guidelines, and they did provide examples of good pickers in their iWork apps. Third-party developers take these precedents and use them to craft document-picking experiences that are right for their own apps.

What I’d like to see Apple encourage in the iOS Human Interface Guidelines, and build into their first-party apps, is in-context undo. If you’re giving visible feedback when a user completes an action, like you should be, you should often be able to build the undo interaction into that feedback.

A deleted or moved item leaves a temporary gap, inside which is a little Undo button.
A sent message gets a temporary little X button in the corner for taking it back.
A changed item gets a Revert button added to its table view until you change another item.
The keyboard gets an Undo key specifically for undoing typing.
Et cetera. This is more UI design than a simple Undo button that sits in the toolbar, for sure. You need to design many different undo buttons instead of just one, each made to work in its own context. You have to make it really clear, so that users aren’t constantly searching around for whether there might be an undo button hiding somewhere. Once the system is fairly sure you’ve moved on and you aren’t going to undo, it needs to gracefully get out of the way. And in most apps it would only save you for one level of undo, not the many levels we have come to expect on the desktop. But I think it’d be worth it. Certainly better than shake shake forever.

By UX Launchpad on November 20, 2015.

Exported from Medium on May 19, 2017.

Material Design’s Bob Dylan Moment

By the summer of 1965, many considered Bob Dylan the spokesperson of his generation thanks to his acoustic folk music. So you can imagine the shock and dismay that some of his fans felt when he went electric that year at the Newport Folk Festival.


On the live recording, you can hear the booing of a shocked and angry audience witnessing their folk hero change his sound. On Wikipedia they’ve titled the event the Electric Dylan Controversy. It’s fascinating to read how a natural artistic evolution came to be such a divisive moment.

Since its unveiling, Google’s Material design language has been widely praised as a thoughtful and beautiful approach to OS and app design. It speaks with a clear voice, one that’s notably different from Apple’s iOS and Microsoft’s Windows. One of the defining characteristics, and one that has always set it apart from iOS, is that navigation doesn’t go on bottom. Google has been very clear that it goes on the top.

As an example, here’s how Twitter looks today on iOS and Android. iOS puts the nav on bottom, Android puts it on the top.



Not all apps are complex enough to require navigation, and not all apps follow this pattern. But it’s pretty common. One of the most obvious ways to make your Material app feel out of place is to use an iOS-style bottom nav. So you can imagine everyone’s surprise when Google+ shipped this:


Well.

One important detail about this change: it wasn’t done by a rogue team deep within the company. This is an official Google product, one that many people will see and imitate, and they understood exactly what they were doing. (Also, it turns out @lukew was involved, which adds credibility to the design. He didn’t just fall off the UX turnip truck, to say the least.)

I won’t dwell on the details, let’s just say that the Material die-hards were just as upset as the Metro die-hards who were just as upset as the Mac OS 9 die-hards who were just as upset as the DOS die-hards who were as upset as those poor folk fans who booed when Bob Dylan “sold out.” Things change.

But let’s not worry about them. Let’s analyze this new design to understand how they landed here. Let’s set aside topics like “will this revive Google+?” (unclear), “are they just trying to align with iOS and web?” (it’s a factor, but not the only one), and instead focus on what features they placed, where they placed them, and why they might have made those decisions.

Here’s what Google+ looks like today: Home, Collections, Communities, and Notifications. (It turns out Notifications is not its own page. It slides over whatever page you’re on, which is a nice innovation.)


Ok, let’s take this design as a baseline and figure out how to gracefully remove that pesky iOS-style bottom bar. Where can we put Home, Collections, Communities, and Notifications? I see a few options:

Put them in the hamburger
Put them in tabs (as shown in the Collections screenshot)
Remove them completely
… and of course there are blended approaches like putting notifications in the chrome and tucking everything else in the hamburger. But whatever we do, #3 is out of scope. That would make a whole new app. And maybe that’s a reasonable direction, but that’s not the design exercise before us.

What about #2? I’ve explored that direction for other apps and it’s painful. You end up with a stacked set of tabs, global on the top and per-screen (such as collections) just below. This stacking means swipe gestures don’t work anymore. Plus you still have the top row of chrome, leaving you with a total of three. Yuck. Look at this:


So that’s not super great. We could argue that the second row could be a dropdown filter of some kind. That could work, though a dropdown or a button will lead to less engagement. For new features, especially ones that benefit from discovery, that’s not something to take on lightly.

Which brings us to option #1: putting everything in the hamburger. And there’s a huge precedent backing up that approach. After all, it’s what most apps on Android do, and the tradeoffs are well known: the hamburger reduces clutter, but results in less engagement. It’s a great example of “out of sight, out of mind.”

Would we be comfortable throwing Collections and Communities into the junk drawer? That wouldn’t be quite as impactful as removing them from the app, but it’s pretty close in practice. People can find those items, but only if they really seek them out. And the software is sending an implicit message that those are optional sides, not the main course.

So here’s what I’d do if I couldn’t use the bottom nav:

Put the notifications bell in the top chrome, next to the search button
Use standard tabs for Home, Collections, and Communities
Put the Featured, Following, and Yours sub-nav in a dropdown
But you should never fool yourself into thinking you’ve found the “perfect” design. There are always tradeoffs. So what are the tradeoffs of this bottom nav-less design compared to what Google+ shipped this week?

A more cluttered top bar
The text for the tabs probably won’t fit in English, let alone German
We can’t use left/right swipes because global nav is using them
Discoverability of the community filters goes way down
It’s hard to jump straight from one section to another (tap targets are far)
But at least there’s no bottom nav, for what that’s worth
Whereas the reverse is true for what Google+ shipped:

The top bar isn’t as cluttered
The text fits in all languages [edit: I have been informed that it doesn’t fit in German, even on the website. This is even more evidence that the text tab model, as nice as it looks, has some real limitations.](#)
We can use left/right swiping per view for per-item actions
Discoverability of community filters is increased
It’s easier to discover (new users) and navigate (experienced users)
But there’s a bottom nav, and boooooooooooo bottom nav!
Bob Dylan and the Google+ team are both making the best decisions they know how, with the information they have. And in both cases, people are upset, shouting Judas, hating that they’re selling out, and not privy all the facts and details. We can’t know what will happen next. We only know that things change, and that design and art are always more nuanced than “change is bad.”

By UX Launchpad on November 20, 2015.

Exported from Medium on May 19, 2017.

Open Letter to People Who Send Feature Requests to Product Team Members

To Whom It May Concern,

Thank you so much for taking the time to write. Everyone wants to feel they’re having an impact, so emails, tweets, private messages, and hallway conversations at conferences remind us that people care about our work. That’s huge.

I’m sorry I can’t say much more than that. I can’t tell you if your feature is in progress, or if we’ve decided never to do it, or if it’s brand new to us and we’re all throwing a party in your honor of your stellar idea. Why? For legal, competitive, and intra-personal reasons. We haven’t come up with any way to signal “thanks!” without breaking our contract or causing trouble.

For example, let’s say I want to do feature X. But my product manager really does not want to do feature X. We’re doing a delicate dance every day where we’re working out the best overall answer as partners. But if you tweet me and say “do feature X!” and I say “working on it!” or retweet it, or tap the heart icon, I’m doing three things wrong. First, I may be breaking my NDA. Second, I’m publicly disagreeing with my team. Third, and worst of all, I’m making it seem like I get to make decisions unilaterally. That’s not how software works unless you’re a one person team.

So, again, please keep the feedback coming. Please know that we read all of it, and yes, the feedback often finds its way into our internal discussions. But it’s not really a good idea for us to respond. We’re sorry about the silence.

Sincerely,
Someone on a product team who has signed an NDA

By UX Launchpad on November 21, 2015.

Exported from Medium on May 19, 2017.

Funny story: the wikipedia article I linked to mentions how various documentaries covered the booing, but I couldn’t find video footage. I think it’s been scrubbed from YouTube. This 2 minute clip provides an inkling: https://www.youtube.com/watch?v=S1TKUk9nXjk

But it’s worth pointing out that it wasn’t unanimously rejected — there were pockets of people that were upset, but plenty others were perfectly fine with the change.

By UX Launchpad on November 23, 2015.

Exported from Medium on May 19, 2017.

Designing a Sketching App for iPad Pro

I recently got an iPad Pro and Apple Pencil. I’ve spent most of my time in Notes and a few Adobe apps, which got me thinking: how would I design my ideal sketching app? Here are some pages I sketched out.


Early on you want to know what problem you’re trying to solve. But sometimes you end up with something vague like “make a world class app that customers love.” I find it helps to write goals and non-goals, followed by pain points I’m trying to address.


This is something I’ve written about several times before: there may be a market for apps that promise not to update (other than bug fixes). So many apps start as straight-forward tools and then bloat into something less delightful. Imagine downloading an app and knowing it wouldn’t radically change on you. This sketching app seems like a good place to try this novel approach.


With my core principles in place, I set to work figuring out how everything would actually work. There are a few design challenges to address here.



While sketching, I texted with my friend and UX Launchpad c0-founder William. He had some good ideas:


That was page one, here comes page two:


This was definitely the most compelling tradeoff to consider. Should the UI be there at all times, so undo is one tap away? Or should it auto-hide, meaning every undo requires two taps, one to get the UI and one to undo? My gut said one tap, even if that means the UI is always there.

Next I aimed my attention at the document management. A lot of apps get a big tricky with this, which makes it harder to pop open the app and get drawing. I wondered if I could make something quicker.


After drawing this out, I started to warm to William’s original “smudge” idea. If the UI wasn’t there, then you could tap and drag left to undo, or tap and drag up to get to other options.

Whereas if the UI was there at all times, I’d need to find a way to show three different buttons: undo, redo, and other. Which starts being a bigger palette than I’d like. So we’ll assume the Dynamic Smudge Palette for now.

I was curious how document management would look with the Dynamic Smudge Palette in place:


Interesting. Maybe when looking at pages, you could pinch and spread to make the gallery thumbnails bigger and smaller. But that opens up other tradeoffs to be considerate of:


At this point I wanted to put a few more lines in the sand to see where that would nudge the design…



So that’s where I’ve gotten so far. A bunch of assumptions, gut feelings, insights, general principles, goals, and non-goals. Some lines in the sand to give us something to play with and try out.

Next up: it’s time to move from theory and opinion into testing how some of these assumptions actually hold up in the real world. Stay tuned!

Edit: part two has been posted here

By UX Launchpad on December 5, 2015.

Exported from Medium on May 19, 2017.

Designing a Sketching App for iPad Pro, pt 2

[This is part two in a series. Here’s part one if you haven’t read it yet.](#)

Prototyping is a funny word. Like design itself, it can mean a range of things, so it’s important to get everyone speaking the same language before using it. When I say “prototyping” I just mean “show me how it will feel.”

In a perfect world, you’d be able to bust out a fully functional compiled app, ready for the App Store, every time you wanted to test a design direction. But in the real world, code is hard and takes a long time. So instead we use a variety of ways of testing out how our vision feels. Some are super easy, like making simple tap targets animate slides in Keynote. Some are more involved, like creating something using FramerJS or Pixate.

The key, every time, is making sure your prototype tells the story in the best possible way but takes the shortest amount of time. If using Pixate will take you one day and tells 75% of the story whereas using XCode will take you two weeks while telling 100% of the story, stick with Pixate. If using Keynote tells 65% of the story and it only takes one hour, your choice is clear. In this stage, speed wins.

After yesterday’s post, I kept drawing so I could think through the tools palette. And just like yesterday, I realized I needed to switch into prototyping mode.


But as I began drawing up how I was going to approach the motion study, I realized I didn’t need a prototype yet. Drawing up the first storyboard for “Prototype #1” was enough to prove out my preferred direction, without placing anything on the After Effects stage or writing code. Total time: about 10 minutes to come to a decision I felt good about, at least for now. Yes!


Maybe this is a bad direction. Maybe I’ll end up revisiting it. But one of the joys of doing this kind of work for two decades is I’ve learned to trust my experience and intuition. When I was starting out, I may have agonized over making an executive decision so quickly, but what’s the worst that can happen? Maybe I’ll have to change my mind. Big whoop! Move on, see where it gets you, push until it breaks. As Steve Jobs told me, “Go!”

Next I tried to figure out what would go in our little undo/redo tool palette:


There I go, changing my mind when presented with more information. All part of the process. So how am I going to deal with this extra UI? Where can it go? Here’s where my sketching took me:





And that’s where my sketching has brought me so far. Next up: thinking though document management. After all, a sketching app can’t just be a single page. I’ve made the decision to drop a new user straight onto a blank canvas, but that means I need to help them find their way “up” in the navigation to the document browser. There be dragons. I have the burns and scars to prove it. But my trusty sketching process will keep us moving forward.

See you next time!

[update: here’s part three](#)

By UX Launchpad on December 6, 2015.

Exported from Medium on May 19, 2017.

Designing a Sketching App for iPad Pro, pt 3

[This is part 3 in a series. You can read part 1 here.](#)

The creative process is notoriously mysterious. But working designers know it’s more science than art. It doesn’t work to go up a mountain and hope for a muse to visit you, you just have to do the work. Trust the process, as they say.

But sometimes the process takes you in interesting directions, and that can make it feel wild and unpredictable. For example, I thought I was going to think about document management next, but that’s not what happened. Instead, I took a screenshot of Apple’s build-in Notes.app and it got me thinking more about my proposed canvas design.


This isn’t the standard view, it’s actually two steps away. If you go into full screen mode (step one) and then swipe the tool palette out of the way (step two) you’re shown this. And it’s actually pretty close to what I want a sketching app to be. If this screen had undo/redo I’d probably be happy.

But it doesn’t. And so I play a sad trumpet and design my own app instead.

So I took this design and threw together some simple icons. I would like to stress that I am not happy with them from a visual standpoint. But that’s ok for now, because I’m just trying to understand the UX and the overall feel.

So here’s what I mocked up first:


Undo and redo are split out from the buttons for the tools palette and document management. Also I made undo/redo bigger but immediately thought it looked wrong. Also, having the UI split into two sections, an idea I liked yesterday, just didn’t feel like a good idea today.

I tried another mock where I tucked everything together and made the canvas #FFF white rather than the off-white in Notes.app. I also dimmed the redo button for its default state. It felt better.


Then I stared at the palette for a while. It feels kind of … heavy, doesn’t it? So I spent some time thinking about the value of each button, for the tenth (and not the last) time. I definitely need undo, which is the whole problem I have with Notes.app’s full screen mode. And if there’s undo, you need redo. So those are in.

Next is document management. I could remove that button by using a pinch gesture the way FiftyThree does with Paper. But I’ve watched so many people struggle with mis-pinches in Paper (two 3 year-olds, an 8 year old, and multiple adults), not to mention the discoverability problem of hiding core functionality behind a gesture. So I don’t want to do that. And the tool palette? I’m not sold on it. Let’s put that on hold for now.

So the big question is this: should the UI be there at all times, or come and go when you tap? I’ve explored this question across countless features, and while I use “shy chrome” sometimes, I’m keenly aware of its drawbacks. And if I have a principle of this app being as fast as possible, which I stated in day one, that means UI should be there at all times. Otherwise you get in that finicky situation where you want to undo but you have to tap or swipe randomly on a blank canvas to get to the feature you want. I don’t want finicky. I want as fast as possible and muscle-memorized, even if that means there’s always UI on the screen.

Will the UI ever get in the way? Yeah, but I’ll make it movable. Maybe you can place it in the four corners, the way Facetime’s self-camera works. Or maybe I’ll also allow it in the middles, for a total of eight snap points. It was around this time that I realized top right would get in the way of normal note-taking. So I tried the bottom left instead.


And that felt good enough to send to my friend William, who had a few minutes to spare before going back to his video game:


Then I got in this Twitter conversation with Curt Clifton, engineer at esteemed iOS/Mac house The Omni Group, who had some important insights to share:


In my original brainstorming, I had played around with the idea that Pencil and finger would trigger different results (Pencil = drawing, finger = UI) and it was still in the back of my mind as a possibility. But Curt’s feedback was a good reminder that an approach like that might be a bit too clever, and perhaps not even accepted in the store.

So: permanent UI is feeling more and more important, and at least 3 of the 4 buttons are feeling like requirements. Next up: document management! Unless I get on some other topic, as we saw happen today :)

[edit: here’s part four](#)

By UX Launchpad on December 6, 2015.

Exported from Medium on May 19, 2017.

Thanks very much for sharing, that’s great to know! I was born in Germany and my brother married a German woman, but my understanding of language/UI issues is very superficial. It ends at “German words are longer.”

And your insight is a great example of challenging assumptions. I’d assume “of course 100% of people want the UI to work in their language without modification” but I could see the reality being a more nuanced “do your best, of course, but in small doses it may be ok to use English rather than change the entire UI to accommodate German.”

By UX Launchpad on December 6, 2015.

Exported from Medium on May 19, 2017.

Designing a Sketching App for iPad Pro, pt 4

[This is part 4 in a series. You can read part 1 here.](#)


The app needs some document management, so I drew this. You’d tap a button to get to your docs, then another button to go back. Makes sense.

Then I thought about the transition between screens. Maybe the button could grow to fill the space. Maybe the document area is another color. Maybe the UI stays in the same place on both views? After all, we need a close button on the new view to dismiss it, right? (spoiler: no. I realize that later on.)


With the placement and animation figured out, I wanted to make sure I understood the full flow for a new user making a page, then going to the documents screen to make a new document. So:


Looks good to me. So then I wrote out some principles about how I wanted the documents screen to work. No search, no titles, no dates, no metadata. Purposely feature light. And over there in the bottom right corner is where I realized I don’t even need a close button in the document browser. In hindsight, it’s obvious why not. But you’ve gotta sketch your way to realizing obvious things like that.


The biggest remaining questions are around tool management. Should I allow multiple pens, pencils, and highlighters? Should I let people change color? What about an eraser? I have a gut sense for what I think I should do. Three built in tools: pencil, pen, yellow highlighter, none with configuration options. No colors. That’s where I’d like to start.

But I get skittish when I get too far down the design path without involving an engineer. I’ve come to expect the execution and assumptions (though not the insights) to change pretty dramatically once engineering gets involved. So if you know of any who are interested in this app, let me know! Consider these designs open sourced. Feel free to take the insights and run with them any way you’d like. And if you end up shipping something, send me a link :)

By UX Launchpad on December 9, 2015.

Exported from Medium on May 19, 2017.

The Apple Pencil Charging Situation

Lets discuss how Apple might redesign the process for charging an Apple Pencil. As of today, it looks like this:


Photo credit: https://twitter.com/J0hnnyXm4s/status/664938834791153664
Understandably, people are wondering if this is really the most well-considered approach. But before we get into that, I’d like to tell a story back from when the App Store changed their “FREE” buy button to say “GET.”

I was chatting with someone when the news broke, and it went like this:

Him: I can’t believe they went with “GET”
Me: Yeah. Quick question though: what would you have done?
Him: Something better. Less ugly.
Me: Yeah, but what?
Him: Well “FREE” was best.
Me: Yeah, but they can’t use it because of the European Union.
Him: I know. But it was the best approach.
Me: Agree. But illegal in the EU can’t be considered “best” anymore.
Him: “GET” is so dumb though.
Me: Yeah. But what would you do? What text string would you use?
Him: Maybe “Download.”
Me: Yeah, but that doesn’t fit. The buttons are tiny.
Him: I dunno, but I’m sure they could have done better.

I dunno, but I’m sure they could have done better. That’s what passes for most design critique on the internet these days. It’s why I started Design Explosions, to push back on this kind of shallow analysis.

Sure, maybe they could have done better. But I’m sure there were dozens of people involved with the “GET” decision, thinking about it over weeks or months. And maybe they shipped a subpar design, but you have to admit they at least thought about it longer than the time it takes to fire off a tweet.

Designers: until you propose something better, and own up to each of the drawbacks of your own proposal, you sound like Bart Simpson in this clip:


So now let’s talk about the Apple Pencil. Does it look silly? Yes. Can it break or bend when it’s sticking out of the iPad Pro precariously? Yes. Is there a better design they could have chosen? Probably, and I’m sure they’re working on it. Let’s to try to predict what they might do.

Idea #1: Charge the Pencil Via a New Battery Slot in iPad

One approach is to make the iPad bigger, with a hole in it, so you could store the Apple Pencil inside. This means you’d have a place to put your Pencil and you’d have a snug way to charge it. These would both be very welcome improvements.

However, it would also make iPad taller or wider, maybe even thicker. That would be strange for people who have no need for the Pencil. And to make it charge as easily as possible, you’d need to design the Pencil to receive a charge without plugging into anything. I know nothing about batteries or charging technology but I assume there’d be some drawbacks there. Maybe a larger Pencil? Maybe slower charging? There are always tradeoffs.

You could decide to keep the Lightning port, but then you’d need to slot the Pencil into it. But the port would be at the bottom of the “well,” meaning you’d have to grasp the tip of the Pencil and push it in, pointy side jabbing your finger, in order to slot it into place. I’m sure there are ways to make this better, but on the face of it, it seems troublesome.

Pros: a place to put your Pencil, built-in charging
Cons: bigger iPad, maybe a bigger Pencil, fiddlier, ouchier, slower charging?

Idea #2: Something More Flexible, Maybe a Cord?

The current design looks super awkward because the Pencil is jutting out like an overzealous fencer. But what if there was a way to somehow bend the Pencil a little more so it’s not strictly perpendicular? Maybe a cord?

We could put the bendy bit into the Pencil itself, like a vacuum cleaner cord. Of course, that would make it larger. We could provide an adaptor of some sort, but then you need to carry it around with you. So those are options but I wouldn’t call them better.

Pros: doesn’t look as awkward in screenshots on social media
Cons: extra stuff to carry, or a larger Pencil (presumably)

Idea #3: Maybe It Could Charge on the iPad Body?

What if placing the Pencil on the iPad automatically powered it up? That would be really cool. But that would mean redesigning the entire iPad to be a charging station. That’s a pretty giant undertaking, and assume it has battery implications on the iPad itself, for a pretty uncommon use case. And it would probably slow down charging.

Because remember, the current charging speed is pretty great. 15 seconds gives you 30 minutes of battery life. So while those 15 seconds look a bit goofy while charging, it’s only 15 seconds. Maybe leave it in for a full minute and you get 2 hours. Those are impressive numbers that you don’t want to mess with if you don’t have to.

Pros: sooooo cool
Cons: Probably affects battery life, charges slower, makes Pencil bigger

So. Does the Apple Pencil charging situation look ridiculous? Absolutely. Can Apple do something better? It depends how you define “better.” They could dial up the coolness factor, or dial down the silliness factor, but everything has a tradeoff. For example, they could end up with slower charging speed, or larger equipment, or dongles to carry around.

So I have no doubt they have ideas for how to improve this. But I know they’ll do it considering multiple factors, not just focusing on one. The former is called design. The latter is called complaining.

By UX Launchpad on December 13, 2015.

Exported from Medium on May 19, 2017.

The Political Spectrum of Design Thinking

Designers are like politicians. We’re deeply dismayed by the status quo, and we’re sure that everything will be fine as long as we’re put in charge. So we get the job and realize — surprise! — everything is a tradeoff and is highly subjective. We do what we think will be great, but for every decision we make, some things get better and some get worse.

Everyone talks about making things “great” as if they alone had the epiphany that great things are better than not great things. As if the concept of great is objective truth that everyone agrees with. As if the big problem with the previous politician or designer is that they simply forgot to be great. It must have just slipped their mind. Whoops!

Fortunately, there are some objective truths in politics that help us understand the tradeoffs of different governing philosophies. In broad brush strokes, Left Wing politics tend to be more concerned about society as a whole, which tends to lead to fewer individual freedoms and a more powerful government. Right Wing politics instead seek to highlight and empower individual rights and freedoms, which tends to mean as little government as possible.

Both sound good to me, so let’s test the approaches using tax policy as a tangible example. The right wing would argue that taxes should be as low as possible or even eliminated. This means more money in the hands of businesses, which could mean they can create more jobs. It also means more money in everyone’s bank account, which means more of the spending that can power an economy.

But the left wing’s counter-argument is with fewer taxes, you can’t build public transit, or fund public schools, or provide assistance to young mothers, or people who are sick, or old. They believe that a strong social fabric helps everyone, from the poorest to the richest. They admit they’re advocating for higher taxes, but they think it’s worth it.

And here’s the thing: they’re both right. When you lower taxes on the middle class, that does help the economy grow. But when economic inequality gets out of whack like in 1929 or 2007, it can have disastrous effects.

But people on the far left and far right argue as if the other side is recklessly trying to destroy the world. That’s not fair, and it’s not true. In reality, both sides are just doing their best to help design our society, and they happen to have different a philosophical approach. That’s healthy.

So what about UX design? I think the left/right paradigm still works. The Left Wing design philosophy is epitomized by Apple. Their argument is if they have more control over things, and higher prices, they can make a better overall experience. You can’t easily open Apple hardware or install custom batteries, but the products are thinner. You can’t install apps on iPhone without the App Store, but they don’t have as many viruses or malware. You can’t easily install Mac OS X on a Windows laptop, but on a Mac laptop, It Just Works. No driver config required, so says the theory.

The Right Wing philosophy used to be Microsoft, but they’ve moved hard left recently. You could also say Google’s Nexus phones that run Android, but they’ve also moved somewhat to the left by wielding greater control over the hardware/software experience. The best example of the Right Wing of UX is probably desktop Linux or heavily modified Android. You can do anything you want on them without anyone else’s permission.

And like politics, it’s not that one side or the other is perfect. It’s that they’ve settled on different tradeoffs, so they’re great in different ways. Apple’s “closed” products (such as iPhone) have objectively higher satisfaction ratings and fewer viruses, so that’s great. But open source software (such as Google’s Android) has objectively more devices in the market, drives costs down, and is at the heart of The Internet of Things. So that’s great too.

I have friends on the left and right side of the political spectrum. They’re not stupid. They’re not trying to destroy their country. But they have different approaches for how to make things “great.”

Similarly, designers are split between left and right wing and they all have good points. Left wing design thinking puts the platform first. When designing for Android they want it to be 100% Material. When designing for iOS they want it to use the very latest Apple tech like 3D Touch. They’re out to win a design award on each platform, meaning two different designs.

Right wing design thinking is more brand oriented and tends more experimental. These designers design the best UI controls for their unique scenarios and brand guidelines, even if those controls are custom built and can potentially clash with the platform. A good example is foregoing Apple’s standard Share Sheet to design something that more closely fits their app’s branding, but loses out on a lot of the benefits of Apple’s Share Sheet.

As always, both sides have a point. Branding is important, but so are platform conventions. The debate is not between objective “great” and “not great,” because if it were there would be nothing to argue over. The debate is far more messy because it’s between subjective tradeoffs we’ll never fully see eye to eye on. And that’s ok if you let it be.

In DC they talk about “reaching across the aisle” to work with people who you don’t agree with. In software, it’s the surest path to greatness I know.

By UX Launchpad on December 23, 2015.

Exported from Medium on May 19, 2017.

A Desk Job For My Child

My oldest kid was born a few months after the first iPhone went on sale. He and the iPhone are currently eight years old and will turn 20 in the year 2027. It’s fun to think about how his school and job might look then.

When most people think about the future, they often lead with futuristic words they’ve heard. Jetpacks! Self-driving flying cars! Space colonies! Another approach is to take today’s world and spiff it up a bit. iPhone 18! Android Y! (York Peppermint Patty) Jenna Bush running for president!

I don’t think either approach has a very good track record. Instead, history tells us that seemingly mundane shifts in culture and attitude can lead to extraordinary things that we did not see coming. And the biggest obstacle to seeing the future clearly is that we’re extraordinarily bad at forecasting, starting with what makes us happy.

I have watched The Surprising Science of Happiness, a popular TED talk, probably a dozen times, I adore the book, and I quote the research to myself and others multiple times a day. I’m one makeshift shrine away from declaring it as my official religion. I’m that into it.

The summary of the talk and the book is this: not only do we not inherently understand what makes us happy, when we try to make ourselves happier, we’re pretty much 100% wrong. Always. It’s a giant flaw in the way our brains are wired, but once you understand the science, you can exploit it.


So what does this have to do with forecasting the future, my child’s eventual desk job, and UX design? Everything, and it all starts with listening. Studiously. The kind of listening where you accept what people are saying without your own bias springing into action, yelling “that can’t be right because of my long-standing perceptions about the world!” It’s really hard to suppress your bias, but I’ve been practicing and learning a lot as a result.

For example, my kid wants an iPhone. Not surprising, right? But here’s why: it’s the only way to get an Apple Watch, which he wants more. But not the currently shipping one. He wants sort of an arm-computer: a bigger screen, always connected to the internet, capable of playing games, but strapped to his arm. Here’s his quote explaining why he’d choose that over an iPhone:

“With an Apple Watch, I wouldn’t have to slide it into my pocket. I could just have it at all times but I could still text or play games.”
My logical brain wants to argue with him. The v1 version of Apple Watch can’t even keep the screen on for more than a few seconds. And even if it could, your wrist is an awkward place to navigate software for more than a moment or two. I can’t figure out what he’s getting at, but that’s because I’m thinking of v1. He’s thinking years out, and he’s not particularly enamored with the phone form factor. Instead, he prefers tablets.

I asked which he would buy: a tablet or some sort of laptop? First he said iPad Pro because it’s cheaper. So I told him to ignore money, and an interesting thing happened. He said “then I guess a Macbook Air or something?” before coming up with this:

“Actually I’d pick an iPad Pro because I’m used to the iPad controls, unlike computers. It’s a lot harder to control games on the computer.”
Ok, what about productivity stuff? Writing term papers, or working in an office job in the future?

“Well, the iPad Pro doesn’t come with a keyboard but you can add one. I don’t know what things will be like in the future but they’ll be more like an iPad Pro than a Macbook.”
Again, my logical mind springs into action here. iPad Pro has worked really well for me, but there are productivity scenarios where the Macbook is better. But, again, I’m thinking about v1. My boy agrees that keyboards are needed sometimes, but they don’t justify complicating the entire operating system. Have you watched a kid try to use Mac OS X or Windows recently? It’s hard. Really hard. The kind of thing you’d* go to great lengths to avoid, like DOS or the bash shell.

My boy talks a lot about games, as you’d expect from an eight year old. But you can see clear trends in the kinds of games he and his friends like and don’t like. Tons of buttons and complexity, like traditional mouse and keyboard PC games, aren’t super popular because they’re too complex.

But that doesn’t mean mobile games aren’t complex. For example, Monument Valley is a challenging Escher-style puzzle game that both my eight and three year olds love. But its complexity is earned fairly through the puzzle design, not in figuring out some obscure control scheme. Check out this screenshot:


You can make the character move to the little pressure plate on the side by simply tapping on it. The game uses path finding rather than forcing you to manipulate a D-pad or let you fall off the sides. That wouldn’t be fun. It would be superficially making the game harder for no real benefit.

We talk about airports practicing “security theatre” where they’re making us feel better without actually making us any safer. Games often do the same thing with baroque interfaces and control schemes. There will always be a market for people who prefer that kind of game*, but like Grateful Dead fans, I suspect their median age will only go up.

My kid thinks he wants to be a robotics engineer or a software designer. This presumes he’ll be using a variety of computers in various forms to get things done. Text input will always matter, which implies a keyboard. Communication, whether done via words, emoji, voice, animated gifs, or whatever we invent next, will matter as long as humans need to collaborate.

But beyond that, I really don’t know. We used to think all big computers would be mainframes. We used to think having computers in the home would be like everyone having their own nuclear power plant. We used to think operating systems would disappear when the web took over. We used to think installable apps were over, then we thought the web was over. Wearables was hot, then it was a dud. The Internet of Things is going to be big, and is already creeping up in significant ways, but we don’t know exactly how it will look yet. Facebook was supposed to taper off, and when it didn’t, we thought it was going to stay “cool” forever. Wrong and wrong. I clearly remember the brightest tech and design minds writing off Kindle v1, saying “how could you possibly improve on physical books?” Chuckle.

So I don’t know what an office job looks like in 2027. But I’d take a huge wager that some key details are going to completely blindside us. Then, a few years afterwards we’ll get used to the sea change, take it for granted, and mistakenly believe we always saw it coming. It’s just how we’re wired.

By UX Launchpad on December 24, 2015.

Exported from Medium on May 19, 2017.

First, Best, Free

I read an article a while back that talked about three ways for a product to win in a category, especially software: be first, be best, or be free.

There have been smartphones for a long time, for example, but you could argue that Blackberry was first, iOS did it better, and when Android showed up, it was free. We can debate if iOS is still best, or if Android is really free, but the point stands: there are multiple ways to set yourself apart.

Most items in the The Internet of Things are currently the “first” category. I keep hearing reports of cold homes because a smart thermostat had a bug, or dining room lights that won’t turn on because a smartphone ran out of battery. The products are relying on one marketing point: “now you can do this!” And that’s fine for people who want to help beta test new computing interaction paradigms. There are a fair number of people like that.

But the fun part is when someone makes something that’s actually good, not just novel. The marketing shifts from “hey nerds, are you ready to do something complicated that you couldn’t previously do” to “now average people can finally make this work, and it has actual worth beyond geek cred.” That’s when I like to get involved, but I sometimes forget and get involved too early.

Case in point: I recently bought REV remote control cars for my son. You can control them from your Android or iOS smartphone or tablet, and you can even race against a computer opponent that controls the second car via Bluetooth. Sounds fun! Sounds novel!

And they’re ok, I guess. But the handling is really bad, and they go really fast. Meaning you spend most of the time unable to control your car and hitting walls at high speed. After the geeky novelty of “cars that can talk to each other in real time” wore off, we asked ourselves if we were having fun. We weren’t. Into a drawer they went, like most “firsts.”

It’s a good reminder for your own product. History tells us that “first” success is fleeting, the product perceived to be “best” gets to live on, and there’s hardly ever any overlap between the two. Plan accordingly.

By UX Launchpad on December 30, 2015.

Exported from Medium on May 19, 2017.

Speed

My friend recently posted this quote on Twitter.


Which immediately reminded me of this quote.


The first quote, while perhaps true, is a feel-good motivational speech that leaves everyone feeling like winner. Designers skim it, say “Yup!” and feel like great designers since they agree with the mightily respected Dieter Rams. There are no designers who read that quote and think “Oh. I wonder if I do that.” Because of course not. That would mean they’re not awesome.

The second quote explains why most apps and websites are too slow. The very things we know to recognize in a “good design” (massive hero images, 2x retina graphics, custom fonts, customized and specialized controls, Disneyified motion design) are the things that are slowing software down more year over year.

Now, no one’s arguing that we should change the web to a purely text medium, but the ballooning size of apps and websites has gone from notable to incredible to a mockery of basic design tenets. And it’s getting worse.

“#notalldesigners,” you might be thinking. “#yesallconsumers” I say.

I was once in a design review. There were two teams present, one that was trying to make a sexy new feature, and my team, who was trying to help the feature work in bandwidth constrained/expensive networks like India and Brazil. This conversation happened:

Me: “This looks great. Have you considered using some of our tools that help your feature’s images degrade gracefully in places like India?”

Team: “We don’t have time to redesign this for every country.”

Me: “Understood. But if you just use our framework, you won’t have to redesign anything. It just means we’ll automatically compress your images when we detect a slow connection.”

Team: “The design is solid. Besides, India will eventually get broadband.”

This is a team incentivized to ship something quote-unquote great. But thinking about slow connection speeds, scenarios where internet is expensive, or the fact that almost no one in the world has a reliably fast connection contradicted those incentives[1](#). It would mean their graphics wouldn’t be as slick. It would mean they couldn’t ship as quickly as possible. It would make their design less great, according to their definition of great.

Let’s read those quotes again.

Indifference towards people and the reality in which they live is actually the one and only cardinal sin in design
and

It is difficult to get a man to understand something when his salary depends on his not understanding it.
The Website Obesity Crisis, by national treasure Maciej Ceglowski, is a hilarious and important talk. You should read it or watch the linked video. And if you’re a designer, you shouldn’t just smugly nod and say “yup,” you should go further and figure out how you, you personally, can do your part to fix it. This article is my first step, and there will be more.

The Website Obesity Crisis
Let me start by saying that beautiful websites come in all sizes and page weights. I love big websites packed with…idlewords.com
Footnote[1](#): It’s worth pointing out that even on wifi, even on LTE in San Francisco, even with a high end phone with no data cap or memory problems, bloated design is still a big problem. These issues are not limited to someone on a feature phone in rural Africa, as is often implied. Speeding an experience by a single second, on any network, leads to direct financial and user experience gains. Speed matters. A lot. Everywhere. Always.

By UX Launchpad on January 8, 2016.

Exported from Medium on May 19, 2017.

How Designers Age

I asked my friend’s parents how they aged so gracefully, and they told me they just try to be open to new things. They were probably referring to pop culture or political discourse, but I immediately thought of Snapchat.

I’m still in my 30's, so I’m a long way from AARP membership. Even so, I’ve been designing software professionally since 2000 and made my first public websites in 1995. So it can feel like I’ve seen everything in software, and that’s an extremely dangerous place to be. An expert only sees one (outdated and boring) solution, the beginner sees many.

I split design into two buckets: visual design and interaction design. Visual design is fashion. What looks cutting age today looks dated tomorrow, and that’s the fun of it. There’s no single “right answer,” only working with what we’ve learned from centuries of visual communication precedent and putting a modern spin on it.

I once read a collection of art/design manifestos throughout history. They’re quite easy to write: “Art/design has stagnated. We reject what came before us. That is why we must do X.” And then, some number of years later, X has stagnated so someone writes the next manifesto about why we need to do Y. Then, eventually, we go back to X again.

This is how we go from laughing at a decade’s fashion for looking silly and dated, to fashion-forward people experimenting with it, to it going mainstream, to it being stale again. It has always happened, and it always will. It just takes some time to recognize the cycle.

Interaction design is a bit different because it’s based on science, not opinion. If I design a call-to-action that makes 4% of users buy a product, it is objectively better than the previous design that only converts 3%. You can disagree with the colors, or the typography, or the amount of whitespace on the page, the tech debt, or discuss unwanted side-effects, but you can’t disregard the data. 4% is better than 3%, even if it’s not your favorite visual treatment à la mode.

And this, I fear, is where aging software designers start to lose touch. We install Snapchat, struggle with its navigation, and we declare that it’s too hard to use, and therefore bad, and therefore doomed to fail. Meanwhile, kids love Snapchat despite real UX flaws. So who’s right?

The kids, of course. If they like it, it’s successful. Even if the information architecture doesn’t make sense. Even if you could redesign it to be more intuitive, or take fewer clicks, or increase engagement stats. And, hold on to your hats, grandpa: the fact that you don’t like it makes it more attractive.

Ouch, right?

A lot has been written about the benefits of play. We know that games help train kids or young animals about the world in a safe environment. We know that novelty and surprise trigger reactions in our mind that help us absorb information. We know that the brain accepts input from millions of data points, so anything that looks standard, or normal, or too straight-forward fades into the background. (this is where bias creeps in — things that look different from what we expect get disproportionate attention)

Many users are pattern matching your “clean, intuitive, well designed” UX with all the other boring, universally-agreed-on, template-driven, safe things they see in a day. And when something crazy and new shows up, even if it’s poorly designed — especially if you think it’s poorly designed — it stands out.

If you’re getting older, you know how to spot what you were taught is “great design.” Your next lesson is learning how not to write off novel ideas just because they don’t look like what they taught you in school.

By UX Launchpad on January 10, 2016.

Exported from Medium on May 19, 2017.

The Design of Penn & Teller

When I founded UX Launchpad, I told my friend and co-founder William that I wanted us to be the Penn & Teller of design instruction. He countered that he also wanted us to be the RUSH of design instruction. Tally ho!

William can explain his inspirational north stars in another essay, but I’d like to explain what I meant by the “Penn & Teller of design instruction.” What I love about those guys’ act is their confidence. Sure, they’re good at magic. But they’re ok demystifying it, deconstructing it, even showing you exactly the way it’s done. As a result, other magicians hate them, which I take as a sure sign they’re on to something.

The design community is full of great people who care deeply. But too often, that passion for design makes designers look self-important, arrogant, and aloof. In medical terms, the design community has a bit of a reputation for poor bedside manner. In the world of magic, we come off like David Copperfield. Or if you prefer, G.O.B. from Arrested Development:


Every time my team needs to present to a client, or another team, or to the board, there’s always a debate about the “big reveal.” Most designers want to make everything perfect, showing absolutely nothing to the client until the big pitch. But in my experience, it often backfires. I’d much rather bring the audience with me, from early sketches, through radical over-communication, all the way to the end. This means what the final pitch might lose in surprise, it more than gains in buy-in and rapport.

Watch how Penn & Teller do the famous Cup and Balls magic trick. First they do it the traditional way, but then they turn around and do it with see-through cups. While explaining how they’re doing it. Breaking every rule of magic along the way. As a result, it’s the most entertaining version of Cup and Balls around. Did I mention that the magic community hates them?


Whenever we teach a class, I’m channeling what I’ve learned from Penn & Teller. So you can imagine my delight when I saw this article in The Atlantic today. In it, Teller (a master performer, teacher, and student) explains the overlap between the three. Read it and wonder: would you rather clients, partners, co-workers, and fellow designers think of you like him? Or G.O.B?

Education Is Performance Art
One half of the entertainment duo Penn & Teller explains how performance and discomfort make education come alive…www.theatlantic.com
By UX Launchpad on January 24, 2016.

Exported from Medium on May 19, 2017.

Dear Open Source: How Can Designers Help?

I wrote code from 1995 until 2005 or so. I‘ve never been very good at it, but I enjoy dabbling for fun. In the last year, I’ve finally figured out how GitHub works, and started forking projects to see what I can do.

At my day job and as part of UX Launchpad, I work with engineers and product managers all the time. And in a traditional workplace, I know exactly what role each member plays on the team. But with open source software, I’m not so sure. I’m hoping someone can help me understand if and how design can help contribute more to OSS.

The obvious examples are things like hero graphics for websites, UI elements and styles like buttons and color schemes, app icons, etc. Those sorts of things are often categorized as “visual design” and it’s easy to imagine how a designer could contribute to that. Make a better graphic, merge, PR.

But that’s a small part of the overall UX. Many designers refer to late-stage visual design tweaks as putting lipstick on a pig. There’s only so much you can do with images alone. Design, in the broad and impactful sense, includes the whole experience. Every flow and every touchpoint, from the website/readme text to the installation instructions to the first-run process to the localization, bugginess, ability to get help when things go wrong, the update experience, and so on. It is how the entire thing works, front to back, and to do it well requires a lot of difficult tradeoffs.

Can all that fit into a pull request? Or am I just describing “ownership” of the entire product, which goes deeper than an issue tracker can handle? If you’re an engineer working on a pet project, what level of design feedback are you looking for from a designer?

Because “great design” doesn’t spring up organically, it requires a design vision. From what I’ve seen on GitHub, this usually falls to the engineers writing code. Even if we could make OSS design better, that implies ceding some control to some random designer. But that sounds crazy, at least the way things are structured today.

So I’m curious: how can designers help open source? Don’t be shy: the more feedback I receive, the more I can help :)

[Part two is here](#)

By UX Launchpad on January 25, 2016.

Exported from Medium on May 19, 2017.

Brainstorming a Better Play Tracker

At UX Launchpad, we spent a lot of time helping people think through design solutions. I always stress the importance of writing out your thoughts when approaching a new design, so here’s an example from my own life. (see also: Designing a Sketching App for iPad Pro, a four part series)

Sometimes I want to peek in on a football game from my phone. The first thing I want to know is the score, but I’m really wondering “will my team win?” And that requires a lot more context.

For example, a score of 31–24 at halftime is different than the same score with a minute left to play. And if the game is nearing the end, I need to know which team has the ball. If the losing team has possession, they have a chance to score. If the winning team has possession, it’s game over.

Now, this seems like a great place to use infographics, right? Well, yes and no. Great infographics find a way to present data in a way that uncovers new insights. But the not-so-great ones are merely using graphic design on something that’s perhaps better suited as a simple spreadsheet or list of bullet points. Here are a few examples:



These are static visuals, of course. But NFL fans are shown a similar design aesthetic when they’re trying to track the progress of a football game in real time. Here’s a screenshot from NFL.com’s realtime play tracker:


The current score and time remaining are easy to spot. (note: the game was over by the time I got this image, so I removed the clock) The points per quarter are also easy to parse: in this game, it looks like Carolina got 31 points before halftime, then the Seahawks scored 24 unanswered. Wow!

But a user has to figure that all out on their own. And that’s before you get to the play tracker itself. In this graphic we see a yellow line, some blue lines, and an arc of white. Once you figure it all out, you can piece together what’s happening, but it’s not immediately clear.

The description text helps because it’s more thorough. But answering the simple question “is my team likely to score/win” is still pretty complicated without some analysis. Here’s the (tiny, hard to read) text:

Last Play — 1:59 Q4
D. Baldwin
(Shotgun) R. Wilson pass short right to D. Baldwin to CAR 29 for 13 yards (C. Finnegan) [K. Short](#)

Bonus points for calling out everyone who was involved in the play (even the person who did the tackling!), but it’s not immediately apparent how it is affecting the game. Shotgun? Doesn’t matter. Name of quarterback? Doesn’t matter. Distance of throw? Doesn’t matter. And what does CAR 29 mean? It means they’re 29 yards away from a touchdown, but the visuals and text barely help you to know that.

The experience isn’t doing a great job at focusing on what people want to know: how many yards were picked up, if there was a first down, and how close the team is to scoring. All that other stuff is secondary. So I did some doodling to see if I could come up with anything better:


A few things jumped out at me by the end of this “think with ink” activity. First, this simple linear structure tells a lot. The first Panthers series (top right) was clearly very successful. The next one (middle right) was clearly a catastrophe for the Seahawks. (fun fact: I turned off the TV when I saw it happen live) And the third one, representing what the Panthers did after getting the ball back, was methodical. One step after the other, one really long pass, a few more plays, and suddenly the Panthers were up 21–0. These graphics hint at that (devastating) storyline.

I also noticed that color helped a lot. The interception on series #2 shows up in red. All the first downs the Panthers were racking up show up in yellow. And it’s hard to spot, but there’s a green line on series #3 that shows a penalty that benefited the team with the ball.

Finally, I liked the “stacking” shown on series #3. They show incomplete passes. So you can see at a glance that the Panthers only had two incomplete passes out of about 16 or 17 plays. That’s a pretty big deal. It shows how much better the Panthers were playing than the Seahawks. It’s clear that a successful design will be able to show that story at a glance.

So I tried converting my sketches into a very abstract visual to learn more. Originally I had skipped a stalled Seahawks series (#3 in this graphic), but this time I left it in to see how it compared:


Pretty good! Obviously I’d need to show which team represents each row, but even abstracted to this degree, we can learn a lot. First series: touchdown! Second series: bad news. Third series, when the Seahawks got the ball back: a little bit of forward, then backward, then forward, and that was the end of the drive. Finally, the fourth series shows that the first one wasn’t a fluke. The Panthers were on fire.

So that’s how I kick off a process. I write out random thoughts like “this is too hard” or “I just want to see this other thing.” I doodle it out some stuff. I never expect my first ideas to be good, but that’s not the point. Your first ideas lead to your second, third, and beyond. And that’s where the good stuff happens. You just have to start with something, even if it’s stupid. Especially if it’s stupid. Go!

By UX Launchpad on January 26, 2016.

Exported from Medium on May 19, 2017.

A Designer Thinks Aloud About OSS

This is part two in a series. Part one was called Dear Open Source: How Can Designers Help? I hoped after posing the question, I’d learn some things. And I did learn something that feels like a good metaphor for the open source community: I got some interest in the form of likes, retweets, recommendations, and views … but not much else.

Though @amyhoy certainly had an opinion:


As someone who hasn’t been involved in the community, I can’t speak to how often this happens, or how accurate it is. But my job requires a solid 8 hours a day of coordinating with non-designers, and I found myself nodding. The issue of design ownership is pretty tricky.

Everyone Wants to Rule the World

I know what a healthy software team looks like: a group of engineers, a designer or two, and a product manager. Everyone, regardless of role, is designing the experience togther. Everyone respects everyone else, not just as people but for the incentives that each role is tuned for.

For example, good product managers care about great experience, but they’re also aware that option A will take six months and option B will take three months. And they want to make sure we really think option A is worth doubling our estimate. It’s an important question to ask.

And good engineers also care about great experience, they’d just like us to be aware that QA will be about ten times harder with option A, and will bring a pretty concerning amount of tech debt. Oh, and we’ll be relying on unproven code, so the bugs might get pretty hairy. So they’re not refusing option A, they just want to make sure everyone is aware how much more expensive it is, now and in the future.

Designers? We just care about great experience, period. And that gives us a huge blind spot. There’s nothing in our incentive structure that counter-balances our “just make it great” thinking. We don’t have to think about tech debt, or competitive threats, or how small maintenance releases can destroy morale, or how a lot of engineers think designers are overpaid, hipster crybabies that usually don’t understand the problem they’re trying to solve.

So when a designer walks in skinny jeans, a silly hat, a Williamsburg beard, and a “good thing I’m here” attitude on day one, it goes over about as well as Comic Sans in a design critique. Designers have spent so long being overlooked in corporations that they now have a bad reputation for ignoring everyone else, all in the service of “designing delightful experiences.”

So we have to work on that. That’s not a healthy reputation to have, and it’s going to make it harder to trust us as leaders if we keep it up.

In Defense of Leadership

A team should be able to debate different points of view openly, regardless of role or seniority, but eventually a call has to be made. You need to have a clear process that explains how to handle disagreement. And that means one or more people who act as the “directly responsible individual.” The leader. And that’s where it gets uncomfortable.

For small open source projects, the lead, the engineer, the designer, the product manager, and the leader are all the same person. If I submit a pull request to someone who is the sole owner of their project, I know they’ll only accept it if they agree. They get to decide. There’s no question.

But once a project grows to include a designer and a product manager, the ownership gets murky. The designer may own the design, but the product manager owns the product. The problem is the overlap between “design” and “product” is pretty significant. And then you factor in that engineering has their own strong opinions about the design of the product, and you’ve got three people awkwardly contesting the leadership position. Yikes.

What Works In Corporations

Here’s what I’ve seen work. It requires everyone putting on their big person pants and getting over their egos, but the results are worth it. First, if something is experienced by a human, the designer owns it. But not in a Donald Trump “I’m right, you’re stupid” way. More like a Barack Obama long game community organizer consensus building way. Master level chess, not checkers with a tantruming three year old.

Say I want to do X and engineers might want to do Y. As we debate, my only goal is understanding their point of view, not pulling rank. With that open attitude, about half of the time I change my mind after I hear the engineers’ disagreement. The other half of the time it’s just a difference of opinion. The kind of thing reasonable people can disagree about. And that’s where I think a lot of leaders drive into the ditch, so I try to be cautious in these situations.

There are very few design decisions I am willing to destroy team morale over. If any. When everyone disagrees with me, I could attempt to play the “I’m the design lead so I get to win” trump card, but for what? My ego gets a slight boost, and then my team feels disrespected and unheard. That’s a bad deal for everyone.

Which isn’t to say I don’t fight hard. I remember one case where 20 people around a boardroom table all disagreed with me. But I felt that I had a deeper understanding that was hard to address in a giant meeting format. So I stuck to my guns, but I wouldn’t dream of doing it disrespectfully. Win or lose, I knew I’d have to work with these people the next day. So I argued my case, but still with an open mind. I tried to be gracious throughout.

(Historical note: in the short term, my point of view prevailed. But about three months later, we did end up changing course. But all that mattered is that I not burn bridges, which I think I managed pretty well.)

So the summary of my proposed approach is “design leads the experience, but is not afraid to align to team consensus.” It works. You need someone calling the shots, but you need that person to leave their ego at the door.

Bringing It Back to Open Source

We’ve talked about how designers can parade around like entitled jerks, and how that’s bad. We’ve also talked about how good design requires someone to make the final call. And how that means someone needs that leadership position, and how that should be design (for issues that affect customers.)

But, to reiterate the most important detail: designers can’t pull rank. Being the design lead doesn’t mean always being right, or being willing to torpedo the team rapport to satisfy your ego. A design leader should be aware of the culture and the sensibilities of the team and honor them, not just force their vision onto everything because they think they can. No one’s always right.

So how would that translate to open source? I don’t think it’s as simple as adding a “design lead” field into new projects and requiring it. Or writing a “Design + OSS Manifesto” and getting people to sign onto it. In fact, I don’t think there’s much that can be done via process or pixels. This comes down to issues of trust, ego, and culture. Things that don’t fit in issue trackers.

Culture Boosting

Whether you’re talking about a company, a marriage, or a product, there’s no magic bullet or list of 10 things to follow. Life is nuanced and every situation is different, so everything complex boils down to “it depends.”

When it comes to open source, I think a lot comes down to managing disagreement. If I’m working with you on a project, I should know that you want to hear my design point of view. And that you’ll tell me if you disagree. And that you’d trust me to make the right call — sometimes that means agreeing with you, and sometimes it doesn’t.

Which means that “drop in” design assistance via pull requests can’t really work in open source, not the way it does with code. If I’m a stranger to you, and you don’t know how well I design, or how I work with others, or how I resolve conflict, we’re a long way off from building the rapport needed to make a great product together. You can’t just test my design philosophy and personality in your dev environment, check for compile errors, and merge back into your product. (If we could, that would save a lot of heartache!)

So this topic isn’t just about open source software. It’s about building relationships between designers and engineers. The more the two roles can learn to work with one another, the better it will be for everyone. It would positively affect the entire industry, from corporations to open source.

Any ideas how we can help that process along? I’m listening.

By UX Launchpad on January 30, 2016.

Exported from Medium on May 19, 2017.

Design Explosions #4: Office Mobile

Hullo and welcome to issue #4! If you’re new to Design Explosions, you can learn more in this introductory post. Here’s the gist: Design Explosions tries to do design critique in a way emphasizes benefit of the doubt. Here are our guidelines while analyzing designs:

The team that built this product is full of smart people.
There are many things we can’t know without joining the team.
You can’t judge and learn at the same time.
Issue #1 compared Apple Maps and Google Maps, Issue#2 went deep on the PBS Kids design, and #3 explained why NeuBible is the best reading app I’ve ever used. For issue #4, I presented live at a conference, and this is the rough transcript of my talk.

Today I’ll be talking about Office Mobile, with a twist: my team actually designed this! This means there’s no need for the normal Design Explosions benefit of the doubt. I know exactly why we did the things we did, and the lessons are applicable to anyone making software on mobile devices. So even if you don’t use Office, or design productivity apps, there’s a lot here to learn from.


One important detail before we get started: I’m not sharing this to make me or my team look good. Many case studies follow this formula:

I am awesome.
So I decided to do this awesome thing.
Isn’t it awesome?
Hire me.
But I’m not trying to convince you I’m a great designer, or land a new job, or argue that our design skills are so great that our final result felt inevitable. Instead, I’d like to speak honestly about what worked, what didn’t, warts and all. Think more behind-the-scenes documentary and less PR puff piece.

My hope is twofold: that junior designers can get a more accurate picture of what it’s like to ship software at scale, (messy, hard, and the end result is often disappointing) and that senior designers write more articles like this one. We don’t need more Thought Pieces About Great Design. We need more people to honestly document the ups and downs so we can all improve together.

Brand vs Platform

Office Mobile actually refers to multiple apps on multiple platforms. I was tasked with leading the design on PowerPoint, Word, and Excel on Windows Phone. This was a natural progression after my time as a design lead on Windows Phone. I have plenty of friends on the Windows Phone team and I know how the interaction models work, so I was a natural fit. But that led to our very first question, one that every team struggles with throughout the design of a new product: how much should we align with Windows Phone and how much should we align with Office?

There’s no right answer. Go too far in the “platform alignment” direction and your app feels boring. Vanilla. Lacking personality. Best practices and common controls are all well and good but brand still matters.

On the other hand, embracing the company brand sounds like a good idea but many companies overdo it. They work to get company typefaces, logos, colors, and even interaction models crammed onto the screen, but it’s easy to go too far and have a result that feels ill-suited to the platform.

Obviously the answer is to strike a balance between the two extremes, but finding that balance can be an extremely difficult process. For example, look at this screenshot of Microsoft Word next to the Windows Phone 8 Calendar app. Everything is different, from the interaction models to the feel of the visual design.


Every company that ships software struggles to find the balance between Brand and Platform. And it’s not the kind of thing you debate once and resolve. In my experience every team, building every feature, for every release, always has this tension on their mind.

At Microsoft the issue is a bit more delicate. The Windows Phone team and the Office team are in different organizations, and they’re not known for working well together. The people are smart and talented, and collaboration between individual stakeholders is good. But in any company, you’re going to get more credit for helping yourself and your team than helping someone on another team in another organization.

So if you ask a Windows Phone designer how Microsoft Office should look on their hardware, they’ll have a Windows Phone-centric answer. And if you ask someone in Office, they’ll have an Office-centric answer. Again, no one is malicious or wrong, but priorities and incentives affect our judgement. If that weren’t hard enough, there’s another difficult question every software team struggles with when porting. How much should the various platforms (and web) align with each other?

Platform vs Platform (vs Platform vs Web)

Even after finding that perfect balance between app branding and platform conventions, you have to figure out how you’re going to port to other platforms. For example, let’s say your company makes an iOS app and later decides to move to Android. Then what?

Most companies decide to reuse the same iOS design. And why not? They’ve already spent a fortune finding the brand/platform balance once, so why re-invent the wheel for a second platform?

They soon crash head first into the realities of porting: designing once for multiple platforms is, and always has been, a myth. It didn’t work with Java. It didn’t work for web apps. It doesn’t work for iOS/Android. Porting will never work as well as designing with the target platform’s best practices in mind. But that doesn’t stop companies from trying. “They’re all just touch-first smartphone operating systems!” the executives reason. “How different can they really be?”

Well. About that.

For starters, Android has a hardware back button and iOS doesn’t. It’s just a single difference, one that doesn’t seem like it should affect the software design. But it does, to a tremendous degree. Oh, and Android strongly recommends only putting navigation across the top (until recently), whereas iOS is fine with bottom nav. Android recommends the “hamburger menu” pattern. iOS strongly recommends against it. Theoretically, those are only three differences. But that’s the same number of theoretical differences between Beyoncé and Danny DeVito — height, color, gender — but even three differences can lead to a vastly different result.



They woke up like this.
And of course our team wasn’t dealing with iOS and Android; we had to figure out how to reconcile Microsoft’s Windows Phone with Microsoft’s Office, while knowing that our designs would also need to apply to iOS and Android as much as possible. Plus the Steve Ballmer edict (since retired) that our software be “first and best on Windows.” That’s a lot to consider!

Responsive Design

Then we needed to ponder how our design would scale from 4-inch phones, to bigger phones, to small tablets, to big tablets, to desktop PCs, all the way up to 50-inch mounted displays.

We quickly realized it was no longer possible to split the experiences based on hardware alone. We knew Windows Phones would be running Windows 10, complete with mouse, keyboard, and monitor support. In fact, I had some friends working on reconciling Windows Phone and Windows, and in our informal lunchroom chats, the design issues they were trying to reconcile sounded meaty and complex.

Beyond that, we knew the Microsoft Surface was trying to be the kind of “tablet that could replace your laptop,” with modes for toggling between the different details you’d expect from touch-first or mouse-first interactions. Further, we began seeing more giant phones and ever-tinier tablets. I’ve done a lot of responsive design on the web, but this was a whole new level for me. It was like going from checkers to 3D chess. Upside down. On fire.

Of course, Android had been designed with this philosophy since day one, and iOS has added more resolutions and views each year for the last several years. So we knew whatever our Office Mobile strategy entailed would need to scale well across Windows Phone, iOS, and Android. Not to mention web.

My Small Self

And of course all this complexity and all these considerations each have stakeholders attached. My two-person design team was in charge of the design, but that role is only as impactful as the supporting cast.

In our case, we needed to work closely with the product manager leads from Word, Excel, and Powerpoint. Since they were leads, they had PMs reporting to them. And each of those teams had developers. And there was also a fourth team for things like open/save, services, and our overall interaction model. So that brought still more product managers and developers.

Oh, then there was that team’s leader and perhaps 15 other executives who cared deeply about what Microsoft’s marquee software would look like on Microsoft’s marquee hardware. Everyone from the CEO of Microsoft to the head of Office to the head of Office Mobile, and plenty more besides.

Then there was the entire Windows Phone team design studio, who weren’t directly working on Office but cared deeply about what they were going to be including by default on their phones. But the designers were just one faction, of course. The Windows organization is full of product managers and developers, each tasked to make their feature great.

Then there were the two other platform teams: iOS and Android. iOS was on campus, a short walk down the hill from my building. The Android team was in India, which meant a lot of long distance travel to make sure we were aligned and moving in the same direction. Then the team working on Office on tablets, and before you know it the map of stakeholders on my product looked like this.


This diagram doesn’t do the partner map justice. At one point I counted around 300 people I’d need to coordinate with (I haven’t even mentioned my manager, the best manager I’ve ever had at any company; she’s now working on HoloLens!), but it was looking too complicated. So we’ll stop there :)

Before you write this complexity off as unique to Microsoft, I should point out I’ve drawn similar stakeholder maps at every company I’ve been in. The larger the company, the more complex the project, and the larger this map gets. It’s not the quantity of people that keeps you from great software, it’s how much incentive each person has to work in good faith with one another.

That’s true on a two-person team* or across a two-hundred-thousand-person corporation. In my case, I was fortunate to be working with a large group of people who truly wanted to collaborate. It was just a matter of understanding their roles and aligning to their incentives.

*Ever been on a team of 2 or 3 people that can’t figure out how to work together? I have. Rapport matters more than the team size or the raw talent.

Vertical vs Horizontal Friction

As a designer, I think of myself as a “horizontal” resource. Most product managers are in charge of a few singular features, which is more of a “vertical” mindset. For example, I may not have as deep an understanding about all the ins and outs of Microsoft Excel the way the lead PM might. He or she will always have a deeper understanding, in this case gained from almost a decade of experience. But while my understanding may not be as deep, it’s broader by design. I need to consider best practices on the platform, or reconcile what Excel is doing with what the Word and PowerPoint teams want to do.


Which isn’t to say that either vertical or horizontal is “better.” They’re not. For example, a party needs vertical thinkers like DJs (music) and caterers (food), but also a host or a party planner to tie it all together. No one expects the DJ to think about anything other than music, and no one expects the party planner to know how to prepare a flambé. If both vertical and horizontal thinkers are doing their jobs, and respecting their partners, things work out great.

In one of our first meetings, I brought the product managers together and told everyone we were going to design each of our three apps on a whiteboard for 90 minutes. There was one catch: no one was allowed to design the apps they were experts in.

So the Excel people designed Word and PowerPoint. Word folks designed Excel and PowerPoint. And the PowerPoint team split up onto the Word and Excel teams. The results were amazing: suddenly my experienced, battle-hardened PMs were thinking like horizontally-minded designers. Perfect!


A lot of research has gone into the benefits of diversity. Whether it’s socioeconomic status, gender, race, or balancing between introverts and extroverts, we know diversity is valuable. We know in the long term, the quality and range of ideas goes up when a group is more diverse. We also know in the short term that there can be some friction as everyone figures out how to communicate with each other. It takes time to build rapport.

This activity was new to the team, but it worked. The head of Excel told me it reminded him of being back in school “… in a good way!” And I think that’s why it was so successful. It was fun trying to design another app, because the stakes were low and everyone was learning together. No one got to pull rank, because we were equally clueless. Especially me, since I had only been on the team for a week. It was extremely helpful to be able to watch from the sidelines to get a bit more vertical understanding about these three very different applications. The better you listen, the better you design. And everyone has something to teach you, especially your vertically-oriented stakeholders.

Say Hello to the Three Sisters

To re-iterate what we needed to do, product-wise: we needed to figure out how much to align with Windows Phone’s traditional “Metro” look and feel versus how much to align to Office’s own look and feel. Then we’d need to figure out how to reconcile that with iOS and Android. Then we’d need to figure out how it’d work across all screen sizes.

That was a lot to consider, so we started using the term “stake in the ground” a lot. We needed a stake to build everything out from. Does this metaphor refer to a tent? I’m not much of a camper. Not after that crazy summer in 1994. Either way, we needed to stop theorizing about everything on whiteboards and do some explorations to see how they felt on screen.

We kicked off three explorations that we called the Three Sisters:

Norma would be pure Windows Phone (“normal”)
Tabitha would align to the Office chrome (“tabs”)
Solana would try very hard to be nothing like Norma or Tabitha
In the interest of telling a clear narrative story, I’ll show the best examples of each of the three. But every one of these ideas had tons of iterations, discussions, and reversals. Like all good software design does.

First up, Norma:


We knew in our gut that Norma wouldn’t work, because Windows Phone’s UI only allows for about 10 menu options, and Office requires hundreds. But this exercise helped us convince ourselves, and more importantly the Windows Phone team, that Metro just wasn’t made for apps of this complexity. We also explored putting Adobe Photoshop on Windows Phone and ran into the same insight: we needed a more scalable interaction model.

We had higher hopes for Tabitha. The thinking here was that we could take the “Ribbon” UI of Office and maybe collapse each of the panels into a dropdown menu. Here’s what we ended up with:


We didn’t love how alien it looked while minimized — nothing on Windows Phone looks like that, let alone iOS or Android — but we did like how items came up in a half-sheet. We started referring to these half-sheets as “palettes,” partially because of our Adobe Photoshop explorations. We also loved how the dropdown allowed us to scale to an infinite number of options. And let’s call a spade a spade: Office feels like it has an infinite number of options. You should see the spreadsheets we kept in order to track all the complexity. It was intense.

Then there was Solana. These explorations were fun because the guidance we gave ourselves was “Solana is not allowed to do anything that Norma or Tabitha are doing.” Which meant things got a bit crazy. We have a zillion screens, but here’s one representative example of the work.


There’s a lot going on here, but what we particularly liked was the personality that could show if you tucked everything away and just had a jaunty little lime green item peeking up. Hm. What if instead of lime green we used Word blue, PowerPoint orange, or Excel green? That could work.

(Side note: I did none of these explorations myself. I was spending all my time shuttling between a few dozen stakeholders, keeping them updated with our thinking, coordinating and negotiating between teams, and predicting the political weather we’d be operating under. Fortunately I had a great designer to work with. She did all these pixels while I played at multi-tier cross-organizational diplomacy.)

The Great Reconciliation

After much discussion, and to the surprise of no one, we decided we liked components of all three designs. We liked how Norma felt at home on the system. We liked how Tabitha scaled. And we liked how Solana deferred to the content, leaving behind just a tiny little touch target for getting into the multiple palettes to edit content.


So we combined all the ideas together and ended up with this:


We liked this direction, so we started shopping it around. Everyone understood the general premise, and had helped us shape it, but it was really useful to have a motion study to show it working in action. In this demonstration, the user deploys the keyboard to correct a typo, then changes the font size, then uses the palette picker to move to another palette.


Around this time the iOS team was shopping around their own motion study. Notice how the visual design and some interactions are more iOS-esque, but the flows are pretty well aligned with ours. This wasn’t an accident, of course.



The Android team agreed with our direction, but had some concerns. This was before Material Design was announced, but the design still struck the team as being a bit too Windows Phone. Notice in this prototype how the chrome doesn’t appear until the user taps. This was all part of trying to make sure we had one vision for Office, but one that didn’t conflict with the three platforms (plus web) we were designing for.

Again, and much to my delight, no team had veto power over any other. Android moves the most units, iOS makes the most money, and Windows Phoneis Microsoft’s own OS. So we all had reasons to fight hard for our own platform point of view, but we also understood that everyone’s perfect design was going to look a bit different across teams. That was good because it let us align to each platform rather than trying to shoehorn one design into every form factor.

Big Windows

Speaking of shoehorning designs, we couldn’t afford to let the “Big Windows” and Windows Phone versions stray from each other too much. The teams spent a lot of time sharing notes, so when we landed on this it was a joint design that both teams agreed with.


We spent a long time making sure this was really the best approach. We played with the idea of putting the tablet ribbon on bottom to be more like Windows Phone, or putting the Windows Phone palette on the top to be more like Windows. But it just didn’t work because of where the keyboard would need to line up. Here’s a whiteboard diagram I drew many times:


If we had tried putting the Ribbon on the bottom of the tablet, it would conflict with the keyboard. If we had tried putting the palette on the top of the Windows Phone, deploying the keyboard would lead to a Venetian Blinds effect, where there were only a few pixels in the middle to show the canvas.

The word “consistency” is tossed around a lot in software design, but there are plenty of cases where a fully consistent design actually makes things worse. This was one of those situations. We used the term “earned differences” to explain this and many other decisions. We would try using the same design across platforms, or across screen sizes, and if that didn’t work, we’d say we earned the right for the designs to be inconsistent.

So we felt pretty good about this approach. But then we started digging into what our internal teams and product managers were calling “hero actions.”

The Hero Actions Journey

Notice the top right of the tablet design. See that small collection of icons? Those are called “Hero Actions” and the tablet team was sure they’d be putting them there. They were curious where my team was going to put ours on Windows Phone. Hm.


First we did some research to determine how important they really are. Maybe we could prove that on Windows Phone they weren’t as important? Or maybe we could tuck the functionality away into the palettes? These were all options, but first we’d have to figure out how to handle undo and redo.

Undo and redo have a rocky history on mobile platforms. On Mac and Windows, you could rely on undo being available at all times, but not so on smartphones. But you can’t just remove undo and redo from Word, Excel, and Powerpoint. The apps are far too complex for that. So undo and redo had to live somewhere. But what else? What did the apps need these hero actions for, anyway? Did they really need to take up all this space?


It turns out the answer is yes. There are a lot of examples, for many reasons, but the most obvious is Word’s toggle for going between print view and a reading mode. It looks like this:


It’s a lot like reading a standard desktop-designed webpage, but then clicking into a Kindle-esque mode when you want something designed better for a small screen. Sometimes you want view, sometimes you want the other, and you always want a quick way to toggle between the two. So, the argument went, why not add hero buttons to the Windows Phone design? But we really, really didn’t want to add a bar on the top. So we tried tucking it into the top of the deployed palette like this:


And that felt … ok. We didn’t like how undiscoverable it was, but we liked how it got us out of putting a bar across the top. We had been debating the bar across the top for months and months. Lots of meetings, lots of user research, lots of coordination with Windows Phone, but through it all this design made the most overall sense to us. Even if the hero actions were a bit buried. And that was definitely a concern.

But there was another issue that we were working through at the same time, one that ended up forcing our hand. The File Menu.

The File Menu

Here’s what the File Menu looks like on tablets.


It’s a pretty long list. And while it is possible to trim and combine them into a smaller list, this menu became the straw that broke the camel’s back. In our explorations, we had hoped we could tuck undo and redo into the palette. And also hero actions. And maybe it was ok that we weren’t planning on showing the document title across the top bar. And maybe we could get away with it feeling awkward on Android. But the evidence was mounting that we’d need a title bar, and the File Menu pushed the issue from “the design is getting a little clunky” to “this isn’t working.”

But that didn’t stop us from trying some last ditch things. Here’s us trying to put a hamburger into the palette, with predictably bad results. There are actually dozens of explorations around this, but here’s one that makes the idea look as dumb as possible.


Yikes.
We actually worked on this for months, because we were so thoroughly against the idea of losing so much real estate to a title bar. And we knew once we added one, our one hero action would turn into five, and that we’d end up with a damn hamburger menu. So we started spending a long time thinking about the hamburger menu and getting comfortable with it.

Guess What, Even Aliens Use Hamburgers

Wow, a lot has been written about the hamburger menu. But we can make it brief for the purposes of this essay. First, you shouldn’t use a hamburger menu if you don’t have to. Second, there are plenty of cases where you need to use it, and that’s perfectly fine. For example, and I’m just picking a scenario completely at random here, maybe when you’re dealing with a responsive design that needs to elegantly scale from four inches to 55 inches and has decades of expectations and features baked into it. Ya know.

We see this all the time in software design. For example, here’s the NFL on a big monitor, with lots of things that would only fit on a large screen:


And here it is on a smaller monitor and on a phone. Notice that some things get tucked away on a smaller screen, then stacked on the phone. There’s just not enough horizontal space to fit everything otherwise.


This is not a design trend. This is not a fad. This is not art, fashion, or opinion. This is design physics. When you run out of space, you have to tuck things away or remove them. You see the same thing everywhere in our physical world, like trying to fit too large luggage into too small a space.


For example, if I come into your kitchen, some things will be one interaction away, like turning on your sink, opening your fridge, or turning on a light. Other things are two steps away but grouped, like finding your silverware or where you keep your coffee mugs. And a level or two away from that, you might find a junk drawer. It’s a little further out of the way, it’s got some odds and ends, but it does its job exactly right.

What if I were to dump your junk drawer out onto your kitchen table, yelling “Obvious always wins!” What if I told you it was a better design to have all those options available at all time to increase engagement with your take-out Chinese food menus, novelty candles that don’t blow out, and a warped 9 volt battery? You’d say you were happy with your junk drawer, because it keeps those things safely out of your view until you need them.


What if I told you you should throw it all away? What if I said if it’s not important enough to put on your kitchen table, it shouldn’t exist at all? That would seem crazy, right? Just because you tuck something away in a junk drawer, or four taps away, doesn’t mean you don’t care about it. It just means it’s not primary.

None of these points are breaking new ground of course. This is called hierarchy. Junk drawers are awesome because they provide a place for lower importance things to get out of the way until you need them. Hamburger menus, when used correctly, do exactly the same thing.

Now imagine an alien race, somewhere far across the galaxy. Let’s assume they have eyes of some kind. (Bear with me, this will make sense soon.) And let’s assume this alien species needs to communicate to each other. If those two things are true, eyes and communication, they come across this same law of design physics. They too print letters larger when things will be seen from further away (e.g. alien billboards). They too print letters smaller when they have to fit in clothing (e.g. alien business cards).



And they too, if they have software, use the hamburger menu or something like it. Maybe it’s not drawn with three horizontal lines, but I guarantee a design pattern that says “touch this button to see more options” is in use across the galaxy. And I don’t even know if aliens exist. But if they do, and they have something resembling software, they have realized that responsive design is the only way to translate a design from a small canvas to a large one. I believe this is as significant a discovery as fire or shopping malls.

Wait, what?


Let me explain. Life forms reach the point where they can use fire, or create machinery, or travel to other planets. And I think across the galaxy you’d see civilizations evolving their understanding of design from “making an artifact that will always be used the same way” to “I am building a self-aware tool that understands its context to present the most relevant version of itself.” I believe this shift is as fundamental and revolutionary as the first time humans learned to domesticate animals or NASA landed on the moon. It will become hard to imagine what life used to be like before it happened, because the shift is so radical and so obvious in hindsight.

So yeah. We used a hamburger. There’s no way around it when you’re dealing with software of sufficient complexity. It’s just design physics.


I originally did this as a provocative internal joke for Microsoft employees, but sure let’s put it up here now for all time
On Object UI

Somewhere along the way, I was asked to provide feedback on a deck that talked about “OOUI” or “On Object UI.” This is what Microsoft Office calls contextual UI, commonly shown via right-click menus. I looked through the deck and was impressed. It was long and well researched by a PM who had clearly done his homework.


This slide was a pretty good summary of what I’d need to think through. See that question mark? That’s my team and the design I was leading up. He wasn’t sure how to make his design work on the phone’s tiny screen, especially since his proposed touch screen design wouldn’t fit:


Left: the standard Windows Phone copy design Right: our original proposal for Office
Notice the vertical space that it takes up, and compare to what Windows Phone’s copy experience looked like up to that point. It was clear the new design wasn’t going to fit very well in a portrait orientation, let alone a horizontal one. Look at how poorly this fits.


So we knew we had some work to do in order to align everything. A team of us got to work trying to come up with an approach that could work. One of the tools I relied on for communication is what I call my “two-minute videos,” in which I narrate over the top of some Sharpie text on a series of slides. Here’s a sampling outlining our two main concerns:







(Note: “SIP” is what the Windows Phone team calls the on screen keyboard)
We had two options at this point:

Ship something completely broken
Fix it
And in option #2, we had two more options to choose from:

The Office team writes the code
Office asks the Windows team to write the code
But there’s the rub. These two options are worlds apart. This is the entire issue, boiled down to a single judgement call. This design decision was going to have a bigger impact than any icon, any typography choice, any copy, any on-boarding flow, any marketing campaign, or any reviews in the App Store. This is the whole ball game, folks.

I tried to explain the significance of this decision in this slide:


“Copy/Paste Won’t Work Across Apps” is as bad as it sounds. You’d copy someone’s phone number from a field in Excel, and go to paste it into Outlook, and it wouldn’t work. Worse, it would work, by pasting whatever was the last thing you copied on your device, just not Excel. This would mean four different clipboard histories — Excel would get one, Word would get another, and Powerpoint would get a third. Oh, and then the phone would have its own. Jaw-droppingly bad, right?

Next we have “Visuals/Interactions fragment,” which meant that Windows Phone would embrace a platform model that lights up copy/paste in completely different ways. The visuals would be different, but the interaction models would be too. Hell naw.

“Not a ‘One Microsoft’ vision” doesn’t sound quite as bad compared to the first two, but I wanted to throw it in there. Microsoft knows it struggles with cross-organizational coordination and collaboration, at least on an intellectual level. But when issues like this come up, it’s so hard to snap your fingers and “do the right thing,” as badly as everyone wants to.

Which leads me to a bit of feedback I got from my product manager that changed how I think about software. It’s partly brilliant, and partly bratty. It’s partly insightful and partly enraging. And it sums up so much.

A Quote I’ll Never Forget

I’m paraphrasing, but he said something like “This is great design work. I agree with it completely. But I’m not currently incentivized to follow the direction you’ve outlined.”

Your first impulse may be to grit your teeth. First, it’s super business-speaky. Second, it seems hostile to design. “Blah blah blah I’m not going to do good design because I don’t want to” is how it comes off, right? But read it again. Look at each section carefully. There’s a lot to learn from here. He said:

This is great design work.
I agree with it completely.
I’m not currently incentivized …
… to agree with you. At least not publicly.
He’s not saying he doesn’t want to. He’s not disagreeing. He’s not blowing me off. We all talk about wanting people to be honest, and here it is. He’s not allowed to agree with me. The incentive structure he’s under, as a young product manager, just won’t allow it.

Think back to the horizontal versus vertical discussion earlier. Product managers need to think very deeply (vertically) about their feature. Designers typically speak with several different PMs, and have to think more horizontally as a result. Horizontally, I had a point. Vertically, though? It’s a harder argument to make. And a harder one to win.

Put yourself in his shoes. You’re 24. This is your first big job out of school. You’re trying to do good work and impress your boss. You’re assigned the “On Object UI” framework, across multiple form factors. It’s a fantastically complex and impactful issue, one with lots of moving parts. You analyze the situation, figure out what the team can do and what it can’t, and then your designer says “we can’t ship this.” So you look into it. How long will it take to get it right? Probably about two years.

And not a low-risk two years, either. We’re talking about handing your feature, your baby, the single task you’ve been assigned to, to another organization. One that has historically caused your organization heartache by cutting features at the last minute. Office taking a big dependency on Windows sometimes feels like handing your baby to a taxi driver. It might work out. But it seems dangerous. So you’d rather not.

So here are the two options the product manager saw in that moment:

Forfeit the feature, hand it to another team, wait two years to ship.
Get my own engineers, who I am teamed with, to make it work.
The first looks passive and weak, and brings a ton of risk, all while pushing the release by two years. Can you imagine pitching that to your boss? “Hey, so it’s not my fault, but I think we should delay Microsoft’s marquee software on Microsoft’s marquee hardware (both Surface and Windows Phone) for two years while we hope another organization does my job for me because the experience will be better.”

And even if the boss understood why, then that boss would have to explain it to their boss. And then to another boss. Until finally you get to Steve Ballmer, where you have to explain why Office for iPad is amazing and ready to ship but Office for Windows won’t ship for over two years. On top of it already being pretty late to market.

So that’s not a particularly compelling argument to make. But designers rarely need to. And that’s a real concern, because it means designers are being asked to live in an alternate universe where only “great design” matters, not all the other factors that go into shipping software. It gives us a giant blind spot. And that’s a problem.

The Privilege of Design Thinking


I used to work at frog design, a design consultancy. I remember being at a client’s headquarters, doing a pitch that went well, then pulling aside a stakeholder to admit something I probably shouldn’t have.

“Hey listen,” I said. “That stuff between slides 7 and 14? Those ideas aren’t technically possible, so-” but he cut me off. He said “I know. That’s fine. We’re not paying you to ship, we’re paying you to be our inspiration. Let the engineers and product managers figure out all the details, but for now we just need you to keep bringing big ideas. We’ll sort them out later.”

Wow. So even when we were pitching things untethered from reality, he didn’t care. “With design work, that’s par for the course,” he told me later. In his mind, designers are asked to bring fresh thinking without having to justify it the same way everyone else in the business does. What a privilege.

If you’re a working designer, you’re probably a little offended by this. Of course we have to justify ourselves! Of course we don’t always win! Technical realities water down our ideas constantly! It’s hard out there! Are you saying it’s easy to be a designer, because it definitely is not!

And I agree. But it’s no harder to be a designer than a PM or engineer. I’ve done all three. Designers may have hard jobs, but only we get to play the “I think it’s awesome and frankly that’s all the reason I need” card. More importantly, we are often encouraged not to worry about technical realities. That’s a huge privilege. And it’s something to be aware of. It’ll make us much easier to work with and be around.

Let’s think back to the 3 core Design Explosion principles. Assume the team is full of smart people, that there are things you can’t know, and that you can’t judge and learn at the same time. Turn those towards your own teammates, especially ones you don’t get along with, and you might see the same principles apply.


Postscript

We ended up delaying the release. By the time the product launched, I was no longer working at Microsoft. But I was curious: what did they do? Did they manage to get the clipboard working across apps? Was it aligned between iOS, Android, and Windows Phone? What did they do with the hero actions? Did they stick with a hamburger? Did it feel right?

Here are some images of the shipping Windows Phone design. The first thing you see on the top left is … a hamburger! Tapping it does what you’d expect, with a slide in from the left, Android-style. I suspect the common control for deploying the palettes from the bottom is the same as they’re using for deploying the File menu from the left. I wonder if they made it a common control for all devs? We talked about that a lot.


They ended up with four hero actions across the top — Search, Reading Mode, Invite Contact (for collaborating on documents), and Undo. The Invite/Share one was a big sticking point for us. Google Docs is an important competitive threat, so Office wanted everyone to know that real-time document collaboration is a marquee feature now.

Then, on screen two, there’s the On Object UI! It doesn’t look like Windows Phone, but it does look like Office and Windows. And much to my delight, it appears they figured out how to make it copy and paste across all apps, not just Office ones. That’s a big deal. It also might partially explain why it took so long to get out the door. Code is hard.

(I wonder why the OOUI is running off the side of the screen? I’d guess it’s probably a bug on smaller devices they have queued up and ready to fix.)

In the third screen, we can see the palette design. They put undo and redo in there too, since all the hierarchy traversing sometimes requires it. Also I see the little lightbulb, which is a way to search for a command anywhere in the app. I’d like to see this everywhere in Windows (the way Mac OS X does) but in the meantime I suspect it’s Office only. We’ll see.

So what did the iOS team do? Let’s take a look:


Lots more going on at the top, and some key differences that make it more iOS-friendly. First, no hamburger. Instead, they split the functionality into two buttons. The back arrow takes you to the document list, and the next button takes you to their File menu.

Then they’ve placed an undo button, followed by an edit button, then the three items on Windows Phone: Reading Mode, Search, and Invite. And that edit button is what deploys the palette. The palette itself has a different visual treatment, but works largely the same way as Windows Phone.

Here’s what the Android team did:


There’s the hamburger, surprising no one. Then the edit button to deploy the palette, same as iOS. Then Search, Reading Mode, Save, Invite, and Undo. Wait, save? Why? I have a theory about this.

In the tech world we often overlook the constrained bandwidth of emerging markets. When you study and design for these markets (something I do at Twitter now) you hear terms like “data sipping” and “offline mode” that all point back to the same central point: bandwidth is costly. When you’re on free wifi, users can afford auto-saving every minute. But on mobile, especially in emerging markets, that kind of aggressive saving can impact battery life and data plan usage. People often want more control over what data their phone is requesting over the wire. Thus, a save button.

And that wraps up issue #4 of Design Explosions! Thanks for reading, and I’ll see you when issue #5 comes out. It’s going to be a fun one.

By UX Launchpad on February 10, 2016.

Exported from Medium on May 19, 2017.

I Don’t Know How to Design a Room In Zero Gravity So Now I’m Going To Learn

A few weeks ago I wrote Design Explosions #4 and presented it on stage. Once the hoopla had died down, I started looking for a new design exercise. I asked a friend and she wrote me this:

“I’d like you to design a studio apartment that contains a queen sized bed, a living area with a couch and a TV, dining room area to eat and work, and a kitchen and bathroom. The catch? There is no gravity inside.”
Challenge accepted. This is the part where my Medium essay should say inspiring things like graphs go up and to the right, always say yes, have a growth mindset, hack your brain with novelty and joy, if you’re not moving forward you’re moving back blah blah blah but you don’t need another Think Piece telling you that, do you? Ok! On to the design process!

Taking Stock of My Ignorance

I’m not a very science-y person and I don’t know much about interior design or architecture. I’ve read some science fiction here and there but not much. I have almost no idea how to design for weightlessness.

I went to a bookstore and asked for books that might help me design for zero gravity environments and was told I should watch the recent OK Go video. Then the nice woman took me to the “Space” section of the store where I bought this:


Also notice Thing Explainer in the background, which actually has a section on the International Space Station that talks about “sleeping boxes.” Oh! Boxes! Because you can’t sleep in zero gravity without moving around! Good point. Pro tip: everything’s a good point when you know nothing.

Sketching Some Stuff

I’ve always been taught to “think with ink.” Your brain works better that way. So here’s some sketching I did to try to wrap my head around the problem space:


And here’s me realizing that this looks pretty uncomfortable to live in. All the systems would need to be specially made. You could probably go into depth just designing the vents, let alone the toilets, let alone the “sleeping box” this person would be doomed to try to rest in. Yikes.


Next Steps

I read some about the Mars book but I’m not sure how much practical information I’ll get from it. We’ll see. And of course I’m sure the internet will be full of interesting references to go check out, so I’ll do that next.

But in the meantime, a few things are dawning on me. First, the existence of gravity just outside the door changes things. For example, this inhabitant doesn’t have to worry about wasting water as much as someone might on the international space station. So maybe that changes my shower solution. Not to mention assumptions about buying, storing, and preparing food, socializing, watching TV, working on a laptop, using power, and so forth.

It’s feeling like I’ll need to first learn what the international space station does, then figure out what restrictions are moot in my zero gravity apartment. Small differences have a tendency to lead to huge implications, so I’m looking forward to learning more. Stay tuned!

Here’s part two.

By UX Launchpad on February 19, 2016.

Exported from Medium on May 19, 2017.

Zero Gravity Apartment, Part Two

I’m designing a zero gravity apartment. Read part one if you haven’t already.

Space Research

It turns out there are a ton of videos about the international space station. Here’s one that takes you on a ten minute tour:


Here’s another one, hosted by Suni Williams. At 1:3o she explains sleeping arrangements. And she says something I hadn’t considered — there’s no concept of “upside down” or “sitting” in zero gravity.


I also watched several videos about how water behaves (blobby), how to cook stuff (heated packs of pre-made food), how exercise works (bouncy), how to use the toilet (two different tubes, each with some suction), how to brush your teeth (normal, then spit into a towel or swallow), and several other “slice of life” realities I hadn’t considered.

There have also been lessons that weren’t explicitly explained, but are obvious nonetheless. For example, everything needs a strap or hook. It makes sense intellectually: if you don’t lock things down, they’ll float. But it’s different to see it in action. Every module of the ISS has hundreds of items, from laptops to handheld tools, and they’re all pinned to the walls like specimens in a butterfly case.

Also the equipment doesn’t need to be very heavy-duty because they don’t jostle and move. Even a big flat-screen tv wouldn’t need a traditional mount. It’d just need a little leash to keep it from floating too far. Interesting!

Small Spaces Research

There’s a great book called The Not So Big House that is all about designing for constrained spaces. Boats, treehouses, yurts, and that sort of thing. I learned a few things when I read it about ten years ago. The biggest insight is how “great rooms” backfire. Have you ever been to a party in a large house? The giant rooms are impressive looking, but no one likes to be in them very long. They typically cluster around smaller spaces like the kitchen. People prefer cozy spaces, to the frustration of hosts everywhere.

I’ve also spent some time on houseboats. There’s a certain decorating style you’re forced into when you’re dealing with boats and waves. Bookshelves are slightly modified with “gates” to keep the books from tumbling out. Odds and ends are chosen carefully and placed so they’re not likely to fall. Drawers often have latches so they don’t pop open. And of course the kitchen layout is a game of Tetris, where every square inch is carefully considered and miniaturized. Designing for small spaces is an art form.

Blue Sky vs Ship It

Research is fun, but it can be dangerous. Authors and designers can both lose sight of the ultimate goal, which is shipping something based on your insights. Most of the designers I’ve worked with struggle with this. They want to make something great, meaning they need lots of research to really understand the space, meaning they need to take a lot of time.

The problem is that career jobs (whether giant corporation, consulting gig, or small start up) require you to hit deadlines. That’s what they’re paying you for, after all. So when the designer is blocking engineering for days (let alone weeks or months!), that’s not good. And the same thing happens in personal projects. Many creative types have dozens of half-done projects that followed the same path: we were excited at first, we dreamed up all the ways it could be awesome, and then … it didn’t get done.

I was taught the “three P’s” in high school. Perfectionism leads to procrastination leads to paralysis. You want to do a great job, so you become all talk and no action, then you finally give up. It’s extremely common, and it’s something creative types need to actively fight against.

So! In that spirit I’m going to give myself a deadline. By next Sunday (8 days from now) I am going to be done with this activity. Setting this deadline will sharpen my focus, bring my research in scope, and keep me excited about the work. This is one of my go-to tricks in my career, and is why I wrote McDonald’s Theory. But it’s not a perfect strategy, of course. There are reasonable counter-arguments to be made. They go like this:

Great design requires research
Doing research takes time
Why do something if you can’t do it great
Creativity can’t be constrained by deadlines
I agree, sort of. Obviously companies shouldn’t ship until they have something worth sharing. Obviously there’s a lot of garbage in the world. Obviously making something great is better than making something not-great, and greatness takes time.

But a designer must be able to manage their time and produce on a schedule. And one of the most important lessons is how to take advantage of the novelty effect. When you’re unleashed on a new problem, everything is exciting and new. But as the novelty wears off, it feels more like a grind. So you stop working with as much passion, and the work suffers. So the key is to trick yourself into perpetual honeymoon phases, with lots of near-term touchpoints to keep yourself going.

Which is why I’m going to stop this project in eight days. There’s nothing stopping me from designing past eight days, of course. But for now it’s vitally important that my brain believe it only has a small amount of time. That’s what leads to the most focused, sustained, exciting work.

Design is more than problem solving. It’s problem solving on a schedule.

Continue reading: Part Three

By UX Launchpad on February 20, 2016.

Exported from Medium on May 19, 2017.

Zero Gravity Apartment, Part Three

[Previously: Part One and Part Two](#)

Sketches are funny. Even when they look like a hopeless dead end of scribbles, there’s often good thinking behind them. I read a story of a woodworking company that started selling their sawdust, not just finished products. That’s what my sketchbooks look like. They’re not impressive in their own right, they’re the byproduct of thinking. Take this one:


This is me trying to figure out the difference between a traditional apartment (top right) and what sorts of cool new things I could do with zero gravity. Maybe there’s a way to do two floors? Maybe see-through walls would be interesting? You don’t see it here, but I was thinking about putting a sleeping bag into a small sleeping closet, and maybe that closet would have a view out the window onto the city street below. That’s what I was thinking about when I was drawing those random circles and lines.

Then I talked to my eight year old boy and asked him how he’d design an apartment. We drew this together, but he stressed that this wasn’t an idea for zero gravity. This was an idea for normal gravity, done in his style:


He wants a lot of bamboo. And pebbles along the floor. And a play room. And a big bathroom. And a small kitchen with an eating counter. And a big TV on the wall surrounded by couches. He thought about a bunk bed, like he has now, but settled on a normal one. He also said Tom Hanks’ movie Big was an important inspiration, which is hilarious and makes a lot of sense.

With a eight-year-old’s dream apartment in hand, we then tried to figure out how much it could relate to zero gravity. Here are our notes:


Starting Wacky

At work, especially when I’m leading a team, I notice that junior designers often struggle knowing where to start. And then they start and they go super broad, the way they teach in design school. But I prefer doing something slightly different. I like starting wacky, to trigger novelty and scope the issue, then I bring it back to the big topic.

When I think “how am I going to design a zero gravity apartment?” my brain tells me “I should go read everything I can.” But when I change the goal to “let’s convert my kid’s dream apartment to zero gravity,” I’m given a very clear to-do list. In this case, the bamboo made me wonder how plants and decoration will work. The pebbles made me think about floor surfaces. His brainstorming around bunk beds made me think more about the “sleeping box.” These are all tangible and tactical design challenges.

If I told you “make the best playlist ever” you’d be paralyzed. If I told you to pick the first three songs for a road trip playlist for people who love 90’s hip hop, it’s still a challenge … but you’d have a place to start.

The Thing About Walls

One big insight that I got from International Space Station YouTube videos, as well as my kid’s sketches, is the fact that the walls have no concept of “up.” In a normal apartment, a wall is a vertical plane. But in zero gravity, that same wall can convert to a floor. All you need to do is step up the wall.

This is a pretty awesome situation to think about. A normal apartment room has six surfaces: floor for walking, ceiling for lights, and four walls for hanging pictures, leaning furniture, and separating areas. But in a zero gravity apartment, everything is a floor. All six surfaces need to be prepared to receive footprints. (note to self: no shoes and no hanging pictures!)

You could argue for a “traditional” alignment, where the layout assumes you’re not going to be walking on the ceiling and the walls. But I’m a purist when it comes to designing to context. I don’t want my media software to look like a 70’s TV set, I don’t want to use a metal key for my hotel room door, and I don’t want my smartphone or tablet to try to squish a desktop OS onto it. The room is zero gravity. I am going to start by designing for zero gravity, not trying to force the old approach into a new one.

Thinking About Demographics

I’ve also been thinking about if this is a mainstream product or tailor-made for me. Do I need to think about families? Pets? Lots of guests? And does the person like zero gravity, or does coming home feel like a burden every day? Is zero gravity fun for this person, or is it a curse?

They talk about NYC apartments encouraging nightlife. If your home is so tiny you can barely open the door without bumping into the bed, that changes your mindset. You’re more likely to eat meals in restaurants, relax in coffee shops, stay out at bars, and work late. Not to mention exercise — you’re far more likely to go to a gym or run down the street than trying to fit an ill-advised treadmill into your kitchen.

So how does that apply to this apartment? Would the inhabitant start showering at the gym, where water falls correctly? Would they eat out more? Would they do everything they could to live their life in normal gravity, then only come home to sleep? (on that note, does the person desperately try to find a steady relationship so they can sleep at someone else’s house, one with normal gravity?)

I’m going to structure it like this:

This design isn’t for me, it’s for the mainstream
The person likes zero gravity fine
Guests come over, but no more than anyone’s house
The person eats out, but no more than anyone else
The person showers at the gym, but no more than anyone else
Next Steps

I’ve set up a lot of parameters for this design. I know who I’m doing it for, I know what success looks like, I know the things I’m not going to bother with. That means I’ve got a forming philosophical direction to follow.

I’ve also done some research, giving me some concrete insights to play with. I’ll be doing more research as I go, but I’ve got enough to start. And with that, the stage is set. This is going to be fun.

By UX Launchpad on February 21, 2016.

Exported from Medium on May 19, 2017.

Zero Gravity Apartment, Part Four

[Previously: Part One, Part Two, Part Three](#)



I feel like I’d have a pretty good start if I had to actually design this apartment. But it would be a good start, not anything near complete. I’m fully expecting most of the ideas to require serious effort to make work, if they’re even worth doing at all. Here are some high level ideas:

Bathroom — whatever, just make it like the space station I guess
Kitchen — same, lots of pre-packaged foods
Bedroom — a bit on the small side
General layout — similar to normal apartment, with smaller bed/bath
And here are some specifics:

It would be interesting to have decorative handles across all four walls, plus the ceiling and floor.
It would be nice for those handles to have power outlets tucked away inside them every few feet.
Chairs don’t make a lot of sense. But hooks do. Not just for the TV but for people trying to sit together and socialize. You’d back into a wall, grab a hook, fasten it like a seatbelt, and be good to go. Maybe these hooks are also placed every few feet, like the power outlets.
Ok, now what?

If I were working with a team, I’d rely on the group for ideas. Especially from engineers explaining what’s possible and what’s not. My thinking up until now helps me get a rough sense of priorities and principles, but nothing so concrete I’d argue too hard for them. This is the moment I need feedback in order to battle-harden my opinions.

As a co-worker once said, I have “strong opinions, loosely held.” I think that’s exactly where a designer should be, doubly so early in a project. Do your homework so you know what you believe, but be ready for the next lesson to change your mind.

Since I can’t prototype zero gravity and I don’t have engineers or product managers to challenge me, I’ll need to find a way to invent pressure. I have a bunch of books on the international space station and I’m reading through them now. So far nothing directly contradicts my designs, but that just means I haven’t done enough research yet. So that’s what’s next!

By UX Launchpad on February 24, 2016.

Exported from Medium on May 19, 2017.

In Defense of Politics

David Brooks recently wrote a column with these three insightful paragraphs. Software folks may relate:

Politics is an activity in which you recognize the simultaneous existence of different groups, interests and opinions. You try to find some way to balance or reconcile or compromise those interests, or at least a majority of them. You follow a set of rules, enshrined in a constitution or in custom, to help you reach these compromises in a way everybody considers legitimate.
The downside of politics is that people never really get everything they want. It’s messy, limited and no issue is ever really settled. Politics is a muddled activity in which people have to recognize restraints and settle for less than they want. Disappointment is normal.
But that’s sort of the beauty of politics, too. It involves an endless conversation in which we learn about other people and see things from their vantage point and try to balance their needs against our own. Plus, it’s better than the alternative: rule by some authoritarian tyrant who tries to govern by clobbering everyone in his way.
This quote reminds me of one of my favorite Steve Jobs videos, where he famously said this:

Apple’s an incredibly collaborative company. […](#) We have wonderful arguments. […](#) If you want to hire great people and have them stay working for you, you have let them make a lot of decisions. You have to be run by ideas, not hierarchy. The best ideas have to win, otherwise the best people don’t stay!
Steve Jobs is talking about trusting the process, embracing creative tension, and understanding that not always getting his way was actually healthy for his company’s products. Notice how different this is from his reputation as a maniacal micromanager who always won arguments.

There’s a great lesson here. Yes, designers have things to contribute. But not to the exclusion of engineers, product managers, marketers, the executive team, the board, and all the other groups in a company. Designers do not have a monopoly on good ideas, or even notoriously slippery concepts like “taste.” We wish we did. We just don’t. And that’s healthy.

Dictatorships don’t lead to great countries or great products. To make the best stuff, you need to listen and work in good faith with many different people. You need to understand at the outset that it’s going to be messy and frustrating, and find strategies for working through disagreements.

Because people will always disagree with you, so you will have three options. You can quit, you can agree with everyone at all times, or you can try to reach a compromise. And that takes negotiation, diplomacy, advocacy, listening, and persuasion. In other words, politics.

Further reading: In Defence of Politics, by Benard Crick
“Politics is a way of ruling divided societies without undue violence.”

By UX Launchpad on March 3, 2016.

Exported from Medium on May 19, 2017.

I Am Confused By How I Feel About iPad Pro

I love using it, but I rarely do

I recently read this tweet that said “When I don’t sleep enough I feel like shit. When I get enough I feel great. I just wish there was something I could do with this information.”

For many people, eating well or exercising leads to similar feelings of ambivalence. You know you should. You know it makes you feel better. You’re motivated to, on an intellectual level. But you just don’t follow through. Your heart isn’t in it.

That’s how I feel about iPad Pro, so I’ve been analyzing why. I build products and teach others how to build products for a living, so the more I can understand about my iPad Pro ambivalence, the better I’ll be as a designer.

I Can’t Hold It Right

You can’t use iPad Pro (the big one) on the bus or in the car. You can’t use it before bed. You can’t use it on the go. You can’t use it in the bathroom. You can’t even use it on the couch the way you can with a laptop, propped on your outstretched legs.

I’ve only found two successful configurations for iPad Pro — on a desk with some sort of prop or using my knees as a prop. That’s it. Unfortunately, I’m not in those postures very often. At work I spend a solid 8 hours a day with my laptop resting on my knees. That doesn’t work for iPad Pro (or Surface).

My Most Important Productivity Scenarios Are Slower

Whether I’m designing in Sketch, building a presentation in Keynote, or coding up a little prototype, I’m used to toggling between a ton of windows to get key tasks done at work. Each of the micro-tasks I perform are now technically possible in iOS, like opening external files, copying and pasting, seeing two apps at once, or writing long form content. They’re just slower on iPad Pro, and the time difference adds up. Ain’t no one got time for that.

But Most Software Is Better

Other than complex stuff like Sketch, Keynote, and development environments, I generally prefer iPad apps to their web or desktop counterparts. They tend to be more focused because they have to be.

Compare what Omni Group, Adobe, Panic, Apple, Microsoft, and Google ship on desktop versus what they ship on iOS and you see the same story each time: fewer features, which is a blessing and a curse. On one hand, you don’t have to sift through a thousand menus to perform the action you want. On the other hand, sometimes the feature you want is missing.

And It’s Very Zen

Here’s the part that’s hardest to explain or understand. I just feel better when I’m using iPad Pro. Once I’ve gotten in the right posture and started in on whatever I’m doing, I feel calm. There are no overlapping windows to draw my attention or require fine-tuning. There are no bouncing dock icons. I don’t have to think too hard. When I’m done, the good feeling persists.

The app launcher, if it could talk, says “What is the one thing you want to do right now?” With one tap, that’s the one thing I’m doing, with a bias to stay there. Sure, I could pop back out, pick another app, pop out again, pick another app, on and on, but I don’t. Now I’m reading a webpage? Cool, I should read this webpage with my full attention. Drawing a comic? Let’s draw! Deciding to watch Netflix? Let’s just stay here and single-task. Huh, the cinematography of this show is actually pretty nice if you pay attention.

In Mac OS X, I’m constantly evaluating what I need to do next. In iOS, I settle in and concentrate. Sometimes my mind wanders. For example, I often wonder if I have new email. So I swipe from the side, see my email, pull to refresh, see nothing there, and dismiss it again. Apple’s decision to demote your second app to a temporary “peek” is brilliant. It biases me towards completing my task. The UI says “You’re done … right?” And I usually am.

But Then I Forget How Much I Like It

Whenever I’m packing my bag and I consider taking my iPad Pro along, I think “Eh, I wonder if I really need it today.” And the answer is always no. The Pencil is amazing, I prefer the apps, I think and focus better while I’m using it … but I don’t need it. Not the way I need a laptop.

So I skip using it for one, two, three days at a time. Sometimes a week. And every time I use it again, I remember all the benefits. I feel my brain shifting from the hyper blinky overlapping-windows Vegas-strip power-user over-caffeinated way I feel in Mac OS to the clear and healthy way I feel in iOS.

But it’s still not enough to get me to use it two days in a row.

By UX Launchpad on March 26, 2016.

Exported from Medium on May 19, 2017.

Ugly Babies

There we were, a squad of hotshot designers dispatched from our hotshot design firm. We were about to have our introductory fancy dinner with our new clients. Before we ordered drinks, our most important stakeholder (the guy who’d be writing the big check at the end of the engagement) told us “Our baby is ugly.” What a great thing to lead with!

Notice how succinctly he was able to state a universal client truth:

We are very passionate about this (it’s our baby)
… but we know it needs work. (it’s ugly)
All product work starts with an ugly baby. Without the belief that changes need to be made and the passion to get it done, there’s no product brief or budget. Ugly Babies are why product designers have jobs! But…

Don’t Insult the Baby

This client did us a big favor. He signaled that he was open to change, but also reminded us to keep it civil. And that’s easy to overlook. Sometimes we’re so focused on explaining how to improve things that we forget that every decision was made by someone who was doing their best. Maybe they’re the client, or your co-worker. They’re often sitting in the room while you call their baby ugly. I see it constantly.

I’ve worked with designers who swoop into a new job and lecture everyone about how poorly run the design studio is and how they can do it better.

I’ve worked with product managers who introduce themselves by saying there aren’t any good product managers (other than them) on their team.

I’ve worked with engineers who seem to have two settings: “I agree with it” or “it’s fucking stupid.”

None of these approaches work. What does work is moving past the ugly baby and into the next stage, where you explain what you’re going to do.

Explain What’s Next

You don’t need to say “This flow is a catastrophe,” you can say “I wonder if we can make successful task completion 50% faster.” You don’t need to say “I can’t believe how dated this looks,” you can say “We were thinking of aligning with Material design and explorations kicked off yesterday.” Don’t bother saying “this is so bad,” just jump straight to the standard and much more engaging “what are the problems we’re trying to solve?”

It makes all the difference.

Bonus: How Do You Make People Feel?

People often reach out to me to find new designers, or to provide references for ones I’ve worked with. When they do, I don’t remember how quickly they could do a comp. I don’t have any idea if they use Photoshop or Sketch. Even talent level is sort of a wash — I know a bunch of “good designers,” so that’s not a very significant metric.

The only thing I consider when I’m asked to give feedback on a designer is “how did they make me feel?” Were they able to disagree without being disagreeable? Were they able to listen? Do they take feedback? Do I like them? If I answer no to any of these questions, they don’t get my reference.

There are a lot of designers in the world. There are a lot of good designers in the world. But I’m only interested in good designers who are kind. So how do you find them? Pay attention to how they talk about ugly babies.

By UX Launchpad on April 7, 2016.

Exported from Medium on May 19, 2017.

The Least Insightful Insight in Design

And one of the most common

Every few months, like clockwork, a designer points out that there are only two sellers of goods that refer to their clients as “users.” Designers … and drug dealers. (cue ominous music; hide the children; rend garments)

Yeah, but designers also talk a lot about “usability.” We consider something good if it’s easy to use and allows for task completion. So we refer to people as users, as in “customers who are using our product to get a job done.”

But some designers argue we need to standardize on a new word like “customer” because words matter. I agree, words do matter. But so does focus. Good designers maximize their impact by choosing their battles carefully. I’d argue this particular battle is not a good use of time.

If you’re trying to get the design community to use different words because of a coincidental collision with a drug dealer’s vocabulary, please set your sights higher. I am sure there are design topics more worthy of your time. Anything would be.

By UX Launchpad on April 13, 2016.

Exported from Medium on May 19, 2017.

Designing Soundtracks for Text

What if sound synced with your reading material?

[update: check out part two here](#)

If I were to embed a song file into a body of text, the media would have a play button and it would be self contained. (illustration #1) The experiences for media versus text would be roughly parallel, (illustration #2) but they’d actually be two separate things. The audio would start, reach the end, then it would stop. The song doesn’t know or care if I’ve read five words or five hundred, so there are no interactivity options. Parents sometimes refer to this as “parallel play” when two children are play near each other but not actually communicating in any way.


What if there was a way to make the content more synchronized? Software for composing music typically looks like a spreadsheet where each instrument is a row and the columns represent time. (illustration #4) Couldn’t each instrument be broken into its own loop, and then told to start when a scroll location has been reached?

For example, say you read the first page of a story without any music. But at a certain point, a quiet string section could subtly fade in. Maybe a few paragraphs later, a crowd noise loop would begin because the main character has gone to a bustling city market. And so on.

Help Wanted

I like this idea a lot, and I’ve sketched out how it could work. I’ve also composed a bunch of songs I’d like to experiment with. Even better, it turns out this is technically feasible. Web browsers can keep track of your scroll location and something called the Web Audio API lets you play loops. You can even make sure everything is synchronized so your song sounds are on time and not a cacophony of overlapping sounds with no shared beat. Which is awesome.

But I can’t get the code to work. (Though I did get a bunch of sounds fighting with each other. It wasn’t what I was going for, but it did make me chuckle.) And I think this moment is where a lot of designers struggle, if they even get this far.

Traditional graphic artists could take their work from concept to delivery because they had all the skills and tools they needed. And engineers from 1986 to 2016 have been in the same boat — if they could imagine it, they had the tools to try and build it. Alone.

Whereas software designers will always need to beg for resources to realize their ideas unless they learn to write code. This might be ok in a big corporation where engineering teams and design teams are assigned to work together. But with side projects like these, a designer is like a screenwriter that wants to make a movie. I can only get to certain point before I need other people to help me make it a reality.

So: are you good with JavaScript and the Web Audio API? I’d love to hear from you. Email me at jon@lot23.com and let’s collaborate!

By UX Launchpad on April 23, 2016.

Exported from Medium on May 19, 2017.

The Ghouls Haunting the V1 Graveyard

Or: How Would Dieter Rams Handle Dick Pics?

Dieter Rams famously wrote Ten Principles for Good design. He wrote that good design…

is innovative
makes a product useful
is aesthetic
makes a product understandable
is unobtrusive
is honest
is long-lasting
thorough down to the last detail
is environmentally friendly
is as little design as possible
That all sounds great. Who could argue against being simple, honest, unobtrusive, long-lasting, thorough, and useful? Well, Ash just did.

She got an abusive message on Instagram, couldn’t find a way to deal with it, and wrote this amazing post that you need to read immediately.

Excerpt:

It feels like everyone got busy on something else and then forgot to finish this feature. They implemented the simplest solution and then never went back to it.
Sorry, Dieter. Simple is not always better. Sometimes simple is way, way worse.
I don’t think Dieter’s rules are wrong, I just think they’re a bit outdated. No matter how badly you design a stereo or blender, normal operation of it will not result in a death threat or 1000 dick pics. But the more “simple” a social product is, the more victims it will create. That is a sobering thought.

Ash continues:

But here’s the rub. Do I think anybody’s going to fix this flow? I give it a 30% chance. Maybe if this somehow goes viral or if TSwift or Lena Dunham complains about it, like, 23 times. I’m sick of living in a graveyard of V1’s that never get fixed and ultimately become confusing places to get lost in, or worse, backdoors for abuse from the badly behaved.
Yup. If you’re going to make great software in this decade, “simple” usually doesn’t mean what it did to Dieter — clean, minimalistic, pure. These days, your “simple” social product probably means you didn’t think through the edge-cases. And if you didn’t, it probably chews up people’s expensive data plans, doesn’t work in languages other than English, only works on expensive new phones, is not accessible, and it has no answer for handling online abuse. Simple? No thanks. Give me thoughtful design instead.

If you’re a product designer, please be sure to read Ash’s post here:

A man sent me a dick pic on Instagram
Do you build consumer products? Cool. This is for you.medium.com
By UX Launchpad on April 29, 2016.

Exported from Medium on May 19, 2017.

Designing a Political Platform From Scratch

Design is just problem solving. Let’s solve some problems.

I love designing random things. Ask me to share my process for designing a zero gravity apartment, or a Japanese bathtub, or a better shuttle system for the Microsoft campus (all actual examples), and I’ll be down every time. I even ran a website on this premise and turned it into a book. So today we’re going to kick off the design of a political platform. Yikes.

I guess we should start with insights, like any other design process.

Insights

People really hate politics. The majority of Americans think politicians are bought and paid for, and are severely distrustful of the whole dang enterprise. It would be hard to overstate this reality. No one, no one at all, thinks things are going well.

We also have an increasingly divided population. The people who believe Obama is destroying the very fabric of American society have only gotten more upset over the last eight years. And the liberal wing, who should be on his side, think he hasn’t gone nearly far enough. Eek.

And it’s not just divided, it’s shrill. We routinely describe the opposing side as “traitors” that are committing “high crimes and misdemeanors.” We’ve moved past thinking people are wrong, or really wrong, or super incredibly wrong, now we think the other side has a stated goal of completely destroying the whole country.

And it’s not just divided and shrill. It’s reactive and heavily funded. Mailboxes and Facebook groups are primed and ready for the latest outrage. Someone says that guns shouldn’t be regulated? What seems like 1000 different organizations are ready and mobilized to hit up their mailing lists for donations, riding the anger wave as we fight to defend our flank against the other side’s immoral attack on our very way of life. Meanwhile, on the other side, the exact same thing is happening any time someone says maybe guns should be regulated.

It happens this way every day, all day, on every single topic. It’s exhausting even if you feel strongly about the cause. And if you’re just an innocent bystander, it’s just a lot of noise. You try to tune it out.

The Problem We’re Trying to Solve

Special interest groups, even ones I thoroughly agree with, feel pretty myopic. This one over here only cares about black folks. This one over here only cares about LBGT. This one only cares about women. This one is about the environment. This one is about reforming Wall Street.

But that’s not a platform. A platform needs to have a unifying philosophy about how to lead. How to pass laws that will make things better rather than worse. So while each single topic is important, they’re only a small part of the overall tapestry to consider.

So: what’s the problem we’re trying to solve? Making a platform worthy of the majority of America’s vote. And I’m convinced the only way to get there is to use a new playbook. This one is old, musty, and mean.

A Word on the Candidate

It’s not me. I’m not running for office. This is an exercise where I imagine crafting a platform, but that platform could be used by anyone.

I believe that politics are messy and awful. But I also deeply believe they’re the only way to address the biggest issues facing mankind. If I thought I could make a positive impact on society and not completely lose my mind or destroy my family, I’d run for something. But I’m not sure how to. It might not be possible. So the actual candidate is a moot point for the purposes of this exercise.

Proposal #1: Leading With Tradeoffs

Today, politicians promise the moon and the stars, then we hope that the media will help us understand what’s possible and what is wishful thinking. This incentivizes politicians to go all-in on their proposals, never admit any flaws, and hope people don’t study up on the issues.

Wouldn’t it be cool if candidates got more specific but also directly owned up to the tradeoffs they were making? So instead of saying “Everyone should have free health care” you’d actually say “I think everyone should have free health care. This implies the average family’s taxes going up by about $600 a year. But also means it would be illegal to lose your home or your job just because you got sick.”

Wouldn’t that be great? I think it would be so cool if the candidates themselves owned up to the tradeoffs so we could have a real conversation about them. I’m a liberal. I’d gladly talk with you about the extra taxes I think we should pay in order to invest in our future. But I would be lying if I tried to claim we don’t need to raise taxes in order to pay for my ideas.

Proposal #2: Sharing the Stage with Opposing Ideas

What if a politician’s website actually had a pros and cons section for each idea? What if every proposal had a counter-point written by someone representing the other point of view? It would mean that a candidate couldn’t just say “My plan adds 100,000 jobs” and stop there. What if on the same website, a conservative would say “His plan adds 100,000 jobs by raising property taxes by 0.5% and assuming the economy grows 20% faster than it has over the last decade?”

Traditional politics would say you should never show weakness. You should never admit any fault. But I think we’ve strip-mined that idea. Now no one trusts anything anyone says. The key in 2016 is not to look as strong and invincible as possible. I think the way forward is admitting that the other side has a point of view. A reasonable one. But that, on balance, you believe your set of tradeoffs makes more sense.

To Be Continued…

Like it or not, we need politicians. Like it or not, the world is complex. Like it or not, neither liberals or conservatives have a monopoly on good ideas. And everyone is just trying to make the country as successful as possible. We just have different philosophies for how to do that, which is healthy. What’s unhealthy is thinking “the other side” is evil and wrong.

Next time I’ll dive into some policy details more thoroughly. And not through the lens of “my idea is perfect” but through the lens of “If we do this idea, X gets better and Y gets worse. But I think it’s right, and here’s why.” I’m going to use tradeoffs, not rhetoric. Wouldn’t that be a nice change from what we’re used to?

In short, I’m going to design process the heck out of this. Because design is just problem solving, and we’ve got some big problems to solve.

[Read part two here](#)

By UX Launchpad on May 12, 2016.

Exported from Medium on May 19, 2017.

All The Things That Don’t Matter

Let me tell you about Project Fuzzy

In 2011 or 2012, I pulled aside two friends at work and asked if they wanted to work on a promising idea. They said yes. Over the next few weeks we followed a standard design process to discuss insights, experiment with some solutions, and put together a little movie and a PDF.

We called it Project Fuzzy, and it’s basically Google Allo or Facebook’s bot-messaging stuff.

This is where a lot of designers go wrong. We get sad and think “That could have been me!” or if our product shipped we say “Meh, but we shipped it first.” But that attitude isn’t just defeatist, it misses the point.

The fact is, if you give 100 design teams the same set of insights and the same problem to solve, they’ll come up with 90% the same solutions. Not because they’re lazy, but because there are finite ways to solve problems well. This isn’t just a theory, I’ve seen it happen over and over in my career and in my design classes. And I think this truth is tremendously exciting.

A design process is more scientific, structured, and reproducible than most people realize. Great design doesn’t run on magic, muse, and talent. It’s just a process. One that can be taught, learned, and improved on. It’s not any different from learning any other skill, like woodworking or accounting.

I am puzzled by how the tech press talks about design. A new product gets made, then like clockwork everyone explains how it’s imitating another product. Or how someone else had that idea first. Or how the team’s talent pre-destined them to find that design solution.

These are all the things that don’t matter. What matters is taking some insights, running them through a design process, and then asking yourself if you solved the problem you set out to solve. Then you do it again.

That’s what matters. And anyone can do it.

By UX Launchpad on May 18, 2016.

Exported from Medium on May 19, 2017.

Designing a Political Platform (pt 2)

It turns out this is really hard, y'all

[This is part two. Read part one here.](#)

I use the word epiphany too much. It’s supposed to be reserved for giant, worldview-shaking insights. Things you don’t come back from without being fundamentally changed. But I’ve been known to use the word somewhat more loosely, like “I had an epiphany: donuts would be awesome right now.”

But this design exercise has given me an epiphany in the traditional sense. It’s something I knew intellectually but now I understand it at a deeper level: there is often a big difference between what you personally believe in and what you can get a majority of people to believe in.

So let’s talk about the implications of that.

Try This At Home

I drew up what I personally believe about major issues of the day like fighting ISIS, the environment, Wall Street, money in politics, gender equality, LGBT rights, abortion, guns, Israel, and so forth.

And then I drew up the platform that I actually thought could pull down at least 51% of the vote. And it turns out my politics and America’s politics aren’t lined up. So … then what?

I highly recommend sketching this out for yourself. It’s easy to wave the banner of your personal beliefs when there’s nothing at stake and no partners in your coalition. (It’s like the great quote about judging parents: “We’re all perfect parents until we have kids.”) But what if the data showed that only 20% of America agreed with you? Would you modify your platform?

It turns out if you want to win, you have to.

Selling vs Selling Out

We’re obsessed with authenticity. We want candidates who have agreed with our current point of view since birth and have never changed their mind. We don’t want them to be changed by anything. Not polls, not facts on the ground, not a thoughtful change of heart, not meaningful compromise in the interest of working across the aisle. How dare they change? That’s inauthentic! But is it really?

Steve Jobs was famous for changing his mind. It gave his co-workers whiplash because he could argue option A with his full heart, but when faced with new data, he’d treat option A like the worst idea of all time. Does that mean he doesn’t know what he believed? Does that mean he was soft or stupid? I don’t think so. I think he cared about results more than ego. I think he had the courage to look foolish due to a change of heart. Because the ultimate goal, building the best product he could, was more important than him. I’d love if more politicians acted this way.

But it doesn’t play well. Take Bill Clinton. He was famously drawn as a waffle in Doonesbury because he was willing to change his mind and work with the Republicans. This enraged liberals who saw him as not liberal enough. Meanwhile, conservatives reacted to his presidency with the kind of welcoming committee that white blood cells reserve for attacking the Ebola virus. (Did you know no one loved Clinton until his affair and the resulting GOP overreach? Look it up.) Think about the implications of that.


So I believe in compromise in the spirit of Steve Jobs and Bill Clinton. But my platform isn’t allowed to say that, because it scares the base. And (get ready for your head to hurt trying to parse this) that means my platform has to compromise … by appearing unwilling to compromise … because that’s what the base requires. So the platform would need to compromise by appearing more partisan than I personally would be comfortable being.

Wait, what?

Case Study: Guns

I’m on the left of the gun control debate. I don’t think they they should be outlawed entirely, but I do support what’s commonly referred to as “common sense gun control.” For example: I believe in licensing. I believe in background checks in all cases, even gun shows. I believe in banning assault-grade weaponry. So I’m a liberal, right? Well, yes and no.

I’m a liberal that has spent a lot of time trying to understand why guns are such a visceral topic for conservatives. It’s not about one rule here or one background check there. It’s much darker and deeper. A large number of people believe that restricting guns, even a little, is the first step to a fascist dictatorship. (indeed, many people believe we’re well on our way and guns are one of the last stands they can take before they’re ultimately defeated) Many people believe we’re headed towards a world where only the police and criminals have weapons, and average civilians can be attacked by a corrupt police state on one side and immoral criminals on the other.

Are they against closing the gun show loophole? Yes, but not because they disagree with closing the gun show loophole per se. It’s because they don’t trust the government not to abuse their power. And I sympathize with that. I think Snowden is a hero for this very reason: overreach of the federal government in a problem and we need to be prepared to fight against it.

So where does that leave my platform if I lead with my personal belief?

It means I can’t get an A+ rating from gun control activists because there are tons of liberals that can attack me from the left as “soft on gun control.” So I lose some of the vote that I need. And then on the right, forget it. What’s my message for them? “Liberal who also is suspicious of government overreach but is still going to strip you of some rights?” That’s not going to win many votes amongst the NRA types. If any.

What I’d Really Want To Say

So if I ran for office and people wanted to know my stance on guns, I’d explain the one or two “common sense” laws I’d want to enact, then I’d promise to veto anything beyond those two steps.

Because that’s where my heart is. I’d want to address gun violence seriously, but I’d want to a signal to the right that my policies would be centrist, not extreme. And there would be no “slippery slope” on my watch.

What I Would Be Forced To Say

But the left would call me a sell out and the right would roll their eyes. They’re used to politicians lying. They wouldn’t want to risk a vote on a liberal, they’d try to find the most red meat, NRA lovin’, gun totin’, cowboy they could. And they’d win on that agenda because they’d be able to rally their base.

Meanwhile my base would be left scratching their heads, meaning they wouldn’t be as enthusiastic. So I wouldn’t be able to maneuver right, and the center has no votes or money. So I’d be forced to go far left. At least in the far left I could drum up support from wealthy liberal donors.

So that’s how things look. At least, that’s how they look today.

So is that all? Am I saying it’s hopeless? Is my political platform dead before it even gets a chance to defend itself? Nope, because problem solving is never hopeless. There’s always something you can do. And I actually have ideas for how to cut through all this. That’s what I’ll share next time.

By UX Launchpad on May 20, 2016.

Exported from Medium on May 19, 2017.

This Essay Is Not Actually About Pull Quotes

Can you spot the magic moment?

Recently William, co-founder of UX Launchpad, tweeted this:


I frequently roll my eyes at over-use of pull quotes. Especially on Medium. So while I sometimes share William’s frustration, I also think pull quotes can be good. So I texted him about it.

Jon:
I am looking at your pull quotes tweet. Here’s the tally of responses: 1 because people skim, 1 design, 1 joke, 1 person relating, 2 cynics about journalism, and 1 white space.

But as much as I hate the dumb Medium.com over-emphasize using pull quote trick … I think pull quotes are extremely important from a visual design standpoint. White space and composition. Plus yeah, just because people don’t “flip” an article they absolutely skim. It’s practically identical even though it’s a new medium.

William wrote back while I was asleep:
Do you think that is the case on mobile too? I am thinking of being like halfway through a 15-minute article and it is still trying to grab my attention and convince me to start reading

So I wrote back this morning:
I do, because I suspect that the scenario where someone starts an article at the top, then reads to the bottom, without ever questioning if they should keep going, is a minority

People scan around. So that pull quote down at the bottom is a way of saying “here’s a summary of what’s down here.” Agree that it’s redundant, but disagree that a giant wall of text with no pull quotes is better for the common scanning scenario

William:
nice, i like your thoughtful explanation

Jon:
That’s all anyone ever wants to hear ever

William:
it is so easy to say, but we’re so wired to hold our ground.

i wonder if there is a way to gently and quietly remove the pull quotes for a person who is deliberately reading through the whole thing word by word

Jon:
So let’s explore that part: “A way to gently and quietly remove pull quotes.” At first blush, it feels like engineers saying “then let’s make it a setting.” But I wonder if there’s something subtle, like … if you’re there for x minutes you trigger the “actually reading” thing and they just go away

William:
seems like if we can tell you are scrolling at a normal reading speed, screen by screen, we could… pull… the pull quote that is still a couple screens away

Jon:
Would they need to go away fully? Because white space can help

William:
hmm

Jon:
Maybe they turn into their own graphic.” The real answer is “just leave white space,” but I like the “there’s a treat at the bottom of the box!” idea :)

William:
maybe they even just become like an ornamental horizontal rule

* * * or whatever

surround the pull quotes with an ornamented frame, and then also have a minimal version of that ornament that you get when the quote is gone

Jon:
Ship it. We just designed.

Did you notice what William did there? He brought up an idea that got people talking. Then when I disagreed, he asked me for more details. Then he told me I had a good point. Then he synthesized his feedback with my feedback and tried coming up with a new solution to combine the two.

If you blink, you’ll miss it. But all great products start with this level of communication. And no product can be great without it.

By UX Launchpad on May 24, 2016.

Exported from Medium on May 19, 2017.

Designing a Political Platform (pt 3)

When you ask the wrong question, every answer sounds wrong

[This is part three. Read part two or go back to the beginning.](#)

They teach designers to understand the problem they’re trying to solve, but that’s only part of the story. You also need to understand why you think that’s the problem you’re trying to solve. So let’s talk about assumptions.

Imagine we work together at a medical startup. You might say “Hey we’re going to make a messaging bot for dispensing medical advice over text.” If I were to ask what problem we were trying to solve, you’d probably say something like “We’re trying to bring health care to more people, especially young people.” And that sounds pretty good. That’s enough to start with. But that’s not the whole discussion. That’s an implementation idea, not an insight. They are different, and should be treated as such.

Let’s start with insights. One is that young people have health questions. Another is that they like to text more than talking on the phone. And often the most important insight that a design team grapples with is “an executive told us to build this feature so that’s why we’re doing it.” Throw that in the hopper too, because that often matters more than any other data point.

But none of those necessarily mean that a messaging bot is the best answer. No matter how much the executive likes it, the bot is simply one implementation idea sloppily wrapped in an insight. Maybe it’s the best idea, maybe it’s not. But there’s no way to know this early in the process. You need to do some investigation, so I do what I call an “assumptions audit.”

The Assumptions Audit

So what are the assumptions I made in sections one and two of this process?

Everyone hates politics
But politics is how you make things better at a societal level
Liberals want to vote for liberals
Conservatives want to vote for conservatives
There is no viable third party
It’s hard to run on a platform of “the other side has a point”
And I think those are pretty solid assumptions. You’d be hard pressed to find a poll saying that most people think the US congress is super awesome at their jobs. Or to find a liberal who’d be more comfortable with a centrist than a fellow liberal.

But there’s one more assumption I’m making here, and it’s implicit in all my thinking up until this point:

I’m designing a national presidential platform
And that’s where everything runs off the rails. Running on a national ticket for the presidency has almost nothing to do with what it’s like running for local government or even in the House of Representatives.

Do most people hate politics nationally? Yup. But at the local level, people are less likely to think it’s all rigged and useless. Do liberals prefer voting for liberals? Yup. But what are the implications of that if you’re running in a liberal district versus a conservative one, or nationally? Night and day.

All Politics Are Local

It took me three essays to get here, but I finally managed to scope my issue. And once you scope effectively, you can start making progress.

So: I’m not designing a national platform, I’m designing a local one. If I’m liberal and living in a liberal area, a lot of things become a lot more clear.

So let’s look at our gun control issue from last time. I personally believe in gun control, but if I were running nationally I would try to explain my point of view to people who don’t. I’d explain that I’m not trying to take away all guns, I just happen to agree with background checks. On the other hand, if you’re living in an area that already believes in gun control, you don’t need to spend a lot of time justifying that point of view.

It’s a bit like common controls in UX design versus customizing everything. Every time you do something different from the norm, you need to spend time and effort explaining yourself. Which is why in UX design and in politics it helps to rely on common patterns to get your messaging across.

Representation Means What It Says

Let’s say you represent 25 houses in your neighborhood. Let’s say you believe that the official neighborhood flower should be a tulip. What would you do if all 25 houses think it should be a rose? You have a few options:

Vote for the tulip, completely ignoring the will of the people
Convince everyone how great tulips are (unlikely)
Just vote for the dang rose since that’s what everyone wants
I think it’s pretty clear that #3 is the winner (maybe after trying #2). It doesn’t matter what you personally believe if you’re that outmatched. You’re a representative, not a tyrant. Everyone wants a rose, so you do too. That’s what representation means in its purest form. It’s pretty simple.

Ok, now let’s say the vote is split. Half want roses, half want tulips. After lots of debate, no one can agree. In this case, I think you can go with the tulip. You’re the deciding vote. There’s nothing to represent because of a perfect tie, so you vote your conscience.

Ok, now let’s try a third and final scenario. Now the votes are 14 to 11 in favor of the rose. You don’t personally agree. But it’s not an overwhelming mandate, either. You could make an argument that the neighborhood could still go with the tulip since it’s close to 50/50 and you personally prefer it.

Mathematically speaking, you’re saying your vote is worth about 3 standard votes since you’re the representative and you feel strongly on this issue. Or you could say you’ll always vote with the group, no exceptions. I could argue either side on a case-by-case basis.

The Platform’s Point of View

Funny story — I recently applied to be a neighborhood representative, so I’ve been thinking about this topic a lot. If I win the position, I’ll vote a few times as a representative of the houses two blocks around my home. And if 51% of them agree with something, I’d be hard pressed to find a reason to disagree. And of course it’s even harder if the sentiment hits 60% or more.

I keep returning to the same insight: the word representative tells you everything you need to know. You’re figuring out what the people want, then you’re giving them a voice in the process one vote at a time. I like the clarity of that. I would be happy to vote against my own point of view (such as tulips) if 60% of my neighborhood wanted roses instead.

This point of view doesn’t scale to the national level, of course. Popular opinion has always lagged behind important federal-level issues like women’s rights, civil rights, and the environment. We need brave leaders to do what they think is right for the country even if they suffer at the polls.

But at the local level, I think a strategy of “Find what the majority of your constituents believe and support them” works quite well. So at a local level, that’s one of the core planks of the platform. Heck, maybe it’s the only plank that really matters. I am there to represent, not impose my will. Easy.

Next Time: An Issue I Feel Strongly About

But this all assumes a reactive approach to local politics. Wait for an issue to arrive -\> see what people think -\> vote that way -\> repeat.

But what about bringing up new issues? I’ve got one I feel strongly about. And a lot of other people share my passion for it. Next time I’ll brainstorm ways to bring it up and raise some awareness to it.

By UX Launchpad on May 25, 2016.

Exported from Medium on May 19, 2017.

Designing Soundtracks for Text — Solved!

(I have a buggy v1, at least)

A while back I mocked up an idea for playing music tracks in the background while reading. I was excited by the idea that instead of embedding a whole song, you could use layers. For example, you could fade in some subtle crowd noise. Then a few paragraphs later, some ominous violins on top. Then maybe the sound of a ticking clock. The overall song would unfold along with the text. Here’s the article on it:

Designing Soundtracks for Text
What if sound synced with your reading material?medium.com
Recently I discovered http://scribble.audio which is absolutely amazing. You should check it out if you haven’t seen it yet. Turns out it was authored by a guy named Kyle Stetz, a designer and developer who works at Slack. I wrote him an email about my idea, he wrote back with a few pointers, and now I have a simple example of my idea up and running. Check it out here.

It’s buggy and just a v1, but it works! Sort of! Hurray!

By UX Launchpad on May 27, 2016.

Exported from Medium on May 19, 2017.

Google Photos is Magic

“You can be the boxes, I’ll be the arrow.”

Several years ago, I was at the whiteboard with a product manager and another designer. The product manager wasn’t aware of the term “boxes and arrows.” After I explained it, she gestured to me and the other designer and said “You can be the boxes, I’ll be the arrow.” I think of this phrase all the time because it sums up so much about great design.

Two weeks ago, my wife and I decided to get more serious about storing old photos. Between backups, multiple computers, the cloud, and social media, we don’t have one single place to put everything. And what we do have gets duplicated awkwardly. It’s a mess that we felt helpless to sort out.

So I looked into Google Photos. Turns out it’s totally free for unlimited storage. So I installed it on two Macs and my iPhone and it got to work uploading everything into the cloud. Next I dug up all my old backups so I could send them to Google as well. Then I waited a few days for it to finish.

Now it’s done and I have the exact experience I’d expect, visually speaking. There’s a giant grid of photos. There’s a search box on the top. When I click on a photo, I can do things like edit or share it. There’s nothing to talk about from a UI standpoint. It’s a great example of Of Course design, which is the highest compliment I can give a product.

So why is it magic? Shouldn’t it be hand-crafted, beautiful, award-winning, adventurous, high fashion pixel work for me to say it’s great design? Not at all. Google Photos is a reminder that UX magic often lives in the arrows.

In Google Photos, I can do a search for the word “sleeping” and find every photo where someone is sleeping. I can search for “sleeping red” and find that cute picture of four-year-old me sleeping with a red shirt on.

It’s created automatic albums for things like “Beach” or “Whiteboards” or “Sunsets.” It’s found every person I’ve ever taken a picture of and made automatic albums out of them. And with hardly any training other than telling Google what each person’s name is. No false positives yet.

That’s the core experience, and the installation and onboarding was just as impressive. Think about the engineering challenge of letting me upload from multiple computers at the same time, find and remove the duplicates, handle all the compression and bandwidth throttling and a million other tiny behind-the-scenes details. Not easy. But the product had no problem.

(And what about the fact that I’m getting unlimited storage for free? I have experience with this very topic, and it’s no small feat.)

The fact that I could install an app, log in, trust that everything would work despite a tremendous amount of complexity, then not be disappointed in the result is the single most important feature of Google Photos. But how much software can claim that? Can yours? Are you sure?

Yet none of that work even has to do with the UI. A lot of it is barely UX. Because it’s often not the boxes that make magic, it’s the arrows. It’s hundreds of different people across multiple roles making sure everything works as expected with minimal fuss. And when a product can pull it off, as Google Photos does, it’s indistinguishable from magic.

Show me the best visual design of our time, something so good we’ll be talking about it in 50 years. Something that will make your toes curl. Something where people go speechless when they see it because it’s just that good. I’ll shrug. Show me the arrows. Show me how it works. There’s not a visual design or layout in the world that could get me to switch. Not unless it has great arrows.

By UX Launchpad on June 9, 2016.

Exported from Medium on May 19, 2017.

Wow, They Fixed the Apple Watch UX

That’s an encouraging sign

One of the most dispiriting things about working in a corporation is how hard it is to make seemingly obvious design improvements. Whether it’s because “this is the way we’ve always done it” or “that’s another team’s call” or “the CEO likes it,” there can feel like there are one thousand ways to shut down ideas before they even get a fair hearing.

Today I saw that watchOS 3 is adding a bunch of new features and improving performance. That’s not surprising. But then they did something sort of nuts and devilishly difficult: they changed the entire interaction model. Top to bottom. Everything is different in terms of the how you interact with the OS, and that’s a big deal.

Here’s an analysis I write about watchOS when it first came out:

An Apple Watch Heuristic Evaluation
At UX Launchpad, we teach design courses and write long articles about design. So here’s one about some Apple Watch design issues.medium.com
And I’m pleased to see that almost all of my biggest UX concerns have been addressed in this release. I’m not surprised when Apple makes big bets. I’m not surprised when their products are beautiful. I’m not surprised when some of the interaction design is a bit odd at first. I’m not surprised when there are some performance issues in version one.

But if Apple still has the ability to spot a flawed product and completely revamp it within a year, with no crushing sentimentality for what came before it, that signals something. It means the spirit of Steve Jobs is alive and well in Cupertino. And that’s the best news I saw at WWDC.

By UX Launchpad on June 14, 2016.

Exported from Medium on May 19, 2017.

The Plecostomus is My Spirit Animal


When I was a boy, I had a little aquarium. It had a few guppies, some goldfish, some terra. They were the main draw of the tank when my friends visited, but to me the star was the plecostomus. That’s the kind of fish that sticks to the sides and bottom of the tank to clean it.

I thought it was kind of cool and subversive that the least noticeable animal in the tank was actually doing the most. It fascinated me that they had zero percent flash but had one hundred percent importance. After all, a tank can survive without this or that colorful goldfish. But a tank without bottom feeders soon becomes inhospitable.

Last night I realized I’ve done the same thing with my career. Over dinner with two coworkers, I asked them to name what big feature could be traced back to me. I was kind of delighted that they didn’t have a ready answer.

I’m involved in lots of little things, in lots of behind-the-scenes ways, throughout my career. But not as many things that are flashy or interesting. No one thing that lights up a slide in a portfolio review. But when I’m doing my job right, the team just works better, even if no one knows why. And that’s just how I like it.

By UX Launchpad on June 24, 2016.

Exported from Medium on May 19, 2017.

On Designing My Own Morning Newspaper

If you want to do the job right…

I’ve been writing more code lately, which brought me to Beautiful Soup, a tool that helps people pull data out of websites. It got my gears turning about what kind of personal publication I would make for myself if all the data on the internet could be compiled into a single sheet.

Right off the bat, I realized there are two kinds of data: things with defined answers and “general awareness” items. For example, I like to know exactly how Hillary Clinton is polling versus Trump. As of today, that number has Hillary up by 4.8% according to the RealClear Politics tracker. That’s a defined answer, like the price of gold or whether or not the Yankees won their last game. Easy.

Then there’s the general awareness stuff. This is typically handled by going to your favorite websites and scanning the news. Or maybe you just rely on what comes through your Facebook and/or Twitter feeds. This is the majority of content people read online — things they didn’t explicitly ask for (“Hey did anyone write an article about Trump losing two more of his advisors today?”) but rather things they asked for implicitly (“What’s happening in politics? Wow, I guess Trump lost two more of his advisors.”)

So we have defined answers and general awareness items. There’s “Hillary is up by 4.8 on average” and “Here’s a bunch of headlines explaining what’s happening in the world.” And each of these kinds of data can be found, but they’re scattered all over the place. Across sites, across multiple pages, buried in deep conversation threads, queued up in our reading lists. And each of these entry points are run by corporations with their own business model to satisfy ever more each quarter. They can’t just let you get the data without making you look at ads. It’d mess up the economy.

But what if I do it myself? What if I do it quietly, without making a fuss?

Mint.com

I like that Mint.com talks to my bank, and there are a few features I like. But the overall experience of using the site is frustrating. I have to log in each time. Most of the screen is filled with things I don’t need to see. There are upsells and ads to try and ignore because they help Mint.com pay the bills.

I wondered what I really wanted out of the experience, and realized I’d be happy just to see my total money in the bank. But not just for today. I want a historical record. I am saving up for a big purchase and I’d like to see how I’m doing as money comes in and goes out. But that a graph like that isn’t something Mint is designed to do. So I thought I’d give it a shot.

Success! It took me a few days, but I wrote some code that connects to Mint, finds my total balance, and saves it in an ongoing log. Awesome. Next I’m going to get it to help me play my “Spend less this month than the month before” game, complete with graphs. I’ve been doing it manually in a spreadsheet, but I’m looking forward to automating it. I hear computers are good at that.

Memeorandum and RealClearPolitics

I am a fan of political news. I like memeorandum.com because it lists all the political news of the day. I like RealClearPolitics so I can see how the Clinton/Trump election is panning out.

But I wondered if I could turn down the volume on Memeorandum a bit. I thought it might be nice to only show the top articles, not all the sub-articles with their own spin on the same story. I also thought it would be nice if the links would disappear after I clicked one. That would make it a bit more like a to-do list. Have you heard of “Inbox Zero?” Imagine “News Article Zero.”

Success! This also took me a few days, but I have it working just how I’d like. I see the most important stuff, and if I read the article, it disappears from the list. Perfect.

Social Networking

Next I’d like to figure out social networking. The standard “stream of content” view isn’t super helpful for me. There are some people such as my wife whose content I just want to read as a single feed. Then a lot of the other stuff is best aggregated based on popularity. A person with 10 followers that normally gets 1 favorite just got 7 favorites on something they said? Cool, an increase of 700% sounds important. Show it to me.

Media is a whole other thing. Pictures are most easily scanned in a grid, not a stream. I’d like to find a way to take the media from people I know and present it in its own view. Let me see the world at a glance. If I care enough, I’ll click through to see the caption, but they’re not necessary at the top level.

Silly Entertainment

Reddit and Metafilter do a great job of bubbling the best content to the top. Maybe show me the 5 best stories from each, based on the amount of engagement they get. If I know it’s always the best stuff, and not a giant list, I’m more likely to take a look.

TV, Podcasts, Music

There’s just too much media out there. A simple reminder of what I listened to the day before goes a long way. Sometimes I watch a few episodes of a show but forget about it. If I had a reminder that I was listening to album x and watching show y yesterday, I could recover some continuity.

People Interacting With My Stuff

I’d like to write a script so I never have to manually check my notifications on Twitter, Facebook, or Medium ever again. Get rid of my “unread” dot and give me a good summary. #downwithdots

Records Of Things I’ve Read

I’m a little concerned about my “information diet.” I’d like to scrape my browser history and get a sense of just how much stuff I’m reading online. I’d like to figure out how much of it is actually helpful and how much isn’t.

Then What?

I’d like to bundle the whole thing into a single webpage that I read each morning. No categories, no tabs, no filtering mechanisms. Just a simple readout of the things I care about most, in exactly the order I want to see it. Success would mean I read this personal newspaper in the morning but then I don’t feel the need to stay up to date the rest of the day.

Also, it should be historical. I want to know exactly how things looked on July 1st, 2016. Then I want a different publication to be issued on July 2nd, 2016. Internet software is great at improving itself, but it’s not great at leaving a paper trail. I’d love to see what a year of these publications would look like, and what I would learn from the story arc they tell.

Parting Thoughts

I love the idea that none of this could be a business. Scraping Mint.com isn’t allowed, so any company that promises to do it will probably be shut down. And the same is true for most of my data sources — scraping websites to pull data for personal use isn’t a big deal. But what if the Personal Newspaper software I’m describing started to be used by a million people? The websites I’m scraping would certainly have an opinion about that.

This is one of the central tensions in modern software. Strong business plans and fantastic user experiences will always conflict. After all, would you prefer a website with or without ads? Without ads, of course, until the company goes out of business. Then you just go to the next source and smirk when they ask you to please turn off your ad-blocking. Next!

Would you prefer news headlines that are earnest and accurate, or “link bait?” Well, earnest and accurate of course … but I have data that says you won’t click it as much as the breathless and meaningless stuff. I don’t mean you of course. The others, the ones that fall for that stuff. You’re better than that. We all are. Just not in aggregate. The data shows who we are, not who we want to be.

When I design a webpage with no ads, one that freely steals from other sites with no concern for anything but a great customized user experience, it’s worth pointing out that it could never exist outside my own hobby. It’s because a great user experience that gives you exactly what you want and nothing you don’t … well, it just doesn’t pay the bills.

Like dinner parties, friendships, and writing, some of the greatest software experiences don’t scale. It’s not an accident. It’s what makes them great.

By UX Launchpad on July 2, 2016.

Exported from Medium on May 19, 2017.

Video: Low Fidelity is Magic


Twitter Design Day, 2016
I recently gave a talk on some advantages I’ve found when staying low fidelity when designing complex software.

By UX Launchpad on July 2, 2016.

Exported from Medium on May 19, 2017.

Bury, Bloat, or Kill

Pick one!

Apps are always adding new features. Features demand screen space. Mobile devices have a limited amount of space available. Something has to budge.

You have three options: bury, bloat, or kill. Bury is the dismissive term for putting a button a few taps away, behind a “long press,” or in a hamburger menu. The argument against burying is that engagement goes down if it’s not on the front screen. This is a very good point. But it’s not a veto card.

Bloat is the dismissive term for putting a button on the main screen. Maybe your app starts with five buttons at first. Maybe it’s three navigation options, a search, and a “add a new thing” button. Then you roll out a new feature and need a new button. Then bizdev signs a deal and needs a new button. A year passes like this and people say your app is bloated. They’re right.

I recently read a post by a designer bemoaning how many “buried” features Gmail has. It’s true, the main UI of GMail feels superficially “clean,” but sometimes it’s hard to find the feature you’re trying to use because it’s buried under dropdowns. (and I always peek in the wrong dropdown before finding the right one, which is an addressable problem)

I think his argument was that burying is bad, but I don’t think he’d appreciate if GMail bloated either. I think he’d argue for kill. But removing features once people rely on them in software is extraordinarily dangerous to the health of the business. It’s easy for a designer to mock up at a whiteboard, but it’s very hard to explain to paying customers why they lost their favorite feature. Not to mention why they should trust you anymore.

So what’s the answer? As with anything else in software design, the answer is “it depends.” But don’t go into these debating thinking it’s easy. Don’t think if someone buried they’re a bad designer anymore than someone who bloated is a bad designer. And don’t assume just because someone managed a kill that makes them a good designer. Product design is far more complex than that. Great design lives or dies based on context, so keep an open mind and get to work.

By UX Launchpad on August 14, 2016.

Exported from Medium on May 19, 2017.

Puppy Features

Everyone loves a feature until it grows up

Every animal shelter has the same story. People fall in love with a silly, adorable, floppy-eared, brand new puppy. Then time passes, the puppy becomes a dog, and the owner realizes dogs are a lot of work. As a result, city shelters are full of animals that weren’t properly cared for after the novelty wore off.

It’s sad, but it happens because it’s human nature. New things are fun, sustaining things over time is hard. It applies to everything. Pets, relationships, jobs, and especially software features.

I’ve worked at a lot of companies. I’ve been on a lot of teams. I’ve worked on lots and lots of features. But I can categorize all the work I’ve ever witnessed into to clear buckets: Puppy Features and … other.

Everyone wants the Puppy Features. A bold new bet! “Blue sky” thinking! Discovery! A way to not mess up like all the other dumb features that came before it. Sometimes they even make a whole new team for it. And it’s hard not to fall in love — everything is exciting and new, and the amount of maintenance and long-term responsibility is basically zero. That’s not to say shipping a feature like this is easy … but there’s no question it’s easier than version five. Just like your first move in chess is a big deal, but it’s nothing compared to the complexity of move five.

So then version one ships, grows up, and you have to take care of it. But that’s not as fun. It’s harder to find people who want to work on it. It’s harder to prioritize it. You have to support users, which costs more time and money than you planned for. And as technology marches forward, aspects of the code or design patterns become outdated. Taking the time to get it feeling bright and fresh feels like a waste of time. After all, there are new puppies coming in every day. Shouldn’t we focus on them instead?

It’s human nature to want to jump onto the new thing. But it’s the old things that need the craftsmanship. It’s the boring foundational stuff that needs your best designers and engineers. And if your “best” are bored by that work, I question your definition of best. They might just love puppies.

Puppy Features don’t make for great design. Novel? Sure. Original? Ok. Fun to work on? No question. But great design that solves real-world problems effectively? That doesn’t happen until after version one, if ever. Invest your design resources accordingly.

By UX Launchpad on August 21, 2016.

Exported from Medium on May 19, 2017.

I’m Pretty Excited About the Year I’ve Had

And I got a new talk format out of it!

I’m writing in the middle of the night from my hotel room in Johannesburg. I have jetlag. But I don’t mind because it’s given me some time to think about the funny way things came together this year.

Stand-up Comedy
I have an insatiable appetite for any media that talks about the creative process. My favorite documentaries follow musicians or comedians while they put their material together. My favorite books are biographies that give you a peek into how highly creative people work.

Yet for some reason, I’ve barely ever seen stand-up comedy in person. Until this year. I went a handful of times, I recorded the audio, and I wrote some some Design Explosions-esque critiques of the sets. It was a lot of fun.

Live Music
Whether straight-ahead rock and roll, noodling improv sets from jam bands, or my all-time favorite artist DJ Shadow, I love analyzing sets. I love bands that take risks and attempt to play a different show every night. And hey, there’s nothing wrong with seeing a highly scripted show — my boy and I saw Taylor Swift and it was amazing — but I have soft spot for high risk/high reward performance.

And not just as someone sitting in the crowd. I listen and re-listen to live performances to do spreadsheet-based data analysis to find the science behind the serendipity of improv work. What songs get paired most often? What’s the rarest set opener? Did it work?

Improv
I’m not a giant theatre or improv nerd. I’ve gone to some improv shows, but it’s not something I do often. But this year I discovered Too Much Light Makes the Baby Go Blind in Chicago. In 60 minutes, the group tries to do 30 plays. The funny stuff is really funny. The poignant stuff is really poignant. The current events stuff feels incredibly vital and important.

I left the show completely transformed. I wanted to write an email to every cast member thanking them. I wanted to collaborate with them on an art project. I wanted to know them in real life. I wanted to find a way to make an audience feel the way they made me feel.

Why I Am Writing from Johannesburg
I was asked to give a talk at Pixel Up! a new UX conference in Johannesburg, South Africa. And I wondered if maybe I could do a new kind of talk. Instead of a standard lecture format around 30 minutes, I wondered if I could do 10 talks at 3 minutes each. It would make each story a self contained thing. Like a song. Or a joke.

So I wrote up a “set list” where I compiled stories that seemed to be crowd favorites. I’ve talked about each of these in the past, whether written in an essay or performed as part of a larger talk. But I had never let each story stand on its own before. So I spent some time restructuring them each to have a clear beginning, middle, and end.


Then I thought it might be cool to have some audience participation, like Too Much Light Makes the Baby Go Blind in Chicago. So each time I finished a section, I planned on asking the audience to shout out the next request, leading to a totally random setlist.

This meant I needed to design the setlist as a webpages so I could click on each talk as each request came in. I went with a strikethrough like this, which helped me to not lose my place even as the talk went non-linear:


And then I had a silly idea: maybe I could have an encore. And here’s a question: if you had an encore to design, what would your style be? Crowd favorites? Rarities? New stuff? Here’s what I ended up with after working through it a few days:


For people who have read my work, this is a dream encore because it’s my most popular stuff (with a newer one thrown in). But it wasn’t just for fans. This is my best material, so I was hoping it would work either way. I spent a lot of time practicing the encore because I’d hate to have a good show followed by a blah encore.

Whew!
So the big day came and I got to have a lot of fun with the new format. It helped that the audience was engaged, which always makes this sort of thing work better.

As I wound down my talk, looking at the smiling audience, I was filled with gratitude for the creative community and the power of inspiration. The talk format I had just performed was new to me, but it was deeply familiar. I was just doing my best to be as good as my heroes.

I wanted it to feel like Too Much Light Makes the Baby Go Blind meets You Enjoy Myself meets Midnight in a Perfect World. I wanted the intensity of Kathleen Hanna, the courage of Richard Pryor, and the kindness of William van Hecke.

And this is how inspiration works, both in the creative community and outside of it. Each time you do a good job, someone is watching. Someone is thinking “hey maybe I can do that too.” This is how we make ourselves better, each of us as individuals and in larger communities.

I had a great year because the world is full of great people.

Postscript
We’re using the new app Talkshow to document what’s happening at the conference. Feel free to follow along!

By UX Launchpad on September 13, 2016.

Exported from Medium on May 19, 2017.

It’s Not Hard to See Where This Goes

Some thoughts on the future of Apple’s product line

I don’t have a strong sense of how settling Mars will pan out, or self driving cars, or Alphabet’s next big bet, or if the Snapchatification of UX is a blip or the beginning of a new era in software design. But when it comes to eyeing Apple’s current product line and tying themes and insights back to what we know of the future of computing, I have some thoughts.

Let’s set the scene with four (or five) key areas:

1. The Internet of Things (IoT)

Designers have been talking about IoT for a very long time. I’ve personally worked on IoT stuff, I interviewed to be on an IoT team at Microsoft, and I’ve read a ton of articles on it. My favorite is called The Basket of Remotes Problem, which points out that if you have a DVR, a TV, and a stereo system, you probably have three remotes. That’s bad.

Now multiply that times everything in your home being internet enabled. Your car, your fridge, your thermostat, access to the doors in your house, security cameras, on and on. If that were to fragment, it wouldn’t just be bad, it would be completely unacceptable. Having ten IoT devices and ten apps to control them just isn’t going to fly in the consumer world.

2. Wearables

Smartwatches are hard to justify right now. Most mobile tasks are better done on a smartphone due to the large screen. And for a variety of reasons starting with battery life, exercise tasks are better done with a standalone unit like a FitBit. So that’s an awkward place for Apple Watch to be.

Three years ago, my friend told me the next big thing in wearables would be bluetooth headphones, perhaps in some kind of necklace form factor. He found while traveling that using his phone was distracting and called attention to himself, whereas speaking to Siri through headphones allowed him to get key information in a more low key way. That was notable.

3. The Living Room

Sony, Microsoft, and Nintendo all have their game systems. Then there are DVR devices for watching TV. Then there’s the rise of Netflix, which for many is becoming the primary app for the living room.

4. Virtual Reality

I have no idea what is going to happen in this space, and certainly not around what Apple thinks about it. Should be fun to find out!

5. Traditional Laptops & Desktops versus Mobile

Tablet sales started off with a boom, but have since tapered off. We now know the traditional PC market wasn’t killed off by the tablet, but it was clearly stalled by mobile in general.

Microsoft was first to blend mobile and desktop together in Windows 8 and then Windows 10, using their Surface product to show off the potential. Apple responded by saying they didn’t believe their mobile and desktop operating systems should merge, but they did release iPad Pro and marketed it in a similar way to Surface.

Apple seems to be saying “iOS is the new platform we’d like you to adopt” and Microsoft seems to be saying “why pick just one?”

Some Things That Will Eventually Be True

There are a few things we don’t know yet, but aren’t hard to see coming:

Apple Watch will get better battery life
Apple Watch will get cellular data
iPad Pro will continue to court professional-grade 3rd party software
iPad Pro will one day have an iOS version of XCode
iPad Pro will become some people’s laptops
HomeKit, Apple’s “one remote” solution to IoT, will gain more support
Siri will continue to improve in accuracy and third party integration
Apple just announced AirPods, their second Siri-enabled wearable
Putting It All Together

Fast forward a few months or a few years, and let’s imagine these things have all come to fruition. You can use an Apple Watch all day … without requiring a phone. You can work at your current job … without requiring a laptop. Your house doesn’t need keys because your doors unlock to let you in as you approach them. You can go on a run while listening to your workout mix, or even ask Siri for directions to a good park, all without needing your phone.

I wrote a bit about the implications of this in an older essay, A Desk Job For My Child. In it, I realized that my love of the traditional laptop form factor doesn’t really mean much to him. Laptops are hard. Laptop games are overly complicated. Toggling between a ton of overlapping windows is frustrating. Now imagine my 9 year old kid going into the workforce in 9 years. Do you think he’s going to work the way we do today? I don’t.

But PCs are easy to call dead. They’ve been written off for two decades. Let’s pick on the smartphone instead. It’s our most cherished device in 2016, so surely we’re going to love them just as much in 9 years, right? I’m not so sure. I think we’ll own them, of course. I think we’ll still use them a ton, of course. But I wonder if we’ll be glued to them in the same way.

My Changing Relationship with My Smartphone

I’ve seen glimmers of the future over the last year with my Apple Watch. I’m still addicted to my phone, of course. I’m not about to give it up and join a monastery. But increasingly I’m tilting my wrist for a bite of data rather than settling down for a whole app meal.

This morning I checked up on my email on my wrist while in the shower. Just two messages, piece of cake. I flagged one for later. While I got dressed I asked Siri aloud what the weather would be. (Meanwhile my kids were doing the same thing with iPads downstairs) My phone was in the other room, tucked in the pocket of my robe.

On the bus, I listened to guided meditation on my headphones — which was disturbed with a tap on my wrist by a question from my wife. I opened my eyes, read the note on my wrist, wrote “sure” as a response, then back to my meditation app — then I switched to a podcast while walking to work. Once there, I opened my laptop and was automatically signed in by Apple Watch.

I worked all morning on my laptop, but I did need to switch to my iPad for several tasks. When I was done, I was able to share the files I had created to my laptop’s desktop. If I had needed to copy/paste across devices, it would have let me do that too, thanks macOS Sierra. Then I went to lunch and only gazed at my Watch between games of cards to check incoming texts.

Other than reading books, there was nothing I needed to do — and nothing I wanted to do — on the smartphone. I still wanted all my apps, data, and internet connectivity. But it was fine seeing it on my wrist. In fact, it was better. And that’s notable.

Apple Will Beat the Basket of Remotes

Apple gets the most criticism in the area they do the best work — vertical integration. The tech industry believes that any app, any hardware, any invention, anything at all should have the same rights to be adopted into a computing platform and a consumer’s life. But this is how you end up with a basket of remotes that all work differently, none particularly well. This is how you end up with products that have no interest in working cohesively.

Apple has said, and will continue to say, if you want to make a door lock that works with iOS, it will work with HomeKit. And if it works with HomeKit, there are things that just won’t be possible. Like sharing personal data as an undisclosed price of admission, for starters. The same for thermostats, cameras, fridges, headphones, and internet enabled toasters. It’ll all go through a single interface and interoperate in a singular way.

So the tech industry will continue to call Apple controlling, closed, anti-competitive, anti-capitalist, anti-American, stifled, and acting in poor faith. But their tight control over what is allowed on their system means they will beat the basket of remotes problem. Which is going to be a big deal.

The Return of “It Just Works”

“It just works” was the highest possible compliment you could pay a computing system in the 80’s and 90’s. (see also: Plug and Play) There were so many competing standards, devices, and software packages that it sometimes felt like a miracle when everything actually worked together.

When smartphones came on the scene, a lot of complexity was removed. Printing wasn’t nearly as important. Cables and dongles weren’t really a thing anymore. And even software was largely reigned in, thanks to the (universally panned at the time) App Store model that meant only vetted software could be installed by end users.

But The Internet of Things is going to throw us back into 90’s. Your smartphone will be at the center of your life, but all these other things are going to want to talk to it. Suddenly it will feel as burdensome as the desktop PC used to, and as confusing to configure as your stereo system. At that point, the system that “just works” is going to have a huge advantage. Any company can set themselves up for this future, but so far only Apple seems to be betting big on it.

So the Apple Watch isn’t really a watch. The AirPods aren’t just headphones. The iPad isn’t “a big iPhone.” Each of these investigations is the result of Apple imagining a future of specialized rather than generalized computing. No one device can be as central and irreplaceable as iPhone (which, recall, is what we used to say about desktop PCs!) but Apple is betting that a portfolio of connected smart devices that can do the same tasks — and in many cases perform better — is where they should be skating.

And so they are. As fast as they possibly can.

By UX Launchpad on September 21, 2016.

Exported from Medium on May 19, 2017.

The NYTimes Unsubscribe Dark Pattern

Purposely bad design can be lucrative. Fight the urge.

If you try to cancel your digital subscription at the New York Times, you’re taken to this screen:


There’s no way to cancel automatically because they’d prefer you call in. I’ve seen the data many times. If 100 people form the goal to cancel from the NYTimes, only a percentage — let’s say 70% — actually make it to the website to start looking around for the unsubscribe link. And of those people, only a percentage — let’s say 50% — successfully find the link. And of those people, only a percentage — let’s say 25% — actually have the time in that moment to call a phone number.

Pretty soon you’ve reduced the number of people who don’t want to pay you anymore from 100 to 9. That’s a big deal. That’s why they do it. And they’ll keep doing it until someone calls them out for their cynical and user-hostile use of lucrative dark patterns. So here I am, calling them out.

Here’s the transcript of the chat I had with a nice gentlemen named Daniel:

Daniel:
Good morning, Jon. Thank you for contacting the New York Times. My name is Daniel, I will be happy to assist you with your account. How may I help you today?

Jon:
Please cancel my account. Also please tell your supervisors that I should be able to cancel via the website rather than having to chat or call. Thank you.

Daniel:
I apologize for the inconvenience. I do have a process to follow and I will try to make it as quick and painless as possible. May I ask what has prompted this change to the account today?

Jon:
The inability to cancel via the website and this line of questioning.

Jon:
If I am not able to opt out of these questions right now, that is a huge problem. It’s disrespectful to people’s time.

Jon:
I am sure you are a wonderful person, Daniel, but the NYTimes policy requires people to take time to sit through this. Which means I will warn people off the service and you won’t be able to “re-hydrate” my account as they say in the biz. That’s bad long term thinking.

Daniel:
I have completed the request, is there anything else I can assist you with today?

Jon:
Nope, thank you. Again, you did a great job Daniel. I’m just annoyed at the larger NYTimes org. Have a nice day.

Somewhere, on a spreadsheet at the New York Times, an integer just decremented. Thousands of people like me are subscribing and unsubscribing all the time, and there are people whose jobs are to make the good numbers go up and make the bad numbers go down.

The problem is those spreadsheets don’t measure long term frustration. They can tell if I subscribed or unsubscribed. They might be able to tell if I’ve returned as a user after unsubscribing for a year. They surely know the kinds of articles I like to read the most. But there’s no column for disrespect. There’s no integer describing my disdain for their business practices.

So the New York Times will use dark patterns to boost their numbers. And people like me will write articles like this. People like you might recommend this or retweet it. And over time, if enough people care, we’ll transcend the spreadsheet. Meetings will be called with names like “Policy Brainstorm re: Medium post.” New policies will be written up. Changes will happen. And things will get better.

That’s the hope, anyway.

By UX Launchpad on September 22, 2016.

Exported from Medium on May 19, 2017.

From “I Don’t Know” to “It Depends”

A reminder to myself while designing


I love talking to people who know they don’t know anything about a topic. Their opinions aren’t sophisticated, but they’re pure. They’re not necessarily right, but they’re great at listening and absorbing new information with an open mind.

I love talking to people who are masters of their field. They have a lot of hard-won experience. Their intuition is pretty solid. And most importantly, they know enough to know that they still have a lot to learn. They say things like “it depends” a lot. They’re not as interested in being right, they’re interesting in learning the truth. They see shades of grey.

But most people, on most topics, are somewhere in between these two extremes. We know a little, but we overplay our hand. We’re more interested in feeding our ego and trusting our gut than humbly listening and gathering information. As we get more sure, we listen less and less well. That’s where the problems arise.

You probably have opinions about politics, religion, parenting, exercise, eating right, alcohol, the educational system, taxes, and the right way to do your job or your hobby. And if you’re like the rest of us, you probably think your conclusions on these topics are self evident. That the merits of your thinking are unimpeachable. You probably wonder sometimes about “the other people” that are too stupid or sadly misinformed to agree with you, vote with you, eat like you, work like you, parent like you, drive like you, whatever.

But maybe in that list above you shrugged at a few of the items. Maybe you don’t have a strong opinion about taxes. Or exercise. Or alcohol. I’ve learned to hang on to that feeling. That’s how it feels to have an open mind about something and I’ve found it’s where all the best progress is made. Not only as a designer, but as a person as well.

By UX Launchpad on October 1, 2016.

Exported from Medium on May 19, 2017.

“I Thought You Were Born With It”

Imagine a man in the corner of a design studio, pacing in tight circles, pulling at his sleeves, muttering to himself. “Hi everyone, today we’re going to- no. Damn it. Product design is about tradeoffs, so- Fuck. Hi, today I want to talk about design tradeoffs, because without them… gaaaaah. No.”

Another designer approaches and says, “This is good to see, Jon.”

“What, your design lead pacing around and talking like a crazy person?”

“It’s good to see you practice. I thought you were born with it.”

Wow. No. I was absolutely not born with it.

Years earlier, I worked at frog design. I tried to keep my head down and do good work. I was intimidated by the other designers, and I was not known for my speaking abilities.

So you can imagine my surprise when I was asked to speak to the entire studio as part of our “Third Thursday” speaker series. Someone else had dropped out and a designer named Dawn decided I’d be a good replacement. I was hard at work when she wheeled her chair over to mine. I scooted away. She scooted again, and I scooted away again.

“You’re doing Third Thursday,” she informed me.

“What? No thank you,” I deflected.

“You’re doing it,” she said, and wheeled away before I could stop her.

I had a talk lying around. It was a deck I had used when sent to the University of Washington to explain what a design career looks like. I figured I could re-work it for my hotshot designer coworkers. Maybe.

For the next 20 days or so I practiced my talk 2 or 3 times a night. I recorded myself on camera and studied the tape to see what parts worked and what parts didn’t. I timed myself again and again to make sure nothing lagged. I poured extra attention on the intro and the conclusion.

“Hello, and welcome. Let’s dive right in. No. Damn it.”

“Design is a common buzzword, but what does it really mean? I’m- gah.”

“My name is Jon Bell and you might know me as- no no no noooo.”

Later, Dawn’s husband explained his theory about how much I practice. “You’re terrified it won’t go well,” he said, and he’s right. It’s not because I’m a naturally gifted speaker, or I read an inspiring book about public speaking, or I’m trying to hone my craft. Nope. I’m terrified, so I practice. That pretty much sums it up.

So Third Thursday arrived and I was terrified. I did my best. When I was done, the head of the studio barely waited for the crowd to stop clapping before saying, “That was good. That was really good. You should … do that always. I’m serious. Like, you need to submit that to SXSW or something.”

I started working on other talks and I got to experience life of a speaker. Green rooms. A/V checks. Q&A. Hotel and airfare paid for. The speaker’s dinner. People recognizing you at the breakfast bar from the day before. And in time, I did end up speaking at SXSW, but that was a long time ago.

And that was right around the time my direct report said he thought I was born with it. It’s not glamorous to say, but no. When I do well, it’s not because of raw talent, it’s because I’ve practiced more than anyone says you should. And that’s something anyone can do. And the feelings of terror are optional, but they work for me.

By UX Launchpad on October 3, 2016.

Exported from Medium on May 19, 2017.

Good book: https://www.amazon.com/Hillbilly-Elegy-Memoir-Family-Culture/dp/0062300547

Good article: http://www.vox.com/first-person/2016/9/30/13090100/confederacy-myths-lost-cause

By UX Launchpad on October 3, 2016.

Exported from Medium on May 19, 2017.

I hear this a lot, and everyone has a different approach that works for them. But I don’t think an actor can over-prepare. I don’t think a stand-up comedian can over-prepare. Same with a rapper, debater, or someone in a portfolio review.

I think the conventional wisdom is that over-prep makes you sound wooden. But the most wooden talks I see are people who are under-prepared. The most fluid, natural, off-the-cuff stuff I see tends to be the most entertaining. And in my experience, those people are the ones who put in enough practice to feel comfortable.

By UX Launchpad on October 5, 2016.

Exported from Medium on May 19, 2017.

Leaning Out

I’m learning to let other people talk more.

Sometimes I can’t stop talking. I tell very long stories and I tend to turn two-sided conversations into one-sided lectures. The irony is I would much prefer to stay quiet and listen. But sometimes it’s a compulsion. I can’t stop talking.

I’ve had many female friends and co-workers describe the other side of this. Not just working with me, but most men. When a person won’t stop talking, it’s hard to wrest control of the conversation. Most women I knew end up thinking “Ok, I guess I’ll just be taking notes in this meeting.” Sometimes that’s easier than finding a way to get a word in edgewise.

Women are often told to “lean in” when faced with these situations. But to date, I’ve never had a conversation with a man about “leaning out.” Going out of your way in a meeting to say less than last time. Looking for opportunities to hear a statement you agree with to then amplify it, as women did in the early years of the Obama administration. Trusting that not every topic needs you to weigh in, and knowing that the more you lean out, the more space you’re giving to others to lean in.


Trump is really bad at Leaning Out.
I still talk too much. I haven’t suddenly gotten great at saying less. But it’s something I’m working on, slowly. And the results have been fascinating. It’s not as if I’ve disappeared from the radar. I’m still working just as hard. I’m still contributing just as much. But our discussions have gotten richer as more people have gotten comfortable talking. And that’s always a good thing. Guys, you should give it a shot in your next meeting.

By UX Launchpad on October 10, 2016.

Exported from Medium on May 19, 2017.

What I Don’t Hear At Conferences

This September, William and I went to South Africa for the first time. We were there to speak and teach at PixelUp’s inaugural UX conference, and we both had a wonderful time. The conference is doing it again next year, and I highly recommend going.

The organizers were great. The speakers were great. The audience was great. We all went on safari, and that was great. By the end of our week together, I had gotten to know my fellow speakers at a much deeper level than I ever have at any other conference I’ve been at. That was amazing, and I’m going to miss it.

Of all the stories the speakers shared with one another, one stood out. I thought it was one of those stories told in confidence. Something that would never get posted publicly and discussed. Just a memory from my South Africa trip from a person I met. But we’re all in luck! Rebecca just wrote about it, so now I can link to it.

You know, I’ve been told a lot of unflattering things after taking the stage. I’ve been told to stop moving so much. I’ve been told I didn’t make any sense. I’ve flubbed my lines, answered the wrong questions during Q&A, and accidentally insulted my hosts. But there are certain things I’ve never heard.

But Rebecca has, and she’s not alone. I’m glad she shared her story.

Designers on Safari & Subconscious Sunscreen
Sept 15, 2016
An awe-inspiring event happened immediately following the PixelUp conference — the conference organizers…medium.com
By UX Launchpad on November 2, 2016.

Exported from Medium on May 19, 2017.

Arrows

I asked my co-worker how corporate life felt different from school life. She told me in school, everyone was on the same team. Like this:


Arrows at different speeds all working towards one goal: a good grade
Sure, sometimes people don’t do their fair share of the work. Or they don’t show up for study hall. Or their ideas aren’t very good. But in a school team you just cover for them, since you’re all working for the same end result.

But in the corporate world, the incentives and configurations are different. Everyone is smart and passionate and wants to leave their mark on the product, the team, and the company. So this happens instead.


Every arrow is passionate and driven … but they’re not going in the same direction.
Making big decisions as a team isn’t about slackers versus people with good work ethics. There are just a whole bunch of people who all think they’re right. They’re all driving in their own direction.

So the trick isn’t about motivating people to show up. It’s about listening and hearing everyone’s feedback. It’s about finding ways to agree. It’s about slowly moving the arrows in the same direction. That’s the hard part.

By UX Launchpad on November 14, 2016.

Exported from Medium on May 19, 2017.

A Non-Profit State of Mind

Shifting from for-profit to non-profit

UX Launchpad was founded with a pretty standard business structure. We charge for goods and services, put the money in a bank account, and use that money to drum up more business through advertising. The company has been profitable since its first year. Things are good in the spreadsheet.

A few weeks ago, we ran a design workshop at SHPE, a conference for Latino engineers. A few months before that, we ran a workshop for underprivileged girls in middle school. Earlier in the year, we ran a workshop for women attending the University of Washington that wanted to learn more about design careers. Each of these sessions were pro-bono, and combined with many other examples of low-key community outreach, we’ve spent more time this year working for free than pursuing our for-pay model.

After the election, we’ve been brainstorming good ways to help the country we love. There are plenty of things people can do as individuals. Donate to charity, volunteer, contact your representatives, be heard. But as a business, we have something to offer as well. We can continue finding ways to help in under-served communities and amongst the people who have the most to lose if we move in the nationalist direction Trump and his advisors have long advocated for.

We haven’t formally dissolved our original business structure yet, so we’re still legally a for-business enterprise. But we’re looking for ways to change our business model from “make money” to “help those who need it most.” America is greatest when everyone has a shot. So we’d like to help move towards than ideal in any way we can.

By UX Launchpad on November 20, 2016.

Exported from Medium on May 19, 2017.

Slow or Choppy? A Litmus Test

Analyzing a no-win question

Recently Dustin asked a great question on Twitter and got a pretty divided response. I analyzed the responses below. Which would you choose?


“It Depends”

26% of his responses asked for more information. This is how designers are trained to approach problems, so this is a good start. But it quickly leads into something designers struggle with, which is …

“Neither”

… refusing the premise outright. Sure, there’s nothing in the real world that would force cause you to make this exact decision. There’s always going to be additional context. So 20% of the respondents went with this answer.

But refusing to go with the premise, in a dumb little Twitter poll, is a red flag for me. If you’re asked to rate something on a five point scale and you say 3.5, I don’t think “wow, this person really sees a lot of nuance,” I think “this person is indecisive.” And if I ask you to pick A or B and you flatly refuse, I don’t think you’re principled, I think you have a fixed mindset and trouble making decisions. That can be a problem while working in creative teams, so this isn’t a great answer.

“Both, then test it”

I’m torn on this one. It’s refusing to answer the question, which isn’t great. But it’s also pointing to an emerging reality in software design: computers know more. Ad companies literally generate a few thousand different visual layouts via code, then find the one that tests best. It always tests better than the one the designer made by hand. Sobering thought, right?

So I wish this person had picked one, but you understand the robots are coming, so I have to give points for that.

“Both, then let the server determine the right path”

One person said this, and it’s an evolved version of the previous answer. All software design requires context. Change the brightness when the room is dark. Change the layout when the language reads right to left. Play different sounds based on whether or not the phone has been set to vibrate. Show different content behind China’s firewall. Show the full version on desktop but load smaller assets on mobile. And so forth.

So this answer isn’t just saying “ship it and track the results,” it’s implying that a lot of the design happens via smart algorithms in the background. This is a Real World way of thinking about the problem, so whatever points are docked for not following directions are added back on for seeing where a lot of the design work of the future will live: in the algorithms powering the rendering of the eventual experience.

Slow load

Only 9% of responses went with option A, meaning they’d be dooming their users to a 15 second wait, which is an eternity. Some people said they’d get around it with informative or cute loading screens, and only if it didn’t happen frequently. With that context, I’m ok with it. But otherwise, no. A 15 second delay without an extraordinary amount of user messaging is not an acceptable design, no matter how buttery smooth the app is once it’s loaded.

I sometimes call these “load-bearing animations,” meaning the whole thing comes crashing down if you don’t get the animation and messaging just right.

Choppy

40% of responses went with choppy and used the same rationale I would: sure, a choppy UI is unacceptable, but loading immediately is more important that smoothness, if you have to pick one. And without additional context to understand what kind of experience we’re designing, we should default to fast loading speed.

Summary

Yes, always gather data. But you’re going to hit a point, even after prototyping and researching, where you have to take a leap of faith. And people on your team may be divided. Embrace those situations. Make your best guess, learn from the data quickly, and wherever possible, let the computer help frame the context and gather result data for you.

And never refuse a challenge on its face. There’s always a way forward, and it’s your job to find it.

By UX Launchpad on December 6, 2016.

Exported from Medium on May 19, 2017.

The Project Folder

I keep a folder called “Projects.”

We’re nearing 2017, which means I’m going to rename my “Projects” folder to “2016” and tuck that folder into “Archived.” I’ve done this for 20 years. Here’s what I worked on this year.

jetpack: started a new kind of mentoring framework
hearthstone: designed custom hearthstone cards with my kid
\<name hidden\>: started a political analysis blog
EVEstory: character-driven writing inspired by a video game
twitterbots: using AI to make twitter impersonate me and my friends
DE live: did a talk called Design Explosions Live
Storytelling class: taught this at work
storium: kept up some characters at storium.com
git: worked with Edna Piranha’s gopher project
data: started a new data journalism blog
rnn: Worked with Robin Sloan’s AI writing project
mint-exporter: learned how to scrape mint to auto-update spreadsheets
mint-cron: learned how to automate that
beautiful soup: learned how to scrape any data from anywhere
musicaljon: music reviews
medium exports: started collecting online essays onto my own computer
\<name hidden\>: new book
scrolling audio: soundtracks that sync themselves to text
xcode projects: lots of work with swift and playgrounds
\<name hidden\>: several issues of a new publication
designday talk: did a talk called Low Fidelity is Magic
The Last Talk of the Day But It’ll Be Awesome, I Promise
\<name hidden\>: two adventure books written with my kid
100 songs: didn’t reach 100, but got pretty close
\<name hidden\>: new publication
autofill_array: write little script to help with spreadsheets
\<name hidden\>: new book
trump_questionmarks: an analysis of how Trump tweets questions
\<name hidden\>: a new comic series
screeps: javascript RTS MMO work
RARITY: a new game I’m developing with my kid
Onward!

By UX Launchpad on December 9, 2016.

Exported from Medium on May 19, 2017.

Material Design 2.5 Years In

I like my design friends that work at Google. I’ve respect Matías Duarte and his since his team blew my mind in 2009. I especially love the design specs, best practices, and documentation that accompanied Material Design’s announcement to the world. I was even ok when they pulled a Bob Dylan, though I felt like the only designer other than Luke W that could claim that.

So I like the idea, execution, documentation, and continued evolution of Material Design. I also like using Material Design, but only on Android. I don’t like it on web. And I really don’t like it on iOS.

It’s no different than using Word 6 for Mac, the release where they tried to use a single visual language between Windows and Mac. Or porting iOS apps to Android. Or Java applets. Or speaking in Esperanto. You cannot design once and use everywhere. That’s not how interaction design works.

Each platform has its own culture and interaction language. The best apps find a way to co-exist with the platform’s culture, but eventually someone wonders why they can’t just design once for all platforms. And you can try to shoehorn other languages into a platform, but at best, you’re speaking with an accent. At worst, you don’t make a damn bit of sense.

So I appreciate what Material Design did for Android. I think big chunks of its philosophy can be applied broadly to lots of different scenarios and screen sizes. I think the documentation is required viewing for any designer trying to communicate complex design systems.

But after two and a half years, and lots of different experimentation across different platforms, apps, and websites, it’s easier to put Material’s early promise into more pragmatic terms. I think Material, despite a tremendous amount more cultural impact, is like Microsoft’s Metro Design. It’s beautiful, it’s bold, and it can be great in everyday use … as long as it stays on the platform it’s designed for.

By UX Launchpad on December 11, 2016.

Exported from Medium on May 19, 2017.

Designing for Opposition #1

How might we counter presidential bullying?

Democracy does really well when a bunch of people come together with different ideas and pick the one with the most widespread support, then iterate on it. Democracy suffers or even fails when people’s voices aren’t heard. In that way, it’s no different than product design:

Understand user goals and needs by listening well
Try solving problems
Listen to feedback and iterate
So I’ve been thinking about how I can use my design background to help people feel heard. So I looked around and noticed a lot of people concerned about how Donald Trump frequently targets his critics via Twitter. Because he’s powerful, a single tweet can devastate someone with death threats and other forms of targeted abuse from his followers. That’s not good. So.

Introducing People Against Presidential Hissyfits

What if there was a way to show support for the victims of Trump’s Twitter outbursts? Wouldn’t it be interesting if they got a check in the mail from people who supported and encouraged their right to speak out in a free and fair democracy?

I set up a Patreon page for a quick v1 release. Check it out here.

But I have some concerns about this approach, and perhaps you can help me iterate on this idea by leaving replies to help steer my thinking.

Issue #1: how do people know this isn’t a scam? How might we prove that 100% of the money actually goes to the affected party? Do charities certify donations? If so, maybe that’s how this should be structured, rather than promising a check.

Issue #2: Some people have said they can’t afford spending $1 every time Trump attacks someone on Twitter, since he does it multiple times a month. Patreon has a “limit my monthly donation” option, but I need to be more clear about that. Or maybe I could move to a monthly donation model instead of trying to align it to each outburst.

Issue #3: Publicity. Right now we’re donating $11 per Trump target, but that’s not much. This is the sort of idea that could take off once we hit critical mass, but getting to that point is difficult. How might we get this idea seen by more people?

Issue #4: The Patreon page is currently ugly. I threw together a few images, but I’d like to put some effort into making it more professional looking.

Feedback welcome

Take a look at the page, let me know how I can improve, and we’ll see where we can take this in the next iteration. Thanks!

By UX Launchpad on December 14, 2016.

Exported from Medium on May 19, 2017.

Hi Elliott! My son is Elliott, with two L’s and two T’s. Does everyone misspell your name the way they do with his name? :)

I’ve done a lot of work on designing things that work across all screen sizes, and I’ve learned a few things.

Imagine you had to design Twitter for a 50-inch screen, like a TV set. Twitter is known for its single narrow stream of tweets, which works well on a mobile device. But on a giant screen it looks sort of ridiculous.
In the other direction, imagine designing the core Microsoft Office apps (Excel, Word, Powerpoint) with every single feature in their “Ribbon” (aka toolbar) available on a 4 inch iPhone screen. Yikes. That’s hundreds or thousands of features on a tiny screen, whereas the product really does work best on a big PC screen.
In my experience, the visual language can and should be as consistent as possible across screens. If Powerpoint is orange, and the Powerpoint logo is famous, then you should be using the logo and orange colors on each screen resolution.

But the interaction model is different. You should strive to mirror the interactions people are expecting on each platform or screen resolution, not trying to shoehorn one model everywhere. For example, I think the FAB (floating action button) that Material popularized is decent on mobile but makes less sense on a website when viewed on a PC monitor.

Or imagine if iOS apps were slapped onto a Mac with a 27 inch monitor. Sure, it could technically work, but it would be strange. Certain gestures don’t map to the mouse. Certain platform conventions like keyboard shortcuts aren’t really baked into iOS. And so on.

So I think keeping visual continuity with your product is a fine idea, but it’s pretty superficial. Use the right typeface, the right colors, the right logos, and you’re fine. But keeping to one interaction model, which Material Design encourages, can lead to some odd results.

By UX Launchpad on December 15, 2016.

Exported from Medium on May 19, 2017.

Machine Learning Is Indistinguishable from Magic

… and that’s the core design problem

Machine Learning and Artificial Intelligence have gotten a lot of press over the last few decades, and for good reason. In 1997, we witnessed a robot beating the best chess master in the world. It was a landmark moment, because it foreshadowed a day where machines could perhaps outthink humans.

(Historical note: some disagreed with this conclusion, because chess isn’t as nuanced a game as the ancient Chinese game of Go. But this year a robot beat a human Go master. And so progress marches on.)

But I’ve been seeing a worrying trend in tech circles over the last few years. Machine Learning, or “ML,” has turned into a shorthand for “magic.” For example, you might say “We could use ML to make sure your phone automatically gets set to silent when you walk into a movie theatre.” Or “We could use ML to make sure you never see abusive comments on the internet.” Sounds great, like everything that comes from a magic wand.

But there’s a very real, very tangible, very tactical side to all this magic. And you have to design for it or you’re going to make a really bad experience. Others have written about this before, but here’s my back of the envelope calculation boiling down what I’ve learned over the years:

Don’t draw attention if you don’t need to. Just do it.
Obvious always wins.
Explain the implications of what you are doing.
Provide opportunities to “teach” the system.
Don’t Draw Attention

When Google auto-fills search results, they don’t need to say “brought to you by Machine Learning.” They just auto-fill search results.

When your car notices that you’re skidding and goes into a specially designed skid algorithm, it doesn’t say “you are no longer controlling the brakes, pesky human.” It lets you believe you are still braking while it silently gets the job done.

When your phone knows you always check your email at 7:02 in the morning, then tap Twitter, then tap Facebook, it can pre-load all those requests one minute earlier (as long as you’re connected to wifi and battery power). It doesn’t need to provide a setting for it, and it doesn’t need to explain it’s doing it.

Obvious Always Wins

People are tuned to understand when they’re losing something because they really don’t like that feeling. It’s called Loss Aversion and it comes up a lot in product design. And it’s a big issue when ML gets a bit too aggressive with the decisions it’s making.

Have you ever missed an important message on Facebook because it buried something you cared about? It’s annoying, right? It makes you lose trust in the algorithm. Even if the algorithm is right 99% of the time, our brains are designed to feel anxiety about that 1% that the robot is getting wrong.

So do I think Facebook should go to a reverse chronological stream so people never miss anything? Well, no. I think they made the right call this time. But when it comes to ML, a little bit goes a long way. But use too much, and make too many assumptions, and you’ll be wrong a lot. And even a 1% failure rate can cause people to lose trust.

Dial down the wow factor and dial up the reliability. People should be saying “Of course it works that way,” not “Wow.” Otherwise the only people using your products will be enthusiasts, not mainstream customers, and the product will probably fail.

Explain the Implications

Email spam filtering is a fitting example. In Gmail and other products you can say “Never show me spam like this again.” And then it will show you all the similar messages that would be affected by your new filter. This way you can see exactly what assumptions the product will be trying to make on your behalf, which increases the quality and the trust level.

All Machine Learning that requires messaging falls into this category. If you’ve decided you need to tell the user about some gee-whiz ML feature, then you’ve signed up for clearly explaining the implications of their actions. And you also need to…

Provide Opportunities to Teach the System

Spam filtering, again, provides a great precedent. Sometimes the robot gets a bit over-zealous with its filter and suddenly your birthday greeting from Aunt Gretta is banished to the spam folder. But when you find it, no problem, you can tell the system “never mark this sender as spam.”

This is a great pattern for a few reasons. You are able to see the implications of the incorrect decision, you trust that the system is never going to delete your messages entirely, and you are able to teach the code to do better next time. This makes the system “smarter” while increasing your trust. Win-win.

If you’re working on products as part of a team, and someone refers to Machine Learning, here’s what you do. First, you should appreciate that they’re thinking boldly. That’s a good start. But then you should get to a whiteboard with this person as soon as possible to hash out the specifics. The seeds of success or failure are sown in those first discussions. What is the problem we’re trying to solve? Where do we want to take the user?

Do you ever wonder why Steve Jobs was so unsuccessful early in his career and so successful later on? This lesson. The moment he learned it and internalized it, Apple flourished. Here’s a quote from 1997, before he returned as CEO.

“One of the things I’ve always found is that — you’ve got to start with the customer experience and work backwards to the technology. You can’t start with the technology and try to figure out where you’re going to try to sell it. And I’ve made this mistake probably more than anybody else in this room. And I’ve got the scar tissue to prove it. And I know that it’s the case.
And as we have tried to come up with a strategy, and a vision for Apple, um, it started with … what incredible benefits can we give to the customer? Where can we take the customer? Not starting with — let’s sit down with the engineers and figure out what awesome technology we have, and then how are we going to market that. And I think that’s the right path to take.”
And that’s where we are with ML. As powerful as it is, it can’t change the reality that the human mind hates black boxes, hates it when the robot makes the wrong decision, hates it when the machine won’t allow the user to undo incorrect assumptions, and hates over-hyped and underwhelming products. In fact, ML’s incredible power is its ability to guess and leave the user out of the loop, which is why it could make even worse software experiences than we’ve seen up until now. A sobering thought.

So it’s our job to keep an eye on that, even as we appreciate what magic ML can perform in the right hands. If we’re careful, listen well, and work hard, those hands could be ours.

By UX Launchpad on December 15, 2016.

Exported from Medium on May 19, 2017.

Imagine

A series of thoughts about we communicate

Scott Adams, the Dilbert Guy, started putting his email address in his comic in the 90’s. I knew someone that tried sending him an email, during a time where no one had email. “He wrote back and he’s awesome!” It was a magical experience, on par with that celebrity sitting at your dinner table. Speak with anyone, anywhere in the world? It felt like the novelty of that would never ever wear off.
It did. Companies over the last few decades have increasingly used the phrase “Join the Conversation!” to mean “engage with us on the internet!” But it’s not magical. No one buys a product and thinks “I should heed this product marketing message and go have a conversation with a bunch of strangers about it now that I have purchased it!” If anything, they think “If this product sucks, I look forward to making them look bad on Twitter as a way to gain leverage in my customer support request.”
I’m not going to say all the most interesting people I know aren’t on social media, but let’s try this instead. If I were to rank the most interesting people I know, you’d notice a very high concentration of people who have sworn off social media at the very top. This is true today and it was true seven years ago. Originally we rolled our eyes at the “I don’t even own a TV” hipster types, now social media abstinence is starting to present like a side effect of robust mental health.
Marketers love to divvy the population into five groups and draw it like a bell curve: innovators, early adopters, early majority, late majority, and laggards. That’s one way to frame it, but it implies everyone will eventually want something, and will not tire of it. It’s perfect for something like a smartphone. But how would you map that bell curve to a decision whether or not to have children or whether or not to buy or rent? It’s not a bell curve. Not everyone ends up in the same place. For that reason, I think communication styles transcend the bell curve.
Some people always want to be the star of the show. Some people prefer supporting roles. Some people don’t want to be involved, and that’s the largest group of all. In an online community it’s assumed that 90% of the audience will “lurk” without contributing anything, 9% contribute sometimes, and 1% contribute a lot. They’re lurking. They want to stay lurking. There’s no way to “upsell” them into being a heavy contributor.
“Lurk” is an online word, and a negative sounding one, but it plays out in all social situations at all times. If you go to Times Square to look at the sights without building a skyscraper, you’re lurking. If you attend a company meeting but don’t present or ask a question, you’re lurking. If you go to a party and enjoy yourself without choosing the music, preparing the food, or making some memorable scene, you’re doing the party version of lurking. “Lurk” just means “consuming.” We all do it, most of the time, in most areas of our life.
It doesn’t take that many people to destroy an online community. You show up, you do a few well-known tricks that any group will fall for, and you watch things fall apart. Social structures are surprisingly fragile and it’s just a matter of knowing which support beams to hit. It’s like how a party can go from fun to blah just by adding a few bad apples. A team’s workplace culture can absolutely collapse by adding one rude co-worker. Now imagine instead of being a rude party guest or co-worker, you’re actively trying to harm a community. It’s pretty easy to do.
I read something that said in China the state-run media isn’t trusted, so nothing is. I thought this was a profound point. It’s not that people are un-critically believing everything. Instead they have to erect their own belief structures. I always trust this person, I never trust that person. It pushes the power away from objective truths and instead towards those who can employ the most emotional tactics.
For five hundred years, conventional wisdom has told us to absorb as much information as possible. Read the news to stay informed. Read books to understand how humankind processes information, goes to war, falls in love, tells itself stories about meaning, morality, and righteousness. Don’t settle for one point of view, or two, or three. Always stay curious, always learn more. But these ideas make an assumption that the data hasn’t been weaponized against you.
People joke that CEOs and politicians aren’t really human, but there’s perhaps some truth to the joke. There is a link between psychopathy and leadership. Excuse me for over-simplifying a lot of nuanced research, but psychopaths don’t feel empathy, meaning they don’t care what you think. And if they don’t care what you think, they can make logical decisions without human emotions clouding their judgement.
Dak Prescott, the rookie quarterback for the Dallas Cowboys, had a bad game last week. Bad enough that rumors were swirling that he would be benched. Asked about how he was handling the pressure, he said “Don’t put myself in a situation to hear. Don’t go on Twitter. Don’t go hanging out asking fans opinions or anything. Just hang out and be myself. Family definitely knows not to come at me with ‘he said, she said.’ They just kind of know that. They know how I react. If they say something I usually don’t respond to them. So they know. It’s noise. No matter if it’s good or bad. No matter if it’s success or adversity. It is all noise. It doesn’t affect what I do or how I feel each and every day or how I go about practice. I just have to stay focused. I know what I can do. I just have to continue to do that.”
When I first showed up at Microsoft, someone pointed me to an internal chatroom/Twitter/bulletin board tool. I couldn’t believe it. It was literally thousands of angry discussions where Microsoft employees were all bashing each other. For example, someone might say “This feature sucks” and then a bunch of people would add their thoughts. Inevitably someone would ask “Where are people from that team? Why can’t we ask them a question directly?” and everyone would agree it was kind of lame they didn’t show up. I tried engaging with the folks on the forum, but it was a waste of time so I never went back. I unsubscribed myself from everything other than the bare minimum I needed to do my job.
Billboards aren’t widely used in Maryland, where I grew up. But when I drove into Virginia or North Carolina, I noticed them everywhere. Compared to what I was used to, even as a city kid, they felt invasive. Near Portland, there’s an infamous billboard that spouts propaganda. This means every time I drive that road, I see the sign, I read it, and then my thoughts get altered and darkened for the next few minutes while the message takes root in my thought process.
Stephen King wrote in his book on writing, creatively named On Writing, that he absolutely believes in telepathy. He described a scene in detail, then says “I sent you a table with a red cloth on it, a cage, a rabbit, and the number eight in blue ink. You saw them all, especially that blue eight. We’ve engaged in an act of telepathy. No mythy-mountain shit; real telepathy.” And I agree with him. Placing ideas in someone else’s head is magic. The people who can do it the best get the most power, whether it’s a friend, a business, or a government. Whether it’s done with pure intentions or to harm you.
Every highly successful person will tell you that you have to pick the things that matter and ignore everything else. Ignoring is your brain’s shield. Without it, you’ll suffer every sling and arrow. But communication channels today aren’t optimized for ignoring. There’s no money in it.
There was a time when being overweight was seen as a sign of prestige. After all, only rich people can afford that much food. But eventually things changed. Food became plentiful, so the new status symbol was being impossibly thin. After all, only rich people can afford the time and effort to sculpt their bodies.
We may be approaching a similar place with our information diets. I’m not impressed by a home library with books to the ceiling. I’m not interested in reading other people’s think pieces all day. I no longer see 100 open browser tabs as a sign of a curious person. Instead I see information gluttony, and I see it making me sick. I don’t see a call to “join the conversation” anymore the way I first did with the Scott Adam’s email story. I see hundreds of thousands of telepathic attack vectors, each with their own goals. I don’t see any reason to continue inviting them into my mind.
By UX Launchpad on December 19, 2016.

Exported from Medium on May 19, 2017.

Pogo, Panos, and Silos

Some historical context

Many years ago, Sunday comics were exclusive to one newspaper. It made sense for everyone. The creator of the comic would get more money. The owner of the newspaper could make their content more “sticky” to increase their subscriber count. And the fan of the comic … well, they were fine as long as they subscribed to the correct newspaper. Red flag.


Pogo, one of the most beloved comics of all time. It directly inspired every comic strip you’ve read since.
The model didn’t work because it didn’t engage consumers fairly. If Pogo was in your local newspaper, you could read it. But if it wasn’t, you couldn’t. This meant people reading the New York Star who loved Pogo were well served, but no one else was. This meant less of an audience for the creator and less choice for consumers. Bad for everyone except the New York Star.

Then we moved to a syndication model. Exclusives were phased out, meaning any newspaper could include any comic for a fee. So if you wanted to read Pogo, you’d tell your local newspaper. They’d gauge demand, and if they decided it was worth it, they’d pay to run Pogo. This meant more money for the creators, more choice for consumers, and it meant that newspapers had fewer ways to differentiate themselves from each other.

Hold that thought.


A Windows Phone “panorama,” sometimes called a Pano
I worked on the Windows Phone design team with about 100 other people. At one point, I was asked to work with two other designers to figure out the future of the Windows Phone “Pano.” See above.

This is a novel attempt at breaking out of app silos. In one app called “Music + Video”, the design calls for music, videos, and podcasts to all live in one place. So does that mean it’s a fancy folder or app launcher? Sort of. Is it a way to see all media you’ve consumed recently, whether it’s a YouTube video, TV show, or song? Sort of. Was it confusing? Yes. Very much so.

People are used to going to one app at a time for this. You launch Spotify and do Spotify things. You launch YouTube and do YouTube things. There wasn’t much of a demand for mashing everything together. It wasn’t working with critics or mainstream users. It was a muddle.

Our team proposed an improved cross-app search. The app would be called “Videos” or “TV” and would conduct a search across your apps and the app store. Load it up, type “Atlanta,” and it would direct you to the show as soon as possible. If you have the right app, it would begin playing. If you don’t, it would tell you how to find the app.

Once you have that framework built, you can put the search into the OS layer itself. Why shouldn’t you be able to say “Cortana, I want to watch Atlanta?” and have the phone do the right thing? Why make someone hunt down the right app by themselves? Heck, you could even have a simple “Shows you might like” app based on understanding your preferences.

Hold that thought.


Apple’s new TV app
Recently Apple shipped a new app called “TV.” It does exactly what newspapers did 75 years ago with syndication and exactly what my team sketched out on the whiteboard four or five years ago. Apple didn’t invent it in 2016, my team didn’t invent it in 2012, and newspaper didn’t invent it when they moved to syndication. But it sheds a light on an important lesson. It’s so basic, and so fundamental, I’d call it a law of design physics. When you make things easier to use, people use them more.

But to make them easier and more seamless, you need centralized planning. You will never see two (let alone ten!) companies working as well together as one company can. But when you move from a multi-company model like Windows and Android use to a one-company model like iPhone uses, the power gets centralized. Meaning there’s less money to be made. Which corporations have opinions about.

Netflix has opted out of Apple’s TV app. And it’s a shame, though I’d do the same thing in their situation. Netflix is enjoying their exclusivity model because it’s making them very rich. “Why bother with competition?” they say. “Just use our app and everything will work out great.”

Except, no. Netflix has some content, yes. But it doesn’t have the majority of the shows I want. I have no more allegiance to Netflix than I do any single newspaper. When Netflix makes a great show, it should appear in my TV app next to great HBO shows, great FX shows, and great Hulu shows. When we talk about “user centered design,” that’s what we mean. And when we talk about “capitalism,” we expect Netflix to act exactly this way. They’re simply incompatible, as much as we wish they weren’t.

This has always been one of the central tensions for product designers. What’s best for your customer will often go against what’s best for your company’s bottom line. Sometimes you can serve both, but don’t assume it will happen naturally. And don’t be surprised if the bottom line wins.

By UX Launchpad on December 23, 2016.

Exported from Medium on May 19, 2017.

The Triangle Epiphany

Courtesy of my cousin the sniper

We hadn’t seen each other in nearly thirty years and now we were hanging out at McDonald’s. He had been to war. Several times. I squinted and tried to reconcile the enormous man before me — fingers so large he couldn’t use a smartphone! — with the four year old I remembered.

At one point I tried to make the point that he was saying a lot of progressive/liberal things, whereas most people I knew in the military tended more conservative. I wanted to know if that made him feel like an outsider. And he said “You’re thinking in a spectrum from left to right. But that’s one-dimensional.” He held his two fingers together in a plane. “But, see, there’s a whole other world over here.” And he rotated his fingers to form a triangle with his thumbs.

And that was the triangle epiphany. It’s stuck with me ever since.

He wasn’t just saying the same old “a pox on both parties!” thing that most people say when they’re frustrated with a two party system. He wasn’t calling himself a moderate. He wasn’t borrowing social concepts from one party and fiscal concepts from another party. He was not in the middle of the spectrum, he was rejecting the entire premise of a one dimensional view of politics. He shared some specifics and I realized his triangle framing checked out. He wasn’t “a little from column A, and a little from column B.” He was saying “I’m in a different spreadsheet.”

I lean on this framing technique at work all the time. When I’m helping a team to the best of my ability, it’s because I’m facilitating and coordinating and helping reconcile a million different incentives and goals across lots of different people. And my best work, the stuff for the trophy case, it never comes down to the pixels. It’s when I’ve been able to see the spectrum of ideas from A to Z, but I’ve somehow been able to turn the entire spectrum of thinking just enough on its side to add a third dimension to it.

By UX Launchpad on December 26, 2016.

Exported from Medium on May 19, 2017.

Jim Morrison vs No Man’s Sky

“Enough to base a movie on?”

Jim Morrison wrote a poem that asks “Did you have a good world when you died? Enough to base a movie on?” It’s an exciting premise. Travel to interesting places, meet and fall in love with amazing people, do great things, rack up the life experiences, leave behind a lot of great memories. Enough to base a movie on.

I briefly had a manager that had a similar worldview. He sketched it all out on a whiteboard while I sat silently. “I want to retire in ten years. It takes two years to make anything great. I have five shots left to be great. That’s it.” I thought about his working style. He was confident and brash, and people found it really hard to work with him. Many people quit because of him, including me. I remember thinking “This is a deeply unhappy man,” and it felt like the reason why might have been scrawled on that whiteboard, a dead-end screenplay masquerading as something more noble.

No Man’s Sky is easy to describe. It’s a video game where you can fly through the galaxy and discover new planets and the lifeforms on them. There’s practically no plot, character development, or conflict. First you’re on a planet. You see things. Then you fly to another planet and see different things. Repeat. Is there enough to base a movie on? No, and I love it.

As long as we’ve had social norms, we’ve have people questioning them. Thoreau was not the first or last person to want to simplify his life, get out of the rat race, and become more introspective. It’s a natural counter-reaction to feeling overwhelmed by all the things you’re supposed to do. Do well in school to get a good job to afford a house to … what? What comes next?

Games like No Man’s Sky put that question front and center. There’s no princess to save, no dragon to slay, no mid-career slump. You’re on a planet. You’re alone. There’s a strange creature over there. Now what? What can you come up with all by yourself? I love that feeling.

One of my favorite essays is called Solitude and Leadership. His points about hoop jumping really resonated with me.

I know what it’s like for you guys now. It’s an endless series of hoops that you have to jump through, starting from way back, maybe as early as junior high school. Classes, standardized tests, extracurriculars in school, extracurriculars outside of school. […](#) what I saw around me were great kids who had been trained to be world-class hoop jumpers. Any goal you set them, they could achieve. Any test you gave them, they could pass with flying colors. They were, as one of them put it herself, “excellent sheep.” […](#) What gets you [up the career ladder](#) is a talent for maneuvering. Kissing up to the people above you, kicking down to the people below you. […](#) Being whatever other people want you to be, so that it finally comes to seem that, like the manager of the Central Station, you have nothing inside you at all. Not taking stupid risks like trying to change how things are done or question why they’re done. Just keeping the routine going.
I meet a lot of Jim Morrisons in tech, trying to make their life a movie. They want to be on the team developing the coolest features, with the smartest people, eating only the best food, drinking the best drinks, and getting the most credit from the widest range of people. Some succeed, but they tend to have a reputation for being hard to work with. I think a lot of it stems from hoop jumper ennui.

My career increasingly reminds me of No Man’s Sky. If I look at it one way, it’s all the same thing with slightly different textures or colors. There might be a trillion design projects, but if you squint, they all pretty much work the same. And that can feel stale and stifling if you’re trying to make a 🔥 🔥 🔥 highlight reel. But if you’re just trying to learn and stay curious as long as possible, wow, the possibilities feel vast and exciting. Let’s go exploring!

By UX Launchpad on January 2, 2017.

Exported from Medium on May 19, 2017.

You Are Your Followers

My new favorite metric

I try really hard not to look at follower counts, likes, retweets, and other engagement metrics. I’ve been doing this since the mid-90's, when early websites showed “hit counters” to see who had visited the page. I don’t think I ever set one up. Probably because it was too hard, but I liked the side effect of not having to worry myself with it much.

A few years later, most bloggers got really good at analyzing their server’s access logs. They’d show you who was visiting you, with what kind of browser, from what country, and lots of other data. Some of the most interesting stuff was from Google search. You’d see that someone searched for “Ugly shoes,” somehow landed on a blog post about your brother’s wedding, then spent 24 minutes and 17 seconds reading your other blog posts, none of which had anything to do with shoes, ugly or otherwise. Often you were left to imagine what they were thinking and why they stuck around. It was fun, but there are only so many hours in the day. I’d rather be writing than analyzing who’s reading.

These days, it’s a lot easier to understand who’s following you, and why. And not just “easier,” but rather “impossible to miss” because it’s everywhere. Engagement metrics power everything, so you’re shown them in your email, in the UI of the app, and they’re powering every new feature. Since I can’t dodge this data anymore, I’ve learned more about who follows me.

On this UX Launchpad account, I get lots and lots and lots of people who work in tech since I write about design. (Boilerplate bio: “I live in \<city\> and I love hand crafting delightful #UX experiences, great \<coffee and/or alcohol\>, and \<hobby that’s not related to tech\>”). That makes sense.

On another Medium account where I once wrote an essay popular with self help types, I get a lot of self help people, a higher percentage of men, and a higher percentage of people from other countries. (Boilerplate bio: Founder of \<project\>, \<project\>, and \<project\>. Dad. Entrepreneur. #\<inspirational phrase as hashtag\> #\<another hashtag\> Find me at \<personal branding websites and other social media accounts.\>)

Knowing your follower information means you alter what you write about and what you say. You write differently when 10 people are following you versus 10 million. You write differently when you know your audience is primarily sports fans versus young lawyers versus gamers. And that’s not necessarily a bad thing. In fact, this is one of the core things they teach designers. You don’t build a thing in a vacuum (not a successful one, anyway), you build a thing for a particular audience, so you need to learn as much about them as you can. The better you listen, the better you design.

Yet that’s the same attitude that leads to poor products. If everyone wants X, you give them X. Until one day, someone makes Y and everyone stops caring about X. How does that happen? Because the greatest leaps forward aren’t focusing on the end result, they’re focusing on the insights driving the end result. For example, Ford didn’t deliver the faster horses people were asking for, he delivered a completely other thing that was even better than a faster horse. But that was hard to imagine at the time.

I see the same balance in my design career. You have to listen to what people are saying they want, of course. But you also need to be able to read between the lines of what they’re really saying. Sometimes they’re asking for faster horses because it’s the only implementation they can imagine. But you need to focus on the insight instead, which will lead you to the right implementation.

And that’s why seeing my follower count and their bios ends up being an unhelpful distraction. It’s really nice to know that you, dear reader, probably a designer or developer, likes reading my work. Thanks for stopping by! But in a perfect world, I’d be writing things that more than just designers can appreciate. In a perfect world, I’d turn around and see just as many non-designers as designers following along.

So that’s emerging as my new favorite metric: the more different a follower is from me, the more I must be doing something right. The way to get there isn’t from reading bios of people like me, reading articles recommended by people like me, and hanging out with people like me. The way to get there is to run far away from everything I know, listen to new voices and experience different things.

I’ve been working on that as a personal side project. But how would you design an algorithm for that? Is it even possible to do well? Anything’s possible to hack together technically, but could you build an entire business around that idea? And if growing beyond your current social graph isn’t a feature that can support a viable business, isn’t that a big problem?

By UX Launchpad on January 4, 2017.

Exported from Medium on May 19, 2017.

Creating vs Talking About It

I’m not going to say anything you don’t already know.

Talking about doing something actually releases happy chemicals in your head. And that makes you less likely to actually do it. So if I say “I’m going to write a novel!” I’m actually robbing myself of some of the chemical motivation. So, step one: stop talking about it.

The human mind likes to grow and succeed. (The three key words are mastery, autonomy, and purpose — you want to get better, control your destiny, and feel like it matters.) So you can set up little games for yourself. My favorite is making “chains” and then trying not to break them. Each day I write. Then I keep a log of my hot streak. As of today, I’m up to day 38, a record! So, step two: make things into a game. High scores are motivating.

In my view, inspiration is misunderstood and overrated. Inspiration is what turns “I’m too lazy to do it” into a much more noble-sounding “I’m just not inspired.” But it’s the same thing. Either way, you’re not doing it. So imagine 100 people are reading this article, and 90 are inspired to go create something. How many do you think actually turn that into a tangible result? Probably five or less. And that adds up. Those 5% are the ones getting the good jobs, breaking through writer’s block, and finding meaning. So, step three: get to work. There’s no trick, there’s no secret. It’s just work. Go!

Now, you’ve probably read this kind of self-help article a million times, right? What can we learn from this? I put it right in the subtitle: you already know everything I just said. Stop reading. You don’t need any more inspiration or lofty talk. You don’t need to passively look on the internet for inspiration. So, step four: close your browser or app and get to work.

By UX Launchpad on January 7, 2017.

Exported from Medium on May 19, 2017.

I Prefer Safari to Chrome

But I’m not trying to convince you to like it

Articles like these tend to come off preachy, so we should set some ground rules up front. I don’t care what browser you use. I am not writing this to get you to change your mind. I am just thinking through some of the design decisions that led to Safari being my browser of choice. Especially since nearly everyone I know swears by Chrome.

(And here’s some pertinent background: for a while I was in charge of the design for Internet Explorer on Windows Phone, so I got very, very familiar with the crazy amount of complexity involved in making something like this work.)

# 1: Command-1 Always Does the Same Thing

This is a fundamental UX concept. If I press the spacebar, I know what it will do. If I press the volume up button, I know what it will do. Same with command-Q, command-W, and command-N. And not just per app. Many keyboard shortcuts work similarly across all apps. And Command-1 is no exception.

I have set my first toolbar item to be my personal Gmail account. This means that every time I hit command-1, my Gmail appears. Every time I hit command-2, my work calendar appears. I pop them up like temporary views, then press command-W to make them go away. I never have to dig for them, I never have to think, they’re just there.

This also applies to Mail.app. Command-1 is my Inbox, but Command-2 loads my “Today” smart folder. Command-4 is set to all my sent mail. Because they don’t shift around, I can access different views quickly.

On Chrome, Command-1 takes you to your current first tab, which is always shifting. I know lots of people love that approach, it’s just not for me. I want to intuitively know what it will be, not have to guess or hope.

# 2 Shared History

I use an iPad, a MacBook, and my iPhone, and my browsing history is synced across all of them. This means I can read a story about frogs on my iPhone, then later in the day type “frogs” on my laptop and it’ll load right away. It didn’t take any configuration or effort, it’s just an experience that knows that I’m constantly switching between devices.

# 3 Privacy

Google makes its money though ads. Ads are made better by tracking you online. Apple makes its money on hardware, and feels very strongly about not tracking you online. To each their own, but this is something that is important to me. Apple couldn’t look at my encrypted data even if they wanted to or were served a warrant. Google’s system, by design, means they track as much of my data as they can in order to make as much money for shareholders as possible. Different priorities, different design results, different preferences for my default browser.

# 4 Merge All Tabs + Tab View

Tech workers load a lot of tabs across multiple browser windows. Several times throughout the day, I select Merge All Tabs (a feature I also use frequently in the Mac OS Finder) to cram all the tabs into one giant browser window. Then I select Tab View to see a high level view of everything, ready for me to prune and simplify.

Since I’ve discovered this trick, I’ve felt a lot less stress about having a ton of stories to read. I now consider success having no stories to read, not having 72 amazing articles I’ll get to eventually. This is where Reading List comes in handy.

# 5 Reading List

I’ve used every trick for saving articles across every platform. But the ability to hit shift-command-D and know that a story will be cached offline across all my devices is the best and most seamless solution I’ve found. And I’ve been trying for decades.

It makes it a lot easier to say goodbye to great stories. I either delete the tab completely or send it to Reading List for later. Simple and fast.

# 6 Data Works Across (Almost) Every App

If I log into the New York Times once, that should work everywhere, with no exceptions. So even if I’m in another app like Twitter, a link that takes me to the NYTimes should recognize me rather than ask me to log in. With Safari, this works. And it does it securely because of how they encrypt data. The same is true for credit card information or my personal address. This is a big deal for me.

(Although this example doesn’t quite work because Twitter doesn’t use the Safari View Controller. That’s unfortunate, but it’s Twitter’s bad for operating outside of iOS design recommendations. Most apps don’t.)

# 7 Chrome’s UI Feels Alien

I want to avoid the same old religious war, but I have to mention it. The interface of Chrome stands out against apps that feel more at home on iOS and Mac. I don’t like the black bar across the top. I don’t like the typography. I don’t like the shape of the tabs. I don’t like how it feels like it was designed for Android first and then ported over.

Opinions on this are heated and differing. But that’s mine.

Summary

To re-iterate, I have no interest in convincing you to change browsers or get in a debate about why Chrome is wonderful or can be hacked to work more like Safari. I just wanted to share some thoughts about the design choices I like in Safari. And you don’t have to agree!

After all, it’s just a dang browser. Surely we have more important things to design and debate. Right? Gosh, I hope so.

By UX Launchpad on January 9, 2017.

Exported from Medium on May 19, 2017.

I’d Hire the Settings Designer

You don’t learn as much from a blank canvas

I’ve seen this play out several times on multiple teams. Everyone wants to captain the fancy new features, the sorts of things that might appear on billboards and commercials. But no one asks to work below deck, in the app’s version of the boiler room: the settings screens.

Not only are settings extremely boring, they’re also much harder than almost every other screen in the app. There’s little room for animations, or delight, or even visual design. But it’s where legal requirements collide with business objectives. It’s where what you want the app to do runs headlong into how people are actually using it. It’s where you want to be concise and clear but end up having to fit in a paragraph of explanation text because the feature is just too complicated to explain otherwise.

You spend a lot of time thinking about horrid things like “Don’t show me this again” checkboxes, and after exhausting every other option, you sigh and go with it because there’s just no other way. The life of a designer deep in the bowels of settings is disappointing and ugly. And the older an app, the more settings there are, because toggles and switches never die. They’re the living embodiment of the design debt built up from hundreds of tough design calls punted with a “let’s just allow the user to decide.”

But it’s great training. There’s no better antidote to Diva Sickness. There’s no quicker way to understand that compromise is not a dirty word. It’s a swift kick in the gut for young designers who have been misled about what software design really entails. If I could choose between a person who only worked above deck on user-facing features or below deck on settings, I’d go with the settings designer every time.

By UX Launchpad on January 10, 2017.

Exported from Medium on May 19, 2017.

iPad Pro, One Year Later

I still love using it, but I still don’t use it enough

Last March I wrote I Am Confused By How I Feel About iPad Pro and a lot of things have stayed the same. I still prefer using it, and I still reach for my laptop first. I have a few new thoughts to sift through.

Window Management

I’ve been consciously paying attention to how I manage my windows on a laptop versus my iPad Pro. Last year I talked about how defaulting to full screen and iOS’s method of “peeking” a second app onto the screen helps me focus. Now it’s not just something I notice on iOS, it’s something I’m actively missing when I’m on Mac OS.

Data Portability

I’ve found great software solutions for nearly everything I do, and they store everything online. There’s next to no tax for switching from iPhone to MacBook Pro to iPad Pro. Data portability is a non-issue, even when using more complex apps like Numbers or Keynote.

The App Story

I spend a fair amount of time making graphics in Sketch and animations in After Effects. And sometimes I need to juggle a lot of files when I’m trying to upload them into bug tracking or make a deck. In those advanced authoring cases, I need to use my Mac.

But it’s not hard to envision a Sketch-like app or an After Effects like app coming to iPad. It’s just a matter of time. And I keep seeing examples of the iPad Pro versions of pro software actually being better than their desktop counterparts. Not weaker, not equal, but genuinely better. That’s exciting to see, and it’s been happening to me more.

The Pencil

I still love the pencil and I still use it a lot. It activates a completely different portion of my brain and my creativity than slogging through a desktop OS to try to wrestle creativity from it. And that’s a big shift.

Focus, Creativity, and the Death of Tinkering

One of the strangest things about using my iPad Pro is the sense that it doesn’t want me to tinker mindlessly. It wants me to do something great, then put it away. My laptop seems to say “I have 150 different things to show you, so settle in. You’re going to be here for a while. Longer than you expect. Longer than you planned for. Longer than you want.”

When I write on my iPad Pro, it feels like I should keep writing. When I get distracted and check my email, their peek approach is designed to say “this is in the foreground for now, but that’s temporary. Ready to get back to it?” And when I focus, I produce more and do it faster.

Instead of a series of small sprints, I feel like I do one focused marathon. I get tired faster by the clock, but I’ve often done much more in a shorter amount of time. Afterwards, It makes me want to get up and take a walk because I feel spent but also successful in the tasks I set out to do. I’m not tinkering and fussing, I’m creating. And that’s it.

The Apple Watch works for me in much the same way. I never stare at it. I never tinker with it. I never aimlessly flick the interface around to find something to do. It’s purpose-built to provide quick information and then be forgotten.

You see the same thing in heads-up displays in video games or car consoles. Those interfaces aren’t judged by how much you want to tinker, they’re judged by how quickly they can do their job and get back out of the way.

I’ve used desktop operating systems for decades, and I’ve never felt sated when I use them. I always feel like there’s one more thing I need to do, one more thing to check. I’ve used modern smartphones for ten years, and I can’t stop fidgeting with them. Pop open an app, pull to refresh, see nothing useful, close the app, wait five seconds, pop it open again. It’s mindless but it feels like the OS is encouraging it.

iPad Pro, in contrast, feels like it’s trying to help. It feels like it’s designed to facilitate my creative focus. It wants me to do a good job, then understand when my job is complete. It wants me to have a healthier approach to consuming technology: take only what you need. Then know when to stop.

By UX Launchpad on January 10, 2017.

Exported from Medium on May 19, 2017.

What’s Next for UX Launchpad?

That’s not rhetorical, I’m actually asking

A few months ago, I wrote A Non-Profit State of Mind. In it, I explained how I wanted to move UX Launchpad from a for-profit enterprise to something non-profit. I don’t want to make as much money as possible, I want to aim my help to those who need it most. I still feel that way. I’d like to jot down some thoughts to help organize them and find a way forward.

Re: Writing

I love to write. For years, I’ve been able to write something, realize it’s about design, slap UX Launchpad branding on it, and huzzah! A built-in audience. That’s fun.

But it’s also blending into personal stuff more than I was intending. There’s nothing wrong with a design essay having a more personal bent, of course. But I had intended on UX Launchpad essays being more about universal design insights and less about me.

Re: Teaching

Teaching something to a person still rates as a 10/10 on my happiness scale. Knowing something seems like a waste of time if you can’t find a way to help someone with that information. I will never understand why some people want to lord knowledge over people. It’s like dangling food out of reach of someone who is hungry. Who would do that? Feed them!

Re: Having a job

I love having a job. I highly recommend it. Being asked to do a thing, and then getting paid to do it, is a blessing. If I didn’t have daily tasks to complete, I’m not sure what I’d do with myself. There’s a reason rich celebrities often go nuts.

Re: Design

I love creating things. I love problem solving. I love working with people. All of those things are design and I really enjoy them.

Re: The Design Industry

But I don’t enjoy the “uppercase d design” industry. It feels as over-hyped, money-saturated, and soulless as a casino. It’s full of wonderful people, and the role of design is still important, and I still want to work as a designer, but the industry at large has gotten mighty full of itself. It’s hard not to notice. But I think some are trying hard not to.

“These eunuchs in their Prada and Gucci flavored clothes / Wouldn’t know a fresh perspective if it bit ‘em up on the nose.” — Les Claypool
Re: Mentoring

I don’t love the word “mentoring” but I think it’s an important thing for more experienced designers to do. Over the years I’ve gotten more serious about it, and I get a lot of joy out of knowing I’m helping people who need it. I struggled as a young designer to find anyone to help me out, so I like trying to leave the industry better than I found it.

Re: Profit

I’ve never taken a salary at UX Launchpad. I took all incoming funds and parked them in a bank account, so they’re just sitting there waiting to improve the business. So moving to a non-profit model isn’t going to change much. So I’m still planning on making that move.

Adding it all up

I’m going to keep teaching and trying to help where I can. I’m going to keep writing, although perhaps I’ll aim more personal writing into other areas. I could fill five books with old design essays. Maybe instead of writing another one, I should think harder about if it’s saying anything new before writing another. And maybe I should publish those books.

One thing I’ve learned about being a designer is you shouldn’t worry about the implementation until you’ve had time to really understand the problem you’re trying to solve. Once you understand your insights, a way forward presents itself. I’m halfway between understanding that I want something to change and having a good first draft for what that might look like. And that’s ok, I’m not in much of a rush.

By UX Launchpad on January 16, 2017.

Exported from Medium on May 19, 2017.

What the Orange Badges Taught Me

When I worked at RealNetworks, they were the leader in “streaming video.” This was before fast internet connections, YouTube, or the iPhone, meaning it was really hard to watch video online. But RealNetworks was a pioneer in the space, so a lot of companies partnered with Real to stream their content for the first time using our software.

As a result, I worked with a ton of big media companies early in my career. I helped design the page that CNN used to get people to watch the Iraq War. (You will be unsurprised and/or saddened to hear that the slick marketing term “Shock & Awe” provided a noticeable bump in our signups.) I designed and wrote the code for streaming Big Brother 3. I worked with BBC, NBC, PBS, NPR, ESPN, NFL, NBA, WNBA, Pearl Jam, MLB, NFL, and dozens more. And then one day I was assigned to a new project. Playboy. Meaning porn.

So they gave everyone orange badges to hang on the doorknobs of our offices that said something like “Warning: there may be content unsuitable for children being viewed behind this door. Please be advised.” We hung them up and got to work. I learned a few things.

Insight #1: You can get used to anything

Someone once told me that nudist colonies are no big deal. Apparently you get used to all the nakedness pretty quickly. But I didn’t really understand that until I experienced it firsthand. We got used to working on porn pretty quickly as well. It was only strange if we called attention to it, so we just didn’t call attention to it.

At one point I was chatting with my female co-worker, in her cubicle, while a graphic sex scene looped on her monitor, with audio playing. But we barely noticed because we were in work mode. She was a Java developer, I was her designer, and we just had to reproduce and then fix an audio/visual bug together. We got the bug to happen again. I wrote some stuff on a whiteboard. We went back to our desks. All while the porn “reproduced” in the background. No big deal.

Insight #2: We go with our personal taste more than we think

Our VP was a woman, and most of our engineers were men. At one point we were in a meeting and the female VP asserted what porn genre should take the most space on the top of the page. I disagreed, but didn’t want to say anything. And that was an interesting feeling. And another lesson.

If we’re discussing the design of anything other than porn, it’s my job to have an opinion, express it, listen to others, and then work together to move forward. But when the topic is porn, everything changes. Suddenly I have to decide if I want to tell a co-worker that I, what? Disagree with their taste in porn? That my taste in porn leads me to another conclusion? Yikes. No.

So I disagreed with her. And the snuck glances from engineers told me that the other engineers disagreed with her too. But no one was interested in talking about it. That might be the first time I have ever seen a room full of engineers not question a plan put forth by management!

But it was a great reminder. The things we feel strongest about are often wielded as fact, when in reality they’re opinion.

Insight #3: Power dynamics are everywhere

I think it’s comfortable to believe that everything at work is just a fair exchange of ideas, and the best ideas always win. But the only people who really believe that are the ones in power. (And they tend to have always been in power, but like goldfish in water they just think it’s normal.) It’s like how rich people wonder why other people can’t stop being poor, or thin people don’t relate to diets, or extroverts can’t understand social anxiety, or white people assuming black folks experience society the same way.

I’m confident in meetings. I have opinions about things. I never have to wonder if someone disagreed with me because of bias against my gender, race, or nationality. But once I had to frame my design decisions through the lens of sex, all of the sudden I got more shy. Meaning I wasn’t designing to the best of my ability. There was an invisible power dynamic at play, and I immediately saw the corrosive results in my work. I lost half my impact.

Isn’t that strange? Being asked to design porn experiences at work gave me a deeper appreciation for power dynamics and the fact that the status quo is designed to benefit people like me the most. Whenever I see an under-represented person holding back in a meeting, I don’t think “well, if you don’t speak up, that’s your fault.” I relate a little more than I might have ten years ago. Sometimes the culture isn’t designed for everyone to speak up the same way and the product suffers as a result.

By UX Launchpad on January 21, 2017.

Exported from Medium on May 19, 2017.

“The Best Work of Your Life!”

Well of course they say that.

I remember reading a job description at a highly respected company that used a line like “we want to empower you to do the best work of your life.” Yee-haw! That fired me up. Where do I sign?!

Then I got the job. I loved the job. I loved the people. I learned so much. And I did, in fact, do some of the best work of my life. No question. But I realize now that it was a marketing line. A pretty standard one. One that most companies aren’t really equipped to invest in as much as you’d think.

Every company needs to tell you that you’re changing the world, and doing things that have deep meaning, and are doing the best work of your life. Why? Market forces. Today’s workforce has made it very clear what they want out of a job. A chance for advancement, and a sense of purpose. So guess what the company talks up? Advancement! Purpose! (and coffee.)

It’s not that they’re lying, it’s that they’re just saying what everyone is saying because they have to. No one would work at a company that says “It’s hard to get promoted!” But guess what. It’s hard to get promoted in the real world. Or imagine if they said “We’ll give you a computer, assign you a manager, a team, and a project, and then we’ll see how you do!” That’s not inspiring. But on the other hand, it’s pretty accurate.

And you could go further. “If you have a bad manager, or don’t like your job, or are having a tough time with your role, we’ll help you some. But we’re not really incentivized to, like, change a lot of things just because of one person’s concerns. It’s more likely they’re not a fit and just need to move on.”

Would you work there? I wouldn’t. People don’t want honesty when they’re shopping. They want to be inspired. They want to go yee-haw.

And so yee-haw they get.

By UX Launchpad on January 23, 2017.

Exported from Medium on May 19, 2017.

Medium’s Best Invention

I won’t bury the lede. It’s the recommendations network.

I’ve been publishing on the web since 1994. I’ve kicked off a metric ton of new websites, publications, and assorted knick-knacks, and over the years there’s always the same question at the beginning of the process: what software should I use?

Do I write my own software, do I install something off the shelf, do I use someone else’s server, or do something new altogether? How important is it that I control the whole widget? Do I trust this company is going to stay in business long enough to warrant my support? What’s my ultimate goal, am I trying to communicate or am I trying to learn a new piece of technology? I always think I can do both, but that’s not how it works out.

Over the last several years, Medium keeps leaping to the front of the line for all kinds of new projects. And it’s for the same reason every time: the network. On Medium I can post something, tag it, and if the material is good enough, I know it’s going to drive awesome people towards my stories. I don’t get that hosting my own blog. I don’t get that starting a new Twitter account. I only get it with Medium. So I keep returning to it.

When we look back at this era to study it, no one is going to understand why it took so long to grasp this idea. “Wait, people were publishing online from 1995 to 2012 before they realized that cross-pollinating recommendation traffic across authors and genres benefits readers, authors, and the corporation to such a powerful degree that the resulting network effects represent a massive competitive advantage?” they’ll say.

Well, probably not. They’ll probably just say “People back then were stupid,” and leave it at that. And they’ll be skipping over a lot of nuance that you’d know if you really cared about the history of this stuff, like 90's webrings or trackbacks or the fact that early content companies weren’t paid to think about high quality content. (or even seeing the authors of high quality content as a customer who is worthy of trying to impress and win over!) But the same general point will still be true.

By UX Launchpad on January 23, 2017.

Exported from Medium on May 19, 2017.

Most Writers Get Worse When People Start Paying Attention.

I sure did.


Everyone flirts with the idea of sharing their voice with the world. Whether it’s writing, dancing, singing, or live game streaming, there’s something romantic about expressing yourself creatively. And maybe getting famous!

So it’s easy to get started, and many people do. Then they realize that no one is paying any attention to their first blog post, their first dance recital, their first song, their first Twitch.tv broadcast. And that’s discouraging.

But a small percentage of people shrug off that early disappointment and keep making. They’re doing it for themselves, whether or not anyone pays attention. And that’s where the best content comes from. People who don’t care what you think. People working there have found the sweet spot of creativity and are fun as hell to discover and follow.

The problem is, the best content has a way of getting popular. It’s easy to ignore an audience of one, or one hundred. But try ignoring an audience of ten thousand. Or a million. Or five hundred million. The more followers you get, the more you care. The more you care, the harder it is to be honest. And if you’re spending more time self promoting on social media than creating, your craft is going to suffer. Meaning it’ll be less fun to follow along.

Today I’ll reach 10,000 followers. But here’s a secret. I’ve been writing in other places much, much more. And the content is much, much better. That’s just how it goes. I’ll forever chase the top of that bell curve.

It’s a horrible business model, but I’m ok with that.

By UX Launchpad on January 25, 2017.

Exported from Medium on May 19, 2017.

Some Stories About Designing for Voice

Her, Spike Jonze’s near-future love story, came out in 2013. The plot is simple: a man installs an advanced Siri-like operating system and then falls in love with it. The entire relationship is conducted via speech, through a single in-ear headphone. It’s an interesting premise, and an interesting way to talk to a computer. And it was especially interesting to me, considering my background designing for this stuff.

In 2007, I worked at a voice recognition startup. You’d call a phone number, speak to the service, and we’d send you a transcript via email. Because it was done with. human assistance, you didn’t need to be as careful with your speech, or get frustrated nearly as often. When I was deciding whether or not to take the job, I called the service and said “that German company that starts with a V” and the transcript replaced that with “Volkswagen.” Obviously a human is capable of doing that sort of calculation, but what if robots could one day do it?

A year later, Apple announced an official SDK for writing iPhone apps. I lobbied my CEO and CTO to give me budget for hiring someone who knew Objective-C. I got the budget, barely, then designed and PM’d the release. It went live on day one of the App Store, hit #1 in Productivity, got featured by Apple in print media, and eventually the company was bought out by Nuance. But before all that was the meeting with Steve Job’s friend, a VC guy in Seattle.

We arrived at his office and showed him a beta of our app. He immediately picked up on the blah visual design. “We’re basing it off the standard design patterns in iOS, like Mail,” I said. “That’s a shame,” he said. He said the design was too blah, too flat, not the kind of thing Steve could show on stage. “And trust me, Steve is obsessed with audio input. Always has been. Remember the 1984 Mac?”

(Indeed. You’ve been able to control a Mac with your voice forever. In the 90's I had trained my computer with AppleScript to run a series of actions from a single command. Like “OK Computer, build a new project,” would close all other windows, start playing music, clean up my desktop, go to my Projects folder, make a new folder, and wait for me to type a new project name. Musicians have long been fascinated by the artistic potential, which is why musicians have long used the Mac’s robotic voice in their songs, and why Radiohead’s album is called OK Computer, the default command that didn’t require holding the Escape key before speaking.)

I don’t recall the exact phrases he used, but this man impressed on us very clearly that Steve believed the future of computing, or at least a big part of it, would be voice controlled. You want to be able to tell an assistant “Set up the house for the party” and just know that everything is going to happen correctly. But imagine the number of clicks and keyboard shortcuts you’d need to go through in order to do the same things via WIMP (windows, icons, menu, pointer) computer interface. How many actions would it take? How much time? It doesn’t scale.

“And then we’ve got this iPhone, a big sheet of glass, and you can fit, what, five navigational items across the bottom? And a few actions on the top?” This guy knew, back in 2008, that we’d soon ask our phones to do nearly everything we ask of desktop operating systems, with far fewer pixels to work with. Our young company was headed in the right direction with voice input, because it was the only thing that could scale to the level of complexity without requiring additional UI. But, he pressed further, we were requiring humans to do the transcription. Which adds a delay. Which would be a problem for us until we could automate it entirely.

Then he asked about integration with other apps. Our company had tons of them, probably over a hundred, and dozen or two really popular ones. But somewhere along the way, there was an elephant in the room. To really do voice right, it would need to be done at a system level. No third party app could address key scenarios, like asking your phone in the morning “is it going to rain today?” Or even something like “Find Sarah’s phone number and text it to Betsy.”

Back in the office, the CEO and CTO quizzed me about Apple’s philosophy. I’m an amateur Apple historian and they were the kind of Microsoft veterans that say “JScript” instead of JavaScript or “the fruit company” rather than saying Apple. So I shared my point of view about the meeting. I agreed wholeheartedly that Steve Jobs has historically been obsessed with audio input and output. I agreed wholeheartedly that Apple would add something like this at the OS level. And I agreed wholeheartedly that the dictation scenario was a sliver of the overall audio strategy for Apple.

“There’s got to be some hook into the OS we can get via the SDK,” they said. “There’s got to be some workaround for being the app that Steve is looking for across the entire phone.”

“Maybe,” I said. I hated breaking the bad news to them, but I went for it. “But Apple believes in owning the whole widget. If they think it’s a big deal, they’re going to build it themselves. At the OS level.”

Within a year, the startup had been bought by a leading voice recognition company called Nuance. Two years after that, Siri launched as an OS-level voice assistant powered by Nuance. It was exactly what his VC friend had told us, and exactly what Steve had been aiming Apple towards since 1984. His company had finally kicked off his long-dreamed about voice assistant. Steve died the day after Siri was born.

Six months prior to Siri’s beta release, I was a lead designer on Windows Phone’s Apps team. We built Mail, Calendar, SMS, Internet Explorer, and so forth. If it was bundled on the device, our team was tasked with designing it. We had been working on an idea called “Capture” which sprung from the insight that jotting down quick notes via voice, scribble, or text is really important. I was fond of saying “we’re losing to the sticky note” to explain why phones were still not the preferred way to take down information on the fly. Software is too slow, requiring too many steps, with too much customization to reasonably compete against the speed of a sticky note.

Capture got killed off multiple times to make room for other features. At one point, while trying to argue my case, I remembered what Steve’s VC friend had told us years earlier about voice input. And I remembered about a vague rumor floating around amongst the Apple faithful, that Apple was working on an “assistant” for iOS. iPhone and Windows Phone already had a rudimentary voice-to-text feature, but “assistant” sounded like a bigger bet. It sounded like it might be Steve’s voice dream coming to pass. So I put together a PowerPoint presentation explaining that I thought the future of “Capture” was not just a Microsoft Design idea. I thought it was what Microsoft calls a “table stakes” feature. Something expected of every product because it’s so obvious.

I’m not claiming that Cortana was kicked off because of my PowerPoint, because it wasn’t. And I’m not claiming that when Siri was launched Microsoft experienced the familiar feeling of a feature going from “we’re not doing it” to “now we have to do it” because who knows. But that’s how it looked from the inside. We build a voice team and they got to work. Many of the designers were my friends, and they sat right next to me, so I got to listen in and share thinking through the early stages.

I remember one of the key things I was concerned about was the idea of Cortana being too much of a “black box.” What if you don’t care for Taylor Swift but somehow the computer has decided you love her? What if your entire search and entertainment experience gets corrupted by this bad assumption? And worse, what if there’s no way to see the bad assumption? And worse still, what if threre’s no way to correct it? I was very pleased to see that the concept of Cortana Notebook survived in the final version. I think it’s a key component. When things are stored in black and white, you can correct mistakes and even input important insights to allow Cortana to help in far more intelligent ways than a black box assistant might.

One final thought, this time about AirPods. The reaction from most people was pretty straightforward. “I don’t need wireless headphones. Besides, they’re expensive. And it looks like I’d lose them.”

My take is a bit different. Running (or walking around normally) with traditional iPhone earbuds often causes me trouble. I accidentally yank them out of my ear, or I get tangled on them and it makes my iPhone slip out of my hands. I don’t consider myself particularly clumsy, but headphone wires are always getting in my way.

I think back to something my friend told me in 2014. I asked him what future trends he saw over the horizon, and he told me that he thought headphone wearables were going to explode in popularity. He explained how while traveling he didn’t want to have his nose buried in his phone while wayfinding. He wanted a gentle voice in his ear explaining when to turn. Sort of like that movie Her.

So he had purchased some headphones that look a bit like a necklace. Most of the the time they just hung around his neck, but when he wanted to ask a question, he liked the idea that he could just say “directions to the Louvre” and have Siri get him there subtly.

He described his needs through the lens of a traveler, but his story resonated with me. I’ve been using my Apple Watch in a similar way. I’ve found that I don’t need to carry my phone through my office or my home, because I know I’ll be able to use my Apple Watch for light computing scenarios, and it will use WiFi to talk to my phone.

And rather than feeling hamstrung by the tiny screen, I find it fits a good triage role. I get less distracted. I go down fewer rabbit holes because there aren’t as many. For example, I can ask “Check for new email” while walking down the street and I’m told I have nothing new. Without being distracted by anything else, which is itself a feature. Or if I do have new email, I can ask to have it read to me. Or I can say “Read me the email from Mike today” to perform a search.

When I’m at the park playing with my kids, my wrist buzzes. I see it’s from my wife. With two taps I can send back a quick 👍 or 😂 or whaetver is needed. Or I can transcribe a quick response like “Home in 10 minutes.” And then I’m back to playing with my kids. We’re used to describing that as a stripped down feature set, but the ability to only see the thing I want to see and ignore everything else might be my favorite feature of something like Apple Watch. And soon, of AirPods too.

By UX Launchpad on February 9, 2017.

Exported from Medium on May 19, 2017.

Wanted: Design Historians

Design rationale documents beat tribal knowledge every time

Many years ago I worked on design team working with a major US phone carrier on their new SMS app. This meant a lot of our time was spent in traditional product specs. Here’s a old screenshot from the work we did, which might look familiar to anyone that spends a lot of time in specs.


If you’ve ever used SMS on an old AT&T flip phone, you were experiencing this app
All of this documentation described what the design called for, yet nearly none of it explained why. For example, this flow diagram is saying “When you launch the app, check if it has been launched before. If it has, do x. If it hasn’t, do y.” This is all very important information, of course. Without it, no product can be built and the engineers might as well go home for the day.

But have you noticed how very few companies put effort into explaining why decisions were made? Companies like to explain the need for the feature at a high level, but skip all the little details and disagreements and tradeoffs that led to a final design. Which makes later iteration difficult, and doubly so when people quit and take invaluable tribal knowledge with them.

In my experience, all the most important, deepest, most tricky tradeoff discussions happen at the whiteboard, are rehashed and revised verbally amongst the team over the course of the project, then are sometimes referred to in passing while pitching to the leadership team, but after that they just sort of … disappear.

That’s not good. And it’s a problem everywhere I go, on every team, on every project. How might we do better? How might we pair our what with our why in our formal documentation? After years of writing what I call “design rationale documents” and writing several lengthy Design Explosion essays, I have some ideas for formalizing this process further at work.

Here’s the scenario

When you come to work at a big and mature tech company like Google, Facebook, Apple, or Microsoft, you’re often assigned onto a feature, which is assigned to a team, which is sometimes part of a greater product, which is part of a greater organization, which is one of several in the corporation. Not only is the information you need spread across multiple other teams, it’s all locked up in people’s heads. There’s no way to quickly get access to it.

Here’s the idea

What if your team had an actual history book explaining the tradeoffs that led to the feature or product you’re being asked to work on? Imagine if you joined the team working on Powerpoint for Android at Microsoft, and on your first day you were handed a book titled Powerpoint: A Design History.

Typically a book like this is filled with glossy marketing photos that focus on what shipped, which is of limited value. What if this book described, in painstaking detail, every notable change to the product’s UX over its whole life, with detailed explanations for why each product decision was made? Helpful, right?

Well, I’ve been doing this at my current job. Here’s what I’ve learned.

1. Speak in layers

Decisions are multi-layered. It reminds me of human anatomy charts, where one chapter might talk about skin, but another talks about the circulatory system, then another talks about digestion, and another talks about muscles, and so forth. There’s no one diagram or chapter that can explain everything. You have to work in layers to isolate each topic from others.

For example, if I was writing the design history of Powerpoint, I’d spend time explaining the evolution of the navigational structure from menus to the ribbon to the simplified ribbon that came when Surface got popular. But the key would be going beyond screenshots with unhelpful captions, (“And then we shipped the ribbon”) embracing instead the meaty discussion about the pros and the cons of everything that led to it.

With that overview established, you could then turn to more detailed topics. A section on design decisions around animation. Another for presenter mode. Another for composition and editing of content. Eventually you’d have to explain differences with the Mac and how the design direction was reconciled with the Windows or web versions. And that’s before you even get to the Android-specific stuff, which would require a lot of documentation itself!

2. What happened vs what’s happening vs what’s next

Historical design documentation shouldn’t have to change much. If I explain why the 1984 Mac shipped with MacPaint, that’s a story that you only have to tell once. It happened, it’s done, and nothing that we can say in contemporary times can change that.

Things that shipped recently are harder because you don’t have enough distance from the feature yet. You can say “and such-and-such just shipped” but it’s harder to explain the feature’s role in the arc of history. So that’s where the design history should hand off to an in-progress design spec, perhaps as a Google Doc. That’s what I’ve been doing.

Process around roadmaps are often handled in a cavalier, shoot-from-the-hip manner that crops up when leadership asks what the next year will look like. But having a design history book to refer to would help move forward with more confidence and less frothy. If you know where you’ve come from, and what tradeoffs got you here, it’s easier to know where you want to go next, and how to best proceed.

3. Figuring out the right writing voice

This is a tough one. A lot of people fall into the trap of harshly criticizing every feature they weren’t a part of. But there’s no need for that when writing design history. It’s important to explain each decision in good faith while explaining why changes were made.

So less “We did this dumb thing because the lead was a jerk, but then he quit and that’s why the product got better” and more “We believed we could get more x if we sacrificed some y. After launch, we decided we had lost too much y, so our follow-up version aimed to address that.” More neutral and informative, less editorialized and opinionated.

So that’s what I’ve been working on. It’s a lot of work. But I think it’ll help a lot. And one day I’d like to see this kind of documentation everywhere. Do you do anything like this? Would you like to start? Let me know!

By UX Launchpad on February 24, 2017.

Exported from Medium on May 19, 2017.

Idea Shopping

“Where do you get your ideas?” some innocent soul asked the author; it was someone unseen, a strangely sexless voice in the vast hall. Allan rolled his eyes. It was what he called “the shopping question”: the homey speculation that one shopped for the ingredients in a novel.
“My novels aren’t ideas — I don’t have any ideas,” Ruth replied. “I begin with the characters, which leads me to the problems that the characters are prone to have, which yields a story — every time.”
— A Widow For One Year, John Irving
I write a lot of stories. I do a lot of design. And for me, it’s really all the same creative process, which people misunderstand whether you’re talking about pixels or novels. There’s a belief that stories and designs are like tangible objects on the shelf of an idea store. All you have to do is go shopping, pick the right one, and you’re done. If only.

When I’m designing something, I start with anything at all. A comic where I draw a character explaining the feature to the reader. A long essay where I write in a stream of consciousness. A walk with a friend where I try to explain the problem out loud. A trip to the park so I can doodle some boxes and arrows. You build something from nothing. Then you whittle it down to only what you need. Then you’re done.

When I’m writing, I open a scene and introduce a character to myself. That character will soon do something. Then another thing. And those actions will inform the character’s description. That’s how you get started. And it may sound strange if you haven’t experienced it yourself, but the character you’re writing will tell you what it wants to do next. Really! You just have to get started and then listen. Then you build up a body of work. Then you whittle down to only what you need. Then you’re done.

By UX Launchpad on February 28, 2017.

Exported from Medium on May 19, 2017.

4 Million Shades of Blue

Sorry, Doug Bowman

Art is subjective. Design is objective. You can personally prefer a certain typeface, color, layout, design pattern, composition, set of icons, or visual trend. But if it makes the company less money, and results in less engagement, and causes confusion amongst more people, it’s a worse design. Objectively speaking.

I learned this the hard way in 2005. I was asked to rank 5 banner ads in order from my favorite to my least favorite. Then we ran the objective results against my subjective art school training, and I was 100% wrong. The ads I liked the most did the worst. The ads I thought were the worst, the grossest, the ugliest, the ones lacking class … they performed the best. By far.

I was looking at the ads through the lens of art school, a common mistake for young designers. Following my training, I was looking for beauty, for tight Swiss grids, for pleasing color choices. But that’s not how people choose to click on ads. Sometimes they click on the garish, blinking, awful ads the most. That’s why they’re used. They work. The biggest myth in art school is that fine taste somehow overrides the objective facts of what non-artists want. They don’t. Increasingly, the data is making art school sensibility look out of touch and quaint. Maybe even — gasp — unnecessary.

Did you know there are huge bot ad networks that collage together thousands of variations of advertising in real time? They’re amazing. They can see which ads are getting the most attention, and algorithmically make new versions on the fly. It’s hyper-Darwinism applied to marketing, it’s been happening for years, and I don’t care how intelligent you think your design is: Darwin ads will always win. They’re faster, get better results, and don’t require a gym membership or coffee. The robots are already putting visual designers out of work, and fancy art school degrees won’t stop it. If anything, they might accelerate the trend.

As designers, we can sit on the sidelines and grumble about it. Or we can figure out how to harness the power of these robots and put them in our creative toolkit and processes. And the first step to doing that is embracing the reality of objective results. (Put more bluntly: you can’t hate and dismiss data if you’re going to be a product designer of the future)

It’s pretty simple: if the numbers go up 10%, that means the design succeeds. If the numbers go down 10%, that means the design didn’t succeed as well. Of course you can (and should!) argue to use the right numbers, because numbers can lie and mislead. But you can’t pretend the numbers don’t exist. They do, and they’re your scorecord. More than Dribble, more than how your portfolio looks to your designer friends, more than your personal level of investment on a project.

Designers are problem solvers being judged against clear metrics. It’s our job to make the numbers go up. The higher they go, the better the design and the better the designer. Objectively speaking.

By UX Launchpad on March 8, 2017.

Exported from Medium on May 19, 2017.

The Un-invite

And figuring out what you stand for

Late in 1999, I saw that a company named RealNetworks had done something I disagreed with. I wrote a blog post about it. That week, they invited me in for an interview.

Weeks earlier, I had applied to work at several companies in the Seattle area, including RealNetworks. They had seen my resume, but they hadn’t yet seen my critical blog post. I wondered if I should delete it. I was afraid that if I kept it up, they’d discover it and un-invite me from the interview.

In the end I decided to leave it, and I’m glad I did. I went in for an interview and did well enough for them to consider offering me a job. But first they went to my blog. And they saw the post. And despite my criticism of their company, they hired me. A good sign! I worked there for five years and learned a lot.

My approach could have backfired, of course. They could have decided my writing was out of line, which would have meant no job, which would have made a very big difference in my career. But standing by my words paid off, which was a good lesson to learn. But there was a deeper lesson lurking that it took me much longer to understand.

I was recently invited to submit a workshop proposal for a conference in New York City in just a few weeks. They accepted it, so I quickly began designing the workshop, then scheduled a second talk in another state to turn it into an east coast mini-tour. I also started making plans to take my son with me and maybe meet my mom as well.

I also wrote the organizers and told them I’d be bringing some of my best material, but due to the sensitive nature of my work, it would be best to keep my name anonymous and the workshop description vague. Excerpt:

Again, I know it sounds crazy, but people in this line of work often face retaliation, so I’ve learned to be tight lipped about it in promotional materials. The workshop itself will be very open with lots of very real answers, but in return I’ll ask that detailed information or my name not be publicly shared.
They said no and rescinded my invitation. My first thought was to ask for some sort of middle ground. Maybe there’s a way to still help them reach their promotional goals while protecting my privacy? But reading their response again, they weren’t leaving the door open for discussion. There is no middle ground. The response wasn’t “let’s talk,” but “thank you, no.”

It’s disappointing, of course. Now I have to cancel a second talk and break the news to my mom and son. Plus the workshop was going to be pretty great, with a lot of important material, and I wish I could still share it.

But none of that matters if I’m willing to compromise and the organizer isn’t. In life, as in tech/design, as in the academic community, as in social advocacy, that’s a red flag. I see it all the time.

1999 follow-up: at RealNetworks, the VP of my organization did see my critical blog post before they offered me a job. He had an odd response. First, he laughed so loudly for so long that everyone in the department heard him. Then he ran to the the hiring manager’s office and said “We’ve gotta hire this guy.” When I heard that story, I realized the key isn’t just standing by your words or fair negotiation. It was also about compatibility.

Not everyone is naturally compatible, and not everyone is willing to compromise to make something work. But some people are. Focus on them!

By UX Launchpad on March 12, 2017.

Exported from Medium on May 19, 2017.

Don’t Try Flag

My biggest mistake at UX Launchpad

Several years ago, I saw a Kickstarter for this cool company named Flag. Their idea is sending people high quality photographs for free. How could they make it free? By putting an ad on the back. Awesome.

The project got a lot of press, and a lot of Kickstarter backers. I loved the idea as a consumer, but I also thought it’d be cool to dive in as one of their first advertisers. I sent them $500 and waited. That was three years ago.

In the meantime, I’ve corresponded with them around 25 times on email, plus a few DM conversations on Twitter. The pattern has gone like this:

Time goes by.
I write asking for a refund or a status update.
No response.
I write again.
A few times the CEO Sam Agboola has written back personally apologizing for the delay, then asks for more patience.
I send a follow-up that gets no response.
I just noticed the Flag Twitter account has been taken offline, as has Sam’s personal Twitter. Searching for it shows a long list of customers who feel let down by their lack of communication:


At this point it’s clear that the company wasn’t able to make their great idea work. And it’s been clear for years that I’m not getting back my $500. Sometimes my mom calls lessons like this “tuition.” I didn’t go to school to learn how to run a business, so I can consider my rash decision to buy ads on a Kickstarter product just part of the tuition I never had to pay.

Onward!

By UX Launchpad on March 15, 2017.

Exported from Medium on May 19, 2017.

Pick a Lane

Please tell me the drawbacks of your great idea

When people tell me they love taking the time to get it right, I mentally calculate in my head “high quality work, but has trouble hitting deadlines.” If they say the reverse, something about how “done is the engine of more,” I think “fond of MVP releases, might not be as good at polish, user delight, and long term brand strategy.” Both are perfectly valid approaches. The problem is when you refuse to believe there’s a tradeoff to make at all.

Here’s an example I see often: when design teams say that reusing components in a design language is important for UX consistency, I agree. Strongly. That’s my default design philosophy and it’d take a unique client, product, or scenario for me to not default to it. But componentized work, for all the benefit, has downsides as well. Some feature ideas just aren’t possible with standard components. Meaning new work often takes much longer, with lower morale, because teams have to wait for the design system team to bless a new interaction model, or for the platform to provide it.

But there’s no magical way to pursue an approach with no drawbacks. It’s helpful if teams don’t just use the happy words (consistent, user-focused, delightful, simple, clean, fast, awesome) but factored in the scary words too. For example, instead of “Quality first,” it should be “We’re comfortable shipping \<x\> months late in order to make sure the feature is as good as possible.” Don’t say “MVP” if you’re not willing to say “We’re ok with a product that could negatively affect our brand because we think the lessons we learn will be worth it.”

Pick a lane. Don’t tell me how great it’s going to be, tell me what you’re willing to make less great in order to get there. That’s where the real conversations happen. It’s good to have them often.

By UX Launchpad on March 20, 2017.

Exported from Medium on May 19, 2017.

The “Frozen Trucker” Case in Sharpie














































Here’s an article at Salon that explains why Gorsuch’s dissent was seen as absurd at best, callous at worst. The text of the dissent can be found here.

Q: What does this have to do with product design?
A: Everything. People will have different opinions, and that’s healthy. People will emphasize different aspects of a case to press their point, and that’s their job. But watch how people negotiate with others. When they leave room for nuance and discussion, they’ll make better policy, products, and teams. When they don’t, their stubbornness can poison the whole team.

Q: He’s replacing Scalia, and I heard he’s pretty similar.
A: Sort of. A key difference is that Scalia strongly believed in Chevron Deference. He often argued that judges should not pretend to understand the nuances of policy to the same degree of agencies and experts. His opinions were often suspicious of judicial overreach and was deeply aware of the limits of the court’s collective intellect. Where Scalia was humble, Gorsuch’s opinions have been strident and arrogant. I’ve been on enough product teams to know how that’s going to pan out for him and the country at large.

By UX Launchpad on March 22, 2017.

Exported from Medium on May 19, 2017.

The Ideal Speech For a New Lead

A design exercise

I had a chat with a co-worker about the best possible way to introduce yourself to a new team when you’re going to be their new lead. Especially a big team. For example, let’s say you got hired to spearhead a cool new product at Google. Imagine that the team had been operating for about a year before you showed up, but the previous head stepped down for some reason, and now it’s your first day. What would you say? Here were some early notes I came up with.

1. Make it about them, not you

Maybe I’m alone here, but I don’t really care much where the person came from and what they did. Either they’re so impressive that I know them already, or we’re going to waste a few minutes letting the person brag about themselves while we check our email and refresh our feeds.

When I’m the audience, I want to know how things will change. Is a re-org coming? Is my project going to be canceled? Am I going to be able to work with you? Trust you? Do you listen well?

2. Skip the boilerplate high level stuff

Blah blah blah excellence. Whatever. Explain how we’re going to get there.

3. Don’t imply that you’re saving them

I see this mistake all the time. People start a pitch by saying “We all know that feature X really sucks, so my proposal is going to finally make it great.” Or “It’s been a tough road until now, but if we work together we can really do something great.” I don’t care how true you think it is, it’s rude.

People are ready to accept new ideas, but not if you frame everything in a “it’s a good thing I’m here” kind of way. So maybe don’t do that.

So here’s the talk I’d lead with:

Hi everyone! Thanks for that nice introduction. I’m really looking forward to jumping in and learning more, and that’s exactly what I want to talk about today. I’m going to talk for about two quick minutes, and then we’re going to open up for discussion, and that’s it. Cool? Here goes.

Any designer knows that you need to understand the problem you’re trying to solve. Which means humility. Listening. Not falling in love with your first idea. Understanding nuance, being open to revision, challenging early assumptions, etc. So, to that end, it’s my job in these first few weeks to listen. The better I listen, the better I’ll be able to lend a hand. So that’s what I’ll be doing, before anything else.

I’ve sat through this kind of meeting many times. Some person shows up, implies that it’s a good thing they took the job because they’re perfect, shoots from the hip, talks about excellence, probably does a re-org, messes up a bunch of stuff, then after not too long, they leave. Right? Too harsh? Maybe. But I see it a lot.

Here’s the thing. I have ideas, obviously. But they’re nothing without knowing how things are really going. I don’t want to shoot from the hip or drastically change direction, I want to help accelerate whatever’s already working. Which means less talking from me and more feedback from you.

So! That’s my piece. Now I’d like to open it up for Q&A, or even better, just straight feedback. If you want to tell me about a thing you think is going well, or not going well, or anything else, please do! Because the more honest we can be right now, while I’m still learning, the better it is for all of us. So, does anyone want to be the brave person to go first? Gold star to the first one who says something. Which is pretty hypocrticial of me to say because I’d rather die than ask the first question.

Ah ha! First question! Very brave. Please take my mic.

By UX Launchpad on March 22, 2017.

Exported from Medium on May 19, 2017.

The Evolution of Designer Ratings

Another lesson in sharpie

My judgement of designer skills has gone through a lot of changes over time. Here’s how I experienced it, though of course your results will vary.


In the 90’s, there was this magical program called Photoshop. It, along with “non-linear editing” tools for video and audio, were getting better, faster, and cheaper. It was the multimedia revolution, coming on the heels of the desktop publishing revolution, and it was in full swing when the internet started going mainstream. At that point, the rating for designers was simple. Do you know Photoshop (or SoundEdit 16, or Quark, or whatever)?


It used to be this simple.
And the follow-up question, the one that determined how much money you could get for that skill, was, of course, how well do you know this software? The bigger the black rectangle, the more valuable you were. Easy.


Some experience. Not an expert. Good enough. Welcome aboard!
But when design made the leap to the internet, it wasn’t good enough to know traditional Photoshop techniques if you couldn’t figure out how to get it up on a website. This was a tough time for a lot of designers, but there was a huge opening for people like me. Suddenly this was the rating system: creativity is a good start, but can you make a webpage?


The birth of the unicorn job description.
I lucked out because I had been building arty things online since the dang 80’s, right around the time the Dirty Dancing soundtrack was enjoyed by millions, non-ironically, as contemporary music. Can you imagine? So in the mid-90’s you could either hire someone with this skill profile…


Pretty good at Photoshop, but no coding ability.
Or this one:


Not as great at Photoshop, but can help with your website.
To me, this was the beginning of what we came to call the Maker movement. I know a ton of people just like me that made the jump from printing art zines to printing art zines on the internet. I also know a ton of people with fine art degrees who would rather die than learn code. So, career-wise, that’s sort of what happened. Knowing just a little HTML soon became a huge differentiator compared to artists/designers who didn’t bother.

But then it became clear that HTML wasn’t enough, because you can’t write interactive software with simple tags. If you’re going to make the product do something instead of just sitting there as a series of static pages, you need to understand technical constraints overall. Not just learn HTML.

The other issue is that traditional art school training (visual communication) is actually a completely different skill from designing for software (interaction design). Which meant we we went from 1 dimension, to 2 dimensions, to — surprise! 3 dimensions.


This is a popular way to think about it.
I’ve come across a lot of different profiles of designer over the years. For example, take a look at a skillset like this:


Prototypers often fit this profile.
This is a person that can write real code that does real things, but also understands design concepts. A lot of early web designers landed here because it took a crazy amount of patience and technical skill to figuere out how to hammer early websites into submission. When you factor in browser incompatibilities, slow download speeds, no custom fonts, and sluggish computers, there wasn’t a lot of room for beautiful artistic expression.

Which isn’t to say some people didn’t try. They did. Their skill profile looked something like this:


This is what leads to websites with one hundred 5 meg images in a giant list.
These were the art school students who didn’t know much code or interaction design but had great taste. Their websites were slow to load, hard to use, and gorgous. When they learned they could only use Arial, Verdana, or Times New Roman, they instead avoided tech reality entirely and exported everything as giant five meg images.

There’s nothing wrong with being stronger as a visual artist than a software designer, it just means you’re not going to make very good software. You wouldn’t ask an engineer with no art skills to design a concert poster, and people soon discovered you can’t ask an artist with no software design training to build your website. They’re different skills.

I’ve known some people like this, though they’re not very common:


Engineer-quality code, art director level visuals, but … bad result.
I’ve exaggerated a little for effect, but you get the idea. There are some people who are true artists, and also can write really high quality code. And there’s nothing stopping them from also being good at UX, of course. But the types I’ve met like this are so strong on the visual side that the usability suffers. Why is that? Can’t they just be great everywhere? Well…

There’s a natural tension between visual and UX. For example, are you going to use the standard \<FORM\> elements in your webpage, or build completely new ones that look a lot cooler? If you go the cool route, it’s easy to do it in a way that breaks screen-readers used by the blind. Or disrupts how the tab key is supposed to work in the browser. Or maybe you’ve put the text into a graphic, meaning it can’t be translated into any other language and Google can’t index it. And you’ve definitely bloated the size of the page, meaning it will load slower, which puts it out of reach of a lot of people. And so forth. [ed. I could have made that paragraph shorter by just saying “Flash. End of story.”](#)

A while later I worked at frog design, who taught me the concept of “openers” versus “closers” when it comes to rating designers. That’s a fascinating way to think about it, because it combines a lot of this stuff into a simple spectrum again:


Most designers and artistic people love “opening” a new project, thinking blue sky, purposely ignoring technical realities to get to novel new insights, and dreaming big. But when it comes time to cut features because of the timeline, or technical realities, or engineering disagreements, (aka “closing”) they have a tougher time. So most designer profiles look like this:


Designers love kicking off new things with big imagination!
And sure, designers can close. It’s just not necessarily something they love to do, because they’re almost always disappointed in the final result. (We love phrases like “lipstick on a pig” or “death by a thousand cuts” in these situations, amirite?) Engineers, on the other hand, often cluster on the other end of this spectrum:


Engineers love imagination and design too, as long as it actually ships.
Engineers do a ton of design work too, and are just as creative as any designer I’ve ever met. But an engineer is trained to be pragmatic where a designer is trained to dream big. If using a custom font is going to add 3 months to a 6 month schedule, a lot of designers will think “By golly, then we’re shipping 3 months late.” But alot of engineers will ask “Is a custom font really worth adding 50% to our schedule?” Both are reasonable points.

But “open” and “close” is probably obscuring a lot more detail than we want. When I went to work at Microsoft, they talked a lot about “BXT” or “Business, Experience, and Technology.” In other words “Make money, have good design, and make sure it’s technically feasible.” All three are important.

A lot of young product designers have a profile that looks like this:


Designers bring the X factor at Microsoft. But this isn’t enough to ship good software.
I wish this weren’t true. I wish I could say young designers usually have a deep respect for technical realities and enough business training to know that a beautiful product with no customers cannot be considered a success. But it’s not how a lot of designers are trained. They’re trained to fight for the user, at all costs, and sometimes that can obscure other important details.

A more experienced software designer starts to expand their understanding and respect of business and technology, so their profile might look like this:


This is a healthier profile. You don’t need to write great code, but at least respect it.
As long as I’ve been in tech, there’s been a debate about designers learning code. I feel like a new article is written about it every five minutes. I have nothing to add to the discussion, other than “you don’t have to possess a skill to respect its importance.” Your engineer doesn’t need you writing their code for them. But they do need to feel like you care when they have questions about your design and how slight changes dramatically affect the quality or schedule of the work you’re asking them to do. And if you can communicate in prototypes, that can only help with the rapport you are going to have with that engineer.

Let’s talk about “unicorns” for a moment. I’ve just shown you two profiles that have defecits in tech and business. What about someone like this who can do it all?


The infamous unicorn.
There are people on this planet who are the best at multiple fields, like being a famous ballet dancer and also being an astronaut. They absolutely do exist. But just because a handful can be found doesn’t make it a reasonable thing to assume you’ll run into one. The same thing happens with BXT skills.

Yes, there are people who can be a successful CEO and a successful CTO and simultanously make amazing designs. But a counter-intuitive thing happens when they’re highly skilled in three areas: it waters down each one! See, if you live for amazing visual design, you’re going to make moves that harm usability, like the \<FORM\> example above. If you live for business, you’re going to make user-hostile decisions to help your business plan. If you live for tech, you’re going to reduce tech debt, which means more common controls, which means a less exiting and eye-catching visual design.

“Can’t you do all three?” people always ask. Yes. But you can’t make all three the top priority, and that’s the key. It’s why a person that’s good at all three isn’t quite the victory it sounds like. Sometimes you want three people, each focusing big on their area, but with enough respect for each other to compromise. It comes down to rapport. Which brings us to our next profile:


Rapport/communication, complexity, and UX
This is how I’ve been thinking about product design skills for the last several years. Most of my design writing has leaned in this direction. My favorite talks have all stressed this point. The fact is, having traditional product design skills (UX) doesn’t get you very far if you’re hard to work with. Or if you can’t do anything more complex than a simple feature on a simple app.

But “UX” is probably too broad a term. As much as we talk about “end-to-end designers,” there is definitely a difference between people who are strong at interaction design and those who aren’t. Visual design affects interaction design, of course. But they are different skills. So this is a better model, and our final one for the day:


Rapport/communication, visual, complexity, interaction design
The great thing about rapport/communication is that it can encompass prototyping skills, pitching skills, how kind you are, whether or not your team enjoys coming to work every day, and so forth. It’s not just about HTML, or “code,” or “tech,” or any one piece of software. It’s simply a question of how well you integrate with others.

I’ve met difficult designers like this many times, and it’s not fun:


The Young Rockstar
This is a young person who makes cool looking things, and maybe got to run the show at a small start-up, but now they’re on a new team and they’re hard to work with. They don’t prioritize teamwork, they haven’t dealt with complex software before, and yet they walk around like they own the place.

Here’s another profile that’s easier to get along with, and the big difference is how kind they are to others. Maybe it’s a young HCI intern, eager to learn:


The excited HCI student
Maybe this person isn’t as amazing on the visual design side, but they deeply understand usability and how to make an easy-to-use feature. And maybe they haven’t gotten a lot of experience yet, but they’re teachable.

I love that word.

Whenever I hear a manager call a designer “teachable” I know that designer is going to have a lot of success. If a person’s humility isn’t there on day one, it’s probably never going to show up. Which is not a good sign for how they’ll do on the team. But if they are humble, and eager, and teachable, great things will happen. I’ve seen it many times. I love those people.

What about this person? Looks pretty good, but pretty bad. Right?


Nice, experienced, and good. But not a graphic designer. Thumbs up or thumbs down?
There is a lot of debate around this. As software design is increasingly done by robots and algorithms, where will product designers make their mark? And even before robots, the simple reality of responsive design, accessibility, and componentized design systems means that building everything by scratch is becoming less and less tenable. No matter how cool your idea is.

Some of the best hires my teams have ever made have had a similar profile to this. A nice person, who is able to deal with a lot of system complexity with minimal drama, with a solid interaction design background … but maybe isn’t the most stellar visual designer on the team. (don’t get me wrong, being great at visual design isn’t disqualifying — but not being great at visual design isn’t either.)

That idea flies in the face of a lot of conventional wisdom in the design community. We want people who are great at visual, and interaction, and ideally they can write code too. But I see it more broadly, and I think personality matters more than we give credit to. Be nice. Be teachable. Be a part of a team. Everything else will work out just fine.

By UX Launchpad on March 23, 2017.

Exported from Medium on May 19, 2017.

Design Challenge: Trump’s Next Speech

Here’s the state of play. Obamacare started out unpopular, but got steadily more popular over the last seven years. Now it’s a permanent fixture in American society, because you can’t get elected by telling people you’re going to repeal their health care.

The House has 435 seats. That means you need 218 votes to pass anything in that chamber. From there, you have to get at least 51 out of the 100 votes in the Senate. So that’s who you need. 218 House members and 51 Senators.

There’s a group of House Republicans that call themselves The Freedom Caucus, oftentimes referred to as “Tea Party” members. They don’t have a published roster, but the conventional wisdom is they have up to 40 members. The Freedom Caucus represents the far, far, far right of American politics. They’re the people that don’t want to replace Obamacare, they just want to remove it entirely. They believe that government’s involvement in almost anything, but especially health care, goes against the constitution. It’s these fiscal die-hards that shut down the government in 2013, harming their own party without winning any sizable concessions to their cause. They’re reckless extremists and they don’t like to deal. Other Republicans think they’re a barrel of laughs. Or not.

When you break down the numbers in the House, it looks like this:

193 Democrats (44% of total House seats)

203 Standard Republicans (46% of total House seats)

40 Freedom Caucus members (9% of total House seats)

So when you combine the Freedom Caucus with the other Republicans, you get a pretty big GOP majority. But when you think of the Freedom Caucus as another entity entirely, a group uninterested in negotiating or governing, a party willing to throw fellow Republicans to the wolves, the political math shifts. You can’t include them in the Republican column, you sort of have to put them in their own column entirely. One that doesn’t care about other Republicans.

Here’s the simplest possible way to say it: if the Republicans can win 20% of Democrats, they can afford to ignore the Freedom Caucus. And if all they want is to get to 218 votes, it’s even easier. They only need to pull 15 Democrats, which is only 7% of their entire conference!

If you can’t pull a meager 7% of the opposing party, maybe your plans for America are a bit too extreme? And yet that’s what the Republicans tried to do. They tried to aim at the furthest right partisans in America and then wondered why their plans weren’t broadly popular. They thought they could just ignore Democrats completely and do whatever they wanted. That failed. Spectacularly.

So here’s the speech I’d write for Trump, based on this data. He’d never follow this advice, of course. But to be clear, this isn’t a wishlist written by a Democrat wishing Republicans acted more like Democrats. This is written as objectively as I can, based purely on what would get him a win, what would help him, what would help Republicans, and in the process, what would help America the most.

The speech I’d encourage him to give:* 
Ladies and gentlemen, members of the press, fellow citizens. You elected me as a straight talker and a deal maker. You cheered me on as a person outside politics that could drain the swamp. You put your trust in me, and for that I am tremendously grateful.

We all saw what happened with the Republican health care proposal last week. It was a good bill, and I extend my gratitude to Paul Ryan and many others for working tirelessly to make it work. The bill had a lot of good ideas, because everyone — Republicans and Democrats alike — know that Obamacare needs updating and improving. This bill aimed to do just that, in order to fix problems normal Americans are having with their health care.

But the biggest mistake I made as your president was reaching out less to the Democrats than I have to an extremist group that calls themselves The Freedom Caucus. These 40 members are the architects of the government shutdown that happened in 2013. The Freedom Caucus are the people who believe we should repeal Obamacare and not replace it with anything, leaving millions of people without health care. And while they’re my friends and fellow Republicans, but I must say I and my administration fundamentally disagree with them. We cannot merely repeal Obamacare, we have a responsibility to make it better. And we will. Starting today.

I have asked all four leaders of Congress, from both parties, to begin drafting new legislation with my team starting today. That means Nancy Pelosi and Chuck Schumer, as well as Mitch McConnell and Paul Ryan, are all going to work on this together. And I’ve directed them to find a plan that works not just for Republicans, and certainly not just for The Freedom Caucus, but everyone. Every American. And I don’t care if I lose 40 Freedom Caucus votes in the process. If their dissatisfaction results in Democratic support, and a better bill, then that is something I can sign without hesitation. So bring me a bill.

There will be disagreements. Democrats and Republicans each have very different ideas about the best way forward. But neither party has a monopoly on great ideas. And at the end of the day, we’re not that far apart if we learn to ignore the extremist Freedom Caucus. Both caucuses that matter want health care in America to be great. So that’s a deal I think we can make, as long as we move beyond the small-minded fringe politics of The Freedom Caucus.

To the Democratic Party, I say: you are welcome as long as you are willing to negotiate. To the Republican Party, I say: we can get further with a bipartisan bill than we can alone. And to the Freedom Caucus, I say: the American people want to move forward. The leaders in congress want to move forward. I want to move forward. Whether you move with us or are cast aside as ineffective radicals is up to you. It’s up to you. I’m ready to deal, but only if you are.

To the American people, I say: thank you for your patience. We’re going to work on a bill together. We’re going to find common ground, and both sides are going to compromise. And when we’re done, we’re going to have a bill that doesn’t just work for the right, or for the left. We’re going to have a bill that works for all Americans. Because that’s my job, that’s my solemn responsibility, and that’s something I take seriously.

Thank you, and God bless America.

The aftermath

The reporting is easy to predict. This speech would be called unprecedented and extremely unconventional. But of course there’s where Trump draws his power. He’s not popular when he’s a standard politician doing standard Republican things. He’s popular when he seems to be apart from the typical DC approach. This speech would look him look like a maverick, which only has upside for him. Many would point out that Trump was throwing his own party under the bus.

But the longer the press chewed on it, the more clear it would be that they abandoned him first. This speech establishes Trump as his own man, with his own presidency, and his own plans. It breaks free from the stranglehold that the Freedom Caucus is looking to keep on Trump and all GOP decisions to come. The press would adjust to the new reality and would like what it sees. It would report on someone willing to make a deal with anyone, not just his own party. That’s a story.

The move would also cause disarray amongst career lobbyists and short-sighted partisans. It’s easy to argue that Democrats should never vote for Trump and Republicans should always vote for Trump. But a statement like this would force new alliances. Suddenly the Democrats would have to wrestle with the idea that some of their members are ok cutting deals with him. Suddenly the Republicans would have to decide if they wanted to work with Trump and Democrats together or be left out of the political calculus of the beltway entirely.

There was a poll a few months ago that said one of the biggest concerns facing Americans is the fact that we’re too divided. That nothing can ever get done. That the two sides are too busy yelling to make anything happen. I doubt Trump has the emotional and strategic abilities to do capitalize on that opening. But then again, he is an outsider to DC, a businessman, and a negotiator. If anyone is willing to look beyond the petty turf wars of the beltway and just pass some dang legislation, maybe it’s this guy. Stranger things have happened.

(Note: I am about as anti-Trump as you can get. But design challenges are more fun when they’re a stretch.)

*Although, let’s be realistic, the Russia investigation might make all this a moot point if his staff faces criminal charges and he gets impeached.

Update, written the next morning:

This morning Trump attacked the Freedom Caucus in a tweet. And his aides on the Sunday morning political shows signaled frustration with them. And more evidence is emerging that he’s toying with many of the same ideas I wrote about above. Toying is different than picking a strategy, of course. But the fact that it’s even being considered as a plan B, as this tweet describes, is fascinating.


By UX Launchpad on March 26, 2017.

Exported from Medium on May 19, 2017.

Sabrina, Steve, and Veronika

A Trainspotting review

I recently read a story about Sabrina Gonzalez Pasterski, a 23 year old Harvard student who is making waves as a young but accomplished physicist. She flew a plane when she was nine years old, so in high school her teacher told her “That’s nice, but what have you done lately?”

That quote led her to build her own plane and fly it when she was 14, documenting the entire process on YouTube. “What have you done lately?” has become her mantra. And it’s a great one to live your life by.


It reminds me of Steve Jobs. I read a story where he returned to Apple and found a back room with a bunch of old hardware and prototypes, the sort of thing you could donate to a musuem. Jobs didn’t want it around. It went against his philosophy, an outlook that was eventually put up on the walls at Apple as a reminder.


Which brings me to the Trainspotting sequel. There’s a scene where Veronica, a woman from Bulgaria, is watching Renton and Sick Boy try to explain the last several decades of Scottish life. It’s a comical and accurate demonstation of male bonding. They’re talking over each other, gesticulating wildly with old football videos playing behind them, and are clearly quite happy to be in their bubble of nostalgia.

She stares back at them, non-plussed. In her native language she says “You live in the past. Everyone here lives in the past. Where I come from the past is something to forget but here it’s all you talk about. That and the weather. So boring.”

They don’t understand her because of the language barrier. But even if she were speaking in English, they wouldn’t have heard. They’d shake it off and do what they did in the movie. More shots! Yeah!


It’s sometimes called “fan service” when recognizable details like camera shots, situations, and dialog are left like gifts for the people that know and love the original material. It’s fun spotting them when you are in on the joke. But Trainspotting took fan service in a dark and stale direction on purpose to make a point. Being locked in the past isn’t cool, fun, or healthy. It’s just pathetic. Sabrina, Steve, and Veronika have it figured out and they’ve spelled out their approach: what have you done lately?

By UX Launchpad on March 27, 2017.

Exported from Medium on May 19, 2017.

How Buy-In Works When Designing Software

A sharpie lesson


















































By UX Launchpad on March 31, 2017.

Exported from Medium on May 19, 2017.

Shut Up and Show Me the Call To Action

I’m ready to go! Don’t try to stop me!

I don’t mean to call out Google’s AutoDraw team, because I’ve seen this simple design mistake at every company, during almost every product announcement, for decades. If anything, AutoDraw is 100 times cooler than most product announcements, and their blog post is actually a lot better than most!

So Dan Motzenbecker and team, you did amazing work with AutoDraw. Truly. But I wish you had made the download link a bit more obvious. Let me take you on a user journey.

1. Scrolling through Twitter, coffee in hand…

I don’t drink coffee, so this header is a lie. But this is what I saw on Twitter as I scrolled through my feed:


“Wait, what?” I thought. “Autodraw? The name is self explanatory. Does it really work?” So I clicked through to the original Tweet to learn more.

2. Drawn in by the amazing animated gif

The Tweet didn’t disappoint. Just like I hoped, the animated gif plainly demonstrated that “AutoDraw” meant “draw something poorly and then it’ll convert into a better version of it on the fly.” Neat!


The comments seemed as impressed as I was. So I clicked through again. “I wonder if it’s available yet?” I thought.

3. Wall of text and media with no clear call to action

The blog post is called Fast Drawing for Everyone and it looks like this.


It’s a beautiful page, and it has a compelling title, but counter-intuitively, that works against the utility of the page. I completely missed the two links that I was trying to find. Here’s how my eyes and brain processed the page:


It’s a much shorter, and much more attractive page than most product announcements, but it left me as confused as the others. I wasn’t sure if the product is available at all. Did I just miss the call to action?

4. Let’s play Detective

First I checked back in with the title of the article. “Fast Drawing for Everyone” is a great summary, but it doesn’t explain if the product is available or not.

Next I looked at the “Floating Action Button” that is common in Material Design. The guidelines for FAB say “only one floating action button is recommended per screen to represent the most common action.” And I’d argue the most common action on this particular blog post should be “download the software” with an “arrow pointing down” icon to match. But I saw the “arrow pointing right” icon, which means share in Google’s visual language. I moved on.

This is the point where “makes sense on paper” and “humans are unreliable and frustrating” collides. If I had read the text carefully, I could have deduced that the blue word “AutoDraw” was a link that would take me where I needed to go. But there are a lot of visual design concepts working against me here. The giant blue header is more eye-catching because of the contrast with the rest of the page. The blue FAB caught my eye next because of the human’s brain’s tendency to pattern match and group similar things. And the amazing animated gif pulled me further down the page because evolution decided to make us incredibly distracted by movement so we don’t get eaten by bears in our peripheral vision. Side effect: I couldn’t ignore the animation. I registed again how cool the tech is and scrolled on.


We know that people don’t read in scenarios like this, they skim. So when I saw this line, I thought I had found the elusive download link and slowed down my skim speed:

If you’re interested in learning more…
Ah ha! I am interested in learning more! But this paragraph is a link to another project. So I read on, and a few other scientific concepts got in my way. First, the boldness of names like HAWRAF and Erin Butner with big white space to the right drew my eye to that section first.

I’m sure they’re lovely people but they’re not a download link, which is all I care about. So then I found myself focusing on the very last word of the post: “Chrome.” This is a phenonomen with how users scroll and skim — they ignore stuff in the middle but focus disproportionately at the end.


Finally, I realized that “Fast Drawing for Everyone” equalled AutoDraw, which meant either of the two subtle blue links on the page would work, once I slowed down to actually read them.

And then I got to use AutoDraw, which is great. Try it out at autodraw.com.

By UX Launchpad on April 12, 2017.

Exported from Medium on May 19, 2017.

I Don’t Own a Laptop

(Other than the one provided by work)

A year and a half ago, I talked about my confusing feelings about iPad Pro in this article:

I Am Confused By How I Feel About iPad Pro
I love using it, but I rarely domedium.com
Then in January I checked back with another article. Its title was pretty straight-forward as well.

iPad Pro, One Year Later
I still love using it, but I still don’t use it enoughmedium.com
It’s been a long time since I bought a laptop for personal use, and the most recent one has since been decommissioned and turned into my son’s Minecraft machine. I get by with the laptops provided by work, and I have for a while.

Today I bought another iPad Pro, the smaller one. I realized that this might be it. I might never buy a MacBook again. I remember the same strange feeling when I realized I had bought my last desktop PC.

Nothing big has changed since my previous two articles. I feel better when I use iOS because of how it helps me focus. I prefer the battery life. I prefer the ability to have cellular data built in. I prefer the quality of the software. I prefer the portability.

And yes, there are still things I can’t do as well as I can with a laptop, a full sized keyboard, an OS that allows infinite overlapping windows, a screen that stays open at the right position without wobbling, and that phenomenal touchpad. There’s no Sketch, BBEdit, Xcode, or command line, and that’s going to get in the way.

So I’ll still use my work computer quite a bit. I’m not quitting cold turkey or trying to make this into a bold statement about the future of computing. I’ll be dual-use for a long time, especially as long as I work for a tech company that gives me a laptop and expects me to use it.

But now I prefer the iPad Pro to MacBooks now. Interesting.

By UX Launchpad on April 17, 2017.

Exported from Medium on May 19, 2017.

Indeed. I think it’s a given that most users don’t need it. But fortunately iCloud Drive returns file management for people who want it.

I have no doubt that pixel work has every chance to be better on a tablet than a traditional laptop, eventually. Holding a tablet on your lap will never be as comfortable as a laptop, but everything else has the ability to be better. The software just needs to be written for touch instead of assuming a cursor.

Writing code is tougher because a lot of coding makes you have a lot of windows open for research, plus access to a command line, etc etc. But I do think there’s a difference between “the way we’ve always done it doesn’t work as well yet” and “it will never work.”

I think balancing a tablet on your lap is in the “it will never work as well” category. But we might look back at Xcode/MacBook development and laugh at how bad it was in 2017. We just need to design the new approaches, and I think Swift + Playgrounds on iPad is a step in that direction.

By UX Launchpad on April 18, 2017.

Exported from Medium on May 19, 2017.

You Can’t Judge and Learn Simultaneously

AirPods are just the latest chapter in a very long book.

I worked as a computer lab tech to help pay my way through art school. During lunch, someone said “Apple’s G4 towers have a giant speaker on them, but it seems like a waste of space. Most people will just plug in external speakers, so why bother?”

And I said “I read about this, and it’s pretty cool — the giant speaker doubles as a subwoofer when it detects external speakers are plugged in.”

And he looked at me, a fellow Apple enthusiast, and said “sometimes your love of Apple frightens me.”

He has a point. My feelings for Apple are so strong that it accidentally became the opening lede in a Bloomberg article about Microsoft. (Sorry, Ballmer) Call me a fanboy and you’re not wrong. But here’s the thing I have trouble getting past: he asked a question, and I had an answer. Do people want to understand design rationale or do they just want to complain?

When AirPods were announced, the reaction was immediate and boilerplate: Too expensive. Short battery life. They look stupid. They’re nothing new. Apple has forgotten how to innovate. But the specifics jumped out at me. A lot of people declared on Twitter that two things were fatal flaws.

Everyone is going to lose them
… because they’re going to fall out of ears constantly
So I went to an Apple Store. I asked for a demo. (It turns out they have a whole system for trying out AirPods so everyone gets a fresh pair and no one has to deal with someone else’s earwax.) I put the AirPods into my ears and jumped around while headbanging. The Apple Store employee looked on, bemused. And the AirPods stayed in, despite my super tiny ear canal and my spastic thrashing.

The next time I went on a run, I paid attention to the black wire connecting my standard headphones to my phone. I was constantly looping the wire around my fingers, and being careful that my phone didn’t extend too far away. I hadn’t considered it before, but do you know the single greatest cause for headphones flinging themselves out of your ear?

A wire.

Connecting your headphones to a heavy weight.

Dangling across your arms.

While you’re pumping your arms during a run.

Hm.

I know some people that bought AirPods, and the reviews are unanimous. They all say “they seemed stupid at first, but now I love them.” And there’s a lesson here. Not just for AirPods, or the G4 speakers, not just Apple, not just for tech, but a more general life skill. When you’re presented with something new, be still. Slow down. Learn about it. Try it. Form your own opinion. Don’t measure yourself or your design ability by how quickly you can get a snarky hot take retweeted by others. Instead, try measuring yourself by how well you give the benefit of the doubt, do original research, and listen.

By UX Launchpad on April 26, 2017.

Exported from Medium on May 19, 2017.

I Had This Scary Thought

It’s about code.

I’ve been a designer/developer since the 90's. I know the command line. I can write code. I’m nerdy. But I also went to art school and I’m a professional product designer. I’ve always combined the two.

I like to point out that design isn’t art, and art isn’t design. If you ship a product and no one can figure it out, it’s a bad design. It doesn’t matter if you picked a really great layout, color scheme, or paired typefaces that seem destined to live together. It doesn’t matter if it’s the prettiest, most satisfying thing you’ve ever done. The only thing that matters is if it can complete its task. If it can’t, it’s a failure, because design is quantifiable. But if you want to just play around with pixels, go nuts. That’s art. There’s no money in it, but it’s fun.

I’m also fond of pointing out that code is very creative. There’s no single way to do anything. Developers are already problem solvers, and therefore they’re already designers. Maybe Fred the designer doesn’t have a strong grasp on color theory or graphic design, but he gets problem solving. That makes him a designer. But the epiphany that I had a month ago scared me a little. The developer isn’t just a problem solver. They’re not just a designer. They’re an artist. In a way I miss.

Uh-oh.

For years I loved the structure in design. If I make a feature and no one likes it, it fails. It’s purely objective. It works or it doesn’t. That’s cool. But it’s also really limiting. It’s obvious to me that computers will be able to algorithmically determine best practices and land on the most optimized design. The field has evolved to a point where it can be done by a sufficiently powerful enough algorithm. That’s good. But also sort of boring for me. And so…

I bought a server. I set it up with a dedicated IP address. I put a Minecraft server on it. I decided to make some custom maps in a mini-game called Bed Wars. (Ask your Minecraft obsessed kid what that is) Then I set up a pirate radio station. Now I’m streaming two different radio stream and I have ideas for more. Beyond that, I want to make bots that respond to the news of the day and display them into arty collages of GIF-powered animation to see what happens. I want to set up a proxy or VPN so all my cell phone traffic is logged and I can analyze the data like Feltron does. Maybe I could pipe it through R in real time. Or what about today, when I saw code that randomly flips 100 bytes in MP3 files each time they’re accessed, so the files degrade over time like tapes and vinyl used to. That’s so cool, and so pointless. I love the creativity and art of it. I’m going to fork the heck out of that repo. Which reminds me, I want to set up my own git repository because I don’t like it to be public on github. And I can. Since I write code.

Every one of those ideas, plus the 100 others I’ve been exploring and can’t fit into this essay, require knowledge of code. And more importantly, only a code mindset illuminates these art projects. I simply wouldn’t be thinking of them if I was restricting myself to iPhone apps using standard HIG controls. Or if I was considering that it feels like every single app has already been made, and even if I added a new one to the App Store, no one would find it.

But that’s VC thinking. That’s design thinking. That’s “one right answer” thinking. As much as I hate the pretentiousness of the word, I’m more of a — gag — artist. I miss it. And I miss code. I miss doing things for no reason at all. That’s where I have the most fun.

Years ago I was at a frog design party and I met my friend’s husband, a hot shot designer. He had heard of me, I had heard of him, and we were finally talking. At one point, eyes on fire, I said I had an idea for a portfolio site I was really excited about:

“When you’re on my portfolio, you should be able to set the background color. For everyone. So you set it to orange, then the next person that visits sees orange. Then they can pick a new color.”

He asked, “But why? What purpose would it serve?”

I said “It would just be cool.”

And he said “You’re better than that.”

But I’m not sure I agree. I know how to make a company money. I know how to work my designs through A/B tests. I know how to help users complete their tasks. I love that.

But I also love playing. Doing things for no reason. Curiosity. Whimsy. Art. Creative freedom. So I’m writing code again.

And it’s awesome.

By UX Launchpad on April 28, 2017.

Exported from Medium on May 19, 2017.

Code for Designers, Part 1

We’ll start with a quick command line trick

If you’re on a Mac, there’s a built-in program called Terminal. Do a search for it and launch it. It’ll look something like this.


So that’s the command line. But it’s a scary place at first. It can feel like waking up in an unfamiliar cabin in the woods after blacking out the night before. You might be asking “where am I?” or even “who am I?” before starting to look around and take inventory. So let’s do the same thing now. “who am I?” can be learned by typing whoami:


Makes sense. If your login is turtleflower, that will say turtleflower. In this case, my test account is named steve, so the response is steve. How do we figure out where we are? We type pwd which stands for —hm. I have no idea what it stands for. A lot of commands are like that. It’s just pwd because we decided decades ago to use that name. Try it now.


That makes sense too. There’s a Users folder, and under that there’s a steve (or turtleflower, or whatever your name is) and that’s where we are. At the command line, this is called “home.” Which is why the titlebar has a little icon of a home. Next let’s figure out what files are here. The command for this is ls which stands for list.


This should look familiar. This is the standard home directory structure on the Mac. If you have other files in your home directory, you’ll see them here. Makes sense, right?

Now let’s try typing clear and hitting enter. Here’s how it looks before and after you enter the command.



This can be helpful when the screen is getting full. Or if you’re about to enter a command that’s going to return a bunch of stuff, which we’re about to do. We’re going to do a search for every file in our home directory. Every single one! The way we do it is by typing find .

(notice the dot at the end)

The word “find” means “find” and the dot means “right here.” So the command means “find everything starting right here, even if things are in subdirectories.” Here’s how it looks before and after running the command.



That’s pretty powerful stuff. Siri or Spotlight don’t have ways to say “show me every single file in this entire directory, even if things are squirreled away in folders” even though sometimes that’s exactly what you need.

But what would be even better is combining this with a way to google for particular names. For example, maybe we want to find every file with the word Adobe in it.

The command for that is find . | grep Adobe as shown here.


We’ve introduced two new concepts here. First is that little line, which is called a pipe. On a Mac laptop you type it by holding shift and pressing the | key right about the enter/return key. The pipe is a way of passing information from one command to another, and it’s really common.

The second new concept is the word grep which is an all-purpose “needle in a haystack” tool. So when you look at our entire command —

find . | grep Adobe

— in plain English it’s saying “find everything starting right here, even if things are in subdirectories. Once you have that list, I want you to see if the word Adobe appears in any of the lines. If so, show them.”

So if we hit enter on that command, here’s what we’re shown.


Whump. A big list of every single time the word “Adobe” shows up in our home directory. (Note that this is case sensitive, so grepping for “adobe” and “Adobe” will return different results.)

But sometimes a giant list in the command line isn’t as helpful as an actual file. So how can we take this command and put all the results into a file? By using the \> sign. When we use “\>” we’re saying “and put all these results into a file. So here’s our new command:

find . | grep Adobe \> adobeResults.txt


In plain English, our command now says “find everything starting right here, even if things are in subdirectories. Once you have that list, I want you to see if the word Adobe appears in any of the lines. If so, put them into a file called adobeResults.txt.”

Now if we open our home directory, we’ll see that we just made a new file! That’s kind of cool.


And if we open it, we see our big list of every time the word Adobe was found in our home directory.


Knowing this tiny bit of code has helped me many times as a designer. Maybe it will help you someday!

By UX Launchpad on April 29, 2017.

Exported from Medium on May 19, 2017.

Introducing the Design Dare Residency

Share a cool internet thing you made, take over designdare.com for a month, get $100


Notes:

Submit entries to designdare@lot23.com.
It would help a lot if you could share this post or a link to http://designdare.com. If I don’t get some publicity, no one’s going to submit anything, and my effort to getting more internet art featured won’t get very far :)
By UX Launchpad on April 30, 2017.

Exported from Medium on May 19, 2017.

Windows RT and Windows 10 S

When Microsoft announced Windows 8, they also announced something called Windows RT. There were differences, and even benefits, to RT. But all anyone talked about was how it couldn’t run legacy Windows apps.

So no Photoshop, no Chrome, no Steam, no major label games. Sure, it had a browser built in. And sure, it had an app store with some apps. So it’s not like it was as stripped down as a Chromebook. But to some, it felt like it.

Today Microsoft announced Windows 10 S, which has a similar pitch. The security is better. The speed is better. Incompatibilities are less rare. But again, like four years ago, you won’t be able to run much software on it.

Google has the web and Android going for it. Apple has the loyal iOS and Mac markets going for it. They’re not the largest, but they’re the most lucrative in software by far. Traditional Microsoft has corporations, gamers, and legacy software going for it. But new Microsoft, as seen in Windows 10 S, has none of that. They don’t dominate the web, smartphone marketshare, corporations, games, or even desktop OS marketshare.

I respect them for trying to move people over to their ecosystem. It’s the least bad option they have. But I’m having trouble aligning the user needs to the business objectives. Users like speed, security, and great devices at cheap prices. But they also value apps. As long as that’s true, and as long as mainstream software like Photoshop doesn’t exist on this platform, I don’t see how it can succeed broadly. No matter how much Microsoft wants it to.

By UX Launchpad on May 2, 2017.

Exported from Medium on May 19, 2017.

A Jock at Design Critique

What creative culture could learn from sports

“Hey,” he called over to me as I gulped down water and tried to catch my breath. “What I was saying out there is when their offense is at midfield, come up to meet them.”

“Yeah,” I croaked. “I totally get it. I’m just out of shape. But I’m working on it.” He nodded in sympathy. Indoor soccer is intense. “The thing to remember is this game is just as much like hockey as it is outdoor soccer.” Great point. I hadn’t thought of it that way. “Yeah, that reminds me. Are there positions like outdoor soccer?” He thought about it. “I mean, yeah. Like, when you’re on a better team you might stick in a formation, then you just know when a defender goes on attack, someone else drops back. But it’s really fluid, you know? That’s how we’ve gotta be out there. Just sort of looping around.”

“Thanks, man. I appreciate the feedback,” I grinned. “And seriously, keep it coming. I’ll take everything I can get!” I started to head out of the giant sports facility and every one of my teammates and some of my opponents called out support as I left. “Great job out there, man” and “Great hustle” and so forth. It got me thinking about design crit and how different the feedback can be. And how differently I feel about it.

First, a disclaimer. I’m not good enough to be considered a jock or an athlete. But I’ve played sports my whole life. Soccer, tennis, lacrosse, wrestling, cross country running, cross country skiing, and now indoor soccer. I’m never very good compared to my teammates. And I never mind getting feedback, but it’s almost always supportive. Which is, for some reason, surprising to tech folks. Which is puzzling to me. Designers and developers: our community has earned a reputation for some of the meanest and most counter-productive feedback in the world. We could learn a lot from jock culture.

There’s a great tumblr post going around where a woman is accidentally eavesdropping on three men, one of whom doesn’t know how to throw a football. And she rolls her eyes, thinking “here we go, they’re going to tease him for being girly.” But no, they spend a long time teaching him how to throw a spiral, all the while being incredibly supportive. At one point the guy says “Did I do ok?” and his friend responds “You did awesome. You’re really getting it!” and the tumblr evesdropper just about dies of happiness. She says something like “I want a tumblr of just men being helpful to other men because this is amazing.” I agree it’s amazing. But in my experience, it’s super common in sports. But not in creative fields. And that’s a shame.

I’m not going to say all crit is bad, because it’s not. I’m not going to say I’ve never gained insight or improved my designs through crit, because of course I have. And yet. The batting average (oops, sports metaphor) is surprisingly low, in my experience. I went to art school, where I witnessed firsthand the “tear down everyone’s work” approach for how to run a crit. And I think that dysfunctional art school approach is still too common in design studios around the world. I don’t think it works. I’ve started to put my finger on why, above and beyond the fact that people learn better when they’re not fearful of failure.

First, there’s a whole lotta personal opinion passing as strong design critique out there. Many times I’ve gotten, frankly, not very good feedback from clients, executives, stakeholders, my managers, other designers, etc. But they don’t know it’s not good feedback because they’re making a snap judgement with limited data. And they’re expecting to be told their feedback is super great because of their role on the team.

On the other hand, the people on the team who have been working on the project since day one have a much deeper and more nuanced understanding. So in a poorly run crit, it can become the job of the person with six months of data to wow a mostly clueless person in six minutes. And to accept the feedback as if it were really helpful. Sometimes it’s not, but it’s rude to point it out if they’re a VIP.

And that’s the other strange problem — when power structures collide with honest feedback. When I was told to position myself more aggressively on the soccer field, it was referring to an objective truth that came from an honest place. He was saying “Hey. Do X. It will help the team.” It’s just that simple, because it’s simply strong soccer technique. And it didn’t have to come from a captain, or a coach. It’s just true, regardless of who says it, and has been indisputable for decades. My job is to listen and improve. Period.

But when you tell me “The logo needs to pop more,” that’s not an objective truth. But if you’re my superior, or my client, now I have to find a way to make the logo pop more. Or try to convince you off your strongly held opinions. But overplay your hand and it’s not about the logo anymore, it’s about who gets to call the shots. It becomes about ego rather than the quality of the final product. Which makes it messier. And harms teamwork. Which harms the final result. I’ve seen it happen a lot. I think we all have.

Interaction design has been around for a long time. But sometimes we exaggerate how much we truly know, and we believe our own strongly held beliefs have passed into the realm of objective truth. One day, maybe. But in the long arc of history, interaction designers today are just cavemen and women banging rocks together and talking about their rock bashing technique at conferences. We’ll look back at 2017 best practices and laugh. We should give our strongly held design feedback accordingly, and always leave room for the idea that we may be wrong.

By UX Launchpad on May 3, 2017.

Exported from Medium on May 19, 2017.

My understanding is it’s $50 to remove the restrictions:

“If you’re not down with that, Microsoft will let you switch any Windows 10 S computer, including the Surface Laptop, to the regular Windows 10 Pro for a one-time $49 fee — less if you’re on a tablet or something else with a smaller screen. But if you do that, Microsoft says, it can’t guarantee you’ll get the improved battery life and performance.”

By UX Launchpad on May 3, 2017.

Exported from Medium on May 19, 2017.

Windows 10 S, Part 2

I originally wrote about Windows 10 S and compared it to Windows RT because neither can run legacy software like Photoshop, Chrome, and the vast majority of games. But it’s a little more complicated.

It turns out you can spend $50 to upgrade your Windows 10 S to Windows 10 Pro, which unlocks legacy apps. I like this maneuver because it’s sort of “cake and eat it too.” Short term, Microsoft can strongly encourage developers to get in their App Store since millions of people will stick with the defaults. And in the long term, fewer and fewer people will opt for the legacy versions of the apps.

But I’m a little puzzled by people that call this a bold or original move. Some press says Microsoft made an OS locked to a store before Apple did. That’s an odd way to look at it. Apple decided to lock iOS to the App Store back in 2007. And a few years ago they made it impossible to double-click a “side-loaded” (non-App Store) app on macOS to launch it. (You can right-click and select open, then click through a warning dialog.) Apple locked both their OSes long ago.

If the design challenge is “how can we gain the security, battery life, and speed improvements inherent in a curated app marketplace while still letting power users access the apps they’re used to,” there are two ways we’ve seen so far. Apple is following a two OS strategy, one for the the future and one for legacy. iOS can’t side load apps. And macOS disallows them unless you know how to get around the protection.

Microsoft has a single OS strategy with three tiers. One of the tiers is locked down, but you can spend $50 to unlock it. But with that unlock, you lose the security angle. A person can simultaneously want Photoshop but still want the rest of the OS as locked down as possible. It would be nice if Microsoft found that grey area. They could just let users right-click through a dialog when apps aren’t from the store. The same security speedbump without requiring $50 for the privilege.

By UX Launchpad on May 3, 2017.

Exported from Medium on May 19, 2017.

I agree with you whole post, and I deeply sympathize with how hard it must be to make the right moves for the future when every move comes saddled with such big drawbacks.

Which brings me back to the idea I mused about before. If Microsoft used the same model as Apple — sideloaded apps need a certificate from Microsoft, otherwise you need to right click them to open them after a dialog — I think that could work.

You still encourage the Windows Store. You still make non-signed apps look shady. (or even lock them down completely in corporate environments) You still boast Photoshop support out of the box. And you can tell companies like Adobe that if they use too much data or battery or whatever that they could lose their license. So you gain some leverage.

As for the “Microsoft can no longer guarentee such and such battery life” stuff, that’s a concern. But I wonder about that. Is it really so hard to explain that the new Windows laptop gets 10 hours of battery life … but some software eats away at the battery at a faster speed? Like games?

By UX Launchpad on May 4, 2017.

Exported from Medium on May 19, 2017.

The Usability of Music Formats

Like 8 track

I don’t think I ever listened to an 8-track tape, but I remember seeing a few in my attic. And I remember the jokes. 8-track was an easy way to get a laugh because it was so lame. We all agreed. It was just understood.

Last year I bought a turntable. And I wanted to play things on it, so I went to thrift stores to find old vinyl albums. It was interesting. But I quickly discovered that I couldn’t get into flow while listening to vinyl. As soon as I got comfortable, I’d inevitably have to get up to flip the record over.

Sometimes we look down our noses at today’s rapid-fire ADHD world. How no one can focus anymore, or read a long book, or process complex things. Well. If I was listening to music in 1965 and I had to stop writing to flip a record every 23 minutes, I’m not sure how I’d ever get anything done.

This weekend I saw a painting that used 8-track tapes for the canvas. And I got to thinking about the clearly inferior 8-track format. I wondered how inferior it really was. After all, 8-track never had to be flipped over. So if you didn’t mind listening to one album on loop, you could get into the flow. Which seems a lot better than vinyl to me.

Wikipedia tells me that 8-track found success in cars, a place where vinyl records weren’t feasible. And in time, cassette tapes took over the market because they had most of the same benefits without some of 8-track’s drawbacks. It’s funny to think that vinyl was, and still is, beloved. But the user experience of a record has some clear problems. Especially if you want to get into a flow. Doubly so in a car.

Meanwhile, 8-track is derided, but it was actually pretty good at some core scenarios. 8-track is a bit like Friendster. It wasn’t best, and it’s easily overlooked, but it saw where things were going and inspired the experiences of the future.

Speaking of which, I was working at RealNetworks when they announced one of the first online music stores and streaming audio products. It was just after 9/11, and iPods were a few months old. There was a lot of discussion about the future of music. Would people steal music? Pay for files? Accept DRM? Prefer streaming?

In the long run, we know what happened. After about a decade of buying music for iPods at about $10 an album or $1 per song, music streaming began to take over. In 2017, the idea that you’d buy an album for $10 that you can just stream whenever you want is feeling as dated as 8-track.

But back then we weren’t sure how things were going to pan out. I remember owning an iPod full of pirated music, which was great. But I also was a RealNetworks employee, meaning I had access to a catalog of streaming music. And it was absolutely amazing. A co-worker could tell me about a new album, then I could go listen to it on the spot. For free.

“Yeah, Grandpa, I know how Spotify works.” But no, hear me out. This was 2001. Almost 20 years ago. Before smartphones and apps. George W Bush had just started his presidency. It was another era.

And yet, even then, it was clear that this streaming experience was where things were going to go. But my coworkers and I made the classic mistake of substituting our personal experiences for actual data. We, as RealNetworks employees, with free accounts, liked streaming. So we thought everyone would.

I remember telling my co-workers that as much as I loved the streaming — truly loved it — I wondered if maybe our target audience was more cubicle dwellers than kids. Streaming takes a lot of bandwidth, after all. And iPods couldn’t connect to the internet. I remember the conversation basically ended with “No. Streaming is amazing. It will win.”

And we were right! But we were over a decade early. And when it comes to consumer products, that’s no different than being wrong.

By UX Launchpad on May 8, 2017.

Exported from Medium on May 19, 2017.

Expats Versus Immigrants

Last year I spoke at Pixel Up!, a wonderful design conference in South Africa. In the car on the way to the speaker’s dinner, I asked a local designer about race. He had a point I’ve thought about ever since.

“You and I are similar. We’re both experienced designers, we’re both fluent in English, we’re both well educated. However, I’m a black African. So if I came to Seattle, I’d be an immigrant. Whereas if you came here to Johannesburg to do design work as a white American, you’d be an expat. The words are only different because of race.”

This has been on my mind a lot because my family and I are moving. And we’ll be doing it as immigrants, not expats.


Beautiful Wellington, New Zealand
Thinking like an immigrant started as a semantic exercise, but has evolved into something more meaningful. It’s framed a lot of our discussions for the better. It’s my first time living in another country (though I was born in Germany!) so the best comparison I can make is to design and tech things. Here are a few silly comparisons to explain how it’s felt.

A new computer or phone
When you get a new device, you have the choice to move over all your files or start fresh. I’ve been moving over my backups for over a decade. Recently I’ve been starting fresh. I miss some things, but I like the fresh canvas. Imagining our move as immigrants feels like that.

Switching to Sketch
Photoshop used to be the reigning champion of interaction design. (I was always partial to Fireworks, but I was in a tiny, vocal minority) Then came Sketch, and within a year or two it was a true contender on design teams.

The switch was scary at first. For someone comfortable with Photoshop, it’s hard to stop cold turkey to try a new thing, even if you have heard it’s worth it. Imagining becoming immigrants, after knowing what it means to be American, feels like that.

An idea you can’t get out of your mind
Every so often people get struck with an idea that they can’t shake. Evolving it into a tangible product can turn into a compulsion. Sketching, coding, and brainstorming late into the night. Manic, excited conversations with anyone who will listen. A dream of something new, something better, something fun. Something worth the hard work.

Our move feels like that. We can’t wait.

Know someone in Wellington, New Zealand looking for a UX designer? I’d love to chat with them! Write me at jon@lot23.com.

Tagged in New Zealand, Design, Wellington

By UX Launchpad on August 4, 2017.

Canonical link

Exported from Medium on November 5, 2017.

“Of Course” Design

[An old favorite from another account. Posting here instead.](#)

When people try to design magical interfaces, they’re often aspiring for the “wow” moment, but that’s the wrong focus. Designers should instead be focusing on “of course” moments, as in “of course it works like that.” Most product design should be so obvious it elicits no response.

The problem with aiming for “wow” is when you try to innovate, your design process encourages novel new interactions. But by definition, novel and innovative are often not familiar or intuitive, which can easily tip over into “hard to use.”

On the other hand, the Nest thermostat, the iPod click-wheel, iOS’s pinch-to-zoom, the Wii controller, the first Google Maps, pull-to-refresh, and many other recent design success stories have a very strong “of course!” sense to them. Think back to using those products the first time. You probably didn’t struggle with them, which is why they succeeded despite employing unfamiliar interactions.

Great design does what it is supposed to with minimal fuss, and without drawing attention to how clever it is. That’s “of course” design. It’s wickedly difficult to pull off, it doesn’t garner headlines outside the design world, but it’s all your customer really wants.

Designers going for “wow” are too often leading with their desire to be recognized for an ability to innovate. Designers going for “of course” are earnestly attempting to fade their design into the background. They want their solutions to feel like they always existed, like there could be no other option.

Forget “wow”. You can’t conjure it. Instead, work to incorporate “of course” to your product. Your users will love the result, even if they can’t put their finger on why.

Tagged in Design

By UX Launchpad on August 5, 2017.

Canonical link

Exported from Medium on November 5, 2017.

McDonald’s Theory

[An old favorite from another account. Posting here instead.](#)

I use a trick with co-workers when we’re trying to decide where to eat for lunch and no one has any ideas. I recommend McDonald’s.

An interesting thing happens. Everyone unanimously agrees that we can’t possibly go to McDonald’s, and better lunch suggestions emerge. Magic!

It’s as if we’ve broken the ice with the worst possible idea, and now that the discussion has started, people suddenly get very creative. I call it the McDonald’s Theory: people are inspired to come up with good ideas to ward off bad ones.

This is a technique I use a lot at work. Projects start in different ways. Sometimes you’re handed a formal brief. Sometimes you hear a rumor that something might be coming so you start thinking about it early. Other times you’ve been playing with an idea for months or years before sharing with your team. There’s no defined process for all creative work, but I’ve come to believe that all creative endeavors share one thing: the second step is easier than the first. Always.

Anne Lamott advocates “shitty first drafts,” Nike tells us to “Just Do It,” and I recommend McDonald’s just to get people so grossed out they come up with a better idea. It’s all the same thing. Lamott, Nike, and McDonald’s Theory are all saying that the first step isn’t as hard as we make it out to be. Once I got an email from Steve Jobs, and it was just one word: “Go!” Exactly. Dive in. Do. Stop over-thinking it.

The next time you have an idea rolling around in your head, find the courage to quiet your inner critic just long enough to get a piece of paper and a pen, then just start sketching it. “But I don’t have a long time for this!” you might think. Or, “The idea is probably stupid,” or, “Maybe I’ll go online and click around for — ”

No. Shut up. Stop sabotaging yourself.

The same goes for groups of people at work. The next time a project is being discussed in its early stages, grab a marker, go to the board, and throw something up there. The idea will probably be stupid, but that’s good! McDonald’s Theory teaches us that it will trigger the group into action.

It takes a crazy kind of courage, of focus, of foolhardy perseverance to quiet all those doubts long enough to move forward. But it’s possible, you just have to start. Bust down that first barrier and just get things on the page. It’s not the kind of thing you can do in your head, you have to write something, sketch something, do something, and then revise off it.

Not sure how to start? Sketch a few shapes, then label them. Say, “This is probably crazy, but what if we.…” and try to make your sketch fit the problem you’re trying to solve. Like a magic spell, the moment you put the stuff on the board, something incredible will happen. The room will see your ideas, will offer their own, will revise your thinking, and by the end of 15 minutes, 30 minutes, an hour, you’ll have made progress.

That’s how it’s done.

Tagged in Culture, Design

By UX Launchpad on August 5, 2017.

Canonical link

Exported from Medium on November 5, 2017.

Solo, Paired, Grouped

A lesson for brainstorms

I learned this trick from Quiet, a book about introverts. Instead of running a brainstorm the standard way, where everyone groups themselves together, try going solo, then paired, then grouped.

Imagine you’re asking the room to collect insights about your team’s current product. First get everyone to write ideas on sticky notes. As many as possible. Then, instead of going straight to the big discussion where you group sticky notes, rank them, blah blah blah, you pair up.

In the pair, nothing gets lost. It’s two people talking, regardless of circumstance. Even if they’re introverted. Even if they’re in the throes of impostor syndrome. Even if they think their ideas aren’t as important, or won’t be popular, or a million other little doubts that get in the way. It’s just two people sharing their thoughts as efficiently as possible.

A Venn Diagram appears. There are ideas that person A has, but person B does not. Then the reverse. Then ideas they both have. All three circles have valuable information. Document them. Then it’s time to jump into the broader discussion, the standard brainstorm.

From there, everything goes as expected. We’ve all been to brainstorms before. But there’s a vast difference between diving straight in versus starting solo, moving to pairs, then graduating to the group. Try it out!

Tagged in Design

By UX Launchpad on August 20, 2017.

Canonical link

Exported from Medium on November 5, 2017.

I No Longer Trust Notification Badges

Fool me once…

Many social apps started out with a simple pattern: one tab for global stuff, one tab for stuff that has to do with you. It was usually called Notifications, and people learned to place higher importance on it. After all, there’s always something happening around the world, but you don’t always have a message waiting for you. For a while, the badge was special.

And then things changed. Companies realized they could put all sorts of stuff in there, throw up the red badge, and people would investigate. Or “engage,” as we say in the biz. Each time we did, someone got closer to reaching their OKRs. Money was made. Promotions were doled out. Hurray for capitalism!

But I don’t trust notification badges anymore, and I bet I’m not alone. The messages waiting for me in the notifications tab are frequently not about me. Instead, they’re encouraging me to take some kind of action, or read promoted content. They’ve turned into spam, so I don’t engage as much. Meaning someone is reading a spreadsheet somewhere and freaking out. There’s an important lesson here.

It’s easy to boost numbers in the short term with design decisions like these. But the gains are often temporary, and it’s incredibly hard to win that trust back.

Let’s say your company is making $10 per user, then you mortgage long term trust for short term gain by putting spam in notifications. That boosts the number to $11 per user. That’s 10%! An OKR victory! Promotions for everyone! Wall Street rejoices!

But as time goes on, people like me learn to ignore the badge. And whoops, now your per user value goes back to $10, then to $9. What does the company do? How can it reverse this slide? How can it please Wall Street? And oops, I almost forgot, pleasing users might be a good idea too.

As a designer, I’d love to think we could revert to the old non-spam pattern. But nope, that would mean the numbers could drop another 10%, which will make the money problem worse. They’re stuck. (Here’s an article I wrote about my time at RealNetworks where this very thing happened.) And what about accountability? The person or team that originally made the change are probably on another team or even another company by then.

So the spam sticks around with no easy fix, and with no one taking responsibility for addressing it or causing the problem in the first place. The experience gets worse, meaning the product is worse, meaning the company makes less money. And the downward spiral continues.

John Gruber has a great insight on this phenomenon for everyone at a product-making company, from CEO to designer to engineer to product manager to CFO. As an industry, we should all take note.

Once you’re backed into a corner like this, where your users’ happiness and satisfaction are no longer aligned with your revenue, you’ve already lost. It’s like the dark side of the Force — you should never even start down that path, or you’ll be corrupted.
You can convert trust to impact for a while, but not forever. Don’t even try.

Tagged in Startup, Design

By UX Launchpad on August 23, 2017.

Canonical link

Exported from Medium on November 5, 2017.

Phoneless

Everything changed when I beta-tested Windows Phone for work

When you work at a software company, you’re expected to beta-test, or “dogfood” the software. Which is how I found myself without a reliable phone for a few years when I worked on the Windows Phone team. I was rarely using production-quality hardware and software. Instead, I was always on the cutting edge, installing and reinstalling my phone software every single day. I learned not to trust my buggy phone. It rewired my brain for the better.

I recently went to Moscow to speak and teach, so before going I erased my phone and laptop. Then I re-added a few things: three games for the long flight, plus a copy of the Keynote files I’d be presenting. And when I returned the US, I never got around to restoring my backups. I’ve re-installed a few things here and there, but I enjoy the realization that I‘m missing an app. It gives me a chance to wonder if I really need it. I usually know the answer. And so put my phone back in my pocket and figure out what I’ll do instead.

Yesterday Apple announced that their Apple Watch now has a cellular connection. I’ve been waiting to hear this for three years. This product has everything I need (incoming emails and texts) without all things I don’t (absent-mindedly reading other people’s hot takes every time I’m bored for more than 2 seconds). In the short term, I’ll carry my iPhone and wear the Watch. But if all goes well, I may leave the reliability and infinite awesomeness of my phone at home.

It makes me wonder about the long run. If many of us agree that we’re overwhelmed with information overload, and battery life continues to improve, and wearables continue to gain popularity, what happens next? Will boredom, a calmer mind, fewer devices, and less incoming news ever be a mainstream user’s goal? And even if it became a well-understood user need, is there enough money in it to justify design and engineering investment?

Put another way: if the world decides to dial back how much time we spend on our phones, would the global economy ever recover? Will we always expect great products to take our breath away, or is there room in a capitalist system for experiences that allow us to breathe once in a while?

Tagged in Apple Watch, Capitalism

By UX Launchpad on September 13, 2017.

Canonical link

Exported from Medium on November 5, 2017.

Getting Stuck in the Gobi Desert

The problem with dreaming

My elementary school did “writer’s workshop,” time set aside for kids to learn creative writing. Somewhere along the way, I began writing an epic story. I invented a new society and a new race with their own customs and rituals. I even went into detail about the special kinds of caves they built, and investigated their actual location in the world. I settled on the Gobi Desert.

My teachers were charmed and thrilled. I was only in second grade, yet you could trace the bold trajectory of my story across an entire trilogy of epc-length books. My enormous project was a novelty around school. I was sent to other classrooms to show it off, like a traveling roadshow. (The first public speaking I ever did, come to think of it!)

But here’s the thing that no one pointed out: the story was all talk. I only ever wrote a single chapter. There were lots of discussions, sure. And some sketches. And my roadshow. But when it came to the actual work, it was a single, threadbare, lonely little chapter. It turned into a thing to talk about, not a thing to actually do. It took me days just to settle on a location. That’s nuts, but it looked on the outside like Hard Work. It wasn’t. It was fear.

At the XOXO conference a few years ago, I spotted one of my early web heroes. I love his writing style, so I struck up a conversation with him. I asked how the craft was coming along. Soon it was clear he was stuck in the Gobi Desert.

“Well the thing about writing an epic is first you have to define the history, which takes some time. Then you need to layer in the culture, and then-” and I tuned him out like the Peanuts listening to the droning teacher. “Oh my,” I thought. He is never going to write this book. Unfortunately, I was right.

(So far. We’ll see! I’m not holding my breath.)

This is the point in any proper feel-good, self-help, thought-leader, sure-to-go-viral post where I tell you to go forth with courage! Don’t talk about it, do it! McDonald’s Theory your way to success! Blah blah blah. You know how these posts end just as well as I do. (Q: why do we keep reading them?)

Instead I want to leave you with a mental picture. Imagine every single piece of writing or design in the entire world, all lined up like they’re in a horse race. Imagine you can see how far along they are, from conception to being released into the world. From 0% to 100%. How would it look over a year? How would the dots move?

You’d see one in a billion that just seems to fly along the track, from start to finish, again and again over the course of months. That’s Stephen King. Hi Stephen King! That guy is in his own league.

But the other thing you’d notice is how few of these dots ever move. They flicker into life at the starting line, and maybe lurch forward 1% or 5%. And then they stop. And fade away. That’s their entire lifespan.

That’s how my Gobi Desert epic died. That’s how my web hero’s story died. That’s happening, again and again, across the entire creative universe, every day. That’s how creative works are born and die too young, over and over, making it by far the most common ending. It happens to me, it happens to you, it happens to everyone. But it doesn’t have to.

Aim for the space between Stephen King and letting your projects die out. Do it by doing a little bit, over and over, and one day you’ll be done.

Tagged in Writing

By UX Launchpad on September 17, 2017.

Canonical link

Exported from Medium on November 5, 2017.

The New Table Stakes

Or: expectation inflation

Software companies often use the phrase “table stakes” to describe something that users expect. For example, you might have heard Microsoft executives say in 2010 “Windows needs an App Store, that’s just table stakes now.”

But if you dial the clock back a year or two, you’d see the same Microsoft leadership explaining why Apple’s App Store was too strictly controlled. This isn’t unique to Microsoft, of course. Apple wrote off the two-button mouse for years, or the importance of gaming, or VR, or a million other things. User expectations have a funny way of inflating over time, and the key is evolving your product offerings to address user needs.

Here are some features I consider to be table stakes in September 2017. These are not all things that are being actively requested by users. But they are things that make your product, platform, or service compare unfavorably if you don’t include them.

End-to-end encryption

Some apps have encryption, others don’t. If you’re making a messaging app in 2017 that hackers and governments can spy on, why would someone choose you over your competition?

Identity theft safeguards

We keep seeing data breaches where personal information like social security numbers, credit cards, addresses, phone numbers, and even passwords are all stored together for easy theft. This is bonkers.

There are better, more secure ways to handle data. Your customers won’t ask about this, but they will eagerly join a class action lawsuit against you if their information ends up being stolen.

Polite email notifications

Customers will put up with some emails from your company. And if you work hard, they might even look forward to the emails the way some people look forward to Medium’s Digests.

But most people’s email boxes are completely jammed full and they don’t want more junk. Some companies understand and respect this by using email sparingly and featuring a one-click way to unsubscribe. If a company can’t run their business in 2017 without spamming customers, they won’t be around long.

Respect of platform conventions

Material Design, designed by Google, should be followed on Android. Apple’s design language, designed by Apple, should be followed on iOS. There’s always some wiggle room, but copy/pasting iOS patterns to Android or vice-versa causes all kinds of issues down the line.

(I could literally write a book about this, and have written thousands of words about this is other essays, but I’m keeping brief here.)

A trustworthy app store

There used to be a time where you could install software that could ruin your computer. With the advent of the App Store, we were able to trust that the software you installed would usually run correctly, without malicious side effects. Sometimes bad software gets through, of course. But it’s not nearly as bad as it used to be. This is no longer an optional feature. Good.

No invasive tracking

You might just think I’m making a laundry list of things I wish existed, but let me reiterate: this technology exists today, it’s just that most companies don’t want to implement it for financial reasons. This is unacceptable.

As John Gruber recently mentioned, if you’re upset with Safari or DuckDuckGo for stopping this immoral practice, you’re basically a peeping Tom who’s upset at blinds manufacturers. People have a right to privacy. Even on the internet. Even in 2017. Especially on the internet in 2017.

Graceful offline mode handling

Your SMS, email, calendar, and phone app don’t fall apart with a series of confused error messages when the internet isn’t available. They just do the best job they can without an internet connection, then they reconnect when they can. Every app should handle the internet this gracefully, because it’s what users expect.

See also: spotty internet. Most people don’t experience all-or-none internet. They experience spotty connections that work for a bit, then lose connection, then come back again, then get slow, then drop out, then back again. “Offline Mode” isn’t binary. It’s an approach to design that means you handle unreliable internet gracefully. Most apps have a long way to go on this front.

Low impact data

If your app uses fifty meg of data per day, and your competitor finds a way to provide a similar experience for only one meg, they’ll have the adventage. People expect apps to be careful with their data.

Low impact storage

The same applies to how much space your app uses. If you’re using a gig of space on the phone and your competitor can provide a similar experience at ten meg, you’re the first to get deleted when a user needs more space.

And to be clear: almost everyone needs more space on their phone. It’s more intense in poorer countries, but it’s a nearly universal issue across the world.

Graceful “out of space” handling

People run out of space on their phone all the time. If your app can’t help people in those situations, it will lose to the ones who can. Google brilliantly highlighted this difference between Google Photos and Photos.app in their ad campaign.

Actual customer support

Customer support is better and worse than its ever been. There are some pioneers who have built great reputations around their superior customer support, and they’ve been rewarded for it. Good!

On the other hand, most tech companies try to dodge customer support costs entirely. A relationship with a user is a privilege, not a right. If you wish you didn’t have to deal with customer’s requests quickly and fairly, you might soon get your wish. They’ll move to your a competitor who treats them better.

Access to files from anywhere

People expect to be able to have files where they need them, when they want them. This is easy to say, but extremely difficult to actually pull off. Apps need to be able to communicate across different platforms and screen sizes, then store data in the cloud, then synchronize everything correctly without dreaded merge conflicts. It’s devilishly complex to get right, and the people who do are making their competitors look bad.

Simple backup/restore

People don’t like to back up their data, even if they know they should. But when disaster strikes, they want to be able to get the files they lost as simply as possible.

Years of security upgrades

Hackers are constantly finding ways to attack devices. Software updates (For example on iOS and Android) as well as carrier updates are the only way for devices to stay ahead of the curve and keep customers as safe as possible. No one actively asks about this feature in the store, but the lack of it will provide a demonstrably worse expereience in the long run.

Leadership people can respect

There are still plenty of people with questionable morals running companies. But the PR damage from being sexist, racist, homophobic, or just plain mean has never been greater. In 2017, people expect to give their money to people and businesses they can respect. Or at least not actively hate.

Accessibility

Some products and features work well if you have a disability or just prefer a more usable interface. Most don’t. This is no longer a low priority, constantly-delayed feature, because some companies have made it a priority and shown the world how it can be done.

A clear and consistent abuse policy

For many years, there were two beliefs that made abuse on the internet worse. One was the idea that simple is always better, meaning an interface shouldn’t be cluttered with user controls like block, mute, and report. (Not to mention the lack of funding for support staff, meaning reports went un-answered. I guess a lack of a reply is “simple” but it’s hard to call it good!)

The other was that you could just throw everyone’s words into a giant pile on the internet and the best, most true, most trustworthy, and most noble content would magically win and rise to the top. Free Speech! This led to a disasterous hands-off policy of hoping everything would get better even as it was proved it there was no limit to how awful people could be to each other on the internet.

People expect companies to notice that abuse and harrassment is a real problem. They expect terms of service that are clear and followed consistently. People expect that they have a right to say what they want, but also that they have a right to not be attacked just for existing.

People talk a lot about the future. Are we going to be commuting via self-driving tube shuttles? Are we going to use VR and AR to experience a new layer of reality on top of the one we already know? Are we going to use nanotechnology to wage war and cure diseases? Will we move to a system of wearables and leave our traditional phones at home? I’m not sure.

All that is really fuzzy to me. I don’t know, and I can’t get too excited about trying to predict the specifics of what’s going to happen. But I do know that trust, privacy, and respect are evergreen concepts that mattered one hundred years ago and will still matter in the next one hundred. People aren’t thinking about a specific technology, they’re thinking of completing tasks that matter to them. If they can trust that a tool will complete their task in the right way, respecting them and their privacy, and work in any context, that tool will win.

Conversely, if the tool can’t gain trust then it’ll have a tough time. You might not trust that the app will work right because it’s poorly designed. You might think the views of the CEO are abhorrent, and as a result not trust the company’s whole mission. You might not trust that the company isn’t going to invade (or leak) your personal data. None of these are easy things to fix.

The future, like the past, will come down to trust. To me, that’s the central goal every tech company needs to work towards. In a world with a lot of options and not a lot of trust, how can you act in a way that makes people want to support you? It’s not just up to engineering, or design, or marketing, or PR. It’s a question of morality and ethics. And that’s not something tech companies have been great at.

Which means there’s an opening. And that’s exciting.

Tagged in Privacy, Design, Trust

By UX Launchpad on September 20, 2017.

Canonical link

Exported from Medium on November 5, 2017.

Design Culture is Holding Back Design

Great design shows in results, not opinions

Taste is subjective. But oh, how designers pretend that it isn’t!

So you don’t like reality TV. Instead you prefer indie shows. That’s great. I agree with you. But if we’re going to start a business together, and the goal of the business is to make money, I’m going to point out that reality TV is a much bigger money maker than most indie shows.

So you don’t like sports. Instead you prefer complaining about sportsball on Twitter. That’s fine. But you have to admit that sports is popular around the world, even if you don’t personally like it. If your goal is to make a product people like, you could learn a lot from sports. Study up.

So you think the Apple Watch is a flop because you don’t personally like it. That’s reasonable. But by any metric you choose, the Apple Watch is a smash hit. No, it’s not as big as the iPhone. Nothing is. But Apple conjured a multi-billion dollar business out of thin air, taking the number one slot from Rolex. That’s a big deal. It’s worth understanding how you got it so wrong so you can perhaps replicate some of that success.

At UX Launchpad, we are fond of saying that design is just problem solving. It’s not actually as complicated or as fussy as designers like to pretend it is. It is problem solving. Anyone can do it. It takes hard work, not “taste.”

A big part of problem solving is understanding what people want so you can target your solution accurately. And part of that is looking beyond your own personal opinions, or the opinions of the Twitter hipster echo chamber. And that takes listening.

If your consumers want reality TV, sports, and Apple Watches, stop bellyaching about it. Stop saying they’re wrong. Stop flaunting your amazing taste. Design is about problem solving, not showing off how fancy you are.

Tagged in Apple Watch, Design

By UX Launchpad on September 27, 2017.

Canonical link

Exported from Medium on November 5, 2017.

Design the Best Rollout for Apple Watch

Role playing what was going through Apple’s mind

Let’s pretend it’s 2012 and you’re a product designer at Apple. Your team has been thinking about iPhone but also how it interacts with the rest of the world. Your team has been looking into this long-hyped concept called “The Internet of Things” where all devices will communicate with each other. But it’s not just talk, either. Your team has explored the space for a while.

2006: Nike+iPod
Nike+ required a user to put a sensor in their special Nike sneaker, which could then communicate with an iPod. The iPod would store that data and then upload it to a computer, which would in turn upload it to the cloud.

2010: Nike+iPhone
Two years after the App Store launched, Nike+ released an app on the App Store. This meant the data could sync via cellular.

Also, the iPad Nano was made into a square. Sort of like a watch.

2011: Nike+iPad Nano+watch faces
The day before Steve Jobs died, Apple announced that iPod Nano would connect with Nike+ without requiring an external dongle. And the watch-like form factor also announced 16 watch faces, including a Mickey Mouse one.

So now it’s 2012 and you’re team is going to go for it. You’re going to make a watch. The watch will have an all-day battery, cellular and wifi data, Apple Pay, Siri, and for some key scenarios you won’t even have to use your phone anymore. And of course it will be beautiful. It won’t just be the most beautiful wearable made by a tech company. Its beauty will rival traditional jewelry and timepieces. It’s going to be great.

And then you talk to engineering and realize that none of this is technically viable. Sure, cellular data would be possible … but the battery would probably only last 30 minutes. And it’d be possible to power an iOS-like interface … but the casing would have to be pretty clunky to store all the equipment needed. The smaller the device, the more underpowered it would have to be. That’s just reality. Oh, and then there’s the issue of cost.

So. What do you do?

Option #1: Hold the bar; don’t ship yet
If the ultimate vision is cellular data, all day battery, a snappy and responsive UX, in a slim package, at a reasonable price point, you could decide not to ship. Apple is a pro at this approach.

Option #2: Ship every feature, but let performance suffer
You could demand that cellular be included and that the price be reasonable. That would result in a larger device, and even then your battery life probably wouldn’t last all day. Full featured but frustrating.

Option #3: Increase the price significantly
If Apple decided the Watch’s base price was going to sell for $1000, it’s possible they could have found other tradeoffs. Maybe they could have made the device small without sacrificing performance, for example.

Option #4: Optimize for price and performance, ship without cellular
This option says “let’s make the Watch we want for the long term. The right price, the right UX, the right battery life, and the right physical size.” And if that means it takes years to add cellular, so be it. This is what Apple chose.

Let’s be honest. Apple Watch isn’t cheap. And the first version’s hardware was sluggish while the software was confusing. So I’m not arguing Apple necessarily succeeded at hitting their targets for price point and level of performance, only that they were optimizing for those factors. Right or wrong, they believed in this form factor, at this price, with these core features.

They decided what features mattered most to them, shipped them the best they could, and got to work. Series 3 isn’t even in the same ballpark as Series 1, from an experience standpoint. But the vision was always there. It just takes a while for everything to catch up.

What would you have done? Shipped something at much lower quality, much higher cost, or ceded the market for three years while you hoped for your R&D labs to keep pace with your competition? Or would you have done what Apple did, trying to find the balancing act between too early and too late, too many features and too few, too expensive and too cheap, but always investing in the next year, and the next, and the next, until you eventually catch up with your original vision?

Tagged in Apple Watch, Design, Apple

By UX Launchpad on September 27, 2017.

Canonical link

Exported from Medium on November 5, 2017.

My Big Pink Watch and Dorky Headphones

Early adventures in a post-phone world

A few days ago I decided to put my phone in my bag and see if my Apple Watch Series 3, the one with cellular, could get the job done by itself.

I popped in some AirPods and they chimed in anticipation. I held my Watch up to my face. It was 7:10am, I had a meeting at 8:00am, and I had 100% battery life. I tapped the Music complication I had placed for quick access and saw some music that had automatically synced overnight. The second option was Kendrick Lamar’s DAMN. Sure, let’s do it, K-Dot.

Oh, wait. A few steps later I realized I usually skip the first song. I brought the Watch up and the software was still on the Music app. This is notable because the first edition always went back to the standard watch face. It was nice not having to hunt and peck for the app. I tapped the forward arrow and the second song played.

I know where my bus stop is, but I was curious how directions work. I tapped my AirPods twice and said “Hey Siri, directions to work.” My Watch buzzed with a response. Ah, I guess directions aren’t done purely over audio. I looked at the face and the directions were loaded up, but for a car, not transit. No time to figure it out, the bus came and I chased it down.

Once seated, I looked at the directions again. Odd. No options for transit. The UI isn’t very big, after all. You can’t litter the interface with every possible option, and clearly transit directions aren’t as important as car ones. But does that mean I can’t use my Watch for transit directions? That can’t be right.

Oh. Maybe 3D Touch? There it is. Good to know. Onward.

Phone use on a commute can be divided into two categories, online and offline. Reading a book is offline. Checking Twitter is online. This is when I realized the first major hole in my phoneless existence: books. I don’t mind adding friction that makes it harder to access the news. You can’t read much news on Apple Watch, and that’s great. But books? I like books. So I took out my iPad and read until I got to work. Somewhere along the way I got a text and drew a quick response on my wrist.

At work, I worked. That means I had access to a full keyboard, the Mac version of iMessage, and a full email client with lots of bells and whistles. And the damn internet. Fortunately, I’ve written a system that blocks all distracting websites while I’m at work while still bundling the most important news into a boring list. This means I know what’s going on in the world, but can’t get sucked into endless discussion and debate. Then work was over and I got ready to head home.

The commute was the same. I read a book while listening to Music. Sometimes I had a text to respond to, and I did. A few times I needed to do other things like look up a friend on Find My Friends or look up an old photo, and I could. The two big exceptions were Camera and podcasts via Overcast. The sun was reflecting off a building in a beautiful way, but I couldn’t take a picture. And later I wanted to listen to a podcast, but Apple Watch doesn’t support them yet. So I settled for Music.

Later that night, I had to move some boxes around. I never realized what a pain wired headphones are, but now that they’re gone, I feel it. I spent hours exerting myself and the headphones popped out exactly zero times. With traditional headphones, I’d give up on listening to music after accidentally hooking them on furniture for the second or third time. It makes a huge difference for me, something I confirmed when I went on a run.

So that’s how it’s been so far. It reminds me of the iPod review I wrote after buying one the first week it was on sale:

The most noticeable thing about owning such a gadget is how boring it is. I opened the case, peeled off the sticker on the faceplate that said “don’t steal music” in 5 languages, and plugged it into my computer.
With no fanfare, it started to swallow my entire MP3 collection, and about 8 minutes later, it was done downloading. “5 gigs in 10 minutes?” I thought, as I pulled up a song. The first thing I noticed was the superior sound quality — after listening to music through my computer for several years, I had forgotten about the intricacies of a good tune.
I wanted to play with the iPod’s features some more, so I downloaded a batch of Beatles MP3s, selected “update iPod” and after 2 seconds I had another dozen songs to play with. I decided to take a walk.
I tried switching songs, increasing/decreasing the volume, and accessing different playlists I had created. They all worked. I returned home. I stared at the iPod. I downloaded an entire Jerry Seinfeld standup routine to see if the 80 meg MP3 would make my iPod choke. After listening for 10 minutes, I gave up, knowing it would perform just fine. The lack of issues I encountered was eerie.
I’ve heard many say that music afficionados will love the iPod. I’m the opposite; I love the iPod, and it’s making me appreciate music again. The simple interface, crisp display, zero skipping, huge hard drive, and 10 hour battery take care of all the problems a person may come across with a portable player. Kudos to the iPod for doing everything possible to blend into your life transparently.
After the 30 second learning curve, all that’s left to do now is buy some new music. Anyone have music recommendations?
That’s how the Apple Watch Series 3 is working for me. It’s just doing what I want it to do. Leaving the iPhone at home isn’t a radical idea that some people might get to in five years. If you have access to a computer during the day, and perhaps a paperback book or two, going phoneless is finally feasible. Oh, and battery life? I can’t remember getting below 50%. I’ve been charging every two days. It’s been a non-issue for me.

Maybe it’s not for you yet. It probably isn’t. But it took three years for the iPod to blow up. I suspect this shift will go even faster.

By UX Launchpad on October 5, 2017.

Canonical link

Exported from Medium on November 5, 2017.

A Call For Feedback

I have a lot of content for a book — what should I do?

I love to write essays. That means I do it a lot. Which means I have heaps of content. I’m always looking for a while to turn them into a book of some sort.

So, if I were to write a book, what sort of content would you want to see from me? I’d love to know what you think. Thanks!

By UX Launchpad on October 6, 2017.

Canonical link

Exported from Medium on November 5, 2017.

The Direct Line from Metro to watchOS

“Be Here Now”

There was one year between the launch of iPhone and the launch of the App Store. During that time, I was working at a company called Jott that had a phone number that transcribed audio and sent you the text. This meant you could dial a number, say “Remember this license plate: SNM 183” and a real human would transcribe it and send it to your email, or the Jott app (a todo list) or a third party todo list, or one of a hundred other options. It was pretty neat. And it was obvious that it would make a great app on iPhone.

We launched our app on day one. We scored the number one spot for several days. There were lots of reviews written. Apple even put together a newspaper ad of the best business apps, and there we were! We had been told by one of Steve Jobs’ friends that Steve was obsessed with audio UI assistants and had been since the 80s. In time, Jott was bought by Nuance. Nuance powered Siri. It all made sense.

I designed that app from scratch. And I remember one of my very first insights being “no one wants to stay in this app very long.” The scenario wasn’t “lean forward” or “lean back.” It wasn’t a “30 foot UI” or “10 foot UI.” It was, for lack of a better word, a “mobile” UI. But not the way we mean it today. Mobile in the true meaning of the word.

People would be using this while traveling from one place to another. For example, walking to the bus stop. They wanted to get something done as quickly as possible, then move on. It was an interesting challenge and a really important insight that guided the direction of our design and development.

In time, terms like “natural user interface” or “ambient UI” got popular. My friend Jake uses the term “slippy UX” as a reminder that the goal isn’t always to make your content “sticky.” All interfaces need to help a user solve a problem as quickly as possible, but in some cases (cars being a great example) that’s not good enough. They need to actively make sure they’re encouraging you to stop using them quickly. Counter-intuitive, but welcome.

A few years later, working my dream job at frog design, Microsoft announced Windows Phone with an iconic start screen. It floored me. I immediately applied for a Windows Phone job, got accepted, and quit my frog job.


Glanceable.
Windows Phone never quite explained its value proposition to the world. Its first commercials made fun of people for liking their phones too much. This is like seeing that the whole world is obsessed with pop music and coming out with a jazz fusion album. Sales weren’t great. You might have noticed.

But I was a true believer. I deeply appreciate any UI that helps me get back to my life. I loved being able to customize my Start screen with the personal information I cared about the most. Because when I tuned it correctly — hurray! — I wouldn’t need to dive into apps so often. And that was an amazing trick. It wasn’t a “lean forward” or a “lean backward” UI. It was a slippy UX. By design. And it was wonderful.

Microsoft is great at design research. Early on I saw a statistic that said the average app is launched something like 1.2 times. I began watching my own usage and I realized that there were a small handful of apps I relied on, then a whole bunch of garbage I kept around just in case. (And then there are things that are just plain bad) At precisely the moment the App Economy was shattering all expectations and becoming the tech story of the century, I was wondering if I really needed my phone as much as I thought. If anyone did.

Years passed, and Apple announced Apple Watch. Tiny screen, thick hardware, bad battery. No internet browser. No camera. Designers were encouraged to make screens that would be looked at for only a “glance” at a time. They specifically told us to design experiences that could work in just 2 or 3 seconds . Many designers and engineers revolted. “I can’t work like this!” they said. And as a designer, I understand the frustration. It’s a challenge to boil something like Twitter down to a three second experience with a single call to action.

But what about as a user? Well, as a user I don’t want to spend so much time on the endless toxicity of Twitter anyway. How about that?

What if the lack of a browser was actually the smartest thing Apple Watch did, not a boneheaded oversight like the Hacker News crowd believed? What if its limited nature is actually the best feature? When I used Apple Watch I saw the same quiet philosophy that drew me to Metro.

Sometimes we’re “leaning back” and want a Netflix UI that helps us binge watch. Sometimes we’re “leaning forward” and typing a giant email or drawing in Photoshop. But most of the time, we’re mobile. As in: moving. Distracted. Just trying to get a task done and get back to our lives. We’re trying to Be Here Now, as Microsoft used to say, sounding like a yoga class.

I thought up this article while walking to the store to get laundry detergent while my son babysat my daughters. I hopped down our front stairs while putting in my AirPods. (They’re always, always, always with me now since they’re so small, smooth, and useful.) I glanced at my Watch and loaded some music. Then I just … walked.

I didn’t fiddle with news or email on my walk, which for me is notable. Then I got to the store, picked up my detergent, paid with my iPhone, then started walking home. I wanted to check in with my son, so I tapped the AirPods twice.

“Siri, text Elliott.”

“What do you want to say?”

“Be home soon.”

“Your message to Elliott says ‘Be home soon.’ Send it?”

“Yes.”

“I’ll send your message.” \<sending sound effect\>

And then I was done with my task, so the music continued playing. And that was it. It’s odd that “software doing its job quickly and then leaving me alone to notice the sunset” could be worthy of a whole essay, but after a decade of screen obsession and mainstream app burnout, well, here we are.

Tagged in Microsoft, Metro, Apple Watch

By UX Launchpad on October 7, 2017.

Canonical link

Exported from Medium on November 5, 2017.

You Don’t Know What You’re Talking About

And that can be pretty awesome

When I kicked off Design Explosions a few years ago, I set out three principles for design critique before I wrote my first word. They are:

The team that built this product is full of smart people.
There are many things we can’t know without joining the team.
You can’t judge and learn at the same time.
Every time I revisit these principles, I believe in them a little more. I believe design culture is in an intensely cynical and self-defeating place right now. We’re awarding our brownie points to the people who can shout the loudest, the most often, about how awful everything is and how it needs to change RIGHT NOW. You can simultaneously think it needs to be changed and think the most effective way forward is collaboration instead of disdain and attacks.

Anyone who tells you differently is trying to get famous as a thought leader through provocation and conflict. How’s that for cynical?

I know a bunch of people who aren’t just great designers, they’re the ones actually making a lot of these high level decisions that can bring true change. I know heads of business, politicians, venture capitalists, professors, and the designers of some of your favorite and least favorite software. I know people in charge of abuse at Twitter, people redesigning Facebook’s navigation, people rebuilding Afghanistan, ex-employees at NSA, doctors and nurses seeing the US “despair epidemic” up close. And besides the people I know personally, I am constantly interviewing experts in their fields: airline pilots, lawyers, school teachers, anyone I can get my hands on to understand a little more how the world works.

I’ve learned an important lesson: that first opinion you have about something is almost certainly wrong. When you talk to (which is to say listen to) someone who is an expert in their field, it is always, always, always more nuanced than it seems to an outsider. Think about your field, or your job, or your hometown, or your country. Now think about an article you’ve seen written about it. If you’re like most people, the article probably disappointed you. Sure, it had some commonly understood facts you can find on Wikpedia. And the article probably wasn’t lying outright. But it’s likely it was written without the depth of understanding that you personally have about it. Chances are, it missed the real story.

I love reminding myself of this. I even wrote a series a few years ago called I Have No Idea What I’m Talking About where I laid out my limited understanding of a bunch of deep topics. (Checking my notes, I wrote about Race, Meditation, Personal Growth, Punk Music, #JeSuisCharlie, Selling Books, Depression, Health, Breaking Into the Tech Industry, Writing a Musical, Running for Office, Feminism, Creativity, and Privacy) The key is that I was explaning what I don’t know. Counter-intuitively, this approach taught me a lot more than pretending to have all the answers.

So I find myself returning to these principles a lot, doubly so when I’m upset about something. The product that’s annoying me? It’s easy to assume they’re all awful, greedy, stupid people. But what if they’re smart? (#1: The team that built this product is full of smart people.) What if they care about their jobs as much as I do? What if they’re working as hard as I am? Wow. That requires a whole new way of thinking about it. A better one. And it leads to richer, more full answers. With the right frame, the world becomes slightly more clear.

Instead of assuming I know every motivation, meaning most decisions are made for no other reason than pleasing Wall Street, I like reminding myself that I’m just guessing from the outside. (#2: There are many things we can’t know without joining the team.) There are always, always, always more details than appeasing Wall Street. There just are. If you think otherwise, you have all the rage of a college freshman without the range of understanding you’d hope to see of a grad student. Take it from someone on the inside and fighting like hell, there’s more to it than “it’s all doomed” or “they must be corrupt.”

And that brings us to judging versus learning. I think the design critique world is under a collective delusion that what the world needs is less common ground and more demonization. We need to call bullshit more! We need to listen less! That’s what we seem to believe right now, but I propose we have it backwards. (#3: You can’t judge and learn at the same time.)

A closed question, such as “How could you think these colors are acceptable” will only make people defensive. You won’t learn a thing. But an open question, such as “Can you tell me more about your process for picking these colors” sets judgement aside just long enough to learn something.

And isn’t that why we got into this field in the first place? To stay curious, to listen, and to try to help solve problems for other people? (I’m starting to think some people got into the field for vanity reasons, and it’s getting increasingly obvious.) Fortunately, real change is happening all around you. The kind you want to see, even if it’s not fast enough for your liking. But the biggest strides forward aren’t happening from sarcastic zings inside your echo chamber. They’re being driven by people couragous enough to know they don’t always know what they’re talking about, but have a desire to listen.

Tagged in Design

By UX Launchpad on October 17, 2017.

Canonical link

Exported from Medium on November 5, 2017.

Seeking vs Flow

We’ve all heard about flow. It’s that fascinating state where time sort of melts away because you’re focusing on a task. You might discover it playing music, or writing comics late at night, or knitting. More here.

And then there’s what I call seeking. It’s a state where you’re foraging for new content, new information, new ideas. You’re seeking right now. And you’re in the same mode when you scroll through a timeline, or put together a mood board, or people-watch at the park.

You can’t stay in flow all the time. And there’s nothing inherently wrong with seeking. It’s just a question of how much time you’re spending on each. Mobile devices and the internet have altered our days and our expectations. Now you can (and probably do) wake up, seek information, seek information, seek information, eat lunch, seek information, seek information, seek information, eat dinner, seek information, seek information, seek information, on and on until you finally pass out. You can go entire days without managing to get into a flow. Which doesn’t feel great.

I’ve been thinking about these modes a lot. I’m always either seeking or I’m flowing. Seeking is metaphorically sitting on the couch, watching TV, eating junk food. It’s the default state, it’s easy, and it’s ok in the right amounts. On the other hand, Flow is going to the gym. It’s hard, but it’s good for you. It makes you feel better. And even an hour here and there is worth a lot.

But the key — and this is really important — you can only do one at a time. You’re reading this article right now, meaning you’re not making anything. And while you’re making things, you don’t have time to absorb other people’s thoughts. Seeking and flow are mutually exclusive. So go forth and seek, but don’t forget to flow. That’s what works for me.

Tagged in Life

By UX Launchpad on October 18, 2017.

Canonical link

Exported from Medium on November 5, 2017.

An Open Letter to Those Moving to Seattle

Please don’t take our complaining personally.

I moved here 19 years ago, so I’m somewhere between new and a native. I’ve learned some things the hard way that may help you fit in. The main takeaway? We have a chip on our shoulder, and we love complaining. Don’t take it personally, even if it’s directed at you.

Traffic

When I arrived in 1998, I was amazed at how manageable the traffic was. I immediately regretted expressing that opinion out loud, because Seattle residents are convinced they have the second worst traffic in America. And they will tell you about it. Passionately. Endlessly. Trust me on this one.

Yes, Seattle has traffic. Yes, it can get really annoying. Yes, it’s gotten worse. But it’s gotten worse everywhere. And if you’re from a big city like Atlanta, LA, DC, Boston, or San Francisco you’ll quickly see that it’s not the second worst in the nation. Some basic research backs up this assertion.

But in the interest of fitting in, just smile and nod. Hell hath no fury like a Seattle resident being challenged on how hard it is to be them.

Transplants vs Locals

Even back in 1998, people hated it when California residents moved in. The anti-transplant sentiment has only gotten worse. But keep it in perspective: Seattle has always complained about people moving here. Seattle will always complain about people moving here. It’s just what we do. It’s the Seattle Way.

I’ve never been in a city this size with such a small town mentality. It leads to a somewhat warped sense of how Seattle stacks up to other cities. And even what it means to live in a city in the first place. In most major cities, people are aware it’s going to be really expensive. In Seattle, this realization seems to shock us anew every day. We have a skyline that looks like a real city, but cost expectations and outsider relations more in line with a one horse town.

Weather

Seattle people will tell you that true residents don’t use an umbrella. Use whatever you want. No one cares, and the people who do are just parroting what generations of grumpy Seattle folks have said before them. They’re best ignored.

That said, most of our gloom is cloud cover and drizzle rather than heavy raindrops and big storms like Florida. In fact, Seattle has less total rainfall than many cities, but more cloud cover than all of them. So I don’t find I need to use an umbrella much but no worries if you do!

Juneuary

Seattle summers have a local reputation for starting late and ending early. A lot of people joke that it’s cold until July 4th (which is where the word “Juneuary” coms from), when it rains, and then summer is over three weeks later.

I wasn’t so sure if the summers were really as fleeting and frustrating as I kept hearing. So I kept actual records to test the soggy conventional wisdom. And yes, it has rained on July 4th more than once. Yes, we have had cold Junes.

But it’s not the norm! The most reliable thing every summer is having an absolutely spectacular run of days of perfect sunshine and late sunsets — the kind of weather that I consider the best summer in the entire world — followed by a single day of rain, resulting in Eeyore comments like “aw man, are we ever going to get a summer?”

Complaining about a little summer rain as if the sun never happened. Every time. In Seattle, you can set your watch to it.

Same as it ever was

You’re going to hear that the Californians raised prices too much. You’re going to hear that techies are ruining the unique culture Seattle is known for. You’re going to hear that Seattle is over by now, and it was best ten years ago when things hadn’t gotten so nuts. Fair enough. These are not unreasonable criticisms. Seattle has changed. Things have in fact gotten more expensive.

But I heard the exact same complaints in 1998. And in 19 years, you’ll be leveling those complaints at the latest batch of techie Californians as they ruin everything. Same as it ever was.

Seattle is one of the best places on earth. In 1998, through today, and with your help, into the future as well. Enjoy it. Do your part. And ignore all the complainers. They’re always welcome to move. After all, we could use the space. Especially after these damn Californians ruined everything, am I right?

Tagged in Seattle

By UX Launchpad on October 21, 2017.

Canonical link

Exported from Medium on November 5, 2017.

“Shipping the Org Chart” and Omelettes

Some of the worst offenders are designers on their personal sites.

From Walt Mosberg’s review of the Samsung Galaxy S7:

Out of the box, there are two email apps, two music services, two photo-viewing apps, two messaging apps, and, except on Verizon, two browsers and dueling wireless payment services. (Samsung says Verizon barred including Samsung’s browser and Samsung Pay out of the box.) And Verizon builds in a third messaging app.
The setup process also guided me to using Verizon’s messaging app rather than Samsung’s and a Verizon backup service. It even warned me I might lose important stuff if I didn’t sign up for the Verizon service.
That’s called shipping the org chart. Or in this case, the org chart of three separate companies. But it also happens within single companies. Each division pushes for their own goals, meaning the user experience is left to suffer. Imagine making an omelette where you had to deal with the egg team, the mushroom team, the butter team, and the tomato team, where each had made a team goal of increasing their usage by 20% quarter over quarter.

You’d make a bad omelette. And that’s called shipping the org chart. What you really need to do is use the correct amount of each team’s output to make the best overall experience, even if it means some teams don’t get to shine quite as bright. That’s good management and should result in good product design.

Many years ago, most product designers made their personal websites by hand. Most had a blog. But now most designers have a personal site that simply points users to a bunch of different sites. If the standard designer website in 2017 could talk, it would say:

“Hi. I like pixels and coffee. Here are six different links to learn more: LinkedIn, Behance, Dribbble, Twitter, Instagram, PDF of my resume, and my email. Good luck with your research.”

Then you click through each of the links to learn more. LinkedIn gives you a sense of what they’ve done in their career. Behance shows their creative work in a portfolio format. Dribbble shows their creative work in a thumbnail format. Twitter shows the last time they were annoyed at an airline delay. Instagram shows the sunsets and brunches they enjoyed last weekend. And that’s all ok. But it’s not great product design.

To use our omelette metaphor, designers are basically putting a bunch of ingredients on the counter and walking away. It’s not a good omelette or even a bad omelette. It’s just an indication that you didn’t feel like cooking. And if you’re trying to get a job as a chef, that’s probably not the message you’re looking to send.

Tagged in Design, Advice

By UX Launchpad on October 25, 2017.

Canonical link

Exported from Medium on November 5, 2017.

Controlling a Round Spaceship

Here’s a strange metaphor for you

Imagine that you have a perfectly round spacecraft. Now imagine that you have an engine attached to one side of it, like this.


In this picture, the orange bit is the actual engine, not the yellow. But that’s fine, the image still applies.
Any project you do by yourself fits this model. You are a round spaceship with a destination in mind, and you have a single engine powering you. Maybe you wish the engine was faster or more consistently operating, but when you are working, you’re moving in a single direction. Simple.

Now imagine that your spaceship has three engines placed at various places around the edge. Or thirty. Or three hundred. It sounds good to have lots more horsepower. But only if the engines are working together. Otherwise it’s chaos. Or worse: stasis.

When I was in my 20s, I lead the design of an app that was to launch on Apple’s then-new App Store on day one. It went great! Apple loved it, people loved it, and the company ended up getting bought out. It all worked out in the end. But an interesting thing happened to me while designing it.

I remember getting an email from the CEO that responded to some of my design vision work by saying something like “I am the CEO, you know.” Gulp. If your CEO ever has to play the “I’m the CEO” card with you, be careful. What I realized is he felt that design was overstepping its bounds. And at first, I was confused. Shouldn’t design lead design? Well, yes and no.

Design is nothing more than “deciding how a thing should be.” And that boils down to “control.” As you’ve no doubt experiened, CEOs, founders, and product managers have a very strong bond with their product. So my CEO wanted to control his own ship. Which is a totally understandable request. It was his name on the company and his money funding payroll, after all.

Think about this the next time you’re debating design on your team. You’re not actually debating user experience, pixels, or “design.” You’re debating who gets to steer. You’re debating who gets to be in control. And not everyone should get a powerful engine on the ship. If they do, the team will eject a lot of hot air but will have trouble actually getting anywhere.

Tagged in Design, Apple, Appstore

By UX Launchpad on October 27, 2017.

Canonical link

Exported from Medium on November 5, 2017.

“Steve Jobs” as Synonym for “Perfection”

The laziest myth in tech

Wired wrote a review about iPhone X that contains the laziest myth in tech: the idea that if only Steve Jobs were here, he’d put a stop to all imperfection. This time it’s referring to the notch at the top of the new phones:

It’s an aesthetic setback (what would Steve Jobs have said?)[…](#)
Perfect doesn’t exist in design. Big, beautiful screen? Takes more battery than a small one. The highest rated car engine of all time? Probably expensive. Really tiny file sizes for movies? Then it won’t be as crisp as one without compression. A sweater that keeps you warm outside? It’s probably too hot when you’re riding on a bus with heating. Everything is a tradeoff. Everything. Perfect does not exist. The sooner you realize that as a designer, the sooner you can do the actual work of design: balancing tradeoffs.

There’s a phrase in role playing games called “min-maxing.” Say you want your wizard to be as smart as possible. (Max intelligence) You might borrow from other skills to get there. (Minimized everything else) So you end up with a god-like level of intelligence, but far below average for everything else. That’s min-maxing.

Steve Jobs was a design min-maxer. He chose the details he wanted to boost, and was ok letting other details suffer as a result. His team’s products stood out because his competition was trying to be well-rounded. But “standing out” is different from being “perfect.” In fact, the approach often fails. The story of Steve Jobs is how he learned to hone his approach and eventually find success. But it took decades!

Lisa

The team wanted to have a graphical user interface, accurately believing it to be the future of computing. It cost about $25,000 in 2016 dollars. It failed.

Macintosh

The team wanted a cheaper (and more beautiful) version of the Lisa. They got the price down to around $5700 in 2016 dollars. It did better, and inspired the industry, but still failed because of reliability issues and that insanely high price tag.

NeXT

The team wanted to make a high end workstation with cutting edge technology and features. They spent tons of money on the hardware design, the branding, and some truly amazing software. It failed.

Bondi Blue iMac

The team wanted to remind the world that Apple still existed and still had the ability to inspire. (being profitable would be a nice bonus too) Their device was bad at everything (limited software selection, sluggish specs, a mouse people didn’t like, no power or restart button, and no disk drive) but its design was instantly iconic. This is one of the first times Steve Jobs actually managed to make profits off his min-maxing. The blue case saved Apple.

G4 Cube

Riding Apple’s resurgence, the team decided to take a standard tower configuration, make it much smaller, prettier, harder to upgrade, and more prone to overheating, and sell it for almost the same price as a real tower. Inspiring to look at, but it failed.

iPod

The team wanted to make the best music player in the world. And they eventually got there. But history has forgotten those first three years. The device was far too expensive, the screens scratched too easily, they only worked on Macs, and a million other issues. It was a luxury novelty at first.

iPod Shuffle

The team wanted the shuffle to be cheap. So they were ok with any tradeoff they needed to get there. So they went with … removing the screen?! They even made a maddening one where you had to control it by voice.

Apple TV

The team wanted a drop-dead simple interface, so that’s what they did. The tradeoff was that it couldn’t do much, and it was more expensive than its competitors.

iPhone

After ten years, we understand iPhone was a massive success. But there were no apps for the first year. It was stupidly expensive. It was on a slow wireless standard and had slow software. It could barely make successful phone calls. It was only on one carrier. The team went big on “great” and hoped the gamble would eventually pay off. But its success as a consumer product wasn’t obvious at first.

Removable Media, Ports, Standards

When the Macbook Air launched, Apple removed the DVD player to save some space and weight. Classic Steve Jobs min-maxing that people hated at the time, then grew to appreciate as years went by. And it wasn’t just Macbook Air, of course. Every time Apple launches a new device, they’re not shy about removing expected ports like wired headphones, USB, MagSafe, Firewire, serial, SCSI, or anything else.

You don’t think Steve Jobs would have been ok with clumsy dongles and workarounds in the latest Apple products? Not only was he ok with them, you’d be hard pressed to find a person with even a tenth of the appetite for them that Steve Jobs did. Everyone else likes the status quo. Jobs wanted to find the next big thing, expectations be damned.

Which brings us to today.

The Notch

What would Steve Jobs say about The Notch? Who knows. Maybe he’d be offended by it, and require the black bar to extend all the way across. But that would mean a smaller screen, which is a tradeoff he’d be unlikely to support. It’s just as likely he’d love the size of the screen and the huge leap forward in technology, and do his best on stage to encourage developers to embrace the notch. Which is exactly what Apple did.

What Jobs Thought of The Past

Here’s a story I love. The Macintosh team was in crunch mode, trying to ship the 1984 Macintosh, and someone asked Jobs about typewriter-specific documentation. Had he considered making a migration guide for people used to typewriters who wanted to buy a Macintosh? He thought about it.

“People will figure it out. And the others will die off.” he reportedly said.

That’s a big part of his design philosophy, and by extension, Apple’s. They’re going to make the best thing they can, based on the things they deem most important. If enough people love it, that proves the direction, and they’ll continue. If no one likes it, they’ll iterate on it. Simple.

Steve Jobs was always a gold mine for this kind of straight-forward, can-do thinking. This is one of my favorite quotes by him, and it sums it all up:

“Some mistakes will be made along the way. That’s good. Because at least some decisions are being made. We’ll find the mistakes … and we’ll fix them!”
He didn’t believe in perfect. He didn’t believe in over-thinking. He believed in working hard, doing your best, seeing what people thought, and trying again.

What would he say about the notch? He’d see that early sales are “off the charts” and believe Apple’s going in the right direction. It’s that simple.

Tagged in Apple, Stevejobs

By UX Launchpad on October 30, 2017.

Canonical link

Exported from Medium on November 5, 2017.

You’re Not Alone

The tech industry is about control, not merit

This article was inspired by Harvey Weinstein’s disgusting actions, but this isn’t an article about sexual harassment or assault. It’s not about Hollywood. But it doesn’t have to be. Because all of these stories, no matter who is involved, in any industry, all center around power and control. It’s about how the people at the top of the org chart can intimidate almost anyone they want, and how in the current system, they’ll keep getting away with it.

People in control want to stay in control. They hate being undermined. They hate it when people compare notes and call them out. This article is about how systems of production work in a capitalist system, and how moral fairness gets in the way of productivity. It’s about how capitalism optimizes for some things, but how you can’t expect a system built around profits to look out for your personal well being. This is about how tech industry, and American-style capitalism at large, isn’t a meritocracy. It’s a power structure, one you’re positioned near the bottom of, by design, and how that feels.

It’s trying to shed some light on how you might be feeling. You’re not alone.

2000: My first review

My first day in tech was the first Monday after Y2K: January 3, 2000. I worked really hard, took on a lot of projects, worked late hours, racked up a lot of kudos for my work ethic, and now I was facing my first review. I got average marks, which seemed low to me. So I asked about them, like a person who knew he was doing a better job than his rating. What an entitled brat, right?

The VP said “You didn’t launch that internal website.” I stared at him. He was the one that blocked the launch! I told him so. He smiled and said “A person deserving of higher marks would have made it happen anyway.”

But … but … he was the one that told me to hold off! It’s not that I hadn’t worked hard, or missed deadlines, or failed to align stakeholders. The person standing in front of me decided not to let it launch, without any reasoning, and now I was getting dinged for it. Odd. Ok, lesson learned.

2002: Transfers blocked

I worked hard, took on new roles, got a promotion, and had transferred into a mentor/leadership role on our team. Now other teams in the company were recruiting me. Exciting! I told my VP about their outreach. He said no.

His rationale was that he needed me on his team. A compliment, but one that kept me bound to him and his goals. He had power, I didn’t, so there wasn’t much I could do. I waited until he let me go out of the kindness of his heart. Which meant it took a while.

2003: “Seriously?”

When I finally scored a team on the fabled fourth floor of the company, where all the hot shot teams sat, the job offer was handed to me in person on Friday evening at 5pm by an HR rep.

I looked at the offer, set it down, and said, “Thanks! I’m excited to look over this. Can I reply on Monday?” She said “Seriously?” And I felt like maybe I did something wrong. “Yeah, I was just thinking I’d think it over during the weekend,” I said, not feeling quite as sure. The HR rep was flabbergasted, or at least she acted really well. As time went on, I realized it was probably an act. A negotiation tactic.

And, actually, it worked. They made me feel like I was biting the hand that fed me, so I accepted my small raise from $34,000 to $35,000 without countering. It turns out the role was worth way, way more than that. I just didn’t know. Not yet. And they had no incentive to tell me.

2004: The Doctored Review

Lots of companies in the Pacific Northwest used the 1–5 model that Microsoft used for many years. 1 is the highest, 5 means you’re probably getting fired. The vast majority of people get a 3 (I think around 80%?), and you’re seen as uppity and entitled if you ask for more. The argument is that everyone is great, so getting a 3 is a huge honor. Don’t make waves, kid.

I had a particularly good review period, so I put myself up for my first 2 ever. And my boss agreed. He handed me his glowing review, I read it, then got ready to sign it. At the last minute, he realized he had given me the wrong review! He quickly swapped it out with a new one. One that claimed I was actually a 3. And how my review was overstating my impact. I was exaggerating. I wasn’t flying in formation. I was a 3, tops.

Shocked, I asked what happened. He told me he had been politically pressured not to go with his conscience. The company was forcing a lower score. There was nothing he could do, even if he agreed with me. Whoa.

2004: The Apology Review

Realizing that meritocracies sure do have to put up with a lot of shadowy politics that have nothing to do with merit or work ethic, I lost my motivation. I stopped the long hours. I just did what was asked of me. I kept my head down. I didn’t stand out. I didn’t volunteer.

And the next review cycle I got a big bonus, promotion, and a “2” rating. That was even more disappointing. Working hard without credit was hard. Slacking and getting rewarded for it was painful.

2004: “We’d have to lay off some people. Do you want to be first?”

I wrote about this story here. When I asked why the company was misleading customers, I was told we had to or else we’d have to lay off half our staff. And then he asked if I was volunteering. Message received!

2005: Surprise! You’re Underpaid

After five years, I was preparing to leave the company for a new opportunity. My CTO asked how to keep me, I said I’d need more money and a promotion. He said if I could wait six months, he could make it happen. But his hands were tied until then. I was highly paid (“top of band”) and promotions could only happen twice a year. (otherwise it was “off-cycle.”)

My co-worker heard my story and said “He’s lying to you.” He pulled up a script that showed every single hire, departing employee, and promotion, day by day. (How? Each night he pulled down the 2000 person company address book and performed a comparison.) It turns out people got promoted all the time. “Off cycle” was a total fabrication by the CTO, according to this data.

Then my co-worker asked to know my annual salary. I told him and he burst out laughing. “Dude, you’re getting screwed.” He told me his salary. I was, in fact, getting screwed. We polled almost everyone on the team. I was getting paid the least, by far. They told me a story of being valued and highly paid. I believed them, which is why I hadn’t stood up for myself earlier.

But why would they be honest? Why would they say “You could ask for a 25% raise and still not be top of band?” That’s not how capitalism works. What would Wall Street investors say if the company was fair and honest with salary numbers? They might sell the stock! Unacceptable!

I told the CTO I knew he lied to me. He looked at me, cafeteria tray in hand, and paused a beat, with a blank face. Then he said “How did you find out?”

2006: I Accidentally See Payroll

I worked hard at my new company. After a year or so, we were bought out. That meant everyone in the company was going to get half their yearly salary as a lump sum bonus. Wow! One day I did an internal search for the word “bonus” to see how much I was going to get. It turns out the payroll could be accessed by anyone so I saw in black and white … I was paid less than anyone.

Being paid less than anyone isn’t the end of the world. But this was combined with the fact that I led the nascent design team, and was the subject matter expert for then-new technologies like AJAX and what we ended up calling “Web 2.0.” I wasn’t the best worker in my peer group, but I sure wasn’t the worst. I just didn’t know how the system worked.

2010: The Harvey Weinstein of the Design Studio

Most people I know have horror stories about people that make everyone in the office miserable. It’s not always sexual in nature, but it is always about selfishness, power and control. If you don’t play ball, your career will suffer. You will suffer. You might as well just go along with it. Don’t resist.

The Harvey Weinstein of my design studio, then and probably now, mistakenly believed everyone loved him. And why wouldn’t he if there’s no incentive for anyone to say anything different? Which brings it to me: why would I jeopardize my job, and by extension, my family’s security, and by extension, my entire future, to try and call attention to his behavior? Well, it turns out I did. By accident. Big mistake.

One day he poked his head into my team’s war-room and said “Jon, when can we talk?” And I said “Oh wow, I’m really under water on this project. How about tomorrow?” We made verbal plans. He left, and I followed up twice the next day to set up our meeting. He didn’t show or communicate with me in any way. A day or two later, he told me my behavior was unacceptable via email, including this line:

“I know you are busy, but it really seemed preposterous and a loss for you given my role in your career development.”
Yikes. Given his role in my career development. Message received. So I immediately looped in HR, feeling concerned about my future at the company. Big mistake. That only got him more angry. He told me I’m allowed to do whatever I want with HR, legally-speaking, but I had made an error in judgement with how I was responding to him.

Pause and think… I’m telling you now that this wasn’t a wise move, that I didn’t appreciate it, and that I’d like you to find time for a meeting [without HR involved.](#)
This only got me more scared. I was clearly pissing him off more, making the intimidation worse. What was he going to do, put me on a Performance Improvement Plan, the first step to setting up a paper trail for firing me? Yup. That’s exactly what he did.

This whole time, I held tight to the idea that we needed to go through proper channels. (And that HR was there to help me, forgetting they’re there to help the business, not my career or my feelings.) I wanted things documented and I hoped HR would help me navigate the situation. So we hopped on a three way conference call, where he built a case for me being a poor employee (despite a big promotion a few months prior) and encouraged the PIP to be enacted. It was. The HR rep gave me a “what am I going to do?” shrug of sympathy afterwards. And that was that. I was about to be let go.

31 Days Later: “Flying Colors”

Exactly a month passed, and HR told me I passed “with flying colors.” Not that it was perfect, of course. There was a mixture of feedback, things to work on, things where I’m exceeding expectations, etc. It was a standard review, despite the fact that the kick-off of it was politically motivated, as the HR rep told me in confidence.

But: “flying colors.” Other than the standard blend of pros and cons you see in every review, my biggest blunder was accidentally offending a powerful man, then hunkering down when I felt threatened. I heard story after story after story about this man’s intimidation. You won’t read them on Medium, of course. Why would anyone subject themselves to that? There’s no upside. You’ll only hear them whispered carefully to trusted friends.

And so

I have more stories (some really good ones too), but they start getting closer to my last few places of employment, and I have to be careful. I’ll stop there, for now. But I will say that everyone I know has stories like this. I’ve probably heard thousands. Including a sickeningly high number of stories about sexual abuse and harassment. If you have stories like this, if you’re just keeping your head down, if you’re often treated like dirt to keep the machine running, you’re not alone. You’re in the majority. And I hear you.

PS: I implied something in this article that I should have been more explicit about. These are the stories I have to tell, and I’m a privileged white man with lots of experience. If you’re a woman, or a person of color, or an immigrant, or LGBT, or younger, or made a career change, or are living paycheck to paycheck, or a million other details, these stories tend to be way worse. For example, I know my female friends don’t tell me every bad story, but the ones I’ve heard are definitely more appalling than these.

Again: You’re not alone. Talk to your friends, compare notes, fight on.

Tagged in Tech, Abuse

By UX Launchpad on November 2, 2017.

Canonical link

Exported from Medium on November 5, 2017.

Designing for a Clear Mind

This essay isn’t like the others

Zen, meditation, living in the moment, being present, learning to do nothing, solitude retreats, deep breaths, finding your center, listening to your inner child, following your bliss, you’ve probably seen those articles before. I’m here to tell you that things have gotten a lot more real over the last few years. And I have some media training suggestions I hope you take to heart.

1. Understand you’re being manipulated

Nearly everything you see, read, or hear is trying to get you to buy into something. And there’s nothing inherantly wrong with that. If a company makes a product, and you want to buy that product, there’s nothing sinister going on.

But you need to understand that you’re outgunned. They have a lot of money to spend on making you feel like you need to buy more things. Your gut feeling about a product is extremely easy to manipulate. The biggest mistake you can make is believing your instincts are operating cleanly, with no outside interference. You’re being manipulated every day. Plan for it.

2. Reconsider how much news you really need

Older generations taught us that good citizens keep up with the news. And I think that used to be true. But the volume and velocity of news has increased to a point where there are some serious downsides to trying to keep up.

And if it were a bunch of respectable news organizations duking it out, it wouldn’t be so bad. Because facts are stubborn things, and you’d find yourself reading pretty much the same story everywhere. So that’s not so bad.

But information has been weaponized. And I don’t use that word lightly. I mean, literally, people are attacking you with disinformation. It’s part of a broader plan, and its much more effective and widespread than people know.

You don’t actually need to read the news every day. You don’t need to follow news sources on Twitter or Facebook. You think you do, to be an informed citizen. But it’s backfiring. You’re getting more overwhelmed, your moral compass is getting randomized, and you feel like you’re drowning in news. That’s by design. Reconsider how much news you really need.

(Read this reddit post to get a glimpse into how it all works.)

3. Leave Twitter

I’m serious. The stated design goal of Twitter is for you to know what’s happening, but there are no safeguards against disinformation. Twitter is how you mainline disinformation, fear, anger, and divison straight into your mental process.

Another option is to turn off every single retweet on your timeline, and reduce the people you follow to a very carefully hand-picked list. But even then, can you really afford the emotional burden every time anyone on your timeline says anything emotionally difficult? It’s too much.

4. Facebook? Not sure.

I don’t even know how to handle Facebook. Keeping up with friends and family is important, and Facebook is good at that. You should do that.

But Facebook’s ability to inject news into your brain is even more damaging than Twitter, because of how its structured. So my main takeaway here is: ignore the news, hang tight to your friends. Good luck.

5. Audit your time

I’m going to say something controversial. You’re almost certainly going to disagree with me. Buckle your seatbelt.

You might have too much free time.

I know, I know. Everyone’s busy. Everyone’s balancing a million things. We’re stressed and harried and there’s never enough time and and and-

But hear me out. If you couldn’t check any social media at all throughout the day, meaning no Twitter, Facebook, Instagram, Snapchat, any of that, how would your life change? And what if you went a step further and you weren’t allowed to look at any news at all? If you’re like me or most people I know, a few things would happen.

First, you’d have more time. But second, that time would reshape itself in odd ways. For me, refusing social media and news meant a lot of little slivers of time throughout my day. 2 minutes here, 5 minutes there. So it wasn’t that I was getting an hour or two back in a lump sum. It was that my entire day just had more daylight in it. And that made me want to fill it up with better things.

Rather than working for a few minutes, darting over to news, reading something, working a little, checking email, darting over to social media, reading an article, getting distracted by work, on and on and on, a new pattern emerged. Instead, I’d sit down. And be really bored. So I’d start working. And get annoyed that I couldn’t do anything else. So eventually I’d just stick with it. Then, gasp, I got my work done faster. Huh. So then I’d read a book. So I’ve been reading and creating more, which has been nice.

To be clear: it took time and effort. It’s not like I just magically stopped reading the news and social media and everything worked out. It was a multi-month, or even multi-year process, and I’m still working at it. (I’ve been reading a lot of news lately, for example) But it all started by auditing my time. Really considering where it was going. Truly understanding what I wanted. And what I didn’t.

When it occurred to me that reading news and opinion online was the single biggest obstacle to a healthier mind, my choice was made clear.

Bonus: Funny and informative video


If you haven’t watched this yet, you should.

Tagged in Social Media

By UX Launchpad on November 3, 2017.

Canonical link

Exported from Medium on November 5, 2017.

Designing Like Trump

A lot of designers make this mistake

Trump has a pretty clear pattern, and unfortunately it should look pretty familiar to a lot of designers and people who work with designers.

A belief that things are currently in awful shape
Bold proclamations that they can make it better
A preference for blowing things up and starting from scratch
Undisguised boredom working on the iterative, long term work
“This is Crap”

I remember watching a creative director reminding a bunch of eager new design students, “Hey, while you’re pitching your great new ideas, be careful. You can call out user problems and pain points, but don’t like make it a griping session. The people who did those designs are sitting in front of you.”

You should always speak up if you think you have something to add. But you should also be aware that the person working on the design problem for five years longer than you might know a thing or two. You might want to listen.

“I can make this better.”

Probably! Because everything can be improved on. But if you think your ideas are novel and unique, you might be in for a rude awakening. Ideas are easy. Ideas are cheap. It’s actually working together as part of a team, not throwing hissy fits when you don’t get your way, and sticking with it for a while that nets the real results. But that takes teamwork. Teamwork is hard. And slow.

“We should just start over.”

Some percentage of the time, this might be true. But to listen to Trump and most designers, it’s easy to believe that blowing it up and starting over is always the best way forward. It just isn’t.

Yes, people are fed up with the status quo. But the idea that you’ll sweep into power, force a long and costly redesign, succeed on all metrics on your first try, and nothing will backfire in your slash and burn approach is laughable. Yes, starting from scratch is fun. But it’s not always the best approach.

“I’m borrrrrrrred.”

Every single designer you’ll ever meet likes thinking about new products and launching version one. It’s exciting! It’s fun!

Very few designers (or outsider politicians) actually have the stamina to stick with it, year over year, making relationships and pushing things forward. Designers will frequently ask to be reassigned because want to be on the exciting stuff. It’s understandable. But the designers who work equally hard on everything, even the less exciting work, are keepers. They don’t get bored because they’re not doing it for fashion or ego.

“Are you seriously comparing us to Trump right now?”

Or Bernie Sanders. But yes. Talking big by painting the world as a hellhole works. People perk up and think “Wow, someone is putting into words my feeling that things can be better. How edgy!” It’s as if we haven’t noticed that literally every single human being is talking like that these days. Those rants are a dime a dozen, and I believe it’s making everything worse.

Those big, divisive, hyper-aggressive pronouncements don’t translate well off the campaign trail, or beyond your “look I’ve got such great taste because I hate every design I see” angry Twitter persona. Actual design, as an actual teammate, in an actual company, with actual technical realities, is a lot more like governance. You don't get to burn everything down and start from scratch. You don’t get to lead as a dictator. You don’t always get to be right. It’s slower, more nuanced, and more important than that.

It just is.

Tagged in Design, Trump, Politics

By UX Launchpad on November 4, 2017.

Canonical link

Exported from Medium on November 5, 2017.

## It's Not Hard to See Where This Goes

#### Notes:

This is a bit Apple-centric. Work on that.

---- 

I don't have a strong sense of how settling Mars will pan out, or self driving cars, or Alphabet's next big bet, or if the Snapchatification of UX is a blip or the beginning of a new era in software design. But when it comes to eyeing Apple's current product line and tying themes and insights back to what we know of the future of computing, I have some thoughts.
Let's set the scene with four (or five) key areas:
#### 1. The Internet of Things (IoT)
Designers have been talking about IoT for a very long time. I've personally worked on IoT stuff, I interviewed to be on an IoT team at Microsoft, and I've read a ton of articles on it. My favorite is called [The Basket of Remotes Problem](https://mondaynote.com/internet-of-things-the-basket-of-remotes-problem-f80922a91a0f#.szs78cdfg), which points out that if you have a DVR, a TV, and a stereo system, you probably have three remotes. That's bad.
Now multiply that times everything in your home being internet enabled. Your car, your fridge, your thermostat, access to the doors in your house, security cameras, on and on. If that were to fragment, it wouldn't just be bad, it would be completely unacceptable. Having ten IoT devices and ten apps to control them just isn't going to fly in the consumer world.
#### 2. Wearables
Smartwatches are hard to justify right now. Most mobile tasks are better done on a smartphone due to the large screen. And for a variety of reasons starting with battery life, exercise tasks are better done with a standalone unit like a FitBit. So that's an awkward place for Apple Watch to be.
Three years ago, my friend told me the next big thing in wearables would be bluetooth headphones, perhaps in some kind of necklace form factor. He found while traveling that using his phone was distracting and called attention to himself, whereas speaking to Siri through headphones allowed him to get key information in a more low key way. That was notable.
#### 3. The Living Room
Sony, Microsoft, and Nintendo all have their game systems. Then there are DVR devices for watching TV. Then there's the rise of Netflix, which for many is becoming the primary app for the living room.
#### 4. Virtual Reality
I have no idea what is going to happen in this space, and certainly not around what Apple thinks about it. Should be fun to find out!

TBD: I wrote “huh?” here

#### 5. Traditional Laptops & Desktops versus Mobile
Tablet sales started off with a boom, but have since tapered off. We now know the traditional PC market wasn't killed off by the tablet, but it was clearly stalled by mobile in general.
Microsoft was first to blend mobile and desktop together in Windows 8 and then Windows 10, using their Surface product to show off the potential. Apple responded by saying they didn't believe their mobile and desktop operating systems should merge, but they did release iPad Pro and marketed it in a similar way to Surface.
Apple seems to be saying "iOS is the new platform we'd like you to adopt" and Microsoft seems to be saying "why pick just one?"
---- 
#### Some Things That Will Eventually Be True
There are a few things we don't know yet, but aren't hard to see coming:
1. Apple Watch will get better battery life
2. Apple Watch will get cellular data
3. iPad Pro will continue to court professional-grade 3rd party software
4. iPad Pro will one day have an iOS version of XCode
5. iPad Pro will become some people's laptops
6. HomeKit, Apple's "one remote" solution to IoT, will gain more support
7. Siri will continue to improve in accuracy and third party integration
8. Apple just announced AirPods, their second Siri-enabled wearable
#### Putting It All Together
Fast forward a few months or a few years, and let's imagine these things have all come to fruition. You can use an Apple Watch all day … without requiring a phone. You can work at your current job … without requiring a laptop. Your house doesn't need keys because your doors unlock to let you in as you approach them. You can go on a run while listening to your workout mix, or even ask Siri for directions to a good park, all without needing your phone.
I wrote a bit about the implications of this in an older essay, [A Desk Job For My Child](https://medium.com/@uxlaunchpad/a-desk-job-for-my-child-d0dabe1cfd0c#.g0g23ahd0). 

TBD: I talked about how I should tie this all together

In it, I realized that my love of the traditional laptop form factor doesn't really mean much to him. Laptops are hard. Laptop games are overly complicated. Toggling between a ton of overlapping windows is frustrating. Now imagine my 9 year old kid going into the workforce in 9 years. Do you think he's going to work the way we do today? I don't.
But PCs are easy to call dead. They've been written off for two decades. Let's pick on the smartphone instead. It's our most cherished device in 2016, so surely we're going to love them just as much in 9 years, right? I'm not so sure. I think we'll own them, of course. I think we'll still use them a ton, of course. But I wonder if we'll be glued to them in the same way.
#### My Changing Relationship with My Smartphone
I've seen glimmers of the future over the last year with my Apple Watch. I'm still addicted to my phone, of course. I'm not about to give it up and join a monastery. But increasingly I'm tilting my wrist for a bite of data rather than settling down for a whole app meal.
This morning I checked up on my email on my wrist while in the shower. Just two messages, piece of cake. I flagged one for later. While I got dressed I asked Siri aloud what the weather would be. (Meanwhile my kids were doing the same thing with iPads downstairs) My phone was in the other room, tucked in the pocket of my robe.
On the bus, I listened to guided meditation on my headphones - which was disturbed with a tap on my wrist by a question from my wife. I opened my eyes, read the note on my wrist, wrote "sure" as a response, then back to my meditation app - then I switched to a podcast while walking to work. Once there, I opened my laptop and was automatically signed in by Apple Watch.
I worked all morning on my laptop, but I did need to switch to my iPad for several tasks. When I was done, I was able to share the files I had created to my laptop's desktop. If I had needed to copy/paste across devices, it would have let me do that too, thanks macOS Sierra. Then I went to lunch and only gazed at my Watch between games of cards to check incoming texts.
Other than reading books, there was nothing I needed to do - and nothing I *wanted* to do - on the smartphone. I still wanted all my apps, data, and internet connectivity. But it was fine seeing it on my wrist. In fact, it was better. And that's notable.
#### Apple Will Beat the Basket of Remotes
Apple gets the most criticism in the area they do the best work - vertical integration. The tech industry believes that any app, any hardware, any invention, anything at all should have the same rights to be adopted into a computing platform and a consumer's life. But this is how you end up with a basket of remotes that all work differently, none particularly well. This is how you end up with products that have no interest in working cohesively.
Apple has said, and will continue to say, if you want to make a door lock that works with iOS, it will work with HomeKit. And if it works with HomeKit, there are things that just won't be possible. Like sharing personal data as an undisclosed price of admission, for starters. The same for thermostats, cameras, fridges, headphones, and internet enabled toasters. It'll all go through a single interface and interoperate in a singular way.
So the tech industry will continue to call Apple controlling, closed, anti-competitive, anti-capitalist, anti-American, stifled, and acting in poor faith. But their tight control over what is allowed on their system means they will beat the basket of remotes problem. Which is going to be a big deal.
#### The Return of "It Just Works"
"It just works" was the highest possible compliment you could pay a computing system in the 80's and 90's. (see also: Plug and Play) There were so many competing standards, devices, and software packages that it sometimes felt like a miracle when everything actually worked together.
When smartphones came on the scene, a lot of complexity was removed. Printing wasn't nearly as important. Cables and dongles weren't really a thing anymore. And even software was largely reigned in, thanks to the (universally panned at the time) App Store model that meant only vetted software could be installed by end users.

But The Internet of Things is going to throw us back into 90's. Your smartphone will be at the center of your life, but all these other things are going to want to talk to it. Suddenly it will feel as burdensome as the desktop PC used to, and as confusing to configure as your stereo system. At that point, the system that "just works" is going to have a huge advantage. Any company can set themselves up for this future, but so far only Apple seems to be betting big on it.

So the Apple Watch isn't really a watch. The AirPods aren't just headphones. The iPad isn't "a big iPhone." Each of these investigations is the result of Apple imagining a future of [*specialized* rather than *generalized*](https://www.youtube.com/watch?v=YfJ3QxJYsw8) computing. No one device can be as central and irreplaceable as iPhone (which, recall, is what we used to say about desktop PCs!) but Apple is betting that a portfolio of connected smart devices that can do the same tasks - and in many cases perform better - is where they should be skating.
And so they are. As fast as they possibly can.

## A Jock at Design Critique

"Hey," he called over to me as I gulped down water and tried to catch my breath. "What I was saying out there is when their offence is at midfield, come up to meet them."

"Yeah," I croaked. "I totally get it. I'm just out of shape. But I'm working on it." He nodded in sympathy and I continued, “Hey I meant to ask, are there positions like there are in outdoor soccer?" He thought about it. "I mean, yeah. Like, when you're on a better team you might stick in a formation, then you just know when a defender goes on attack, someone else drops back. But it's really fluid, you know? That's how we've gotta be out there. Just sort of looping around. Indoor soccer is a lot like hockey, honestly. Just keep it up, you’ll figure it out.”

I grinned. “Thanks, man. And seriously, keep the feedback coming. I'll take everything I can get!" As I headed out of the giant sports facility, opponents and teammates called after me with encouraging words. It got me thinking about design critique and how much better it could be if we acted more like jocks and less like the way we were trained to critique work in art school. 

Sport often operates in the realm of fact and logic. For example, there are right and wrong ways to swing a golf club or tackle someone in rugby. There’s always debate about strategy and constantly evolving best practices, but that evolution happens within the confines of statistics. If throwing a baseball one way results in 20% more strikes than another way, there’s some solid evidence it may be 20% better. If a player gets 10% more aces in tennis than another, it’s easy to make the point that they’re objectively better at acing.

Objectivity doesn’t exist in art, and that’s where we go wrong in art critique. 

Art doesn’t have the same objectivity. The problem comes when we pretend 

But it’s not because 

I went to art school, and I don’t miss the dysfunctional way people talk to each other there. But it follows me around, because that same art school approach is still practiced in design studios around the world. But it doesn’t work.

First, there's a whole lotta personal opinion passing as strong design critique out there. Many times I've gotten, frankly, not very good feedback from clients, executives, stakeholders, my managers, other designers, etc. But they don't know it's not good feedback because they're making a snap judgement with limited data. And they're expecting to be told their feedback is super great because of their role on the team.

On the other hand, the people on the team who have been working on the project since day one have a much deeper and more nuanced understanding. So in a poorly run crit, it can become the job of the person with six months of data to wow a mostly clueless person in six minutes. And to accept the feedback as if it were really helpful. Sometimes it's not, but it's rude to point it out if they're a VIP.

And that's the other strange problem - when power structures collide with honest feedback. When I was told to position myself more aggressively on the soccer field, it was referring to an objective truth that came from an honest place. He was saying "Hey. Do X. It will help the team." It's just that simple, because it's simply strong soccer technique. And it didn't have to come from a captain, or a coach. It's just true, regardless of who says it, and has been indisputable for decades. My job is to listen and improve. Period.

But when you tell me "The logo needs to pop more," that's not an objective truth. But if you're my superior, or my client, now I have to find a way to make the logo pop more. Or try to convince you away from your strongly held opinions. But overplay your hand and it's not about the logo anymore, it's about who gets to call the shots. It becomes about ego rather than the quality of the final product. Which makes it messier. And harms teamwork. Which harms the final result. I've seen it happen a lot. I think we all have.

---- 

Interaction design has been around for a long time. But sometimes we exaggerate how much we truly know, and we believe our own strongly held beliefs have passed into the realm of objective truth. One day, maybe. But in the long arc of history, interaction designers today are just cavemen and women banging rocks together and talking about their rock bashing technique at conferences. We'll look back at 2017 best practices and laugh. We should give our strongly held design feedback accordingly, and always leave room for the idea that we may be wrong.

## You're Not Alone

This is about how tech industry, and American-style capitalism at large, isn't a meritocracy. It's a power structure, one you're positioned near the bottom of, by design, and how that feels. It's trying to shed some light on how you might be feeling. If you’re in tech, and you feel like things are pretty gross, you're not alone.

#### 2000: My first review
My first day in tech was the first Monday after Y2K: January 3, 2000. I worked really hard, took on a lot of projects, worked late hours, racked up a lot of kudos for my work ethic, and now I was facing my first review. I got average marks, which seemed low to me. So I asked about them, like a person who knew he was doing a better job than his rating. What an entitled brat, right?
The VP said "You didn't launch that internal website." I stared at him. He was the one that blocked the launch! I told him so. He smiled and said "A person deserving of higher marks would have made it happen anyway."

But … but … he was the one that told me to hold off! It's not that I hadn't worked hard, or missed deadlines, or failed to align stakeholders. The person standing in front of me decided not to let it launch, without any reasoning, and now I was getting dinged for it. Odd. Ok, lesson learned.

#### 2002: Transfers blocked
I worked hard, took on new roles, got a promotion, and had transferred into a mentor/leadership role on our team. Now other teams in the company were recruiting me. Exciting! I told my VP about their outreach. He said no.
His rationale was that he needed me on his team. A compliment, but one that kept me bound to him and his goals. He had power, I didn't, so there wasn't much I could do. I waited until he let me go out of the kindness of his heart. Which meant it took a while.

#### 2003: "Seriously?"
When I finally scored a team on the fabled fourth floor of the company, where all the hot shot teams sat, the job offer was handed to me in person on Friday evening at 5pm by an HR rep.

I looked at the offer, set it down, and said, "Thanks! I'm excited to look over this. Can I reply on Monday?" She said "Seriously?" And I felt like maybe I did something wrong. "Yeah, I was just thinking I'd think it over during the weekend," I said, not feeling quite as sure. The HR rep seemed flabbergasted. As time went on, I realised it was probably an act. A negotiation tactic.

And, actually, it worked. They made me feel like I was biting the hand that fed me, so I accepted my small raise from $34,000 to $35,000 without countering. It turns out the role was worth way, way more than that. I just didn't know, and  they had no incentive to tell me.

#### 2004: The Doctored Review
Lots of companies in the Pacific Northwest used the 1-5 model that Microsoft used for many years. 1 is the highest, 5 means you're probably getting fired. The vast majority of people get a 3, and you're seen as uppity and entitled if you openly aim for more. The argument is that everyone is great, so getting a 3 is a huge honour. Don't make waves, kid.

I had a particularly good review period, so I put myself up for my first 2 ever. And my boss agreed. He handed me his glowing review, I read it, then got ready to sign it. At the last minute, he saw he had given me the wrong review! He quickly swapped it out with a new one. One that claimed I was actually a 3. And how my review was overstating my impact. I was exaggerating. I was a 3, tops.

Shocked, I asked what happened. He told me he had been politically pressured not to go with his conscience. The company was forcing a lower score. There was nothing he could do, even if he agreed with me. Whoa.

#### 2004: The Apology Review
Realising that meritocracies sure do have to put up with a lot of shadowy politics that have nothing to do with merit or work ethic, I lost my motivation. I stopped the long hours. I just did what was asked of me. I kept my head down. I didn't stand out. I didn't volunteer.

And the next review cycle I got a big bonus, promotion, and a "2" rating. That was even more disappointing. Working hard without credit was hard. Slacking and getting rewarded for it was devastating.

#### 2004: "We'd have to lay off some people. Do you want to be first?"
When I asked why the company was misleading customers, I was told we had to or else we'd have to lay off half our staff. And then he asked if I was volunteering. Message received! I stopped asking those sorts of questions.

#### 2005: Surprise! You're Underpaid
After five years, I was preparing to leave the company for a new opportunity. My CTO asked how to keep me, I said I'd need more money and a promotion. He said if I could wait six months, he could make it happen. But his hands were tied until then.

My co-worker heard my story and told me I had been lied to. He pulled up a script that showed every single hire, departing employee, and promotion, day by day. It turns out people got promoted all the time. "Off cycle" was a total fabrication by the CTO.

Then we shared salaries and he said "Dude, you're getting screwed." We polled almost everyone on the team. I was getting paid the least, by far. They told me a story of being valued and highly paid compared to others on the team. I believed them, which is why I hadn't stood up for myself earlier.

I told the CTO I knew he lied to me. He looked at me, cafeteria tray in hand, and paused a beat, with a blank face. Then he said "How did you find out?"

#### 2006: I Accidentally See Payroll
I worked hard at my new company. After a year or so, we were bought out. That meant everyone in the company was going to get half their yearly salary as a lump sum bonus. One day I did a search on the intranet for the word "bonus" to see how much I was going to get. It turns out the payroll could be accessed by anyone so I saw that I was paid less than everyone else.

Being paid less isn't the end of the world. But this was combined with the fact that I was the subject matter expert for then-new technologies like AJAX and what we ended up calling "Web 2.0." I wasn't the best worker in my peer group, but I sure wasn't the worst. I just didn't know how the system worked.

#### 2010: The Harvey Weinstein of the Design Studio
Most people I know have horror stories about people that make everyone in the office miserable. It's not always sexual in nature, but it is always about selfishness, power and control. 

The Harvey Weinstein of my design studio, then and probably now, mistakenly believed everyone loved him. And why wouldn't he if there's no incentive for anyone to say anything different? Why would a person jeopardise their job, and by extension their entire future, to try and be a whistleblower? I did, by accident. Big mistake.

One day he poked his head into my team's war-room and said "Jon, when can we talk?" And I said "Oh wow, I'm really under water on this project. How about tomorrow?" We made verbal plans. He left, and I followed up twice the next day to set up our meeting. He didn't show up. A day or two later, he told me my behaviour was unacceptable via email, including this line:

> "I know you are busy, but it really seemed preposterous and a loss for you given my role in your career development."

Yikes. Given his **role in my career development**. Sensing a threat, I immediately cc’d HR, feeling concerned about my future at the company. Big mistake. That only got him more angry. He told me I'm allowed to do whatever I want with HR, legally-speaking, but I had made an error in judgement with how I was responding to him.

> Pause and think… I'm telling you now that this wasn't a wise move, that I didn't appreciate it, and that I'd like you to find time for a meeting [without HR involved.]

This only got me more scared. I was clearly angering him further, making the intimidation worse. What was he going to do, put me on a Performance Improvement Plan, the first step to setting up a paper trail for firing me? Yup. That's exactly what he did.

This whole time, I held tight to the idea that we needed to go through proper channels. (And that HR was there to help me, forgetting they're there to help the business, not my career or my feelings.) I wanted things documented and I hoped HR would help me navigate the situation. So we hopped on a three way conference call, where he built a case for me being a poor employee (despite a big promotion a few months prior) and encouraged the PIP to be enacted. It was. The HR rep gave me a "what am I going to do?" shrug of sympathy afterwards. And that was that. I was about to be let go.

#### 31 Days Later: "Flying Colours"
Exactly a month passed, and HR told me I passed "with flying colours." Not that it was perfect, of course. There was a mixture of feedback, things to work on, things where I'm exceeding expectations, etc. It was a standard review, despite the fact that the kick-off of it was politically motivated, as the HR rep told me in confidence.

But: "flying colours." Other than the standard blend of pros and cons you see in every review, my biggest blunder was accidentally offending a powerful man, then hunkering down when I felt threatened. I heard story after story after story about this man's intimidation. You won't read them on Medium, of course. Why would anyone subject themselves to that? There's no upside. You'll only hear them whispered carefully to trusted friends.

#### And so
I have more stories (some really good ones too), but they start getting closer to my last few places of employment, and I have to be careful. I'll stop there, for now. But I will say that everyone I know has stories like this. I've probably heard thousands. Including a sickeningly high number of stories about sexual abuse and harassment. If you have stories like this, if you're just keeping your head down, if you're often treated like dirt to keep the machine running, you're not alone. You're in the majority. And I hear you.
---- 
PS: I implied something in this article that I should have been more explicit about. These are the stories I have to tell, and I'm a privileged white man with lots of experience. If you're a woman, or a person of color, or an immigrant, or LGBT, or younger, or made a career change, or are living paycheck to paycheck, or a million other details, these stories tend to be way worse. For example, I know my female friends don't tell me every bad story, but the ones I've heard are definitely more appalling than these.

Again: You're not alone. Talk to your friends, compare notes, fight on.















// THIS SHIT IS OLD

1.  figure out what the different labels mean in the UX Launchpad folder
2. Pull out the best essays
3. 


BLUE: Means it’s posted online
GREY: Probably means it’s sucky
ORANGE: Might mean I like it? (Nope, it means it has an image)

GREEN: No fucking clue
PURPLE: No fucking clue


////

Pretty sure these scripts aren’t working correctly.
But I can manually grep through everything for every IMG SRC
…
Ok, I downloaded every image. I think we’re up to date.

NEXT UP:
Remove the dumb comments. Those aren’t super important.
(Also: none of the images are pointing correctly yet. That sort of sucks.)

OK: I DID THAT.

////





